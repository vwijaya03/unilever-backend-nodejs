# Installs and configures mariadb to staging server
```
superluminal --ini ini/playbook-mariadb.ini --section prod --inventory staging --user dev \
--ask-become-pass --ask-pass
```

# Installs and configures mariadb to production server
```
superluminal --ini ini/playbook-mariadb.ini --section prod --inventory production --user dev \
--ask-become-pass --ask-pass
```

# post-steps
To expose mysql service:
either use haproxy or set bind-adddress to 0.0.0.0 in my.cnf
#!/bin/bash

CDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
NARGS=$#
ME=`basename $0`

#

ENV=$1
GRP=$2
source $ITP_UNILEVER_BACKEND/build/backend-nodejs/vars.sh

#

echo "[ $TAG :: cleaning previous $COMPONENT_NAME build output for env:$ENV grp:$GRP]"
(cd $DIR && ./clean.sh $ENV $GRP)

echo "[ $TAG :: invoking $COMPONENT_NAME for env:$ENV grp:$GRP]"
(cd $ITP_UNILEVER_BACKEND/build/backend-nodejs && ./build.sh $ENV $GRP)
echo ""


---
# Deploy unilever-backend artifact
#
# required external variables:
# - local_artifact_file
# - version_code
#
- name: "Deploy unilever-backend artifact {{ lookup('pipe','date +%Y-%m-%d:%H:%M:%S') }}"
  hosts: "{{ target_hosts | default('none') }}"
#  hosts: "all"
  gather_facts: true
  any_errors_fatal: true
  sudo: true

  pre_tasks:

    - include_vars: "{{ lookup('env', 'ITP_UNILEVER_BACKEND') }}/config/common.yml"
      tags: always
    - include_vars: "{{ lookup('env', 'ITP_UNILEVER_BACKEND') }}/config/deploy.yml"
      tags: always
    - include_vars: "{{ playbook_dir }}/vars/main.yml"
      tags: always
    - set_fact: uploader_data_root_dir='{{ cfg_deploy_unilever_backend_root }}/uploader-data'

  tasks:

    - name: 'debug current timestamp'
      debug: msg="{{ time_now }}"
      tags:
        - always

    - name: 'create deployment root directory : {{ cfg_deploy_root }}'
      file:
        path: "{{ cfg_deploy_root }}"
        state: directory

    - name: 'make sure pm2 is installed'
      npm:
        name: pm2
        global: yes
        state: present

    - name: 'check if existing file exists {{ remote_artifact_file }}'
      stat:
        path: '{{ remote_artifact_file }}'
      register: artifact_file_stat

    - debug: msg="{{ artifact_file_stat }}"

#    - name: 'remove backup files, free disk space below 20%'
#      sudo: yes
#      command: "find -type f -printf '%T+ %p\n' -maxdepth 1 | sort | head -10 -exec rm"
#      args:
#        chdir: "{{ deploy_root }}"
#      when: "item.mount == '{{ deploy_root }}' and ( item.size_available < item.size_total * 0.2 )"
#      with_items: ansible_mounts

    - name: 'check if existing exploded directory exists {{ exploded_directory }}'
      stat:
        path: '{{ exploded_directory }}'
      register: exp_dir

    - name: 'delete existing exploded archive directory {{ exploded_directory }}'
      command: 'rm -rf {{ exploded_directory }}'
      when: exp_dir.stat.exists and exp_dir.stat.isdir

    - name: 'backup previous artifact as {{ remote_backup_artifact_file }}'
      copy:
        src: '{{ remote_artifact_file }}'
        dest: '{{ remote_backup_artifact_file }}'
        remote_src: True
      when: artifact_file_stat.stat.exists

    - name: 'remove existing artifact file {{ remote_artifact_file }}'
      file:
        path: '{{ remote_artifact_file }}'
        state: absent
      when: artifact_file_stat.stat.exists
      sudo: true

    - name: 'copy artifact file {{ local_artifact_file }} to {{ remote_artifact_file }}'
      copy:
        src: "{{ local_artifact_file }}"
        dest: "{{ remote_artifact_file }}"
        mode: 0755
        owner: root
        group: root
      sudo: true
      tags:
        - copy

    - name: 'unarchive artifact file {{ remote_artifact_file }} to {{ cfg_deploy_root }}'
      unarchive:
        src: "{{ remote_artifact_file }}"
        dest: "{{ cfg_deploy_root }}"
        copy: no
        mode: 0755
      sudo: true
      tags:
        - unarchive

    - name: 'create {{ cfg_deploy_unilever_backend_root }}/dist directory'
      file:
        path: "{{ cfg_deploy_unilever_backend_root }}/dist"
        state: directory
      tags:
        - symlink

#    - name: 'update exploded archive directory modified time to latest'
#      file:
#        path: "{{ exploded_directory }}"
#        state: touch

    - name: 'install nodejs dependencies. npm install. chdir:{{ remote_app_chdir }}'
      shell: "sudo npm install"
      args:
        chdir: '{{ remote_app_chdir }}/'
      tags:
        - symlink

    - name: 'create node_modules symlink. dest:{{ cfg_deploy_unilever_backend_root }}/src/ src:{{ cfg_deploy_unilever_backend_root }}/dist'
      file:
        src: '{{ cfg_deploy_unilever_backend_root }}/src/node_modules'
        dest: '{{ cfg_deploy_unilever_backend_root }}/dist/node_modules'
        state: link
        force: yes
      sudo: true
      tags:
        - symlink

    - name: 'fix ownerships of exploded directory: {{ cfg_deploy_unilever_backend_root }}'
      file:
        path: '{{ cfg_deploy_unilever_backend_root }}'
        state: directory
        owner: root
        group: root
        mode: u=rwX,g=rX,o=rX
        recurse: true
      sudo: true

    - name: 'create running user {{ running_user }} for nodejs app'
      user:
        name: "{{ running_user }}"
        shell: /bin/bash
        groups: www-data
        append: yes
      tags: user
      sudo: true

    - name: 'add www-data to {{ running_user }} group'
      user:
        name: 'www-data'
        groups: "{{ running_user }}"
        append: yes
      tags: user
      sudo: true

    # create the user '{{running_user}}' with a bash shell, appending the group 'www-data' to the user's groups
    - user: name='{{ running_user }}' shell=/bin/bash groups=www-data append=yes

    - name: 'create directory for holding uploaded files {{ uploader_data_root_dir }}'
      file:
        path: "{{ uploader_data_root_dir }}"
        state: directory
        recurse: True
        group: www-data
        owner: www-data
        mode: 0775
      tags: datadir

    - name: 'create tmp directory for holding temporary files {{ uploader_data_root_dir }}/tmp'
      file:
        path: "{{ uploader_data_root_dir }}/tmp"
        state: directory
        recurse: True
        group: www-data
        owner: www-data
        mode: 0775
      tags: datadir


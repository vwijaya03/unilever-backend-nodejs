#!/usr/bin/env bash

superluminal --ini ini/playbook-deploy-fastdev.ini --section prod --inventory staging --user dev --ssh-port 22 --vars env=dev
# Setup environment
```
superluminal --ini ini/playbook-setup-env.ini --section prod --inventory production --user root --ask-become-pass --ask-pass
```

# Deployment workflow

## 1. build the artifact on local machine
```
./build.sh prod g1
```

## 2. deploy the artifact to remote staging server
```
superluminal --ini ini/playbook-deploy.ini --section prod --inventory staging --user dev --ask-become-pass --ask-pass
```

## 3. build the nodejs-app on remote staging server
```
superluminal --ini ini/playbook-build.ini --section prod --inventory staging --user dev --ask-become-pass --ask-pass
```

## 4. start the nodejs-app on remote staging server
```
superluminal --ini ini/playbook-admin.ini --section prod --inventory staging --user dev --ask-become-pass --ask-pass --tags start
```

# Fast-dev deployment iteration to development server (Only for development):
```
superluminal --ini ini/playbook-dev.ini --section prod --inventory development --user dev --ssh-port 22 --vars env=dev
```

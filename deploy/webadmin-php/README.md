## create archive
```
tar -C ./unilever-webadmin -cvf unilever-webadmin.tar . --transform="s|/|/webadmin/HEAD/|"
```

## setup env
```
superluminal \
--ini ini/playbook-setup-webadmin.ini \
--section prod \
--inventory staging \
--user dev \
--ssh-port 22 \
--ask-become-pass
```

## deploy archive
```
superluminal \
--ini ini/playbook-deploy-webadmin.ini \
--section prod \
--inventory staging \
--user dev \
--ssh-port 22 \
--vars 'env=qa artifact_file=/home/dev/workspaces/itprovent/unilever-webadmin.tar' \
--ask-become-pass
```
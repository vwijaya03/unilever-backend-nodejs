
### Install nginx
```
superluminal \
--ini ini/remote/playbook-install.ini \
--section prod \
--inventory staging \
--user dev \
--ssh-port 22 \
--vars env=qa \
--ask-become-pass
```

### Configures nginx for webadmin
```
superluminal \
--ini ini/remote/playbook-webadmin.ini \
--section prod \
--inventory staging \
--user dev \
--ssh-port 22 \
--vars env=qa \
--ask-become-pass
```
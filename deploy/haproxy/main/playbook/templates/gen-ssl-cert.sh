#!/bin/bash

# Reference: https://blog.brixit.nl/automating-letsencrypt-and-haproxy

# Required environment variables:
# LE_OUTPUT - points to the output directory: /etc/letsencrypt/live/{top-level-domain}/
# LE_CONFIG_INI - the configuration file
#
# Optional variables:
# LE_DRYRUN - To run as a dry-run please specify environment variable LE_DRYRUN to '--dry-run'
# LE_STAGING - to create 'test' certs specify LE_STAGING environment variable to '--staging --break-certs'

# Path to the LetsEncrypt client tool (Here we use certbot)
LE_TOOL=/root/certbot-auto

# Directory where the acme client puts the generated certs
LE_OUTPUT=/etc/letsencrypt/live

# This will not be read since we have specified .ini file.
# Concat the requested domains
DOMAINS=""
for DOM in "$@"
do
    DOMAINS+=" -d $DOM"
done

TARGET_DOMAIN={{ cert_name }}

# Create or renew certificate for the domain(s) supplied for this tool
# Below command starts a temporary standalone webserver on port 9999 and specifies to use HTTP challenge

cmd="$LE_TOOL $LE_DRYRUN $LE_CONFIG_INI $LE_STAGING --noninteractive --agree-tos --renew-by-default --keep-until-expiring --standalone --standalone-supported-challenges http-01 --http-01-port 9999 certonly $DOMAINS"
echo $cmd
$cmd

if [ $? -eq 0 ]; then
    # if both dryrun and staging are not set then create pem file and copy to haproxy
    if [ -z ${LE_DRYRUN+x} ] && [ -z ${LE_STAGING+x} ] ; then
        echo "$(date +%c) success! now creating ${TARGET_DOMAIN}.pem and saving file as /etc/haproxy/cert/${TARGET_DOMAIN}.pem"
        # Cat the certificate chain and the private key together for haproxy
        cat $LE_OUTPUT/${TARGET_DOMAIN}/{fullchain.pem,privkey.pem} > /etc/haproxy/cert/${TARGET_DOMAIN}.pem
        # Reload the haproxy daemon to activate the cert
        service haproxy restart
    fi
else
    echo "$(date +%c) Error creating certificate with error code $?,exiting script.."
fi
exit 0


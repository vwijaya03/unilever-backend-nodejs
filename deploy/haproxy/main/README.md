# Please see architecture diagram


# HAProxy stats page
http://www.itplever.com:8088/haproxy?stats

# Generate/Update HAProxy admin password
TODO

# How to install haproxy
```
superluminal \
--ini ini/remote/playbook.ini \
--section prod \
--inventory staging \
--user dev \
--ssh-port 22 \
--vars env=qa \
--ask-become-pass
```

# How to configure haproxy
```
superluminal \
--ini ini/remote/playbook-configure.ini \
--section prod \
--inventory staging \
--user dev \
--ssh-port 22 \
--vars env=qa \
--ask-become-pass
```

# How to add another API version endpoint
TODO

# How to generate SSL cert using LetsEncrypt

## The flow:

* 0. (optional) disable force-ssl-redirection in HAproxy.
* 1. Perform cert-generation dry-run, to make sure generation script is working.
* 2. Generate staging cert, check that cert generation is successful.
* 3. If step #2 is successful then go ahead generate live-production cert (only once!)
* 4. A live cert file will be copied to /etc/haproxy/cert
* 5. Reconfigure /etc/haproxy/haproxy.cfg to use live cert file.
* 6. (optional) re-enable force-ssl-redirection in HAProxy
* 6. Restart haproxy

### Dry-run cert generation (Execute as much as you like)
```
superluminal \
--ini ini/remote/playbook-ssl.ini \
--section prod \
--inventory staging \
--user dev \
--ssh-port 22 \
--vars env=qa \
--ask-become-pass
```

### Staging cert generation (Dont execute too much)
```
superluminal \
--ini ini/remote/playbook-ssl.ini \
--section prod \
--inventory staging \
--user dev \
--ssh-port 22 \
--vars 'env=qa staging=1' \
--ask-become-pass
```

### Live-production cert generation (Do not execute more than once!)
```
superluminal \
--ini ini/remote/playbook-ssl.ini \
--section prod \
--inventory staging \
--user dev \
--ssh-port 22 \
--vars 'env=qa live=1' \
--ask-become-pass
```
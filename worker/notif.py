import urllib.parse # pip3 install pymysql
import json
import datetime
import time
import pymysql # pip3 install pymysql
import requests # pip3 install requests
import smtplib
import logging
import sys
import smtplib
import os

from logging.handlers import TimedRotatingFileHandler
from urllib.request import Request, urlopen

def send_email(user, pwd, recipient, subject, body):
    gmail_user = user
    gmail_pwd = pwd
    FROM = user
    TO = recipient if type(recipient) is list else [recipient]
    SUBJECT = subject
    TEXT = body

    # Prepare actual message
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    server.login(gmail_user, gmail_pwd)
    server.sendmail(FROM, TO, message)
    server.close()

def update_notif(cursor, data):
    cursor.execute("""UPDATE `notifications` SET `state`=1 WHERE `id`='%s' """, (data[0]))
    conn.commit()
    
def insert_notif(cursor, data, to):
    sql = "INSERT INTO `notifications` (`id`, `to`, `type`, `message`, `state`, `createdAt`, `updatedAt`) VALUES (null, '%s', '%s', '%s', 0, '%s', '%s')" % ((", ").join(to), data[2], data[3], datetime.datetime.now(), datetime.datetime.now())
    cursor.execute(sql)
    conn.commit()

def get_fcm_id_from_va(cursor, to):
    cursor.execute("""SELECT `fcm_id` FROM `users` WHERE `valdo_account`=%s """, (to))
    
    return cursor.fetchone()


self_path = os.path.dirname(os.path.abspath(__file__))
file = open(os.path.join(self_path,'..','main','src','config', 'database.json'), "r")
config = json.loads(file.read())
host = config.get("mariadb").get("host")
user = config.get("mariadb").get("user")
password = config.get("mariadb").get("password")
db = config.get("mariadb").get("db")

if os.path.isdir(os.path.join(self_path, "log")) != True:
    os.makedirs(os.path.join(self_path, "log"))

logger = logging.getLogger("Rotating Log")
logger.setLevel(logging.INFO)
rotatingFileHandler = TimedRotatingFileHandler(os.path.join(self_path, "log", "worker.log"), when="d", interval=1)
stdoutHandler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
rotatingFileHandler.setFormatter(formatter)
logger.addHandler(rotatingFileHandler)
stdoutHandler.setFormatter(formatter)
logger.addHandler(stdoutHandler)


while True:
    try:
        conn = pymysql.connect(host=host, port=3306, user=user, passwd=password, db=db)
        cursor = conn.cursor()
        sql = """SELECT * FROM notifications WHERE state=0"""
        cursor.execute(sql)

        logger.info("Checking new notification...")
        datas = cursor.fetchall()

        if len(datas) > 0 :
            logger.info("Found new %s Notifications" % len(datas))
        else:
            logger.info("No new notifications, gonna sleep for 5 second")
            time.sleep( 5 )

        for data in datas:
            id_notif = data[0]
            to = data[1]
            send_type = data[2]
            message = json.loads(data[3])

            if str(send_type) == 'email':
                user = 'viko@itprovent.com'
                pwd = 'jokowidodo'
                recipient = to.split(", ")
                subject = message.get("title")
                body = message.get("body")
                try:
                    send_email(user, pwd, recipient, subject, body)
                    logger.info('Send email to %s success' % to)
                    update_notif(cursor, data)
                except:
                    logger.info('Cannot send email to %s' % to)

            # elif str(send_type) == 'data' or str(send_type) == 'notification':
            #     url = 'https://fcm.googleapis.com/fcm/send'
            #     serverApiKey = "AAAAy3uTtk8:APA91bG7UNyPOC3LpULuzzD6LSBq179r7UMd2IpzWA7nfAAIlvL5x1WId3p294MhS0mgmQUaO56OlYWc9sSXQh0sZjO2GPXLbfvmWho7WvytuFHEEfbNv2mr9BsQ_ePc-hjxBo3IfQiX"
            #     parameter = {"to":str(to)}
            #     parameter[send_type] = message
            #     multiple_to = to.split(", ")
            #     i = 0
            #     for to in multiple_to:
            #         param = json.dumps(parameter).encode('utf8')
            #         req = urllib.request.Request(url, data=param, headers={'content-type':'application/json', 'Authorization':'key='+str(serverApiKey)})
            #         response = urllib.request.urlopen(req)
            #         result_response = json.loads(response.read().decode('utf8'))

            #         if str(send_type) == 'notification':
            #             if result_response.get('success') != 1:
            #                 logger.info('Cannot send notification to %s' % to)
            #             else:
            #                 logger.info('Send notification to %s success' % to)
            #                 multiple_to[i] = ''
            #             i = i + 1
            #         elif str(send_type) == 'data':
            #             if result_response.get('message_id') == None:
            #                 logger.info('Cannot send notification to %s' % to)
            #             else:
            #                 logger.info('Send notification to %s success' % to)
            #                 multiple_to[i] = ''
            #             i = i + 1

            #     new_multiple_to = list(filter(None, multiple_to))
            #     if len(new_multiple_to) != len(multiple_to):
            #         update_notif(cursor, data)
            #         if len(new_multiple_to) > 0 :
            #             insert_notif(cursor, data, new_multiple_to)

            elif str(send_type) == 'notification':
                url = 'https://fcm.googleapis.com/fcm/send'
                serverApiKey = "AAAAy3uTtk8:APA91bG7UNyPOC3LpULuzzD6LSBq179r7UMd2IpzWA7nfAAIlvL5x1WId3p294MhS0mgmQUaO56OlYWc9sSXQh0sZjO2GPXLbfvmWho7WvytuFHEEfbNv2mr9BsQ_ePc-hjxBo3IfQiX"
                multiple_to = to.split(", ")
                count_to = len(multiple_to)
                i = 0
                to_cant_sends = []
                for to in multiple_to:
                    fcm_id = get_fcm_id_from_va(cursor, to)
                    if fcm_id:
                        parameter = {"to":str(fcm_id[0])}
                    else:
                        parameter = {"to":str(to)}
                    parameter[send_type] = message
                    param = json.dumps(parameter).encode('utf8')
                    req = urllib.request.Request(url, data=param, headers={'content-type':'application/json', 'Authorization':'key='+str(serverApiKey)})
                    response = urllib.request.urlopen(req)
                    result_response = json.loads(response.read().decode('utf8'))

                    if result_response.get('success') != 1:
                        logger.info('Cannot send notification to ' +to+' Error: '+str(result_response.get("results")[0].get("error")))
                        to_cant_sends.append(to)
                    else:
                        logger.info('Send notification to %s success' % to)
                        update_notif(cursor, data)
                        count_to = count_to - 1

                if count_to > 0 and len(multiple_to) != len(to_cant_sends):
                    for to_cant_send in to_cant_sends:
                        insert_notif(cursor, data, [to_cant_send])

            elif str(send_type) == 'data':
                url = 'https://fcm.googleapis.com/fcm/send'
                serverApiKey = "AAAAy3uTtk8:APA91bG7UNyPOC3LpULuzzD6LSBq179r7UMd2IpzWA7nfAAIlvL5x1WId3p294MhS0mgmQUaO56OlYWc9sSXQh0sZjO2GPXLbfvmWho7WvytuFHEEfbNv2mr9BsQ_ePc-hjxBo3IfQiX"
                multiple_to = to.split(", ")
                count_to = len(multiple_to)
                i = 0
                to_cant_sends = []
                for to in multiple_to:
                    parameter = {"to":str(to)}
                    parameter[send_type] = message
                    param = json.dumps(parameter).encode('utf8')
                    req = urllib.request.Request(url, data=param, headers={'content-type':'application/json', 'Authorization':'key='+str(serverApiKey)})
                    response = urllib.request.urlopen(req)
                    result_response = json.loads(response.read().decode('utf8'))

                    if result_response.get('message_id') == None:
                        logger.info('Cannot send notification to topic ' +to+' Error: '+str(result_response.get("results")[0].get("error")))
                        to_cant_sends.append(to)
                    else:
                        logger.info('Send notification to %s success' % to)
                        update_notif(cursor, data)
                        count_to = count_to - 1

                if count_to > 0 and len(multiple_to) != len(to_cant_sends):
                    for to_cant_send in to_cant_sends:
                        insert_notif(cursor, data, [to_cant_send])
                
            elif str(send_type) == 'sms':
                param = {
                    "userkey": "8y4kgf",
                    "passkey": "Top4n789"
                }
                
                multiple_to = to.split(", ")
                sms_message = message.get("body")
                i = 0
                for to in multiple_to:
                    param['nohp'] = to
                    param['pesan'] = sms_message
                    param = urllib.parse.urlencode(param)
                    url = 'https://reguler.zenziva.net/apps/smsapi.php?'
                    req = urllib.request.Request(url+param)
                    response = urllib.request.urlopen(req)
                    result_response = response.read().decode('utf8')

                    if ("<text>Success</text") in result_response:
                        logger.info('Send sms to %s success' % to)
                        multiple_to[i] = ''
                    else:
                        logger.info('Cannot send sms to %s' % to)
                    i = i + 1
                new_multiple_to = list(filter(None, multiple_to))
                if len(new_multiple_to) != len(multiple_to):
                    update_notif(cursor, data)
                    if len(new_multiple_to) > 0:
                        insert_notif(cursor, data, new_multiple_to)
                    
        conn.close()
        logger.info("Done for the job, gonna sleep for 5 second")
        time.sleep(5)
    except:
        logger.info("Oops something bad happened, gonna sleep for 10 second")
        time.sleep( 10 )
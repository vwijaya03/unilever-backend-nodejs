## Test Users (username, password)
* user1, kNxGzRh6mErysk7U
* user2, t3HSS6kAWWjmgbDX
* user3, KTsCuJ5p9Hcsabk3
* dist1, WPVX9&Z3DkSXVz2U
* pickup1: 5j6wUDhNnCe4GkPU
* superadmin1: M2s9USmDxDNbRbqK
* superadmin2: zXDPhPAU2NanY2jf
* unileveradmin1: 77gHzsTzC2Zeqw2B
* unileveradmin2: WMeT5stDGGZjA3jU

## Notes
Call POST /signin to get access token.
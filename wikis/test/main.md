# Snippets

## payment invoice (Valid)
```
curl -X POST --header 'Content-Type: application/x-www-form-urlencoded' --header 'Accept: application/json' --header 'token: user1token' -d 'invoice_id=160005003280&outlet_code=00179940&dt_code=15195745&pin=712337&amount=1' 'http://localhost:10010/payment'
```

## payment (Invalid Request: no recipient or no invoice)
```
curl -X POST --header 'Content-Type: application/x-www-form-urlencoded' --header 'Accept: application/json' --header 'token: user1token' -d 'pin=712337&amount=1' 'http://139.162.16.144:10010/payment'
```

## payment peer (Valid Request)
```
curl -X POST --header 'Content-Type: application/x-www-form-urlencoded' --header 'Accept: application/json' --header 'token: user1token' -d 'recipient_username=dist1&pin=712337&amount=1' 'http://localhost:10010/payment'
```

## payment peer (Invalid Request: invalid pin)
```
curl -X POST --header 'Content-Type: application/x-www-form-urlencoded' --header 'Accept: application/json' --header 'token: user1token' -d 'recipient_username=dist1&pin=712338&amount=1' 'http://localhost:10010/payment'
```

## double invoice payment concurrent test
```
curl -X POST --header 'Content-Type: application/x-www-form-urlencoded' --header 'Accept: application/json' --header 'token: user1token' -d 'invoice_id=160005003280&outlet_code=00179940&dt_code=15195745&pin=712337&amount=1' 'http://localhost:10010/payment'
curl -X POST --header 'Content-Type: application/x-www-form-urlencoded' --header 'Accept: application/json' --header 'token: user1token' -d 'invoice_id=160005003280&outlet_code=00179940&dt_code=15195745&pin=712337&amount=1' 'http://localhost:10010/payment'
```

# Test Data
## Retailer Outlet Code
* 00179940
* 00179963
* 00180309

## Distributor Code
* 15195745
* 15195746

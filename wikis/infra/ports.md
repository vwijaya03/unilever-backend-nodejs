# Ports

## haproxy
* http: 80
* https: 443
* haproxy admin: 8088 (url = [ip:8088]/haproxy?stats)

## node-backend
### v1
* api: 10010
* swagger: 8080

### v2
* api: 10011
* swagger: 8080

## nginx-webadmin
* 8081

## hhvm
* N/A - uses socket


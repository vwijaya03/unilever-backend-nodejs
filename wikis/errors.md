#### All Public Error Codes
* SigninError.code = 1000;
* ApiTokenInvalidError.code = 1001;
* AccessTokenExpiredError.code = 1002;
* UserRegistrationError.code = 2000;
* ChangePinError.code = 2001;
* InvoiceUploadError.code = 3000;
* ExcelParseError.code = 3004;
* DbRecordCreationError.code = 4000;
* ResourceNotFoundError.code = 5000;
* UserNotFoundError.code = 5001;
* InvoiceAlreadyPaidError.code = 6000;
* BalanceNotEnoughError.code = 6001;
* MakePaymentError.code = 6002;
* UnauthorizedRequestError.code = 7000;

* MoneyAccessGetHistoryError.code = 68000;
* MoneyAccessInquiryError.code = 68001;
* MoneyAccessRegisterError.code = 68002;
* MoneyAccessTransferError.code = 68003;
* MoneyAccessUpdateCustomerInfoError.code = 68004;
* MoneyAccessResetPinError.code = 68005;
* MoneyAccessChangePinError.code = 68006;
* MoneyAccessGetCustomersListError.code = 68007;

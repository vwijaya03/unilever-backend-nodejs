#!/bin/bash

HOST="itprovent-unilever-development:10010"

echo 'reset invoice to unpaid'
curl -X POST --header 'Content-Type: application/x-www-form-urlencoded' --header 'Accept: application/json' --header 'token: be19b7bfbd2dc644f96fc34199c3e18bec749c64b9a8bd47ef10535f176f592d' -d 'invoice_id=160005003280' "http://${HOST}/test/reset_paid_invoices"
echo ""
echo 'reset to invoice unpaid. done'

echo 'pay invoice'
curl -X POST --header 'Content-Type: application/x-www-form-urlencoded' --header 'Accept: application/json' --header 'token: b6be18ce39432d7f6ea262788e2f40ecbf5889913d3dff6423092f6dced4647c' -d 'request_id=req123&invoice_id=160005003280&outlet_code=00179940&dt_code=15195745&pin=712337&amount=1000' "http://${HOST}/payment" &
# curl -X POST --header 'Content-Type: application/x-www-form-urlencoded' --header 'Accept: application/json' --header 'token: b6be18ce39432d7f6ea262788e2f40ecbf5889913d3dff6423092f6dced4647c' -d 'invoice_id=160005003280&outlet_code=00179940&dt_code=15195745&pin=712337&amount=1000' 'http://localhost:10010/payment' &

module.exports = function(grunt) {

  var profile_id = grunt.option('profile') || 'dev';

  // Note that Grunt current working directory is the location of Gruntfile.js 

  var config = {};

  //

  var itp_unilever_backend = process.env.ITP_UNILEVER_BACKEND;
  // read deployment properties from configuration files

  var deploy_config = grunt.file.readYAML(itp_unilever_backend + '/config/deploy.yml')
  service = deploy_config['cfg_deploy_unilever_backend_service']

  config.deploy_artifact_filename = deploy_config['cfg_deploy_unilever_backend_artifact']
  config.deploy_service_root_dirname = deploy_config['cfg_deploy_service_root_dirname'];

  // build up the directories name

  config.component_name = service;
  config.build_dir = itp_unilever_backend + '/target/' + profile_id + '/' + config.component_name;
  config.service_build_src = itp_unilever_backend + '/main';

  // Note: this is the one you must care about
  config.dest_rootfoldername = service + '/' + config.deploy_service_root_dirname;

  console.log( '########################################################' );
  console.log( 'config.build_dir = '+config.build_dir);
  console.log( 'config.service_build_src = '+config.service_build_src);
  console.log( 'config.deploy_artifact_filename = '+config.deploy_artifact_filename);
  console.log( '########################################################' );

  /*

  will be extracted as:
  <service-name>/HEAD/<archive contents>

  */

  grunt.initConfig({
    config: config,
    compress: {
      artifact: {
        options: {
          archive: '<%= config.build_dir %>/' + config.deploy_artifact_filename,
          mode: 'tgz',
          pretty: true
        },
        expand: true,
        flatten: false,
        dot: true,
        cwd: '<%= config.service_build_src %>',
        src: ['**/*', '!**/node_modules/**'],
        dest: '<%= config.dest_rootfoldername %>'
      }
    }
    ,
    clean: {
      build: {
        src: ["<%= config.build_dir %>"],
        options: {
          force:true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-compress');

  grunt.registerTask('prod', ['clean:*','compress:*']);
  grunt.registerTask('qa', ['clean:*','compress:*']);
  grunt.registerTask('dev', ['clean:*','compress:*']);
  grunt.registerTask('default', ['clean:*','compress:*']);

}
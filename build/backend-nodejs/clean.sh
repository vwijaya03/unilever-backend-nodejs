#!/bin/bash

CDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
NARGS=$#
ME=`basename $0`

#

ENV=$1
GRP=$2
source $ITP_UNILEVER_BACKEND/build/backend-nodejs/vars.sh

#

echo "[ $TAG :: cleaning $COMPONENT_NAME for env:$ENV grp:$GRP]"
(cd $ITP_UNILEVER_BACKEND && sudo rm -fR target/$ENV/)
echo ""




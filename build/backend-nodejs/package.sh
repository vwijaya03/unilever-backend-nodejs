#!/bin/bash

CDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
NARGS=$#
ME=`basename $0`

#

ENV=$1
GRP=$2
source $ITP_UNILEVER_BACKEND/build/backend-nodejs/vars.sh

#

echo "[ $TAG :: resolve libraries/dependencies for grunt]"
sudo npm install --save-dev

echo "[ $TAG :: $COMPONENT_NAME packaging env:$ENV grp:$GRP]"

cmd="grunt $ENV --profile=$ENV --force"
echo $cmd
(cd $DIR && $cmd)
echo ""
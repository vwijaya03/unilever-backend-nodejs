## How to start dev-mode with autorestart (in development env) manually using npm:
```
NODE_ENV=development npm run dev
```

## How to start in staging env manually using npm:
```
sudo NODE_ENV=staging npm run start
```

## How to start in production env manually using npm:
```
sudo NODE_ENV=production npm run start
```

## Using pm2 to start development
```
pm2 start process.yml --env development
```

## Using pm2 to start staging
```
pm2 start process.yml --env staging
```

## Using pm2 to start production
```
pm2 start process.yml --env production
```


function WebSessionExpiredError(message) {
    this.message = message;
    this.name = "WebSessionExpiredError";
    Error.captureStackTrace(this, WebSessionExpiredError);
}
WebSessionExpiredError.prototype = Object.create(Error.prototype);
WebSessionExpiredError.prototype.constructor = WebSessionExpiredError;

function WebtierPingError(message) {
    this.message = message;
    this.name = "WebtierPingError";
    Error.captureStackTrace(this, WebtierPingError);
}
WebtierPingError.prototype = Object.create(Error.prototype);
WebtierPingError.prototype.constructor = WebtierPingError;

module.exports = {
    WebtierPingError: WebtierPingError,
    WebSessionExpiredError: WebSessionExpiredError
};
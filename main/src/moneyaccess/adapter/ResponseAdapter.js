import Promise from 'bluebird'
import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import properties from '@/properties'

/**
Parses the response from MoneyAccess and
returns an array of size two. first element is the error descriptor.
second element is the original message response.
@return {Object} - {error:<Object>, response:<Object>]
**/
function parse({response=null}={}) {
    // console.log('parse: ', response);
    var error = null;
    var hasError = false;
    var outResp = response;
    var pagination = null;
    try {
        if (response.status) {
            hasError = (response.status.code == 400);
            if (hasError) {
                error = {code:response.errors.error, message:response.errors.error_desc};
            } else {
                outResp = response.result;
                pagination = response.pagination;
            }
        }
    } catch (err) {
        console.error(err);
    }
    var out = {error:error, response:outResp, pagination:pagination};
    // console.log('isi out di response adapter');
    // console.log(out);
    return out;
}

function parseMandiriResponse({response=null}={}) {
    // console.log('parse: ', response);
    var error = null;
    var hasError = false;
    var outResp = response;
    try {
        if (response.status) {
            hasError = (response.status.code == 400);
            if (hasError) {
                error = {code:response.errors.error, message:response.errors.error_desc};
            } else {
                outResp = response.mandiri_response;
            }
        }
    } catch (err) {
        console.error(err);
    }
    var out = {error:error, response:outResp};
    return out;
}

var obj = {};
obj.parse = parse;
obj.parseMandiriResponse = parseMandiriResponse;
export default obj
import Promise from 'bluebird'
import _ from 'lodash'
import moment from 'moment'
import { sprintf } from 'sprintf-js'
import properties from '@/properties'


/**
Parses transaction response from MoneyAccess (POST) history API and
@return {Object} - {error:<Object>, response:<Object>]
**/
function parse({response=null}={}) {

    var transDateMoment = null;

    // console.log('isi trans adapter');
    // console.log(response);
    var formattedList = response.map(item => {
        console.log('parsing item:', item);
        transDateMoment = moment(item.trans_date, "YYYY-MM-DD HH:mm:ss");
        var formatted = {};
        formatted._source = 'moneyaccess'; // to differentiate the source of the record.
        formatted.valdoTxId = item.transid;
        formatted.historyid = item.historyid;
        formatted.transactDate = (transDateMoment.unix() * 1000) + '';
        formatted.type = item.trans_type;
        formatted.description = item.desc;
        formatted.amount = item.amount;
        formatted.dk = item.dk;
        if (item.saldo) {
            formatted.saldo = _.replace(item.saldo, /,/g, "");
        } else {
            formatted.saldo = 0;
        }
        return formatted;
    });


/*
    {
        itemno: 15,
        historyid: '1155',
        trans_date: '2016-11-21 18:06:11',
        trans_type: 'topup',
        amount: '999',
        desc: 'Topup Rp.999',
        vacc_number: '8804112345678900',
        dk: 'k',
        transid: null,
        saldo: '0'
    }
*/
    // console.log('isi total item nya');
    // console.log(response.totalitem);
    formattedList.pagination = response.pagination;
    formattedList.pagenumber = response.pagenumber;
    formattedList.itempage = response.itempage;
    formattedList.totalpage = response.totalpage;
    formattedList.totalitem = response.totalitem;
    
    var out = formattedList;
    return out;
}

var obj = {};
obj.parse = parse;
export default obj
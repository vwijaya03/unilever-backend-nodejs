import ExtendableError from '@/error/ExtendableError'

class MoneyAccessChangePinError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'cant reset secret pin';
    }
    var code = MoneyAccessChangePinError.code;
    super(code, m);
  }
}

export default MoneyAccessChangePinError


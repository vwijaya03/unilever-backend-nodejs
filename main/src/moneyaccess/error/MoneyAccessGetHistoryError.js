import ExtendableError from '@/error/ExtendableError'

class MoneyAccessGetHistoryError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'cant get transaction history';
    }
    var code = MoneyAccessGetHistoryError.code;
    super(code, m);
  }
}

export default MoneyAccessGetHistoryError


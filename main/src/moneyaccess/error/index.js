import MoneyAccessGetHistoryError from './MoneyAccessGetHistoryError'
import MoneyAccessInquiryError from './MoneyAccessInquiryError'
import MoneyAccessRegisterError from './MoneyAccessRegisterError'
import MoneyAccessTransferError from './MoneyAccessTransferError'

import MoneyAccessUpdateCustomerInfoError from './MoneyAccessUpdateCustomerInfoError'
import MoneyAccessResetPinError from './MoneyAccessResetPinError'
import MoneyAccessChangePinError from './MoneyAccessChangePinError'
import MoneyAccessGetCustomersListError from './MoneyAccessGetCustomersListError'
import MoneyAccessDoPaymentError from './MoneyAccessDoPaymentError'

MoneyAccessGetHistoryError.code = 68000;
MoneyAccessInquiryError.code = 68001;
MoneyAccessRegisterError.code = 68002;
MoneyAccessTransferError.code = 68003;

MoneyAccessUpdateCustomerInfoError.code = 68004;
MoneyAccessResetPinError.code = 68005;
MoneyAccessChangePinError.code = 68006;
MoneyAccessGetCustomersListError.code = 68007;
MoneyAccessDoPaymentError.code = 68008;

export { MoneyAccessRegisterError }
export { MoneyAccessInquiryError }
export { MoneyAccessGetHistoryError }
export { MoneyAccessTransferError }

export { MoneyAccessUpdateCustomerInfoError }
export { MoneyAccessResetPinError }
export { MoneyAccessChangePinError }
export { MoneyAccessGetCustomersListError }
export { MoneyAccessDoPaymentError }

import ExtendableError from '@/error/ExtendableError'

class MoneyAccessResetPinError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'cant reset secret pin';
    }
    var code = MoneyAccessResetPinError.code;
    super(code, m);
  }
}

export default MoneyAccessResetPinError


import ExtendableError from '@/error/ExtendableError'

class MoneyAccessRegisterError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'cant register';
    }
    var code = MoneyAccessRegisterError.code;
    super(code, m);
  }
}

export default MoneyAccessRegisterError


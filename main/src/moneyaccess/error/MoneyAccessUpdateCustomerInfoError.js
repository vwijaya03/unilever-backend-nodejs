import ExtendableError from '@/error/ExtendableError'

class MoneyAccessUpdateCustomerInfoError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'cant update customer info';
    }
    var code = MoneyAccessUpdateCustomerInfoError.code;
    super(code, m);
  }
}

export default MoneyAccessUpdateCustomerInfoError


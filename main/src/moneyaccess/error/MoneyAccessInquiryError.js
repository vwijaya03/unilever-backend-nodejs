import ExtendableError from '@/error/ExtendableError'

class MoneyAccessInquiryError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'cant get account balance information';
    }
    var code = MoneyAccessInquiryError.code;
    super(code, m);
  }
}

export default MoneyAccessInquiryError


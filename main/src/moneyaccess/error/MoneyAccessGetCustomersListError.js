import ExtendableError from '@/error/ExtendableError'

class MoneyAccessGetCustomersListError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'cant get customers list';
    }
    var code = MoneyAccessGetCustomersListError.code;
    super(code, m);
  }
}

export default MoneyAccessGetCustomersListError


import ExtendableError from '@/error/ExtendableError'

class MoneyAccessTransferError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'cant transfer money';
    }
    var code = MoneyAccessTransferError.code;
    super(code, m);
  }
}

export default MoneyAccessTransferError


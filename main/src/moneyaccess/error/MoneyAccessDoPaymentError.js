import ExtendableError from '@/error/ExtendableError'

class MoneyAccessDoPaymentError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'cant do payment';
    }
    var code = MoneyAccessDoPaymentError.code;
    super(code, m);
  }
}

export default MoneyAccessDoPaymentError


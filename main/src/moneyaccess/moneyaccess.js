import bunyan from 'bunyan'
import Promise from 'bluebird'
import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import properties from '@/properties'
import request from 'request'
import format from 'string-format'

import Adapter from './adapter/index'

import { MoneyAccessUpdateCustomerInfoError } from './error/index'
import { MoneyAccessGetHistoryError } from './error/index'
import { MoneyAccessRegisterError } from './error/index'
import { MoneyAccessTransferError } from './error/index'
import { MoneyAccessInquiryError } from './error/index'
import { MoneyAccessResetPinError } from './error/index'
import { MoneyAccessChangePinError } from './error/index'
import { MoneyAccessDoPaymentError } from './error/index'

import service_logger from '@/server/services/logger'
var log = service_logger.getLogger({name: 'moneyaccess'});

var LOG_RAW_RESPONSE = true;

var retailer_host = properties.valdo_wallet_retailer_host;
var pickup_host = '192.168.12.121';
// var pickup_host = properties.valdo_wallet_pickup_host;
var apikey = properties.moneyaccess_apikey;
var protocol = 'http';
var private_prefix = '/api/v1';
var public_prefix = private_prefix;
// var public_prefix = '/public/api/v1';
var protohost = protocol+"://"+retailer_host;
var proto_pickup_host = protocol+"://"+pickup_host;
var tokenkey = 'gUxOsYYXbeywpM2xaV37RaNvdZ5nJdI8d8AdYvy4';

var pickup_transfer_retailer_endpoint = proto_pickup_host + private_prefix + "/pickup/trf";
//pickup done
var register_endpoint = protohost + private_prefix + "/customer/reg";
var register_pickup_endpoint = proto_pickup_host + private_prefix + "/customer/reg";

//pickup done
var transfer_endpoint = protohost + public_prefix + "/vacc/trf"; // ?mobnum=${mobnum}&vaccount_from=${from}&vaccount_to=${to}&amount=${amount}";
var transfer_pickup_endpoint = proto_pickup_host + public_prefix + "/vacc/trf"; // ?mobnum=${mobnum}&vaccount_from=${from}&vaccount_to=${to}&amount=${amount}";

//pickup done
var inquiry_endpoint = protohost + public_prefix + "/vacc/inq"; // mobnum=${mobnum}&vaccount=${vaccount}";
var inquiry_pickup_endpoint = proto_pickup_host + public_prefix + "/vacc/inq"; // mobnum=${mobnum}&vaccount=${vaccount}";

//pickup done
var history_endpoint = protohost + public_prefix + "/vacc/hist";
var history_pickup_endpoint = proto_pickup_host + public_prefix + "/vacc/hist";

var custlist_endpoint = protohost + public_prefix + "/customer/custlist";
var updatecust_endpoint = protohost + public_prefix + "/customer/updcust";

//pickup done
var resetpin_endpoint = protohost + public_prefix + "/customer/rpin";
var resetpin_pickup_endpoint = proto_pickup_host + public_prefix + "/customer/rpin";

//pickup done
var changepin_endpoint = protohost + public_prefix + "/customer/cpin";
var changepin_pickup_endpoint = proto_pickup_host + public_prefix + "/customer/cpin";

var withdraw_endpoint = proto_pickup_host + public_prefix + "/";

//pickup done
var dopayment_endpoint = protohost + public_prefix + "/manacc/dopayment";
var dopayment_pickup_endpoint = proto_pickup_host + public_prefix + "/manacc/dopayment";

//pickup done
var account_inquiry_name_endpoint = protohost + public_prefix + "/manacc/accinqname";
var account_pickup_inquiry_name_endpoint = proto_pickup_host + public_prefix + "/manacc/accinqname";

log.info('Valdo Endpoints: ');
log.info('register:', register_endpoint);
log.info('transfer:', transfer_endpoint);
log.info('inquiry:', inquiry_endpoint);
log.info('history:', history_endpoint);
log.info('custlist:', custlist_endpoint);
log.info('updatecust:', updatecust_endpoint);
log.info('resetpin:', resetpin_endpoint);
log.info("changepin:", changepin_endpoint);
log.info("withdraw:", withdraw_endpoint);
log.info("dopayment:", dopayment_endpoint);

const DEBUG_LOG = false;

function commonHeaders() {
    return {
        'Accept': 'application/json',
        'Cache-Control': 'no-cache',
        'apigatekey': apikey
    };
}

function authHeaders(headers) {
    headers = headers || {};
    headers['Authorization'] = 'Bearer ' + tokenkey;
    return headers;
}

function handleResponse(resolve, reject, rawJsonResponse, errorClass) {
    if (DEBUG_LOG) console.log('handle-response', rawJsonResponse);
    // console.log('isi fungsi adapter');
    // console.log(Adapter.Response.parse({response:rawJsonResponse}));
    var {error:err, response:successJsonResp, pagination: pagination} = Adapter.Response.parse({response:rawJsonResponse});
    if (err) {
        if (DEBUG_LOG) console.error('money-access error:', err.message);
        reject(new errorClass(err.message));
    } else {
        // console.log('isi pagination');
        // console.log(pagination);
        successJsonResp = Object.assign(successJsonResp, pagination);
        // console.log('successJsonResp');
        // console.log(successJsonResp);
        resolve(successJsonResp);
    }
}

function handleMandiriResponse(resolve, reject, rawJsonResponse, errorClass) {
    console.log('masuk handle');
    if (DEBUG_LOG) console.log('handle-response', rawJsonResponse);
    var {error:err, response:successJsonResp} = Adapter.Response.parseMandiriResponse({response:rawJsonResponse});
    if (err) {
        if (DEBUG_LOG) console.error('money-access error:', err.message);
        reject(new errorClass(err.message));
    } else {
        // console.log(rawJsonResponse);
        resolve(successJsonResp);
    }
}

function logRawResponse(response) {
    if (LOG_RAW_RESPONSE) {
        console.log('money-access raw-response:', response);
    }
}

function bigFatRespHandler(resolve, reject, err, responseBody, errorClass) {
    logRawResponse(responseBody);
    if (err) {
        reject(err);
        return;
    }
    var json = null;
    try {
        json = JSON.parse(responseBody);
    } catch(parse_err) {
        var errMesage = 'failed to parse response as JSON.' + parse_err;
        // throw new errorClass("remote server error");
        // reject(new errorClass());
        console.error(errMesage);
        resolve({failed:true});
        return;
    }
    handleResponse(resolve, reject, json, errorClass);
    // console.log('isi resolve nya');
    // console.log(resolve);
}

function bigFatMandiriRespHandler(resolve, reject, err, responseBody, errorClass) {
    logRawResponse(responseBody);
    if (err) {
        reject(err);
        return;
    }
    var json = null;
    try {
        json = JSON.parse(responseBody);
    } catch(parse_err) {
        var errMesage = 'failed to parse response as JSON.' + parse_err;
        // throw new errorClass("remote server error");
        // reject(new errorClass());
        console.error(errMesage);
        resolve({failed:true});
        return;
    }
    handleMandiriResponse(resolve, reject, json, errorClass);
}

/**
@param {Object} args
@param {String} args.mobnum - mobile number
@param {String} args.fname - first name
@param {String} args.lname - last name
@param {String} args.email - email
@param {String} args.pin - pin
@param {String} args.dob - dob
@param {String} args.pob - pob
@returns {Promise} - resolved to moneyaccess json response
*/

function account_inquiry_name({accountnumber=null, usertype=null}={}) {
    console.log('moneyaccess-account_inquiry_name', accountnumber);

    return new Promise(function(resolve, reject) {
        var form = { 'accountnumber': accountnumber};
        // console.log('form:', form);
        var headers = commonHeaders();
        var url;

        if(usertype==2)
            url = account_pickup_inquiry_name_endpoint;
        else
            url = account_inquiry_name_endpoint;

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatMandiriRespHandler(resolve, reject, err, body, MoneyAccessRegisterError);
        });
    });
}

function register({mobnum=null, fname=null, lname=null, email=null, pin=null, dob=null, pob=null, usertype=null}={}) {

    console.log('moneyaccess-register', mobnum);

    return new Promise(function(resolve, reject) {
        var form = { 'mobnum':mobnum, 'fname':fname, 'lname':lname, 'dob':dob, 'pob':pob, 'email':email, 'pin':pin };
        console.log('form:', form);
        var headers = commonHeaders();
        var url;
        if(usertype==2)
            url = register_pickup_endpoint;
        else
            url = register_endpoint;
        console.log('isi url: '+ url);
        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessRegisterError);
        });
    });
}

function updatecust({vaccount=null, fname=null, lname=null, pob=null, dob=null, email=null}={}) {
    console.log('updatecust', 'vaccount:', vaccount, 'fname:', fname, 'lname:', lname, 'pob', pob, 'dob:', dob, 'email', email);

    return new Promise(function(resolve, reject) {
        var headers = authHeaders(commonHeaders());
        var form = { 'vaccount':vaccount, 'fname':fname, 'lname':lname, 'pob':pob, 'dob':dob, 'email':email };
        request.post( {url:updatecust_endpoint, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessUpdateCustomerInfoError);
        });
    });
}

/*
Resets pin
@returns {Promise}
*/
function resetpin({vaccount=null, usertype=null}={}) {
    console.log(sprintf('resetpin. endpoint:%1$s vaccount:%2$s', resetpin_endpoint, vaccount));
    return new Promise(function(resolve, reject) {
        var headers = authHeaders(commonHeaders());
        var form = {'vaccount':vaccount};
        var url;

        if(usertype==2)
            url = resetpin_pickup_endpoint;
        else
            url = resetpin_endpoint;

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessResetPinError);
        });
    });
}

function custlist({}={}) {
    console.log(sprintf('custlist endpoint: %1$s', custlist_endpoint));

    return new Promise(function(resolve, reject) {
        var headers = authHeaders(commonHeaders());
        var form = {};
        request.get( {url:custlist_endpoint, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessGetCustomerListError);
        });
    });
}

/**
change pin

@returns {Promise}
*/
function changepin({vaccount=null, old_pin=null, new_pin=null, retype_new_pin=null, usertype=null}={}) {
    console.log(sprintf('changepin. endpoint:%1$s', changepin_endpoint));
    return new Promise(function(resolve, reject) {
        var headers = authHeaders(commonHeaders());
        var form = {'vaccount':vaccount, 'old_pin':old_pin, 'new_pin':new_pin, 'retype_new_pin':retype_new_pin };
        var url;

        if(usertype==2)
            url = changepin_pickup_endpoint;
        else
            url = changepin_endpoint;

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessChangePinError);
        });
    });
}

/**
transfer money

@param {String} transId - transaction id
@param {Number} mobnum - virtual account
@param {Number} from - from virtual account
@param {Number} to - to virtual account
@param {Number} amount - the amount to transfer
@returns {Promise}
*/
function transfer({transId=null, desc=null, mobnum=null, from=null, to=null, amount=null, pin=null, usertype=null}={}) {

    var msg = sprintf('moneyaccess-transfer. transId:%1$s mobnum:%2$s from:%3$s to:%4$s amount:%5$s', transId, mobnum, from, to, amount);
    console.log(msg);

    if (! transId) {
        throw new MoneyAccessTransferError('please provide transId');
    }

    if (! desc) {
        desc = sprintf('transfer amount:%1$s from:%2$s to:%3$s', amount, from, to);
    }

    return new Promise(function(resolve, reject) {
        var headers = authHeaders(commonHeaders());
        var form = {'transid':transId, 'desc':desc, 'mobnum':mobnum, 'vaccount_from':from, 'vaccount_to':to, 'amount':amount, 'pin':pin };
        var url;
        if(usertype == 2)
            url = transfer_pickup_endpoint;
        else
            url = transfer_endpoint;
        console.log(url);
        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessTransferError);
        });
    });
}

function pickup_transfer({transId=null, desc=null, mobnum=null, vaccount_pickup=null, vaccount_toko=null, amount=null, pin=null, usertype=null}={}) {

    var msg = sprintf('moneyaccess-transfer. transId:%1$s mobnum:%2$s vaccount_pickup:%3$s vaccount_toko:%4$s amount:%5$s', transId, mobnum, vaccount_pickup, vaccount_toko, amount);
    console.log(msg);

    if (! transId) {
        throw new MoneyAccessTransferError('please provide transId');
    }

    if (! desc) {
        desc = sprintf('transfer amount:%1$s vaccount_pickup:%2$s vaccount_toko:%3$s', amount, vaccount_pickup, vaccount_toko);
    }

    return new Promise(function(resolve, reject) {
        var headers = authHeaders(commonHeaders());
        var form = {'transid':transId, 'desc':desc, 'vaccount_pickup':vaccount_pickup, 'vaccount_toko':vaccount_toko, 'amount':amount, 'pin':pin };
        var url;
        if(usertype == 2)
            url = pickup_transfer_retailer_endpoint;
        else
            url = pickup_transfer_retailer_endpoint;
        console.log(url);
        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatMandiriRespHandler(resolve, reject, err, body, MoneyAccessTransferError);
        });
    });
}

function dopayment({credit_accountno=null, value_date=null, value_amount=null, destination_bankcode=null, beneficiary_name=null, preferred_transfer_method_id=null, remark1=null, extended_payment_detail=null,beneficiary_email_address=null, payment_method=null, reserved_field1=null, reserved_field2=null, vacc_number=null,pin=null, transid=null, usertype=null, desc=null}={}) {

    if (! transid) {
        throw new MoneyAccessDoPaymentError('please provide transId');
    }

    return new Promise(function(resolve, reject) {
        var headers = authHeaders(commonHeaders());
        var form = {
            'credit_accountno': credit_accountno, 
            'value_date': value_date, 
            'value_amount': value_amount, 
            'destination_bankcode': destination_bankcode, 
            'beneficiary_name': beneficiary_name, 
            'preferred_transfer_method_id': preferred_transfer_method_id,  
            'remark1': remark1, 
            'extended_payment_detail': extended_payment_detail, 
            'beneficiary_email_address': beneficiary_email_address, 
            'payment_method': payment_method, 
            'reserved_field1': reserved_field1, 
            'reserved_field2': reserved_field2, 
            'vacc_number': vacc_number,
            'desc': desc,
            'pin': pin,
            'transid': transid
        };
        var url;

        if(usertype==2)
            url = dopayment_pickup_endpoint;
        else
            url = dopayment_endpoint;

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatMandiriRespHandler(resolve, reject, err, body, MoneyAccessDoPaymentError);
        });
    });
}

/**
get saldo balance

@returns {Promise}
*/
function inquiry({mobnum=null, vaccount=null, usertype=null}={}) {

    console.log('moneyaccess-inquiry.', 'mobnum:', mobnum, 'vaccount:', vaccount);

    return new Promise(function(resolve, reject) {
        var headers = authHeaders(commonHeaders());
        var form = { 'mobnum':mobnum, 'vaccount':vaccount };
        var url;

        if(usertype==2)
            url = inquiry_pickup_endpoint;
        else
            url = inquiry_endpoint;

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessInquiryError);
        });
    });
}

/**
get transaction history

@param {String} mobnum - the vaccount
@param {String} type - transaction type. transfer|topup
@param {Object} page - the page
@param {Number} itemPerPage - items per page
@param {Moment} startDate - start date
@param {Moment} endDate - end date
@returns {Promise}
*/
function history({vacc_number=null, type=null, startDate=null, endDate=null, pagenum=1, item_per_page=15, usertype=null}={}) {

    console.log('moneyaccess-history', vacc_number);

    return new Promise(function(resolve, reject) {
        var headers = authHeaders(commonHeaders());
        var url;

        if(usertype==2)
            url = history_pickup_endpoint;
        else
            url = history_endpoint;
        console.log('url:' + url);

        var form = {};
        form.vacc_number = vacc_number;
        form.pagenum = pagenum;
        form.item_per_page = item_per_page;
        console.log('isi trans type');
        console.log(type);
        if (startDate && endDate) {
            form.start_date = (null != startDate) ? startDate.format('YYYY-MM-DD') : '';
            form.end_date = (null != endDate) ? endDate.format('YYYY-MM-DD') : '';
        }

        if (type) {
            form.trans_type = type;
        }

        console.log('ma-history. form:', form);

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessGetHistoryError);
        });
    });

}

/**
withdraw money

@param {String} mobnum - the vaccount
@returns {Promise}
*/
function withdraw({mobnum=null}={}) {
    console.log('moneyaccess-withdraw', mobnum);

    return new Promise(function(resolve, reject) {
        var headers = authHeaders(commonHeaders());
        var url = history_endpoint;
        console.log('url:' + url);

        var form = {};
        form.vacc_number = mobnum;
        form.pagenum = page;
        form.item_per_page = itemsPerPage;

        if (startDate && endDate) {
            form.start_date = (null != startDate) ? startDate.format('YYYY-MM-DD') : '';
            form.end_date = (null != endDate) ? endDate.format('YYYY-MM-DD') : '';
        }

        if (type) {
            form['trans_type'] = type;
        }

        console.log('ma-history. form:', form);

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessGetHistoryError);
        });
    });
}

var obj = {};

obj.register = register;
obj.updatecust = updatecust;
obj.resetpin = resetpin;
obj.custlist = custlist;
obj.changepin = changepin;

obj.transfer = transfer;
obj.history = history;
obj.inquiry = inquiry;
obj.dopayment = dopayment;
obj.account_inquiry_name = account_inquiry_name;

obj.withdraw = withdraw;
obj.pickup_transfer = pickup_transfer;

export default obj


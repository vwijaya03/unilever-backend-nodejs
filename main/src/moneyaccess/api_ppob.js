import bunyan from 'bunyan'
import Promise from 'bluebird'
import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import properties from '@/properties'
import request from 'request'
import format from 'string-format'

import Adapter from './adapter/index'

import { MoneyAccessUpdateCustomerInfoError } from './error/index'
import { MoneyAccessGetHistoryError } from './error/index'
import { MoneyAccessRegisterError } from './error/index'
import { MoneyAccessTransferError } from './error/index'
import { MoneyAccessInquiryError } from './error/index'
import { MoneyAccessResetPinError } from './error/index'
import { MoneyAccessChangePinError } from './error/index'
import { MoneyAccessDoPaymentError } from './error/index'

import service_logger from '@/server/services/logger'

var LOG_RAW_RESPONSE = true;

const protocol = 'http://';
const host = '192.168.12.227';
const prefix = '/api/v1/';

const url_ppob_price_service = protocol+host+prefix+'ppob/price_service';
const url_ppob_topup_service = protocol+host+prefix+'ppob/topup_service';
const url_ppob_inquiry_service = protocol+host+prefix+'ppob/inquiry_service';
const url_ppob_payment_service = protocol+host+prefix+'ppob/payment_service';

var tokenkey = 'gUxOsYYXbeywpM2xaV37RaNvdZ5nJdI8d8AdYvy4';

const DEBUG_LOG = false;

function commonHeaders() {
    return {
        'Accept': 'application/json',
        'Cache-Control': 'no-cache',
        'apigatekey': 'webKEY123456789'
    };
}

function authHeaders(headers) {
    headers = headers || {};
    headers['Authorization'] = 'Bearer ' + tokenkey;
    return headers;
}

function handleResponse(resolve, reject, rawJsonResponse, errorClass) {
    if (DEBUG_LOG) console.log('handle-response', rawJsonResponse);
    // console.log('isi fungsi adapter');
    // console.log(Adapter.Response.parse({response:rawJsonResponse}));
    var {error:err, response:successJsonResp, pagination: pagination} = Adapter.Response.parse({response:rawJsonResponse});
    if (err) {
        if (DEBUG_LOG) console.error('money-access error:', err.message);
        reject(new errorClass(err.message));
    } else {
        // console.log('isi pagination');
        // console.log(pagination);
        successJsonResp = Object.assign(successJsonResp, pagination);
        // console.log('successJsonResp');
        // console.log(successJsonResp);
        resolve(successJsonResp);
    }
}

function logRawResponse(response) {
    if (LOG_RAW_RESPONSE) {
        console.log('money-access raw-response:', response);
    }
}

function bigFatRespHandler(resolve, reject, err, responseBody, errorClass) {
    logRawResponse(responseBody);
    if (err) {
        reject(err);
        return;
    }
    var json = null;
    try {
        json = JSON.parse(responseBody);
    } catch(parse_err) {
        var errMesage = 'failed to parse response as JSON.' + parse_err;
        // throw new errorClass("remote server error");
        // reject(new errorClass());
        console.error(errMesage);
        resolve({failed:true});
        return;
    }
    handleResponse(resolve, reject, json, errorClass);
    // console.log('isi resolve nya');
    // console.log(resolve);
}

function ppob_check_price({product_type=null, provider=null}={}) {

    return new Promise(function(resolve, reject) {
        var form = { product_type: product_type, provider: provider};
        var headers = commonHeaders();

        request.post( {url:url_ppob_price_service, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessRegisterError);
        });
    });
}

function ppob_topup_service({model=null, product_code=null, customer_number1=null, vaccount=null, pin=null}={}) {

    return new Promise(function(resolve, reject) {
        var form = {model:model, product_code:product_code, customer_number1:customer_number1, vaccount:vaccount, pin:pin};
        var headers = commonHeaders();

        request.post( {url:url_ppob_topup_service, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessRegisterError);
        });
    });
}

function ppob_inquiry({product_code=null, customer_number1=null, customer_number2=null, customer_number3=null, misc=null}={})
{
    return new Promise(function(resolve, reject) {
        var form = {product_code:product_code, customer_number1:customer_number1, customer_number2:customer_number2, customer_number3:customer_number3, misc:misc};
        var headers = commonHeaders();

        request.post( {url:url_ppob_inquiry_service, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessRegisterError);
        });
    });
}

function ppob_payment_service({product_code=null, customer_number1=null, customer_number2=null, customer_number3=null, nominal=null, ref_number=null, misc=null, vaccount=null, pin=null}={})
{
    return new Promise(function(resolve, reject) {
        var form = {product_code:product_code, customer_number1:customer_number1, customer_number2:customer_number2, customer_number3:customer_number3, nominal:nominal, ref_number:ref_number, misc:misc, vaccount:vaccount, pin:pin};
        var headers = commonHeaders();

        request.post( {url:url_ppob_payment_service, headers:headers, form:form}, function(err, httpResponse, body) {
             bigFatRespHandler(resolve, reject, err, body, MoneyAccessRegisterError);
        });
    });
}

var obj = {};

obj.ppob_check_price = ppob_check_price;
obj.ppob_topup_service = ppob_topup_service;
obj.ppob_inquiry = ppob_inquiry;
obj.ppob_payment_service = ppob_payment_service;

export default obj
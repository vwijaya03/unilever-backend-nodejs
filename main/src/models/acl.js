import _ from 'lodash'

import Principal from '@/models/principal'
import service_logger from '@/server/services/logger'
import { ACCESS_TOKEN_INTERNAL_DEV } from '@/properties'
const DEBUG_LOG = true;

const log = service_logger.getLogger({name: 'models', service:'acl'});

/**

# Rules for resources: principal_roles, principal_user, terms

principal_roles - check for the principal role
    - operations:
        contains - check if principal roles is a superset of the 'contains' set.

principal_user - check the attribute of principal user
    - operations:
        matches - check if the target resource value matches with attribute value of principal_user.

terms - operates on the target resource value
    - operations:
        has - check if the target resource value is in the 'has' set.

Example #1:
- Checks if principal roles contains the role "internal"
- Checks if the value of username resource matches with the username of principal_user
- Checks if the value of username resource is part of the following whitelist : ["self"]
acl.addResources(
    resources: {
        username: [
            // circuit breaks upon first satisfied condition.
            { principal_roles:{ contains:['internal'] } },
            { principal_user:{ matches:['username'] } },
            { terms:{ has:['self'] } }
        ]
    }
);

Example #2: checks principal roles against whitelist for all resources.
acl.roles(['role1','role2']);

Example #3: checks access token against a whitelist for all resources
acl.tokens(['token1', 'token2']);

Example #4: adds roles or tokens to existing ACL
import { factory } from 'ACL'
factory.getDefault().addRoles('king').addTokens('123abcd');

*/
const ACL = class ACL {
    constructor({
        resources=[],
        roles=[],
        parent=null
    } = {}) {
        this._resources = resources;
        this._roles = roles;
        if (parent) {
            this._roles = parent._roles;
            this._resources = parent._resources;
        }
    }
    /**
    sets role white-list
    @param {Array.<String>} list - a list of permissible roles for this acl
    */
    roles(list) {
        if (!(list instanceof Array)) {
            list = [list];
        } else {
            // create a set
            list = list.filter(function(elem, index, self) {
                return index == self.indexOf(elem);
            });
        }
        this._roles = list;
        return this;
    }
    addRoles(list) {
        if (!(list instanceof Array)) {
            list = [list];
        }
        this._roles.concat(list);
        this._roles = _.union(this._roles, list);
        return this;
    }
    /**
    sets access-token white-list
    @param {Array.<String>} list - a list of permissible access-token for this acl
    */
    tokens(list) {
        if (! _.isArray(list)) {
            list = [list];
        } else {
            // create a set
            list = list.filter(function(elem, index, self) {
                return index == self.indexOf(elem);
            });
        }
        this._tokens = list;
        return this;
    }
    addTokens(list) {
        if (! _.isArray(list)) {
            list = [list];
        }
        this._tokens = _.union(this._tokens, list);
        return this;
    }
    /**
    sets resource contraints
    @param {Object} desc - resource constraint descriptor object
    */
    resources(descriptor) {
        this._resources = descriptor;
        return this;
    }

    compareSet({label=null, expected=null, actual=null}={}) {
        log.debug('actual',actual,'expected', expected);
        var diff_list = [];
        var is_satisfied = false;

        if (actual.length > 0) {
            diff_list = _.difference(actual, expected);
            is_satisfied =  diff_list.length == 0;
        } else {
            if (expected.length == 0) {
                is_satisfied = true;
            }
        }

        // log.debug(label+' diff_list', diff_list);
        // log.debug(''+label+' compare-set. ok or not?', is_satisfied);

        return is_satisfied;
    }

    /**
    checks if principal is allowed to access given resource
    @param {Principal} principal - the principal
    @param {Object} resource - the requested resource descriptor
    @returns {Boolean} - true if access is granted otherwise false
    */
    isAllowed(principal, req_resource) {
        if (!principal || !(principal instanceof Principal)) {
            return false;
        }

        var principal_roles = principal.roles;
        var principal_accesstokens = [principal.getAccessToken()];

        // check roles
        log.debug('roles: ', this._roles);
        if (this._roles && this._roles.length > 0) {
            if (DEBUG_LOG) log.debug('check-roles', 'acl-roles:', this._roles, 'principal-roles:', principal_roles);
            if (this._roles.indexOf("all") >= 0) {
                // grant all roles
                return true;
            } else {
                var diff_list = [];
                if (this.compareSet({expected:this._roles, actual:principal_roles, label:'roles'})) {
                    return true;
                }
            }
        }
        // check tokens
        log.debug('tokens: ', this._tokens);
        if (this._tokens && this._tokens.length > 0) {
            if (DEBUG_LOG) log.debug('check-tokens', 'acl-tokens:', this._tokens, 'principal-access-tokens:', principal_accesstokens);
            if (this._tokens.indexOf("all") >= 0) {
                // grant all access tokens
                return true;
            } else {
                var diff_list = [];
                if (this.compareSet({expected:this._tokens, actual:principal_accesstokens, label:'tokens'})) {
                    return true;
                }
            }
        }
        // check resources
        if (this._resources && req_resource) {
            if (DEBUG_LOG) log.debug('check-resources');
            const requested_res_keys = Object.keys(req_resource);
            const self_resources = this.compute(principal);

            var is_rescheck_succeed = false;

            for (var i in requested_res_keys) {
                var req_res_k = requested_res_keys[i];
                var req_res_v = req_resource[req_res_k];
                var self_res_v = self_resources[req_res_k];
                if (this._resources[req_res_k]) {

                    var rules_list = this._resources[req_res_k];
                    var is_satisfied = false;
                    for (var x in rules_list) {
                        var rule = rules_list[x];
                        // var rule_id = Object.keys(rule)[0];
                        is_satisfied = this.handleRule(rule, principal, {key: req_res_k, value: req_res_v} );
                        if (is_satisfied == true) {
                            break;
                        } else {
                            log.debug('rule broken:', rule);
                        }
                    }

                    if (is_satisfied) {
                        return true;
                    }
                }
            } // end of outer for-loop
        }

        return false;
    }

    handleRule(rule, principal, target) {
         var id = Object.keys(rule)[0];
         var value = rule[id];
         if (id === 'principal_roles') {
            return this.checkPrincipalRoles(value, principal, target);
         }
         else if (id === 'principal_user') {
            return this.checkPrincipalUser(value, principal, target);
         }
         else if (id === 'terms') {
            return this.checkTerms(value, principal, target);
         }
         return false;
    }

    checkPrincipalRoles(desc, principal, target) {
        if (DEBUG_LOG) log.debug('check principal roles', desc, target);
        if (desc.contains) {
            if (principal.hasRoles(desc.contains)) {
                return true;
            }
        }
        return false;
    }

    checkPrincipalUser(desc, principal, target) {
        if (DEBUG_LOG) log.debug('check principal user', desc, target);
        if (desc.matches) {
            if (!principal.user) {
                return false;
            }
            var actual_value = principal.user[target.key];
            if (_.isUndefined(actual_value)) {
                return false;
            }
            if (actual_value != target.value) {
                return false;
            }
            return true;
        }
        return false;
    }

    checkTerms(desc, principal, target) {
        if (DEBUG_LOG) log.debug('check terms', desc, 'target:', target);
        const has = desc.has;
        if (has) {
            var tvalue = target.value;
            if (!(_.isArray(tvalue))) {
                tvalue = [tvalue];
            }
            var no_common_members = _.intersection(has, tvalue).length == 0;
            if (no_common_members) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
    @param {Principal} principal
    @returns {Object} resources of calling principal.
    */
    compute(principal) {
        var resource = {};
        if (principal.roles.indexOf('internal') >= 0) {
            resource.username = '*';
        } else {
            resource.username = principal.username;
        }
        return resource;
    }

}

var acl_default = getDefault()
    .resources({
        username: [
            { principal_roles:{ contains:['internal'] } },
            { principal_user:{ matches:['username'] } },
            { terms:{ has:['self'] } }
        ]
    }
);

var acl_internal = getDefault().roles(['internal']);
var acl_distributor = getDefault().roles(['distributor']);
var acl_distributor_manager = getDefault().roles(['distributor_manager']);
var acl_sales = getDefault().roles(['sales']);
var acl_retailer = getDefault().roles(['retailer']);
var acl_unilever_admin = getDefault().roles(['unilever_admin']);
var acl_super_admin = getDefault().roles(['super_admin']);
var acl_pickup_agent = getDefault().roles(['pickup_agent']);
var acl_call_center = getDefault().roles(['call_center']);

function getDefault() {
    return new ACL()
        .tokens(ACCESS_TOKEN_INTERNAL_DEV)
        .roles('super_admin');
}

var factory = {};
factory.getDefault = getDefault;

export default ACL
export { factory }
export { acl_default as AclDefault }
export { acl_internal as AclInternal }
export { acl_distributor as AclDistributor }
export { acl_distributor_manager as AclDistributorManager }
export { acl_sales as AclSales }
export { acl_retailer as AclRetailer }
export { acl_unilever_admin as AclUnileverAdmin }
export { acl_super_admin as AclSuperAdmin }
export { acl_pickup_agent as AclPickUpAgent }
export { acl_call_center as AclCallCenter }



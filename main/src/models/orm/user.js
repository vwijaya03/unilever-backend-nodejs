import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'
import CustomDataTypes from '@/lib/sequelize-custom-datatypes';
import moment from 'moment'
import service_logger from '@/server/services/logger'

const log = service_logger.getLogger({name: 'orm', service:'user'});

const User = orm.define('users', {
    id: {
        type: Sequelize.INTEGER, field: 'user_id',
        primaryKey: true,
        autoIncrement: true
    },
    fullname: {
        type: Sequelize.STRING, allowNull:true
    },
    nama_toko: {
        type: Sequelize.STRING, allowNull:true
    },
    password: {
        type: Sequelize.STRING, allowNull:false,
        get(key) {
            var value = this.getDataValue(key);
            if (value instanceof Buffer) {
                value = value.toString('utf8');
            } else {
                value = '';
            }
            // console.log('get password:', value);
            return value;
        }
    },
    password_changed: {
        type: Sequelize.STRING, allowNull:true
    },
    onbehalf: {
        type: Sequelize.STRING, allowNull:true
    },
    reference: {
        type: Sequelize.STRING, allowNull:true
    },
    phone: {
        type: Sequelize.STRING, allowNull:true
    },
    // tanggal_lahir: {
    //     type: Sequelize.STRING, allowNull:true
    // },
    // tempat_lahir: {
    //     type: Sequelize.STRING, allowNull:true
    // },
    // ktp: {
    //     type: Sequelize.STRING, allowNull:true
    // },
    email: {
        type: Sequelize.STRING, allowNull:true, unique:true
    },
    pin: {
        type: Sequelize.STRING, allowNull:false
    },
    type: {
        type: Sequelize.INTEGER, allowNull:true
    },
    salt: {
        type: Sequelize.STRING, allowNull:false, unique:true
    },
    valdoAccount: {
        type: Sequelize.STRING, allowNull:true, field: 'valdo_account'
    },
    fcm_id: {
        type: Sequelize.STRING, allowNull:true, field: 'fcm_id'
    },
    imei: {
        type: Sequelize.STRING, allowNull:true, field: 'imei'
    },
    accessToken: {
        type: Sequelize.STRING, allowNull:false, field: 'access_token'
    },
    accessTokenExpiry: {
        type: CustomDataTypes.TIMESTAMP, allowNull:false, field: 'access_token_expiry',
        set(value, fieldName) {
            this.setDataValue('accessTokenExpiry', value);
        },
        get(key) {
            var value = this.getDataValue(key);
            log.debug('access token expiry raw db value:', value);
            if (value instanceof Date) {
                value = moment(value.getTime()).unix();
                log.debug('access token expiry token timestamp value:', value);
            }
            return value;
        }
    },
    retailerId: {
        type: Sequelize.INTEGER, allowNull:true, field: 'retailer_id'
    },
    distributorId: {
        type: Sequelize.INTEGER, allowNull:true, field: 'distributor_id'
    },
    pickupId: {
        type: Sequelize.INTEGER, allowNull:true, field: 'pickup_id'
    },
    salesId: {
        type: Sequelize.INTEGER, allowNull:true, field: 'sales_id'
    },
    status: {
        type: Sequelize.INTEGER, allowNull:true
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});

/**
setups query filter: user types
@param {Object} filter
@param {Array.<String>} rolesList
*/
function queryTypes({filter=null, typesList=null}={}) {
    if (! typesList) {
        return;
    }
    console.log('queryTypes. ', typesList);
    filter.$or = typesList.map(type => {
        return {'type':type};
    });
}

export default User

var query = {};
query.types = queryTypes;
export { query as UserQuery }
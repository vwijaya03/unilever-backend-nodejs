import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'
import CustomDataTypes from '@/lib/sequelize-custom-datatypes';

const Notification = orm.define('notifications', {
    id: { type: Sequelize.INTEGER, field: 'id', primaryKey: true, autoIncrement: true },
    to: { type: Sequelize.STRING, allowNull:false },
    type: { type: Sequelize.STRING, allowNull:false },
    message: { type: Sequelize.INTEGER, allowNull:false },
    state: { type: Sequelize.STRING, allowNull:false },
    createdAt: { type: CustomDataTypes.TIMESTAMP, allowNull:true },
    updatedAt: { type: CustomDataTypes.TIMESTAMP, allowNull:true }
});

export default Notification
import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'


const Logs = orm.define('logs', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    action: {
        type: Sequelize.STRING, allowNull:false, field:'action'
    },
    description: {
        type: Sequelize.STRING, allowNull:false, field:'description'
    },
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});

export default Logs
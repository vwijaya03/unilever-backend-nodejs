import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'
import CustomDataTypes from '@/lib/sequelize-custom-datatypes';

const MobileVersion = orm.define('mobile_versions', {
    id: { type: Sequelize.INTEGER, field: 'id', primaryKey: true, autoIncrement: true },
    version: { type: Sequelize.STRING, allowNull:true },
    download_link: { type: Sequelize.STRING, allowNull:true },
    force: { type: Sequelize.BOOLEAN, allowNull:true },
    update_description: { type: Sequelize.STRING, allowNull:true },
    createdAt: { type: CustomDataTypes.TIMESTAMP, allowNull:true },
    updatedAt: { type: CustomDataTypes.TIMESTAMP, allowNull:true }
});

export default MobileVersion
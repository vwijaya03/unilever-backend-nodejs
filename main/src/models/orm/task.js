import { orm } from '@/server/services/mariadb'
import CustomDataTypes from '@/lib/sequelize-custom-datatypes'
import Sequelize from 'sequelize'

const Task = orm.define('task', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    dt_code: {
        type: Sequelize.STRING, allowNull:false
    },
    original_name: {
        type: Sequelize.STRING, allowNull:false
    },
    file_name: {
        type: Sequelize.STRING, allowNull:false
    },
    status: {
        type: Sequelize.INTEGER, allowNull:true
    },
    total_row: {
        type: Sequelize.INTEGER, allowNull:false
    },
    processed_row: {
        type: Sequelize.INTEGER, allowNull:true
    },
    updated_row: {
        type: Sequelize.INTEGER, allowNull:true
    },
    action: {
        type: Sequelize.STRING, allowNull:true
    },
    createdAt: {
        type: CustomDataTypes.TIMESTAMP, allowNull:true
    },
    updatedAt: {
        type: CustomDataTypes.TIMESTAMP, allowNull:true
    },
    user_id: {
        type: Sequelize.STRING, allowNull:true
    },
    total_upload: {
        type: Sequelize.INTEGER, allowNull:true
    }
});

export default Task
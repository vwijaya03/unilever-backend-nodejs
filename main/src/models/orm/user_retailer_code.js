import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'
import CustomDataTypes from '@/lib/sequelize-custom-datatypes';
import moment from 'moment'
import service_logger from '@/server/services/logger'

const log = service_logger.getLogger({name: 'orm', service:'user retailer code'});

const UserRetailerCode = orm.define('users_retailer_codes', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_id: {
        type: Sequelize.INTEGER, allowNull:true
    },
    outlet_code: {
        type: Sequelize.STRING, allowNull:true
    },
    dt_code: {
        type: Sequelize.STRING, allowNull:true
    },
    valdo_account: {
        type: Sequelize.STRING, allowNull:true
    }
});

export default UserRetailerCode
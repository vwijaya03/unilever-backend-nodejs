import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'


const Distributor = orm.define('distributors', {
    id: {
        type: Sequelize.INTEGER, field: 'distributor_id', primaryKey: true,
        autoIncrement: true
    },
    distributorCode: {
        type: Sequelize.STRING, allowNull:false, field:'dt_code'
    },
    name: {
        type: Sequelize.STRING, allowNull:true
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});


export default Distributor
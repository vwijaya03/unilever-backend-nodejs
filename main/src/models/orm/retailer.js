import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'


const Retailer = orm.define('retailers', {
    id: {
        type: Sequelize.INTEGER, field: 'retailer_id',
        primaryKey: true,
        autoIncrement: true
    },
    distributor_id: {
        type: Sequelize.INTEGER, allowNull:true, field:'distributor_id'
    },
    outletCode: {
        type: Sequelize.STRING, allowNull:true, field:'outlet_code'
    },
    name: {
        type: Sequelize.STRING, allowNull:true
    },
    address: {
        type: Sequelize.STRING, allowNull:true
    },
    le_code_dry: {
        type: Sequelize.STRING, allowNull:true
    },
    le_code_ice: {
        type: Sequelize.STRING, allowNull:true
    },
    le_code_ufs: {
        type: Sequelize.STRING, allowNull:true
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});


export default Retailer
import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'
import CustomDataTypes from '@/lib/sequelize-custom-datatypes';
import moment from 'moment'
import service_logger from '@/server/services/logger'

const log = service_logger.getLogger({name: 'orm', service:'informasi dan promosi'});

const Informasi = orm.define('informasi', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    createdAt: {
        type: CustomDataTypes.TIMESTAMP, allowNull:true
    },
    updatedAt: {
        type: CustomDataTypes.TIMESTAMP, allowNull:true
    },
    title: {
        type: Sequelize.STRING, allowNull:false
    },
    content: {
        type: Sequelize.STRING, allowNull:false
    },
    image: {
        type: Sequelize.STRING, allowNull:true
    }
});

export default Informasi
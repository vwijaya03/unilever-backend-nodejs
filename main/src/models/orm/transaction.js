import { orm } from '@/server/services/mariadb'
import _ from 'lodash'
import Sequelize from 'sequelize'
import moment from 'moment'
import CustomDataTypes from '@/lib/sequelize-custom-datatypes'
import DataTypes from '@/node_modules/sequelize/lib/data-types'

const DEBUG_LOG = false;

const Transaction = orm.define('transactions', {
    id: {
        type: Sequelize.BIGINT, field: 'transaction_id', primaryKey: true,
        autoIncrement: true
    },
    fromUserId: {
        type: Sequelize.INTEGER, allowNull:false, field:'from_user_id'
    },
    toUserId: {
        type: Sequelize.INTEGER, allowNull:true, field:'to_user_id'
    },
    dt_code: {
        type: Sequelize.STRING, allowNull:true, field:'dt_code'
    },
    invoiceId: {
        type: Sequelize.STRING, allowNull:true, field:'invoice_id'
    },
    invoice_code: {
        type: Sequelize.STRING, allowNull:true, field:'invoice_code'
    },
    retailerId: {
        type: Sequelize.INTEGER, allowNull:true, field:'retailer_id'
    },
    distributorId: {
        type: Sequelize.INTEGER, allowNull:true, field:'distributor_id'
    },
    valdoTxId: {
        type: Sequelize.STRING, allowNull:true, field:'valdo_tx_id'
    },
    type: {
        type: Sequelize.INTEGER, allowNull:true
    },
    amount: {
        type: Sequelize.DECIMAL(12, 4), allowNull:true
    },
    currencyCode: {
        type: Sequelize.STRING, allowNull:true, field:'currency_code'
    },
    transactDate: {
        type: CustomDataTypes.TIMESTAMP, allowNull:false, field: 'transact_date',
        set(value) {
            if (DEBUG_LOG) console.log('### set transact date', value);
            // the key is the ORM property not db column name
            this.setDataValue('transactDate', value);
        },
        get(key) {
            var value = this.getDataValue(key);
            // Note: getDataValue() reads sql TIMESTAMP and returns Date object.
            if (value instanceof Date) {
                value = value.getTime();
            }
            if (DEBUG_LOG) console.log('### get transact date', key, value);
            return value;
        }
    },
    balance: {
        type: DataTypes.VIRTUAL,
        set: function (val) {
            this.setDataValue('balance', val); // Remember to set the data value, otherwise it won't be validated
            // this.setDataValue('password_hash', this.salt + val);
        }
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});

/**
setups query filter: type
@param {Object} filter
@param {Number} type
*/
function queryType({filter=null, type=null}={}) {
    filter['type'] = type;
}

/**
setups query filter: retailer id
@param {Object} filter
@param {String} retailerId
*/
function queryRetailerId({filter=null, retailerId=null}={}) {
    filter['retailerId'] = retailerId;
}

/**
setups query filter: transact date
@param {Object} filter
@param {Number} start - the starting due date in unix seconds
@param {Number} end - the ending due date in unix seconds
*/
function queryTransactDate({filter=null, start=null, end=null}={}) {
    filter['transactDate'] = { $gte:start, $lte:end };
}

/**
setups query filter: tx sent by user and received by user.
@param {Object} filter
@param {String} userId
@param {Boolean} send - only get sending type
@param {Boolean} receive - only get receiving type
*/
function queryUserId({filter=null, userId=null, isSend=true, isReceive=false}={}) {
    console.log('queryUserId. ', userId, isSend, isReceive);
    if (isSend && isReceive) {
        filter['$or'] = [
            { fromUserId: { $eq: userId } },
            { toUserId: { $eq: userId } }
        ];
        return;
    }
    if (isSend) {
        filter['fromUserId'] = userId;
    }
    else if (isReceive) {
        filter['toUserId'] = userId;
    }

}

export default Transaction

var query = {};
query.userId = queryUserId;
query.transactDate = queryTransactDate;
query.retailerId = queryRetailerId;
query.type = queryType;
export { query as TransactionQuery }
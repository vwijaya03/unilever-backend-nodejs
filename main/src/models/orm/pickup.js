import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'


const Pickup = orm.define('pickup', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    pickupCode: {
        type: Sequelize.STRING, allowNull:false, field:'pickup_code'
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});


export default Pickup
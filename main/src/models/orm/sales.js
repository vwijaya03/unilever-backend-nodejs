import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'


const Sales = orm.define('sales', {
    id: {
        type: Sequelize.INTEGER, field: 'sales_id', primaryKey: true,
        autoIncrement: true
    },
    distributorId: {
        type: Sequelize.INTEGER, allowNull:true, field: 'distributor_id'
    },
    salesCode: {
        type: Sequelize.STRING, allowNull:false, field:'sales_code'
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});


export default Sales
import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'


const RetailBalance = orm.define('retailer_balances', {
    id: {
        type: Sequelize.BIGINT, field:'retailer_balance_id', primaryKey: true
    },
    balance: {
        type: Sequelize.DECIMAL(12,4), allowNull:true
    },
    currency_code: {
        type: Sequelize.STRING, allowNull:true
    },
    transactDate: {
        type: Sequelize.DATE, allowNull:true
    },
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});


export default RetailBalance
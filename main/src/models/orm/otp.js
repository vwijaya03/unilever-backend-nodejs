import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'
import CustomDataTypes from '@/lib/sequelize-custom-datatypes';

const Otp = orm.define('otps', {
    id: { type: Sequelize.INTEGER, field: 'id', primaryKey: true, autoIncrement: true },
    ipaddress: { type: Sequelize.STRING, allowNull:true },
    secret_key: { type: Sequelize.STRING, allowNull:true },
    otp: { type: Sequelize.STRING, allowNull:true },
    token_user: { type: Sequelize.STRING, allowNull:true },
    valid: { type: Sequelize.BOOLEAN, allowNull:true },
    createdAt: { type: CustomDataTypes.TIMESTAMP, allowNull:true },
    updatedAt: { type: CustomDataTypes.TIMESTAMP, allowNull:true }
});

export default Otp
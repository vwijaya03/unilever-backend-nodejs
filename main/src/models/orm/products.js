import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'
import CustomDataTypes from '@/lib/sequelize-custom-datatypes';
import moment from 'moment'
import service_logger from '@/server/services/logger'

const log = service_logger.getLogger({name: 'orm', service:'table products'});

const Products = orm.define('products', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    dt_code: {
        type: Sequelize.STRING, allowNull:false
    },
    invoice_id: {
        type: Sequelize.STRING, allowNull:false
    },
    product: {
        type: Sequelize.STRING, allowNull:false
    },
    product_name: {
        type: Sequelize.STRING, allowNull:false
    },
    quantity: {
        type: Sequelize.DECIMAL, allowNull:true
    },
    product_price_cs: {
        type: Sequelize.DECIMAL, allowNull:true
    },
    product_price_dz: {
        type: Sequelize.DECIMAL, allowNull:true
    },
    product_price_pc: {
        type: Sequelize.DECIMAL, allowNull:true
    }
});

export default Products
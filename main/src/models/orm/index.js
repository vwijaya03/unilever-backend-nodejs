import User from '@/models/orm/user'
import Distributor from '@/models/orm/distributor'
import Sales from '@/models/orm/sales'
import RetailBalance from '@/models/orm/retail_balance'
import Retailer from '@/models/orm/retailer'
import Pickup from '@/models/orm/pickup'
import Transaction from '@/models/orm/transaction'
import Invoice from '@/models/orm/invoice'
import BankAccount from '@/models/orm/bank_account'
import Products from '@/models/orm/products'
import Informasi from '@/models/orm/informasi'
import UserRetailerCode from '@/models/orm/user_retailer_code'
import MobileVersion from '@/models/orm/mobile_version'
import Notification from '@/models/orm/notif'
import Otp from '@/models/orm/otp'
import Logs from '@/models/orm/logs'
import Task from '@/models/orm/task'

Retailer.hasMany(User, { as:'Users', foreignKey: 'retailer_id' })
Distributor.hasMany(User, { as:'Users', foreignKey: 'distributor_id' })
User.hasMany(BankAccount, {foreignKey: 'user_id' })
BankAccount.belongsTo(User, {foreignKey: 'user_id'})
/*
user belongs to either retailer of distributor.

This will allow us to do:

user.getRetailer(); // returns Promise
user.getDistributor(); // returns Promise

*/
User.belongsTo(Retailer, { foreignKey:'retailer_id' } )
User.belongsTo(Pickup, { foreignKey:'pickup_id' } )
User.belongsTo(Distributor, { foreignKey:'distributor_id' } )
User.belongsTo(Sales, { foreignKey:'sales_id' } )

/*
invoice belongs to either retailer OR distributor

// note: targetKey is pointing to the orm property of destination (Retailer/Distributor)
// note: foreignKey is pointing to the actual db column name of table referred by Invoice.
*/
Invoice.belongsTo(Retailer, {as:'Retailer', foreignKey: 'outlet_code', targetKey:'outletCode'} )
Invoice.belongsTo(Distributor, {as:'Distributor', foreignKey: 'dt_code', targetKey:'distributorCode'} )

Invoice.belongsTo(Transaction, {foreignKey: 'invoice_id', targetKey: 'invoiceId'})
// Transaction.hasMany(Invoice, { foreignKey:'invoice_id' } )
// Invoice.hasOne(Transaction, { foreignKey:'invoice_id' } )

/*
transaction belongs to retailer OR distributor

// note: targetKey is pointing to the orm property of destination (Retailer/Distributor)
// note: foreignKey is pointing to the actual db column name of table referred by Transaction.
*/
Transaction.belongsTo(Retailer, {as:'Retailer', foreignKey: 'retailer_id', targetKey:'id' } )
Transaction.belongsTo(Distributor, {as:'Distributor', foreignKey: 'distributor_id', targetKey:'id' } )

export { User }
export { BankAccount }
export { Distributor }
export { Retailer }
export { Pickup }
export { Sales }
export { RetailBalance }
export { Transaction }
export { Invoice }
export { Products }
export { Informasi }
export { UserRetailerCode }
export { MobileVersion }
export { Notification }
export { Otp }
export { Logs }
export { Task }
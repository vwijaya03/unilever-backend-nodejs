import { orm } from '@/server/services/mariadb'
import Sequelize from 'sequelize'
import CustomDataTypes from '@/lib/sequelize-custom-datatypes';
import moment from 'moment'
import service_logger from '@/server/services/logger'

const log = service_logger.getLogger({name: 'orm', service:'bank account'});

const BankAccount = orm.define('users_banking', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_id: {
        type: Sequelize.INTEGER, allowNull:false
    },
    name: {
        type: Sequelize.STRING, allowNull:true
    },
    no_rekening: {
        type: Sequelize.STRING, allowNull:false
    },
    bank: {
        type: Sequelize.STRING, allowNull:true
    }
});

export default BankAccount
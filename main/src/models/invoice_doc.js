class InvoiceDoc {
    constructor({
        id=null,
        composite_id=null,
        date=null,
        due_date=null,
        amount=0,
        paid=false,
        sku=null,
        qty=0,
        price_sku=null
    } = {}) {
        this.keys = ['id','composite_id','date','due_date','amount','paid','sku','qty','price_sku'];

        console.log('this.props:');

        this.id = id;
        this.composite_id = composite_id;
        this.date = date;
        this.due_date = due_date;
        this.amount = amount;
        this.paid = paid;
        this.sku = sku;
        this.qty = qty;
        this.price_sku = price_sku;
    }

    getKeys() {
        return this.keys;
    }
}

export default InvoiceDoc


const RETAILER_CODE = 0;
const DISTRIBUTOR_CODE = 1;
const PICKUP_AGENT_CODE = 2;
const DISTRIBUTOR_MANAGER_CODE = 3;
const SALES_CODE = 4;
const UNILEVER_ADMIN_CODE = 9;
const SUPER_ADMIN_CODE = 10;
const CALL_CENTER_CODE = 11;

const RETAILER = {'retailer':RETAILER_CODE}
const DISTRIBUTOR = {'distributor':DISTRIBUTOR_CODE}
const DISTRIBUTOR_MANAGER = {'distributor_manager':DISTRIBUTOR_MANAGER_CODE}
const SALES = {'sales':SALES_CODE}
const PICKUP_AGENT = {'pickup_agent':PICKUP_AGENT_CODE}
const UNILEVER_ADMIN = {'unilever_admin':UNILEVER_ADMIN_CODE}
const SUPER_ADMIN = {'super_admin':SUPER_ADMIN_CODE}
const CALL_CENTER = {'call_center':CALL_CENTER_CODE}

var USERTYPES = {};
Object.assign(USERTYPES, RETAILER, DISTRIBUTOR, DISTRIBUTOR_MANAGER, SALES, PICKUP_AGENT, SUPER_ADMIN, UNILEVER_ADMIN, CALL_CENTER);

var obj = {};

/**
gets the name of the usertype with given id.
@param id - usertype id
@returns {String} name of usertype
*/
obj.getName = function(id) {
    return Object.keys(USERTYPES).filter(name => {
        return USERTYPES[name] == id;
    }).pop();
}

/**
check if usertype id is valid.
@param {Number} id - usertype id
@return {Boolean}
*/
obj.isValidType = function(id) {
    var name = obj.getName(id);
    if (!name) {
        return false;
    }
    return true;
}

export default obj
export { USERTYPES }
export { RETAILER_CODE }
export { DISTRIBUTOR_CODE }
export { DISTRIBUTOR_MANAGER_CODE }
export { SALES_CODE }
export { SUPER_ADMIN_CODE }
export { UNILEVER_ADMIN_CODE }
export { PICKUP_AGENT_CODE }
export { CALL_CENTER_CODE }


import _ from 'lodash'
import User from '@/models/orm/user'


class Principal {
    constructor({
        roles=[],
        user_id=null,
        username= null,
        retailer_id=null,
        distributor_id=null
    } = {}) {
        this.access_token = null;
        this.roles = roles;
        this.user = null;
        this.username = username;
        this.user_id = user_id;
        this.retailer_id = retailer_id;
        this.distributor_id = distributor_id;
    }
    /**
    @param {Array.<String>} expected - a list of expected roles
    */
    hasRoles(expected) {
        return _.difference(expected, this.roles).length === 0;
    }
    /**
    @param {Array.<String>} list - a list of roles will be added to the set.
    */
    addRoles(list) {
        this.roles = this.roles.concat(list).filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        });
    }
    /**
    @param {Array.<String>} list - a list of roles will be assigned to the roles set.
    */
    setRoles(list) {
        // create a set. (i.e: remove duplicate items)
        this.roles = list.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        });
    }
    /**
    @param {User} user - the principal user.
    */
    setUser(user) {
        if (user instanceof User.Instance) {
            this.user = user;
            this.user_id = user.id;
            this.username = user.username;
        }
    }
    setRetailerId(value) {
        this.retailer_id = value;
    }
    setDistributorId(value) {
        this.distributor_id = value;
    }
    getAccessToken() {
        return this.access_token;
    }
    setAccessToken(value) {
        this.access_token = value;
    }
}

export default Principal


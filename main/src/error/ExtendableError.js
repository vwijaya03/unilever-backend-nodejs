class ExtendableError extends Error {
  constructor(code, message) {
    super(message);
    this.name = this.constructor.name;
    this.message = '' + message;
    this.code = (!code)? 0 : code;
    if (typeof Error.captureStackTrace === 'function') {
        Error.captureStackTrace(this, this.constructor);
    } else {
        this.stack = (new Error(message)).stack;
    }
  }
}

export default ExtendableError
import InternalServerError from './InternalServerError'
import ApiTokenInvalidError from './ApiTokenInvalidError'
import AccessTokenExpiredError from './AccessTokenExpiredError'
import ChangePinError from './ChangePinError'
import PinError from './PinError'
import InvoiceUploadError from './InvoiceUploadError'
import SigninError from './SigninError'
import ChangePasswordError from './ChangePasswordError'
import UserRegistrationError from './UserRegistrationError'
import DOBInvalidFormat from './DOBInvalidFormat'
import UsernameAlreadyExistsError from './UserRegistrationError'
import UserNotFoundError from './UserNotFoundError'
import ResourceNotFoundError from './ResourceNotFoundError'
import DbRecordCreationError from './DbRecordCreationError'
import ExcelParseError from './ExcelParseError'
import UnauthorizedRequestError from './UnauthorizedRequestError'
import InvoiceAlreadyPaidError from './InvoiceAlreadyPaidError'
import BalanceNotEnoughError from './BalanceNotEnoughError'
import MakePaymentError from './MakePaymentError'
import MoneyAccessError from './MoneyAccessError'

InternalServerError.code = 500;

SigninError.code = 1000;
ApiTokenInvalidError.code = 1001;
AccessTokenExpiredError.code = 1002;

UserRegistrationError.code = 2000;
ChangePinError.code = 2001;
UsernameAlreadyExistsError.code = 2002;
PinError.code = 2003;
DOBInvalidFormat.code = 2004;
ChangePasswordError.code = 2005;

InvoiceUploadError.code = 3000;
ExcelParseError.code = 3004;

DbRecordCreationError.code = 4000;

ResourceNotFoundError.code = 5000;
UserNotFoundError.code = 5001;

InvoiceAlreadyPaidError.code = 6000;
BalanceNotEnoughError.code = 6001;
MakePaymentError.code = 6002;

UnauthorizedRequestError.code = 7000;

// wrapper for all moneyaccess specific error.
MoneyAccessError.code = 69000;

export { InternalServerError }
export { AccessTokenExpiredError }
export { ApiTokenInvalidError }
export { ChangePinError }
export { InvoiceUploadError }
export { SigninError }
export { UserRegistrationError }
export { DOBInvalidFormat }
export { UsernameAlreadyExistsError }
export { PinError }
export { UserNotFoundError }
export { ResourceNotFoundError }
export { DbRecordCreationError }
export { ExcelParseError }
export { UnauthorizedRequestError }
export { InvoiceAlreadyPaidError }
export { BalanceNotEnoughError }
export { MakePaymentError }
export { MoneyAccessError }
export { ChangePasswordError }
import ExtendableError from '@/error/ExtendableError'

class InvoiceUploadError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'failed uploading invoice';
    }
    super(InvoiceUploadError.code, m);
  }
}

export default InvoiceUploadError



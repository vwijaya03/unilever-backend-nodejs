import ExtendableError from '@/error/ExtendableError'

class ChangePasswordError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'Password lama anda salah.';
    }
    var code = ChangePasswordError.code;
    super(code, m);
  }
}

export default ChangePasswordError

import ExtendableError from '@/error/ExtendableError'

class PinError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'Pin salah.';
    }
    var code = PinError.code;
    super(code, m);
  }
}

export default PinError

import ExtendableError from '@/error/ExtendableError'

class ChangePinError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'failed changing pin';
    }
    var code = ChangePinError.code;
    super(code, m);
  }
}

export default ChangePinError

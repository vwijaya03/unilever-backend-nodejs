import ExtendableError from '@/error/ExtendableError'

class AccessTokenExpiredError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'access token has expired';
    }
    var code = AccessTokenExpiredError.code;
    super(code, m);
  }
}

export default AccessTokenExpiredError


import ExtendableError from '@/error/ExtendableError'

/**
Wrapper for all specific moneyaccess errors
*/
class MoneyAccessError extends ExtendableError {
  constructor(m, code) {
    if (!m) {
        m = 'failed invoking moneyaccess.';
    }
    if (!code) {
        code = MoneyAccessError.code;
    }
    super(code, m);
  }
}

export default MoneyAccessError


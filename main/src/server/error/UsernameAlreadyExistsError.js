import ExtendableError from '@/error/ExtendableError'

class UsernameAlreadyExistsError extends ExtendableError {
  constructor(m, code) {
    if (!m) {
        m = 'username already exists';
    }
    if (!code) {
        code = UsernameAlreadyExistsError.code;
    }
    super(code, m);
  }
}

export default UsernameAlreadyExistsError

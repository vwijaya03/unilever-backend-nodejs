import ExtendableError from '@/error/ExtendableError'

class UnauthorizedRequestError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'unauthorized request';
    }
    var code = UnauthorizedRequestError.code;
    super(code, m);
  }
}

export default UnauthorizedRequestError


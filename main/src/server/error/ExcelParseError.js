import ExtendableError from '@/error/ExtendableError'

class ExcelParseError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'failed parsing excel';
    }
    super(ExcelParseError.code, m);
  }
}

export default ExcelParseError


import ExtendableError from '@/error/ExtendableError'

class MakePaymentError extends ExtendableError {
  constructor(m, code) {
    if (!m) {
        m = 'failed to transact payment.';
    }
    if (! code) {
        code = MakePaymentError.code;
    }
    super(code, m);
  }
}

export default MakePaymentError


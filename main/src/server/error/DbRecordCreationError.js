import ExtendableError from '@/error/ExtendableError'

class DbRecordCreationError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'unable to create db record.';
    }
    super(DbRecordCreationError.code, m);
  }
}

export default DbRecordCreationError

import ExtendableError from '@/error/ExtendableError'

class UserRegistrationError extends ExtendableError {
  constructor(m, code) {
    if (!m) {
        m = 'failed registering user';
    }
    if (!code) {
        code = UserRegistrationError.code;
    }
    super(code, m);
  }
}

export default UserRegistrationError

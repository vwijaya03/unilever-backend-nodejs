import ExtendableError from '@/error/ExtendableError'

class ResourceNotFoundError extends ExtendableError {
  constructor(m, code) {
    if (!m) {
        m = 'requested resource not found';
    }
    if (! code) {
        code = ResourceNotFoundError.code;
    }
    super(code, m);
  }
}

export default ResourceNotFoundError

import ExtendableError from '@/error/ExtendableError'

class UserNotFoundError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'user not found';
    }
    super(UserNotFoundError.code, m);
  }
}

export default UserNotFoundError

import ExtendableError from '@/error/ExtendableError'

class SigninError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'invalid credentials';
    }
    super(SigninError.code, m);
  }
}

export default SigninError


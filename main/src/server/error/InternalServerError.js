import ExtendableError from '@/error/ExtendableError'

class InternalServerError extends ExtendableError {
  constructor(m, code) {
    if (!m) {
        m = 'internal server error.';
    }
    if (!code) {
        code = InternalServerError.code;
    }
    super(code, m);
  }
}

export default InternalServerError


import ExtendableError from '@/error/ExtendableError'

class ApiTokenInvalidError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'invalid api token';
    }
    var code = ApiTokenInvalidError.code;
    super(code, m);
  }
}

export default ApiTokenInvalidError


import ExtendableError from '@/error/ExtendableError'

class BalanceNotEnoughError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'sorry, not enough money.';
    }
    var code = BalanceNotEnoughError.code;
    super(code, m);
  }
}

export default BalanceNotEnoughError


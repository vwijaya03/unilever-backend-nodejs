import ExtendableError from '@/error/ExtendableError'

class InvoiceAlreadyPaidError extends ExtendableError {
  constructor(m) {
    if (!m) {
        m = 'invoice already paid';
    }
    var code = InvoiceAlreadyPaidError.code;
    super(code, m);
  }
}

export default InvoiceAlreadyPaidError


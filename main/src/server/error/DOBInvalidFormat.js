import ExtendableError from '@/error/ExtendableError'

class DOBInvalidFormat extends ExtendableError {
  constructor(m, code) {
    if (!m) {
        m = 'Invalid format date of birth.';
    }
    if (!code) {
        code = DOBInvalidFormat.code;
    }
    super(code, m);
  }
}

export default DOBInvalidFormat

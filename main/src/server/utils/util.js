import moment from 'moment'

/**
* converts a list of objects to a map of objects.
* @param {array} list - array of objects. schema: [ {id:id1}, {id:id2}, {id:id3} ]
* @param {string} key - the property will be used in each item to generate id.
* @returns {Object} map of objects
*/
function list2obj(list, key) {
    // console.log('list2obj. list:', list, 'key:', key);
    var out = {};
    if (!list) {
        return out;
    }
    for (var i=0,n=list.length;i<n;i++) {
        var obj = list[i];
        if (! obj) {
            continue;
        }
        var kv = obj[key];
        if (kv) {
            out[kv] = obj;
        }
    }
    return out;
}

/**
@param {Object} obj - object. schema: { id1:{id1obj}, id2:{}, id3:{} }
@returns {Array} a list of form [{id1_obj},{id2_obj},{id3_obj}]
*/
function obj2list(obj) {
    return Object.keys(obj).map( (key) => {
        return obj[key];
    });
}

function flatten_list(topic) {
    if( topic.indexOf('#') >= 0) {
        return {res:false, reason:'must not contain hashes'};
    }
    if( topic.indexOf('//') >= 0) {
        return {res:false, reason:'must not contain double-slashes //'};
    }
    return {res:true, reason:null};
}

function isNumeric(input){
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
}

/**
@param {Moment} defaultValue - default value
@param {Moment} now - origin date time.
@param {String} pattern - pattern = (<amount><unit> | 'now')
                <amount> = <signed_integer>
                <unit> = (m|d)
@returns {Moment} - returns null if cant parse pattern string,
otherwise returns a Moment object.
*/
function parseDateString({pattern=null, now=null}={}) {
    if (! pattern) {
        return null;
    }

    // unix timestamp (seconds)
    if (/^\d+$/.test(pattern)) {
        return moment.unix(parseInt(pattern));
    }
    else {
        if (pattern === 'now') {
            return moment.utc();
        }
        else {
            var rel_pattern = /(-?\d+)(\w{1})/;
            var groups = pattern.match(rel_pattern);
            // console.log('matches:', groups);
            if (groups && groups.length > 0) {
                var amount = groups[1];
                var unit = groups[2];
                if (unit === 'm') {
                    unit = 'month';
                }
                else if (unit === 'w') {
                    unit = 'week';
                }
                else {
                    unit = 'day';
                }
                var ref = now?moment.utc(now):moment.utc();
                var out = ref.add(amount, unit);
                // console.log('parsed relative due date:', amount,
                // unit, out.format('LLL'));
                return out;
            }
        }
    }
    return null;
}

function getStartEndDates({startDate=null, endDate=null}={}) {
    var startDateMoment = null;
    var endDateMoment = null;

    const iso8601format = "YYYY-MM-DD";

    var is_format_start_match = /^\d{4}-\d{2}-\d{2}$/.test(startDate);
    var is_format_end_match = /^\d{4}-\d{2}-\d{2}$/.test(endDate);
    if (is_format_start_match) {
        // Note: important that we use moment.utc otherwise it will be a local date time.
        startDateMoment = moment.utc(startDate, iso8601format);
    } else {
        startDateMoment = parseDateString({pattern:startDate, now:null});
    }
    if (is_format_end_match) {
        // Note: important that we use moment.utc otherwise it will be a local date time.
        endDateMoment = moment.utc(endDate, iso8601format);
    } else {
        endDateMoment = parseDateString({pattern:endDate, now:startDateMoment});
    }
    return {start:startDateMoment, end:endDateMoment};
}

function stringifyOnce(obj, replacer, indent) {
    var printedObjects = [];
    var printedObjectKeys = [];

    function printOnceReplacer(key, value){
        var printedObjIndex = false;
        printedObjects.forEach(function(obj, index){
            if(obj===value){
                printedObjIndex = index;
            }
        });

        if(printedObjIndex && value && typeof(value)=="object"){
            return "(see " + value.constructor.name.toLowerCase() + " with key " + printedObjectKeys[printedObjIndex] + ")";
        }else{
            var qualifiedKey = key || "(empty key)";
            printedObjects.push(value);
            printedObjectKeys.push(qualifiedKey);
            if(replacer){
                return replacer(key, value);
            }else{
                return value;
            }
        }
    }
    return JSON.stringify(obj, printOnceReplacer, indent);
}

var obj = {
    flatten_list: flatten_list,
    list2obj: list2obj,
    obj2list: obj2list,
    isNumeric: isNumeric,
    parseDateString: parseDateString,
    getStartEndDates: getStartEndDates,
    stringifyOnce: stringifyOnce
}

export default obj;
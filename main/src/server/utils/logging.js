import clc from 'cli-color'
import { sprintf } from 'sprintf-js'
import config_deployment from '@/config/deployment'

//

var flattenArgs = function(arg) {
    var n = arg.length;
    var dalist = [];
    for (var i=0;i<n;i++) {
        dalist.push(arg[i]);
    }
    var msg = dalist.join(', ');
    return msg;
}

/**
 * any outgoing request to external services
 */
var logEgress = function() {
    var msg = flattenArgs(arguments);
    console.log(clc.yellow(msg));
};

/**
 * any incoming expected response from external services
 */
var logIngress = function() {
    var msg = flattenArgs(arguments);
    console.log(clc.blue(msg));
};

/**
 * any async event from unexpected sources. (different from ingress)
 */
var logEvent = function() {
    var msg = flattenArgs(arguments);
    console.log(clc.cyan(msg));
};

/**
 * any internal state changes
 */
var logState = function() {
    var msg = flattenArgs(arguments);
    console.log(clc.magenta(msg));
};

var logError = function() {
    var msg = flattenArgs(arguments);
    console.log(clc.red(msg));
};

var logInfo = function() {
    var msg = flattenArgs(arguments);
    console.log(clc.white(msg));
};

var logDebug = function() {
    var msg = flattenArgs(arguments);
    console.log(clc.white(msg));
};

var logDonePromiseJoin = function(label, args) {
    if (config_deployment.debug_promise_join) {
        if (typeof (label) === 'undefined') {
            label = '';
        }
        console.log('[done] [Promise.join()] ' + label, args);
    }
}

var obj = {}
obj.logEgress = logEgress
obj.logIngress = logIngress
obj.logEvent = logEvent
obj.logState = logState
obj.logError = logError
obj.logInfo = logInfo
obj.logDebug = logDebug
obj.logDonePromiseJoin = logDonePromiseJoin
export default obj

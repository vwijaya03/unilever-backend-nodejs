import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import Promise from 'bluebird'
import Sequelize from 'sequelize'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { User } from '@/models/orm/index'
import { Retailer } from '@/models/orm/index'
import { BankAccount } from '@/models/orm/index'
import { orm } from '@/server/services/mariadb'
import base_repo from '@/server/repositories/base'

import service_logger from '@/server/services/logger'

import test_users from '@/test/data/users'
import properties from '@/properties'
import moment from 'moment'
import async from 'async';

const LOG_TAG = '[services:retailer] '
const log = service_logger.getLogger({name: 'service', service:'user'});

function postUploadRetailer({records=null}={}) {

    var array_args = [];
    var filtered_array_args = [];
    var array_map = [];
    var i = 0;

    records.splice(0, 1);
    //records.splice(records.length-2, records.length-1);
    async.eachSeries(records, function(eachRecord, callbackEach) {
        
        console.log(eachRecord);
        Retailer.create({le_code_dry: eachRecord[2], name: eachRecord[0], address: eachRecord[1]});
        setImmediate( () => {
            callbackEach();
        });

        i++;
    },function(errEach) {
        //console.log(errEach);
        // Done
    });

    // for (var i = 0; i < records.length; i++) {
    //     if(i != 0) {            
    //         array_args.push({le_code_dry: records[i][0], name: records[i][1], address: records[i][2] });
    //     }
    // }

    // console.log(array_args);

    // Promise.map(array_args, (data, index)  => {
    //     return data.le_code_dry;
    // })
    // .then(array_le_code => {
        
    //     return Retailer.findAll({
    //         attributes: ['le_code_dry'],
    //         where: {
    //             le_code_dry: array_le_code 
    //         }
    //     })
    //     .then(result => {
    //         // console.log('isi array le code');
    //         // console.log(array_le_code);
    //         for (var i = 0; i < array_args.length; i++) {
    //             if(result.length == 0) {
    //                 filtered_array_args.push({le_code_dry: array_args[i].le_code_dry, name: array_args[i].name, address: array_args[i].address });
    //             } else {
    //                 if(result[i] === undefined) {
    //                     filtered_array_args.push({le_code_dry: array_args[i].le_code_dry, name: array_args[i].name, address: array_args[i].address });
    //                 } else if(array_args[i].le_code_dry != result[i].le_code_dry) {
                        
    //                     filtered_array_args.push({le_code_dry: array_args[i].le_code_dry, name: array_args[i].name, address: array_args[i].address });
    //                 } else {

    //                 }
    //             }
    //         }

    //         if(filtered_array_args.length != 0) {
    //             Retailer.bulkCreate(filtered_array_args);
    //             records = null;
    //             array_args = null;
    //             filtered_array_args = null;

    //             return {code: 201, message: 'Upload berhasil.'};
    //         } else {
    //             return {code: 201, message: 'Upload berhasil.'};
    //         }
    //     });
    // });

    return {code: 201, message: 'Upload berhasil, data sedang di proses'};
}

function destroyBankAccount({user_id=null, id_bank_account=null}={}) {
    log.debug('[service-destroy-bank-account] get ', user_id);

    return BankAccount.destroy(
        { where: { id: id_bank_account, user_id: user_id }}
    )
    .then(bankaccount => {
        return {code: 201, message: 'Data berhasil dihapus.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

function updateBankAccount({user_id=null, id_bank_account=null, no_rekening=null, bank=null}={}) {
    log.debug('[service-update-bank-account] get ', user_id);

    return BankAccount.update(
        { no_rekening: no_rekening, bank: bank },
        { where: { id: id_bank_account, user_id: user_id }}
    )
    .then(bankaccount => {
        return {code: 201, message: 'Data berhasil diubah.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

function updateBankAccountAndroid({user_id=null, id_bank_account=null, no_rekening=null}={}) {
    log.debug('[service-update-bank-account-android] get ', user_id);

    return BankAccount.update(
        { no_rekening: no_rekening },
        { where: { id: id_bank_account, user_id: user_id }}
    )
    .then(bankaccount => {
        return {code: 201, message: 'Data berhasil diubah.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

function get({user_id=null}={}) {
    log.debug('[service-bank-account] get ', user_id);
    return Promise.resolve()
    .then(()=>{
        return BankAccount.findAll({
            attributes: ['id', 'user_id', 'no_rekening', 'bank', 'name'],
            include: [{
                attributes: ['id', 'password', 'username', 'fullname', 'email', 'accessTokenExpiry', 'valdoAccount', 'accessToken'],
                model: User,
                required: true
            }],
            where: {
                user_id: user_id
            }
        }); 
    })
    .then(result_set => {
        // console.log('############', filter, result_set);
        if (!result_set || Object.keys(result_set).length == 0) {
            // throw new UserNotFoundError();
            return {};   
        }

        return result_set;
    });
}

function get_bank_account_only({user_id=null}={}) {
    log.debug('[service-bank-account] get ', user_id);
    return Promise.resolve()
    .then(()=>{
        return BankAccount.findAll({
            attributes: ['id', 'user_id', 'no_rekening', 'bank', 'name'],
            where: {
                user_id: user_id
            }
        }); 
    })
    .then(result_set => {
        // console.log('############', filter, result_set);
        if (!result_set || Object.keys(result_set).length == 0) {
            // throw new UserNotFoundError();
            return {};   
        }

        return result_set;
    });
}

/**
@returns { Promise }
*/
function createBankAccount({user_id=null, name=null, no_rekening=null, bank=null}={}) 
{
    var args =
    {
        user_id: user_id,
        name: name,
        no_rekening: no_rekening,
        bank: bank
    };

    log.debug('execute create bank account record in db.', args);

    return BankAccount.create(args).then(bankaccount => {
        return bankaccount;
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

var obj = {}

obj.postUploadRetailer = postUploadRetailer
obj.get = get
obj.get_bank_account_only = get_bank_account_only
obj.createBankAccount = createBankAccount
obj.updateBankAccount = updateBankAccount
obj.updateBankAccountAndroid = updateBankAccountAndroid
obj.destroyBankAccount = destroyBankAccount

export default obj
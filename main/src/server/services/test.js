import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import crypto from 'crypto'
import Promise from 'bluebird'
import Sequelize from 'sequelize'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { Retailer } from '@/models/orm/index'
import { Invoice } from '@/models/orm/index'
import { User } from '@/models/orm/index'

import service_logger from '@/server/services/logger'
import base_repo from '@/server/repositories/base'
import test_users from '@/test/data/users'
import properties from '@/properties'
import moment from 'moment'

const log = service_logger.getLogger({name: 'service', service:'test'});


/**
Processes raw test data
@return {Promise}
*/
function init() {

    var attributes = [[Sequelize.literal('DISTINCT `outlet_code`'), 'outlet_code']];

    return base_repo.get({entity:Invoice, attributes:attributes}).then(result_obj => {
        let {result_list:result_list} = result_obj;
        return Promise.map(result_list, item => {
            var outletCode = item.dataValues['outlet_code'].trim();
            console.log('outletCode:', outletCode);
            if (! outletCode || outletCode.length == 0) {
                return Promise.resolve(false);
            }
            var name = 'Retailer-'+outletCode;
            var address = name + '-address';

            console.log('create retailer record for', outletCode);
            return createRetailer({outletCode:outletCode, name:name, address:address})
            .then(p_result => {
                return;
            })
            .catch(DbRecordCreationError, err => {
                console.log('err:', err);
            });
        });
    })
    .catch(err => {
        return Promise.resolve(false);
    });
}


/**
Make invoices to unpaid
@param {String} invoiceId - invoice id
@return {Promise}
*/
function reset_paid_invoices({invoiceId=null}={}) {

    var attributes = ['id', 'invoice_id', 'cash_memo_total_amount','invoice_payment_status_paid'];

    var where = {};
    if (invoiceId) {
        where.invoice_id = invoiceId;
    }

    return base_repo.get({entity:Invoice, where:where, attributes:attributes}).then(result_obj => {
        let {result_list:result_list} = result_obj;
        return Promise.each(result_list, item => {
            var totalAmount = Number(item['cash_memo_total_amount']);
            var isPaid = item['is_paid'];
            var isRefunded = item['is_refunded'];
            var id = item['id'];
            var invoiceId = item['invoice_id'];

            var high = 50;
            var low = 0;

            var ratio = 1 - (Math.random() * (high - low)) / 100;

            var updated_status_paid = Math.floor(totalAmount * ratio);

            if (isPaid && !isRefunded) {
                log.info('id', id, 'invoice-id:', invoiceId, 'isPaid?', isPaid, 'isRefunded?', isRefunded, 'ratio:', ratio, 'total_amount:', totalAmount, 'updated-status-paid:', updated_status_paid);
                return item.update({
                    invoice_payment_status_paid: updated_status_paid+''
                });
            }

            return Promise.resolve(totalAmount);

        }).then(p2_result => {
            log.info('done reset_paid_invoices !');
        });
    })
    .catch(err => {
        log.error('failed reset_paid_invoices !', err);
        return Promise.resolve(false);
    });
}

/**
@returns {Promise}
*/
function createRetailer({outletCode=null,name=null,address=null}={}) {

    var args = {
        outletCode: outletCode,
        name: name,
        address: address
    }

    console.log('execute create Retailer record in db.', args);

    return Retailer.create(args).then(entity => {
        return entity;
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        console.log('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

var obj = {}
obj.init = init
obj.reset_paid_invoices = reset_paid_invoices
export default obj


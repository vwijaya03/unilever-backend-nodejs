import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import Promise from 'bluebird'
import Sequelize from 'sequelize'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { User } from '@/models/orm/index'
import { BankAccount } from '@/models/orm/index'
import { Logs } from '@/models/orm/index'
import base_repo from '@/server/repositories/base'

import service_logger from '@/server/services/logger'

import test_users from '@/test/data/users'
import properties from '@/properties'
import moment from 'moment'

const LOG_TAG = '[services:user] '
const log = service_logger.getLogger({name: 'service', service:'user'});

/**
@param {Object} filter
@param {Object} options
@param {Boolean} options.throwIfEmpty - throw error if resultset is empty
@throws { UserNotFoundError } - if throwIfEmpty is true and empty result set.
@returns {Promise} - resolved to user object
*/

function destroyBankAccount({user_id=null, id_bank_account=null}={}) {
    log.debug('[service-destroy-bank-account] get ', user_id);

    return BankAccount.destroy(
        { where: { id: id_bank_account, user_id: user_id }}
    )
    .then(bankaccount => {
        return {code: 201, message: 'Data berhasil dihapus.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

function updateBankAccount({user_id=null, id_bank_account=null, no_rekening=null, bank=null}={}) {
    log.debug('[service-update-bank-account] get ', user_id);

    return BankAccount.update(
        { no_rekening: no_rekening, bank: bank },
        { where: { id: id_bank_account, user_id: user_id }}
    )
    .then(bankaccount => {
        return {code: 201, message: 'Data berhasil diubah.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

function updateBankAccountAndroid({user_id=null, id_bank_account=null, no_rekening=null}={}) {
    log.debug('[service-update-bank-account-android] get ', user_id);

    return BankAccount.update(
        { no_rekening: no_rekening },
        { where: { id: id_bank_account, user_id: user_id }}
    )
    .then(bankaccount => {
        return {code: 201, message: 'Data berhasil diubah.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

function get({user_id=null}={}) {
    log.debug('[service-bank-account] get ', user_id);
    return Promise.resolve()
    .then(()=>{
        return BankAccount.findAll({
            attributes: ['id', 'user_id', 'no_rekening', 'bank', 'name'],
            include: [{
                attributes: ['id', 'password', 'username', 'fullname', 'email', 'accessTokenExpiry', 'valdoAccount', 'accessToken'],
                model: User,
                required: true
            }],
            where: {
                user_id: user_id
            }
        }); 
    })
    .then(result_set => {
        // console.log('############', filter, result_set);
        if (!result_set || Object.keys(result_set).length == 0) {
            // throw new UserNotFoundError();
            return {};   
        }

        return result_set;
    });
}

function get_bank_account_only({user_id=null}={}) {
    log.debug('[service-bank-account] get ', user_id);
    return Promise.resolve()
    .then(()=>{
        return BankAccount.findAll({
            attributes: ['id', 'user_id', 'no_rekening', 'bank', 'name'],
            where: {
                user_id: user_id
            }
        }); 
    })
    .then(result_set => {
        // console.log('############', filter, result_set);
        if (!result_set || Object.keys(result_set).length == 0) {
            // throw new UserNotFoundError();
            return {};   
        }

        return result_set;
    });
}

/**
@returns { Promise }
*/
function createBankAccount({user_id=null, name=null, no_rekening=null, bank=null}={}) 
{
    var args =
    {
        user_id: user_id,
        name: name,
        no_rekening: no_rekening,
        bank: bank
    };

    log.debug('execute create bank account record in db.', args);

    return BankAccount.create(args).then(bankaccount => {
        return bankaccount;
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

var obj = {}

obj.get = get
obj.get_bank_account_only = get_bank_account_only
obj.createBankAccount = createBankAccount
obj.updateBankAccount = updateBankAccount
obj.updateBankAccountAndroid = updateBankAccountAndroid
obj.destroyBankAccount = destroyBankAccount

export default obj
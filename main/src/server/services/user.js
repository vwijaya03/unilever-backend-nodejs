import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import crypto from 'crypto'
import Promise from 'bluebird'
import Sequelize from 'sequelize'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { orm } from '@/server/services/mariadb'
import { User } from '@/models/orm/index'
import { Retailer } from '@/models/orm/index'
import { UserRetailerCode } from '@/models/orm/index'
import { BankAccount } from '@/models/orm/index'
import base_repo from '@/server/repositories/base'

import service_logger from '@/server/services/logger'

import test_users from '@/test/data/users'
import properties from '@/properties'
import moment from 'moment'

const LOG_TAG = '[services:user] '
const log = service_logger.getLogger({name: 'service', service:'user'});

function resetpin({pin=null, user_salt=null, user_id=null, email=null}={}) {
    
    return Promise.resolve()
    .then(response => {
        var hashedPin = getHashedPassword(user_salt, pin.toString());

        User.update(
            { pin: hashedPin },
            { where: { user_id: user_id }}
        );

        var transporter = NodeMailer.createTransport({
            host: 'mail.tokopandai.id',
            port: 465,
            tls: {
                rejectUnauthorized:false
            },
            secure: true, //true --> will use ssl
            auth: {
                user: 'cs@tokopandai.id',
                pass: 'h3d8d@EhaAAPx2y3qvyV'
            }
        });

        var mailOptions = {
            from: 'cs@tokopandai.id',
            to: email,
            subject: 'Konfirmasi Perubahan Pin',
            text: 'Ini adalah pin sementara anda: '+ pin,
            html: 'Ini adalah pin sementara anda: '+ pin
        };
        //console.log(mailOptions);
        transporter.sendMail(mailOptions, function(error, info) {
            console.log('masuk transporter');
            if (error) {
                console.log('masuk if error');
                console.log(error);
            } else {
                console.log('Message sent: ' + info.response);
                transporter.close();
            }
            
        });

        return {code: 201, message: 'Reset pin berhasil.'};    
    });
}

function resetpin_by_call_center({fullname=null, pob=null, dob=null}={}) {
    
    return Promise.resolve()
    .then(() => {
        return User.findOne({
            where: {
                fullname: fullname,
                tanggal_lahir: dob,
                tempat_lahir: pob
            },
        }); 
    })
    .then(response => {

        if(response != null)
        {
            //console.log('isi response');
            //console.log(response);
            console.log('isi response salt');
            console.log(response.salt);

            var new_pin = RandomString.generate({
                length: 6,
                charset: 'numeric'
            });

            var hashedPin = getHashedPassword(response.salt, new_pin);
            console.log('isi response email');
            console.log(response.email);
            User.update(
                { pin: hashedPin },
                { where: { email: response.email }}
            );

            var transporter = NodeMailer.createTransport({
                host: 'mail.tokopandai.id',
                port: 465,
                tls: {
                    rejectUnauthorized:false
                },
                secure: true, //true --> will use ssl
                auth: {
                    user: 'cs@tokopandai.id',
                    pass: 'h3d8d@EhaAAPx2y3qvyV'
                }
            });

            var mailOptions = {
                from: 'cs@tokopandai.id',
                to: response.email,
                subject: 'Konfirmasi Perubahan Pin',
                text: 'Ini adalah pin sementara anda: '+ new_pin,
                html: 'Ini adalah pin sementara anda: '+ new_pin
            };
            console.log(mailOptions);
            transporter.sendMail(mailOptions, function(error, info) {
                console.log('masuk transporter');
                if (error) {
                    console.log('masuk if error');
                    console.log(error);
                } else {
                    console.log('Message sent: ' + info.response);
                    transporter.close();
                }
                
            });

            return {code: 201, message: 'Reset pin berhasil.'};  
        }
        else
        {
            throw new UserNotFoundError();
        }
        
    });
}

function reset_password({email=null}={}) {

    const salt = crypto.randomBytes(64).toString('base64');
    var new_password = RandomString.generate(8);
    var hashedNewPassword = getHashedPassword(salt, new_password);

    return Promise.resolve()
    .then(() => {
        return User.findOne({
            where: {
                email: email,
            },
        }); 
    })
    .then(response => {

        if(response != null)
        {
            User.update(
                { password: hashedNewPassword, salt: salt, password_changed: 'true' },
                { where: { email: email }}
            );

            var transporter = NodeMailer.createTransport({
                host: 'mail.tokopandai.id',
                port: 465,
                tls: {
                    rejectUnauthorized:false
                },
                secure: true, //true --> will use ssl
                auth: {
                    user: 'cs@tokopandai.id',
                    pass: 'h3d8d@EhaAAPx2y3qvyV'
                }
            });

            var mailOptions = {
                from: 'cs@tokopandai.id',
                to: email,
                subject: 'Konfirmasi Perubahan Kata Sandi',
                text: 'Ini adalah password sementara anda: '+ new_password,
                html: 'Ini adalah password sementara anda: '+ new_password
            };
            // console.log(mailOptions);
            transporter.sendMail(mailOptions, function(error, info) {
                console.log('masuk transporter');
                if (error) {
                    console.log('masuk if error');
                    console.log(error);
                } else {
                    console.log('Message sent: ' + info.response);
                    transporter.close();
                }
                
            });

            return {code: 201, message: 'Reset password berhasil.'};
        }
        else
        {
            throw new UserNotFoundError();
        }
        
    });

    
    // return Promise.resolve()
    // .then(() => {
    //     return base_repo.getOne({entity:User, where:{username:username}, options:{} });
    // })
    // .then(entity => {
    //     log.debug('found entity: ', entity);
    //     if (entity) {

            
    //         return Promise.resolve(true);
    //     } 
    //     return Promise.resolve(false);
    // });
}

function reset_password_by_call_center({fullname=null, pob=null, dob=null}={}) {

    const salt = crypto.randomBytes(64).toString('base64');
    var new_password = RandomString.generate(8);
    var hashedNewPassword = getHashedPassword(salt, new_password);

    return Promise.resolve()
    .then(() => {
        return User.findOne({
            where: {
                fullname: fullname,
                tanggal_lahir: dob,
                tempat_lahir: pob
            },
        }); 
    })
    .then(response => {

        if(response != null)
        {
            User.update(
                { password: hashedNewPassword, salt: salt, password_changed: 'true' },
                { where: { email: response.email }}
            );

            var transporter = NodeMailer.createTransport({
                host: 'mail.tokopandai.id',
                port: 465,
                tls: {
                    rejectUnauthorized:false
                },
                secure: true, //true --> will use ssl
                auth: {
                    user: 'cs@tokopandai.id',
                    pass: 'h3d8d@EhaAAPx2y3qvyV'
                }
            });

            var mailOptions = {
                from: 'cs@tokopandai.id',
                to: response.email,
                subject: 'Konfirmasi Perubahan Kata Sandi',
                text: 'Ini adalah password sementara anda: '+ new_password,
                html: 'Ini adalah password sementara anda: '+ new_password
            };
            console.log(mailOptions);
            transporter.sendMail(mailOptions, function(error, info) {
                console.log('masuk transporter');
                if (error) {
                    console.log('masuk if error');
                    console.log(error);
                } else {
                    console.log('Message sent: ' + info.response);
                    transporter.close();
                }
                
            });

            return {code: 201, message: 'Reset password berhasil.'};
        }
        else
        {
            throw new UserNotFoundError();
        }
        
    });
}

function getOne(filter, options) {
    return get(filter, options).then(result_set => {
        // console.log('get-one result-set:', result_set);
        if (_.isEmpty(result_set)) {
            return null;
        } else {
            var k = Object.keys(result_set);
            return result_set[k[0]];
        }
    });
}

/**
@param {Object} filter
@param {Object} options
@param {Boolean} options.throwIfEmpty - throw error if resultset is empty
@throws { UserNotFoundError } - if throwIfEmpty is true and empty result set.
@returns {Promise} - resolved to user object
*/
function get(filter, options) {
    options = options || {};
    log.debug('[service-user] get ', filter);
    return Promise.resolve()
    .then(()=>{
        // var users = _.find(test_users, filter);
        if (filter) {
            return User.findAll(
                {
                    where: filter
                }
            );
        } else {
            return User.findAll();
        }
    })
    .then(result_set => {
        // console.log('############', filter, result_set);
        if (!result_set || Object.keys(result_set).length == 0) {
            if (options.throwIfEmpty) {
                throw new UserNotFoundError();
            }
        }
        return Object.assign({}, result_set);
    });
}

/**
@param {String} username
@returns {Promise} - resolved to a boolean
*/
function isExist(email) {
    log.debug('isExist. ', email);
    return Promise.resolve()
    .then(() => {
        return base_repo.getOne({entity:User, where:{email:email}, options:{} });
    })
    .then(entity => {
        log.debug('found entity: ', entity);
        if (entity) return Promise.resolve(true);
        return Promise.resolve(false);
    });
}

/**
@param {Object}
@param {Object.args} - args
@param {Object.args.user} - {User} object
@returns {Promise} - resolved to {String} - token
*/
function renewAccessToken({user=null}={}) {
    log.debug('renewing access token for user-id:', user.id);
    const secret = properties.server_secret;

    return Promise.resolve()
    .then(()=>{
        const user_salt = user.salt.toString('utf8');
        const time_millis = moment.utc().unix() * 1000;
        const token = crypto.createHmac('sha256', user_salt).update(''+time_millis).digest('hex');
        return token;
    })
    .then(p_result => {
        var token = p_result;
        const expiry = moment.utc().add(30, 'days');
        const expiryUnixTsMillis = expiry.unix() * 1000;

        return User.update({
            accessToken: token,
            accessTokenExpiry: expiryUnixTsMillis
        }, {where:{id:user.id}}).then(p_result => {
            log.info(sprintf('user-id %1$s access token has been updated in db.', user.id));
            return token;
        });
    });
}

function getHashedPassword(salt, rawPassword) {
    var hasher = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hasher.update(rawPassword);
    var value = hasher.digest('hex');
    return value;
}

function getHashedPin(salt, rawPassword) {
    var hasher = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hasher.update(rawPassword);
    var value = hasher.digest('hex');
    return value;
}

function changepassword({user_id=null, user_salt=null, newpassword=null}={}) {
    console.log('masus service changepassword');
    var hashedNewPassword = getHashedPassword(user_salt, newpassword);
    return User.update(
        { password: hashedNewPassword, password_changed: 'false' },
        { where: { user_id: user_id }}
    )
    .then(bankaccount => {
        return {code: 201, message: 'Data berhasil diubah.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

function changepin({user_id=null, user_salt=null, pin=null}={}) {
    var hashedPin = getHashedPassword(user_salt, pin.toString());

    return User.update(
        { pin: hashedPin },
        { where: { user_id: user_id }}
    )
    .then(response => {
        return {code: 201, message: 'Pin berhasil diubah.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}
/**
@returns { Promise }
*/
function checkVANumber({code_role=null,dry_code=null,sequential_code=null,code=null,imei=null}={}) {
    // return Promise.resolve()
    // .then(() => {
    //     return User.findAndCountAll({
    //         where: {
    //             valdo_account: {
    //               $like: '%'+code
    //             }
    //         },
    //     }); 
    // })
    // .then(function(result) {
    //     console.log('Isi Result');
    //     console.log(result.count);
    //     return result.count;
    // });
    let data = [];

    return orm.query('SELECT MAX(user_id) as count_user_id FROM users', { type: orm.QueryTypes.SELECT})
    .then(function(count_user_id){

        data[0] = count_user_id[0];

        if(imei != null || imei != '') {

            return orm.query('SELECT COUNT(user_id) as count_imei FROM users where imei = '+imei, { type: orm.QueryTypes.SELECT})
            .then(function(count_imei){
                data[1] = count_imei[0];
                return data;
            });
        } else {
            data[1] = '';
            return data;
        }
    });
}

function getOneUser({vaccount=null}={}) {
    return Promise.resolve()
    .then(() => {
        return User.findOne({
            attributes: ['fullname', 'valdo_account', 'fcm_id'],
            where: {
                valdo_account: vaccount
            },
        }); 
    })
    .then(function(result) {
        return result;
    });
}

function getOneRetailerName({vaccount=null}={}) {
    return Promise.resolve()
    .then(() => {
        return User.findOne({
            attributes: ['retailer_id'],
            where: {
                valdo_account: vaccount
            },
        }); 
    })
    .then(function(user) {
        
        return Retailer.findOne({
            attributes: ['name'],
            where: {
                retailer_id: user.dataValues.retailer_id
            },
        }); 
    });
}

function getOneUserByCallCenter({fullname=null, pob=null, dob=null}={}) {
    return Promise.resolve()
    .then(() => {
        return User.findOne({
            where: {
                fullname: fullname,
                tanggal_lahir: dob,
                tempat_lahir: pob
            }
        }); 
    })
    .then(function(result) {
        return result;
    });
}

function getOneCustomerByCallCenter({fullname=null, pob=null, dob=null}={}) {
    return Promise.resolve()
    .then(() => {
        return orm.query('select fullname, nama_toko, phone, tanggal_lahir, tempat_lahir from users where fullname COLLATE utf8_general_ci = "'+fullname+'" and tempat_lahir COLLATE utf8_general_ci = "'+pob+'" and tanggal_lahir = "'+dob+'"', { type: orm.QueryTypes.SELECT})
        .then(function(data){
            console.log('isi data');
            console.log(data);
            return data[0];
        });
    })
    .then(function(result) {
        return result;
    });
}

function createUserRetailerCode(
{
    user_id=null,
    outlet_code=null,
    dt_code=null,
    valdo_account=null
}={}) {
    var args =
    {
        user_id: user_id,
        outlet_code: outlet_code,
        dt_code: dt_code,
        valdo_account: valdo_account
    }

    log.debug('execute create User retailer code record in db.', args);

    return UserRetailerCode.create(args).then(user => {
        return user;
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
    
}

function createUser(
{
    username=null,
    fullname=null,
    nama_toko=null,
    password=null,
    password_changed=null,
    email=null,
    pin=null,
    type=null,
    distributor_code=null,
    valdoAccount=null,
    no_rekening=null,
    retailerId=null,
    distributorId=null,
    pickupId=null,
    salesId=null,
    reference=null,
    phone=null,
    dob=null,
    pob=null,
    ktp=null,
    onbehalf=null,
    code_role=null,
    dry_code=null,
    sequential_code=null,
    code=null,
    imei=null
}={}) {

    // generate secret salt
    const salt = crypto.randomBytes(64).toString('base64');
    // generate access token and expiry
    const accessToken = crypto.randomBytes(64).toString('base64');
    const accessTokenExpiry = moment().add(30, 'days').unix(); //unix timestamp is in seconds

    // generate hashed password
    var hashedPassword = getHashedPassword(salt, password);
    log.debug('hashed-password:', hashedPassword);
    var hashedPin = getHashedPin(salt, pin);
    log.debug('hashed-pin:', hashedPin);

    var args =
    {
    fullname:            fullname,
    nama_toko:           nama_toko,
    password:            hashedPassword,
    password_changed:    password_changed,
    email:               email || '',
    pin:                 hashedPin,
    type:                type,
    salt:                salt,
    valdoAccount:        valdoAccount,
    accessToken:         accessToken,
    accessTokenExpiry:   accessTokenExpiry,
    retailerId:          retailerId,
    distributorId:       distributorId,
    pickup_id:           pickupId,
    salesId:             salesId,
    reference:           reference,
    phone:               phone,
    tanggal_lahir:       dob,
    tempat_lahir:        pob,
    ktp:                 ktp,
    onbehalf:            onbehalf,
    imei:                imei
    }

    log.debug('execute create User record in db.', args);

    if(type == 1)
    {
        return Promise.resolve()
        .then(() => {
            return orm.query('select users.valdo_account from users inner join distributors on users.distributor_id = distributors.distributor_id where users.type = 1 and distributors.dt_code = '+distributor_code, { type: orm.QueryTypes.SELECT}); 
        })
        .then(data => {
            //console.log('isi data');
            //console.log(data.length);
            if(data.length >= 1)
            {
                throw new Error('1 Kode distributor hanya untuk 1 distributor.');
            }
            else
            {                
                return User.create(args).then(user => {
                    if(no_rekening != null)
                    {
                        var args =
                        {
                            user_id: user.id,
                            no_rekening: no_rekening
                        };
                        BankAccount.create(args);
                    }
                    
                    return user;
                })
                .catch(Sequelize.UniqueConstraintError, err => {
                    var err_message = 'unique constraint error';
                    try {
                        err_message = err.errors[0].message;
                    } catch (err2) {
                    }
                    log.error('unique constraint error:', err_message);
                    throw new DbRecordCreationError(err_message);
                });
            }
        });
    }
    else
    {
        // console.log('isi args retailer');
        // console.log(args);
        return User.create(args).then(user => {
            if(no_rekening != null)
            {
                var args =
                {
                    user_id: user.id,
                    no_rekening: no_rekening
                };
                BankAccount.create(args);
            }
            
            return user;
        })
        .catch(Sequelize.UniqueConstraintError, err => {
            var err_message = 'unique constraint error';
            try {
                err_message = err.errors[0].message;
            } catch (err2) {
            }
            log.error('unique constraint error:', err_message);
            throw new DbRecordCreationError(err_message);
        });
    }
    
}

function createDistributorManager(
{
    username=null,
    fullname=null,
    nama_toko=null,
    password=null,
    password_changed=null,
    email=null,
    pin=null,
    type=null,
    distributor_code=null,
    valdoAccount=null,
    no_rekening=null,
    retailerId=null,
    distributorId=null,
    pickupId=null,
    salesId=null,
    reference=null,
    phone=null,
    dob=null,
    pob=null,
    ktp=null,
    onbehalf=null,
    code_role=null,
    dry_code=null,
    sequential_code=null,
    code=null,
    imei=null
}={}) {

    // generate secret salt
    const salt = crypto.randomBytes(64).toString('base64');
    // generate access token and expiry
    const accessToken = crypto.randomBytes(64).toString('base64');
    const accessTokenExpiry = moment().add(30, 'days').unix(); //unix timestamp is in seconds

    // generate hashed password
    var hashedPassword = getHashedPassword(salt, password);
    log.debug('hashed-password:', hashedPassword);
    var hashedPin = getHashedPin(salt, pin);
    log.debug('hashed-pin:', hashedPin);

    return Promise.resolve()
    .then(() => {
        return orm.query('select users.valdo_account from users inner join distributors on users.distributor_id = distributors.distributor_id where users.type = 1 and distributors.dt_code = '+distributor_code, { type: orm.QueryTypes.SELECT}); 
    })
    .then(data => {
        if(data.length > 0)
        {
            // console.log('masuk if');
            // console.log(response);
            return Promise.resolve()
            .then(() => {
                return User.findAll({
                    where: {
                        valdo_account: data[0].valdo_account,
                        $or: [
                            {
                                type: 1 
                            },
                            {
                                type: 3
                            }
                        ]
                    },
                }); 
            }) 
            .then(response => {
                if(Object.keys(response).length >= 2)
                {
                    return {code: 400, message: '1 VA hanya boleh 2 user.'}; 
                }
                else
                {
                    var args =
                    {
                    fullname:            fullname,
                    nama_toko:           nama_toko,
                    password:            hashedPassword,
                    password_changed:    password_changed,
                    email:               email || '',
                    pin:                 hashedPin,
                    type:                type,
                    salt:                salt,
                    valdoAccount:        data[0].valdo_account,
                    accessToken:         accessToken,
                    accessTokenExpiry:   accessTokenExpiry,
                    retailerId:          retailerId,
                    distributorId:       distributorId,
                    pickup_id:           pickupId,
                    salesId:             salesId,
                    reference:           reference,
                    phone:               phone,
                    tanggal_lahir:       dob,
                    tempat_lahir:        pob,
                    ktp:                 ktp,
                    onbehalf:            onbehalf,
                    imei:                imei
                    }

                    log.debug('execute create User record in db.', args);

                    return User.create(args).then(user => {
                        if(no_rekening != null)
                        {
                            var args =
                            {
                                user_id: user.id,
                                no_rekening: no_rekening
                            };
                            BankAccount.create(args);
                        }
                        
                        return user;
                    })
                    .catch(Sequelize.UniqueConstraintError, err => {
                        var err_message = 'unique constraint error';
                        try {
                            err_message = err.errors[0].message;
                        } catch (err2) {
                        }
                        log.error('unique constraint error:', err_message);
                        throw new DbRecordCreationError(err_message);
                    });
                }
            });
        }
        else
        {
            return {code: 400, message: 'Data distributor tidak ada, buat distributor terlebih dahulu.'};
        }
    });

    
}
/**
resolve retailer and distributor objects for user.
@param {User} user - user
@returns {Promise} - resolved value to same user argument.
*/
function resolveRetailerAndDistributor({user=null}={}) {

    log.debug('resolveRetailerAndDistributor ', user);

    if (! user) {
        return Promise.resolve({user:user});
    }

    const fnGetRetailer = user.getRetailer;
    const fnGetDistributor = user.getDistributor;

    return Promise.join(
        user.getRetailer(),
        user.getDistributor()
    )
    .then(p_result => {
        console.log('isi p result resolve');
        console.log(p_result);
        const [retailer, distributor] = p_result;

        if (retailer) {
            user.retailer = retailer;
        }
        if (distributor) {
            user.distributor = distributor;
        }
        if(user.sales_id != null)
        {
            return orm.query('select sales_id, sales_code from sales where sales_id = '+user.sales_id, { type: orm.QueryTypes.SELECT})
            .then(function(sales){
                user.sales = sales[0];
                return {user:user};
            }); 
        }
        if(user.pickup_id != null)
        {
            return orm.query('select id, pickup_code from pickup where id = '+user.pickup_id, { type: orm.QueryTypes.SELECT})
            .then(function(pickup){
                console.log('isi pickup');
                console.log(pickup[0]);
                user.pickup = pickup[0];
                return {user:user};
            }); 
        }
        return {user:user};
    })
    .catch(err => {
        log.error(LOG_TAG + '[error] failed to resolve retailer and distributor from user.', err);
        throw err; // bubble-up the exception
    });
}

function registerFcm({user_id=null, fcm_id=null}={}) {
    console.log("user_id : ", user_id)
    console.log("fcm_id : ", fcm_id)

    return User.update(
        { fcm_id: fcm_id },
        { where: { user_id: user_id }}
    )
    .then(response => {
        return {code: 201, message: 'Fcm id berhasil diubah.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

var obj = {}

obj.get = get
obj.getOne = getOne
obj.getOneUserByCallCenter = getOneUserByCallCenter
obj.getOneCustomerByCallCenter = getOneCustomerByCallCenter
obj.isExist = isExist
obj.createUser = createUser
obj.createUserRetailerCode = createUserRetailerCode
obj.createDistributorManager = createDistributorManager
obj.getHashedPassword = getHashedPassword
obj.getHashedPin = getHashedPin
obj.renewAccessToken = renewAccessToken
obj.resolveRetailerAndDistributor = resolveRetailerAndDistributor;
obj.changepassword = changepassword;
obj.reset_password = reset_password;
obj.reset_password_by_call_center = reset_password_by_call_center;
obj.resetpin = resetpin;
obj.resetpin_by_call_center = resetpin_by_call_center;
obj.changepin = changepin;
obj.checkVANumber = checkVANumber;
obj.getOneUser = getOneUser;
obj.getOneRetailerName = getOneRetailerName;
obj.registerFcm = registerFcm;

export default obj


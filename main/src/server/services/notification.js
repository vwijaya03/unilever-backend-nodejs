import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import crypto from 'crypto'
import Promise from 'bluebird'
import Sequelize from 'sequelize'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { orm } from '@/server/services/mariadb'
import { Notification } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import base_repo from '@/server/repositories/base'

import service_logger from '@/server/services/logger'

import test_users from '@/test/data/users'
import properties from '@/properties'
import moment from 'moment'

const log = service_logger.getLogger({name: 'service', service:'notifikasi'});

function service(){
    this.get = function(filter, options){
        options = options || {};
        /*log.debug('[service-informasi-dan-promosi] get ', filter);*/
        return Promise.resolve()
        .then(()=>{
            // var users = _.find(test_users, filter);
            if (filter) {
                return Notification.findAll(
                    {
                        where: filter
                    }
                );
            } else {
                return Notification.findAll();
            }
        })
        .then(result_set => {
            // console.log('############', filter, result_set);
            if (!result_set || Object.keys(result_set).length == 0) {
                if (options.throwIfEmpty) {
                    throw new UserNotFoundError();
                }
            }
            return result_set;
        });
    }

    this.addNotification = function(
        {
            to=null,
            type=null,
            message=null,
            state=null,
            createdAt=null,
            updatedAt=null
        }={}) {
            var args =
            {
            to:            to,
            type:          type,
            message:       message,
            state:         state, 
            createdAt:     createdAt,
            updatedAt:     updatedAt
        }

        log.debug('execute create notifikasi record in db.', args);
    
        return Notification.create(args).then(notification => {
            return notification;
        })
        .catch(Sequelize.UniqueConstraintError, err => {
            var err_message = 'unique constraint error';
            try {
                err_message = err.errors[0].message;
            } catch (err2) {
            }
            log.error('unique constraint error:', err_message);
            throw new DbRecordCreationError(err_message);
        });
    }
}
export default service
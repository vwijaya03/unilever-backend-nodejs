import bunyan from 'bunyan'
import util from 'util'
import _ from 'lodash'

function getLogger({name=null, service=''}={}) {
    const tag = [].concat(name, service).filter((item) => {
        return ! _.isUndefined(item) && ! _.isNull(item) && item.trim() !== '';
    }).join('/');

    var log = bunyan.createLogger({
        name: tag,
        streams:[
            {
                level: 'debug',
                stream: process.stdout
            },
            {
                // type: 'rotating-file',
                // period: '1d',
                level: 'debug',
                path: '/var/log/itp-unilever.json'  // log ERROR and above to a file
            }
        ]
    });
    return log;
}

var obj = {}
obj.getLogger = getLogger
export default obj
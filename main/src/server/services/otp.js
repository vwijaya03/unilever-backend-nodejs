import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import crypto from 'crypto'
import Promise from 'bluebird'
import Sequelize from 'sequelize'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { orm } from '@/server/services/mariadb'
import { Otp } from '@/models/orm/index'
import base_repo from '@/server/repositories/base'

import service_logger from '@/server/services/logger'

import service_notification from '@/server/services/notification'

import test_users from '@/test/data/users'
import properties from '@/properties'
import moment from 'moment'
import notp from 'notp'

const log = service_logger.getLogger({name: 'service', service:'otp'});

function service(){
    this.secret_key = 'xrj-0nh#lb1fsw7p6k!)vdn36^ywbuzu)7h@rl6i-+f*@5%!ct';

    this.addOtp = function({
            ipaddress=null,
            type=null,
            token_user=null,
            to=null,
            createdAt=null,
            updatedAt=null
        } = {}) {
        // Search if there are any OTP already generated for same IP Address for last 5 minutes?
        return Otp.findOne({ 
            where: { 
                ipaddress: ipaddress, 
                valid: true, 
                createdAt: {
                    $between: [Sequelize.fn('DATE_SUB', Sequelize.fn('NOW'), Sequelize.literal('INTERVAL 5 MINUTE')), Sequelize.fn('NOW')]
                }
            }
            // ,logging: console.log 
        }).then(otp=>{
            if (!otp) {
                var secret_key = (new Date()).getTime().toString() + token_user;
                var otp = notp.totp.gen(secret_key, {});

                var args = {
                    ipaddress:      ipaddress,
                    otp:            otp,
                    secret_key:     secret_key,
                    token_user:     token_user,
                    valid:          true,
                    createdAt:      createdAt,
                    updatedAt:      updatedAt
                }

                var notification = new service_notification();
                return Otp.create(args).then(otp => {    
                    var message = { title:'OTP', body:"TokoPandai. OTP anda adalah " + otp.otp + " Berlaku untuk 5 menit"  }
                    var notification_args = {
                        to: to,
                        type: type,
                        message: JSON.stringify(message),
                        state: 0,
                        createdAt: createdAt,
                        updatedAt: updatedAt
                    }
                    return notification.addNotification(notification_args).then(()=>{
                       return Promise.resolve(otp);
                    });
                })
                .catch(Sequelize.UniqueConstraintError, err => {
                    var err_message = 'unique constraint error';
                    try {
                        err_message = err.errors[0].message;
                    } catch (err2) {
                    }
                    log.error('unique constraint error:', err_message);
                    throw new DbRecordCreationError(err_message);
                });
            } else {
                return Promise.resolve(otp);
            }
        })
    }

    this.verifyOtp = function(token, otp) {
        var tokenQuery = "";
        var replacement = { 
            otp: otp,
            valid: true
        }
        if (token) {
            tokenQuery = ' AND token_user = :token_user';
            replacement.token_user = token; 
        }
        /*return orm.query('SELECT * FROM otps WHERE otp = :otp AND valid = :valid ' . tokenQuery, { replacements: replacement, type: orm.QueryTypes.SELECT })*/
        return Otp.findOne({ where: replacement })
        .then(function(response){
            if (response == null || response == undefined) {
                return Promise.resolve([]);
            }
            var valid = true;
            var verified = notp.totp.verify(response.otp, response.secret_key)
            if(!verified) {
                valid = false;
            } else {
                if (verified.delta < -10) {
                    valid = false;
                }
            }
            return Otp.update({ valid: 0 }, { where: replacement}).then(()=>{
                if (valid) {
                    response.valid = false;
                    return Promise.resolve([response]);
                } else {
                    return Promise.resolve([]);
                }
            });
        });
    }
}
export default service
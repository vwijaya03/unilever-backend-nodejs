import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import crypto from 'crypto'
import Promise from 'bluebird'
import Sequelize from 'sequelize'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { orm } from '@/server/services/mariadb'
import { MobileVersion } from '@/models/orm/index'
import base_repo from '@/server/repositories/base'

import service_logger from '@/server/services/logger'

import service_notification from '@/server/services/notification'

import test_users from '@/test/data/users'
import properties from '@/properties'
import moment from 'moment'

const log = service_logger.getLogger({name: 'service', service:'mobile_version'});

function service(){
    this.get = function(filter, options){
        options = options || {};
        /*log.debug('[service-informasi-dan-promosi] get ', filter);*/
        return Promise.resolve()
        .then(()=>{
            // var users = _.find(test_users, filter);
            if (filter) {
                return MobileVersion.findAll(
                    {
                        where: filter
                    }
                );
            } else {
                /*return MobileVersion.findAll();*/
                return MobileVersion.findAll({sort: { 'createdAt' : -1 } });
            }
        })
        .then(result_set => {
            // console.log('############', filter, result_set);
            if (!result_set || Object.keys(result_set).length == 0) {
                if (options.throwIfEmpty) {
                    throw new UserNotFoundError();
                }
            }
            return result_set;
        });
    }

    this.getOne = function() {
        return orm.query('SELECT * FROM mobile_versions WHERE createdAt = (SELECT MAX(createdAt) FROM mobile_versions)')
        .then(function(response){
            if (response == null || response == undefined) {
                return Promise.resolve([]);
            }
            return Promise.resolve(response[0]);
        });
    }

    this.addMobileVersion = function(
        {
            version=null,
            download_link=null,
            force=null,
            update_description=null,
            createdAt=null,
            updatedAt=null
        }={}) {
            var args =
            {
            version:            version, 
            download_link:      download_link,
            force:              force,
            update_description: update_description,
            createdAt:          createdAt,
            updatedAt:          updatedAt
        }

        log.debug('execute create mobile version record in db.', args);
        var notification = new service_notification();
        //Insert to table mobile_versions & notifications
        return MobileVersion.create(args).then(mobileversion => {
            var message = { title:'Versi Baru Tersedia', message:"Version: " + version + ", Update description: "+ update_description }
            var notification_args = {
                to: '/topics/mobile-version',
                type: 'data',
                message: JSON.stringify(message),
                state: 0,
                createdAt: createdAt,
                updatedAt: updatedAt
            }
            return notification.addNotification(notification_args).then(()=>{
                return Promise.resolve(mobileversion);
            });
        })
        .catch(Sequelize.UniqueConstraintError, err => {
            var err_message = 'unique constraint error';
            try {
                err_message = err.errors[0].message;
            } catch (err2) {
            }
            log.error('unique constraint error:', err_message);
            throw new DbRecordCreationError(err_message);
        });

    }
}
export default service
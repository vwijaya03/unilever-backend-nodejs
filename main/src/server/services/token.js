import util from 'util'
import Promise from 'bluebird'
import _ from 'lodash'

import { ApiTokenInvalidError } from '@/server/error/Error'

var obj = {};

/**
@param {String} user_id - the user id
@param {String} token - api token to check
@returns {Promise}
*/
obj.checkToken = function(token) {
    return new Promise(function(resolve,reject){
        let msg = util.format('check-token. token:%s', token);
        console.log(msg);
        var user = service_user.find({token:token});
        console.log('check-token. found user:', user);
        if (user) {
            resolve(user);
        } else {
            throw new ApiTokenInvalidError();
        }
    });
}


/**
@param {String} user_id - the user id
@param {String} token - api token to check
@returns {Promise}
*/
obj.checkToken = function(user_id, token) {
    return new Promise(function(resolve,reject){
        let msg = util.format('check-token for user:%s token:%s', user_id, token);
        console.log(msg);
        var user = service_user.find({user_id:user_id, token:token});
        // console.log('check-token. found user:', user);
        if (user) {
            resolve(user);
        } else {
            throw new ApiTokenInvalidError();
        }
    });
}

/**
@param {String} user_id - user id
@returns {String} token
*/
obj.generateToken = function(user_id) {
    let msg = util.format('generateToken for user:%s', user_id);
    console.log(msg);
    return user_id + 'token';
}

export default obj
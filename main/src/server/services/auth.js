import _ from 'lodash'
import Principal from '@/models/principal'
import service_user from '@/server/services/user'
import properties from '@/properties'
import { UserNotFoundError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { User } from '@/models/orm/index'
import usertypes from '@/models/usertypes'

import service_logger from '@/server/services/logger'

import { ACCESS_TOKEN_INTERNAL_DEV } from '@/properties'
import { allowed_user_status } from '@/properties'

import ACL from '@/models/acl'

import { RETAILER_CODE } from '@/models/usertypes'
import { DISTRIBUTOR_CODE } from '@/models/usertypes'
import { DISTRIBUTOR_MANAGER_CODE } from '@/models/usertypes'
import { SALES_CODE } from '@/models/usertypes'
import { SUPER_ADMIN_CODE } from '@/models/usertypes'
import { UNILEVER_ADMIN_CODE } from '@/models/usertypes'
import { PICKUP_AGENT_CODE } from '@/models/usertypes'
import { CALL_CENTER_CODE } from '@/models/usertypes'

const log = service_logger.getLogger({name: 'service', service:'auth'});
const DEBUG_LOG = false;

var internal_users = [
    properties.internal_webadmin_access_token,
    properties.internal_dev_access_token
];

/**
Returns a principal from access token
@param {String} token - the access token
@return {Promise} - resolved to a {Principal}
@throws UnauthorizedRequestError - if unauthorized
*/
function getPrincipal(token) {

    if (internal_users && internal_users.indexOf(token) >= 0) {
        let principal = new Principal();
        principal.setAccessToken(token);
        principal.roles = ['internal'];
        return Promise.resolve(principal);
    }

    return Promise.resolve()
    .then(() => {
        return service_user.getOne( {accessToken:token, $or: allowed_user_status}, {throwIfEmpty:true} );
    })
    .then(user => {
        if(user == null)
        {
            throw new UnauthorizedRequestError();
        }

        if (DEBUG_LOG) log.debug('[auth] getPrincipal user:', user);

        var principal = new Principal();
        var roles = [];

        if (user) {
            principal.setUser(user);
            roles = _.union(roles, generateRoles({user:user}));

            if (user.type == DISTRIBUTOR_CODE || user.type == DISTRIBUTOR_MANAGER_CODE) {
                if (user.distributorId) {
                    principal.setDistributorId(user.distributorId);
                }
            }
            else if (user.type == RETAILER_CODE) {
                if (user.retailerId) {
                    principal.setRetailerId(user.retailerId);
                }
            }
        }

        principal.setAccessToken(token);
        principal.setRoles(roles);
        return principal;
    })
    .catch(UserNotFoundError, err => {
        var principal = new Principal();
        principal.setAccessToken(token);
        return principal;
    })
    .catch(UnauthorizedRequestError, err => {
        var principal = new Principal();
        principal.setAccessToken(token);
        return principal;
    });

}

/**
Generates roles for principal
@param {User} user - User object
@param {Integer} usertype - user type
@returns {Array.<String>} list of roles.
*/
function generateRoles({user=null, usertype=null}) {
    if (user) {
        var value = usertypes.getName(user.type);
        return [value];
    } else if (! _.isNull(usertype)) {
        if (! _.isNumber(usertype)) {
            return [];
        }
        var value = usertypes.getName(usertype);
        return [value];
    }
    return [];
}

/**
@param {Number} expirySeconds
@throws AccessTokenExpiredError - if token is expired or expirySeconds argument is null
*/
function checkTokenExpiry({expirySeconds=null}={}) {
    if (!expirySeconds) {
        throw new AccessTokenExpiredError();
    }
    const nowSeconds = new Date().getTime() / 1000;
    log.debug('access token exp: ', 'now:', nowSeconds, 'expiry', expirySeconds);
    if (nowSeconds > expirySeconds) {
        throw new AccessTokenExpiredError();
    }
}

/**
@param {String} token - the api token
@param {Object} acl - the acl descriptor
@returns {Promise} - resolved value : [Principal, allowed:boolean]
@throws UnauthorizedRequestError - if not authorized
@throws AccessTokenExpiredError - if token has expired
*/
function checkAuth({token=null, acl=null, resource=null} = {}) {

    return Promise.resolve()
    .then(()=>{
        return getPrincipal(token);
    })
    .then((principal)=>{
        
        if (DEBUG_LOG) log.debug('principal:', principal);
        if (!principal) {
            if (DEBUG_LOG) log.debug('[check-auth] no principal for token:', token);
            throw new UnauthorizedRequestError();
        }
        // if(principal.user == null) 
        // {
        //     throw new UnauthorizedRequestError();
        // }
        
        if (!acl) {
            // WARN no acl
            if (DEBUG_LOG) log.warn('[check-auth] no acl.');
            return [principal, true];
        }

        var is_check_token_expiry = !(principal.hasRoles(['internal'])) && principal.user;

        if (is_check_token_expiry) {
            checkTokenExpiry({expirySeconds:principal.user.accessTokenExpiry});
        }

        // apply ACL
        const is_allowed = acl.isAllowed(principal, resource);
        if (! is_allowed) {
            throw new UnauthorizedRequestError();
        }
        log.debug('checkAuth done');
        return [principal, true];
    });
}

var canRegisterUserType = (()=>{

    const rx = {};
    rx[CALL_CENTER_CODE] = [{token:'all'}];
    rx[RETAILER_CODE] = [{token:'all'}];
    rx[DISTRIBUTOR_CODE] = [{token:ACCESS_TOKEN_INTERNAL_DEV}, {usertype:SUPER_ADMIN_CODE}];
    rx[DISTRIBUTOR_MANAGER_CODE] = [{token:'all'}, {usertype:DISTRIBUTOR_MANAGER_CODE}];
    rx[SALES_CODE] = [{token:'all'}];
    rx[PICKUP_AGENT_CODE] = [{token:'all'}];
    rx[SUPER_ADMIN_CODE] = [{token:ACCESS_TOKEN_INTERNAL_DEV}, {usertype:SUPER_ADMIN_CODE}];
    rx[UNILEVER_ADMIN_CODE] = [{token:ACCESS_TOKEN_INTERNAL_DEV}, {usertype:SUPER_ADMIN_CODE}];

    var data = {};
    Object.keys(rx).forEach(key => {
        var descriptors_list = rx[key];
        var acl = new ACL();
        descriptors_list.map(descriptor => {
            if (descriptor.usertype) {
                var usertype = parseInt(descriptor.usertype);
                var roles = generateRoles({usertype:usertype});
                acl.roles(roles);
            }
            if (descriptor.token) {
                var token = descriptor.token;
                if (! _.isNull(token) && ! _.isUndefined(token)) {
                    acl.tokens(token);
                }
            }
        });
        data[key] = acl;
    });
    var obj = {};
    obj.check = function({principal=null, usertype=null}) {
        log.debug('check-can-register-user', principal, usertype);
        var acl = data[usertype+''];
        var is_authorized = acl.isAllowed(principal);
        return is_authorized;
    };
    return obj.check;
})();

var obj = {}
obj.getPrincipal = getPrincipal;
obj.checkAuth = checkAuth;
obj.generateRoles = generateRoles;
obj.canRegisterUserType = canRegisterUserType;
export default obj


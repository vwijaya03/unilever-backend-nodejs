import URLSafeBase64 from 'urlsafe-base64'
import moment from 'moment'

/*
Generates transaction id for generic transaction
@param {Number} userId
*/
function getTransId({userId=null}={}) {
    var encodedUserId = URLSafeBase64.encode(userId+'');
    var timestamp = moment.utc().format('YYYYMMDDHHmmssSSS');
    var transId = 'TX' + encodedUserId + timestamp;
    return transId;
}

/*
Generates transaction id for invoice payment
@param {Number} userId
*/
function getPayInvoiceTransId({userId=null}={}) {
    var encodedUserId = URLSafeBase64.encode(userId+'');
    var timestamp = moment.utc().format('YYYYMMDDHHmmssSSS');
    var transId = 'PI' + encodedUserId + timestamp;
    return transId;
}

/*
Generates transaction id for retur
@param {Number} userId
*/
function getReturInvoiceTransId({userId=null}={}) {
    var encodedUserId = URLSafeBase64.encode(userId+'');
    var timestamp = moment.utc().format('YYYYMMDDHHmmssSSS');
    var transId = 'RE' + encodedUserId + timestamp;
    return transId;
}

var obj = {}
obj.getTransId = getTransId
obj.getPayInvoiceTransId = getPayInvoiceTransId
obj.getReturInvoiceTransId = getReturInvoiceTransId

export default obj

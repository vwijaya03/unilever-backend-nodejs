import XLSX from 'xlsx'
import process from 'process'
import { ExcelParseError } from '@/server/error/Error'
import InvoiceDocProcessor from '@/models/invoice_doc_processor'
import fs from 'fs'
import excelstream from 'excel-stream'
import Excel from 'exceljs'
import parse from 'csv-parse'
require('should');

var Workbook = Excel.Workbook;
var WorkbookReader = Excel.stream.xlsx.WorkbookReader;

var obj = {}

function getTimeout() {
  var memory = process.memoryUsage();
  var heapSize = memory.heapTotal;
  if (heapSize < 90000000000) {
    return 0;
  }
  if (heapSize < 90000000000) {
    return heapSize / 5000000;
  }
  if (heapSize < 100000000000) {
    return heapSize / 20000000;
  }
  return heapSize / 100000000;
}

///**
//@returns Promise
//*/
//obj.parse = function(filePath) {
//    if (!filePath) {
//        throw new ExcelParseError();
//    }
//    var workbook = XLSX.readFile(filePath);
//    var sheet_name_list = workbook.SheetNames;
//
//    console.log('sheets list:', sheet_name_list);
//
//    var p = new Promise(function(resolve,reject){
//        InvoiceDocProcessor.readDocument(workbook.Sheets['Sheet1'],  {
//            onRecord(record) {
//                console.log('read record:', record);
//            },
//            onFinished() {
//                console.log('finished reading excel doc');
//            }
//        });
//    });
//    return p;
//    // console.log(workbook.Sheets['Sheet1']['J20534'].v);
//}


/**
@returns Promise
*/
//obj.parse = function(filePath) {

//    if (!filePath) {
//        throw new ExcelParseError();
//    }
//    console.log('parsing excel file', filePath);
//    var p = new Promise(function(resolve,reject){
//
//        var options = {
//            reader: ('stream'),
//            filename: filePath,
//            useStyles: false,
//            useSharedStrings: true,
//            gc: {
//                getTimeout: getTimeout
//            }
//        };
//
//        var wb = new WorkbookReader(options);
//        console.log('workbook: ', wb);
//
//        wb.on('end', function() {
//            console.log('reached end of stream');
//            console.log('shared-strings:', wb.sharedStrings);
//            resolve();
//        });
//        wb.on('finished', function(){
//            console.log('finished');
//            resolve();
//        });
//        wb.on('worksheet', (worksheet) => {
//            worksheet.on('row', function(row) {
//                // console.log('on-row', row);
//                var cells = row._cells;
//                var values = cells.map(function(cell){
//                    var obj = cell._value;
//                    obj.val2 = obj.value;
//
//                    //var sharedStringIdx = obj.value.sharedString;
//                    //cell.sharedStringIdx = sharedStringIdx;
//
//                    if (null != wb.sharedStrings) {
//                        console.log('sharedstrings:', wb.sharedStrings);
//                        var value = wb.sharedStrings[sharedStringIdx];
//                        cell.value = value;
//                    }
//
//                    return obj;
//                });
//
//                // console.log('shared-strings:', wb.sharedStrings);
//
//                console.log('values:', values);
//                // console.log('values:', JSON.stringify(values, null, ' '));
//
////                var ws = row._worksheet;
////                console.log('cols:', ws._columns);
////                for (var i in row._cells) {
////                    var cell = row._cells[i];
////                    console.log('cell:', cell);
////                }
//
//            });
//        });
//        wb.on('entry', function(entry) {
//            console.log('entry:', entry);
//        });
//
//        wb.read(filePath, {
//            // entries: 'emit',
//            sharedStrings: 'cache',
//            worksheets: 'emit'
//        });
//
//
//        // read from a file
////        var workbook = new Excel.Workbook();
////        workbook.xlsx.readFile(filePath)
////        .then(function() {
////            console.log('done reading ', filePath);
////            resolve();
////        });
//
//        resolve(true);
//
//    });
//
//    return p;
//}
//


/**
@returns Promise
*/
obj.parse = function(filePath) {

    return Promise.resolve();

//    var output = [];
//    // Create the parser
//    var parser = parse({delimiter: ':'});
//    // Use the writable stream api
//    parser.on('readable', function(){
//      while(record = parser.read()){
//        output.push(record);
//      }
//    });
//    // Catch any error
//    parser.on('error', function(err){
//      console.log(err.message);
//    });
//    // When we are done, test that the parsed output matched what expected
//    parser.on('finish', function(){
//      output.should.eql([
//        [ 'root','x','0','0','root','/root','/bin/bash' ],
//        [ 'someone','x','1022','1022','a funny cat','/home/someone','/bin/bash' ]
//      ]);
//    });
//    // Now that setup is done, write data to the stream
//    parser.write("root:x:0:0:root:/root:/bin/bash\n");
//    parser.write("someone:x:1022:1022:a funny cat:/home/someone:/bin/bash\n");
//    // Close the readable stream
//    parser.end();

}

export default obj


import clc from 'cli-color'
import { sprintf } from "sprintf-js"
import mariasql from 'mariasql'
import Sequelize from 'sequelize'


import properties from '@/properties'
import service_logger from '@/server/services/logger'
import util_logging from '@/server/utils/logging'
import singleton from '@/singleton'

const log = service_logger.getLogger({name: 'service', service:'mariadb'});

const mariadb_config = properties.mariadb;

var host = mariadb_config.host;
var port = mariadb_config.port;
var username = mariadb_config.user;
var database = mariadb_config.db;
var password = mariadb_config.password;

log.info('tes tes initializing mariadb service..');

log.debug('mariadb connection config:', mariadb_config);

var orm = new Sequelize(database, username, password, {
  host: host,
  dialect: 'mariadb',
  // logging: false,
  freezeTableName: true,
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

var orm_a = new Sequelize('unilever', 'nodebackend', 'exKcbj*9Z?MbLUc', {
  host: '192.168.12.122',
  dialect: 'mariadb',
  // logging: false,
  freezeTableName: true,
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

// var orm = new Sequelize('unilever_bravo', 'root', 'root', {
//   host: '127.0.0.1',
//   dialect: 'mariadb',
//   // logging: false,
//   freezeTableName: true,
//   pool: {
//     max: 5,
//     min: 0,
//     idle: 10000
//   }
// });

// var orm_a = new Sequelize('unilever_alpha', 'root', 'root', {
//   host: '127.0.0.1',
//   dialect: 'mariadb',
//   // logging: false,
//   freezeTableName: true,
//   pool: {
//     max: 5,
//     min: 0,
//     idle: 10000
//   }
// });

/*
@returns Promise
*/
function init() {
    return checkHealth();
}

/**
@returns Promise
**/
function checkHealth() {
    log.info('checking db health');
    return orm.query("SELECT 1;").then(function(results, metadata) {
        // Results will be an empty array and metadata will contain the number of affected rows.
        log.info('DB connection ok.');
    });
}

var obj = {}
obj.init = init
obj.checkHealth = checkHealth

export default obj
export { orm }
export { orm_a }


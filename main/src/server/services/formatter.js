import _ from 'lodash'
import moment from 'moment'
import { User } from '@/models/orm/index'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { RetailBalance } from '@/models/orm/index'
import { Transaction } from '@/models/orm/index'
import { Invoice } from '@/models/orm/index'
import { Informasi } from '@/models/orm/index'
import Principal from '@/models/principal'

const DEBUG_LOG = false;

const mappings = (()=>{
    let data = {};
    let data_inverse = {};

    function computeInverseDesc({desc=null}={}) {
        if (! _.isObject(desc)) return;
        var inverse = Object.keys(desc).map(key => {
        var value = desc[key];
        var rename = value.rename;
        var obj = {};
        if (rename) {
            obj[rename] = value;
        } else {
            obj[key] = value;
        }
        });
        return inverse;
    }

    let obj = {};
    obj.add = function({id=null, descriptor=null}={}) {
        if (!descriptor || !_.isFunction(descriptor)) {
            return;
        }
        data[id] = descriptor;
        data_inverse[id] = computeInverseDesc({desc:descriptor()});
    }
    obj.get = function({id=null, options=null}={}) {
        var fn = data[id];
        if (fn) {
            return fn({options:options});
        }
        return null;
    }
    obj.getInverse = function({id=null}={}) {
        return data_inverse[id];
    }
    return obj;
})();

var invoiceMap = {
    id:'invoice',
    descriptor({options=null}={}) {
        var desc =  {
            // "id": {},
            "invoice_id":{rename:'id'},
            "distributorCode": {rename: 'dt_code'},
            "outletCode": {rename:'outlet_code'},
            "le_code": {},
            "outlet_name": {},
            "cash_memo_total_amount":{rename:'total_amount'},
            "cash_memo_balance_amount":{rename:'balance_amount'},
            "cashmemo_type":{},
            "invoice_payment_status_paid":{rename:'payment_status_paid'},
            "invoice_payment_status_unpaid":{rename:'payment_status_unpaid'},
            "invoice_sales_date":{rename:'sales_date'},
            "invoice_due_date":{rename:'due_date'},
            "invoice_payment_status_paid":{},
            "get_last_date_invoice_id":{},
            "transaction":{},
            "is_paid":{},
            "is_refunded":{},
            "has_refund":{},
            "transact_date":{rename:'paid_date'},
            "transact_date":{},
            "distributor":formatDistributorItem,
            "approval_date": {},
            "status": {},
            "retur": {},
            "transactions": {},
            "products":formatInvoiceItem
        }
        return desc;
    }
};

mappings.add(invoiceMap);

function computeOptions(principal) {
    var output_options = {};
    if ((principal instanceof Principal) && principal.hasRoles(['internal'])) {
        output_options.dev = true;
    }
    return output_options;
}

/**
Gets formatter for the entity
@param {String} typeId - (optional) type id. if not specified will use type of entity to figure out formatter.
@param {Object} entity - ORM entity model
*/
function getFormatterFn({entity=null, typeId=null}={}) {
    // console.log('isi entity nya ', typeId);
    // console.log(entity);

    if (typeId === 'user' || entity instanceof User.Instance) {
        return formatUser;
    }
    if (typeId === 'informasi' || entity instanceof Informasi.Instance) {
        return formatInformasi;
    }
    else if (typeId === 'transaction' || entity instanceof Transaction.Instance) {
        return formatTransaction;
    }
    else if (typeId === 'invoice' || entity instanceof Invoice.Instance) {
        // console.log('isi format invoice');
        // console.log(formatInvoice);
        return formatInvoice;
    }
    else if (typeId === 'distributor' || entity instanceof Distributor.Instance) {
        return formatDistributor;
    }
    else if (typeId === 'retailer' || entity instanceof Retailer.Instance) {
        return formatRetailer;
    }
    return null;
}

/**
applies transformation on entity given the descriptor.
@param {Object} desc - descriptor
@param {Sequelize.Model} entity - ORM entity model.
*/
function apply(desc, entity) {

    if (!entity) {
        return null;
    }

    try {
        // console.log('isi entity di apply');
        // console.log(entity);
        // console.log('isi desc di apply');
        // console.log(desc);
        let out = {};

        for (var key in desc) {

            

            if (desc.hasOwnProperty(key)) {
                var transform = desc[key];
                var new_key = key;

                var entity_value = null;

                if (! _.isUndefined(entity[key])) {
                    entity_value = entity[key];
                } else {
                    // check if has default value
                    const defaultValue = transform.default_value;
                    if (! _.isNull(defaultValue) && ! _.isUndefined(defaultValue)) {
                        entity_value = defaultValue;
                    } else {
                        entity_value = null;
                    }
                }

                if (transform.rename) {
                    new_key = transform.rename;
                }

                if (_.isFunction(transform)) {
                    // console.log('transform is a function !!!', key);
                    if (_.isArray(entity_value)) {
                        // console.log('entity value is an array !!!');
                        out[new_key] = entity_value.map(item => {
                            return transform(item);
                        });
                    } else {
                        // console.log('transform: ', new_key, ' ,', _transformed_value);
                        var _transformed_value = transform(entity_value);
                        if (! _.isNull(_transformed_value)) {
                            out[new_key] = _transformed_value;
                        }
                    }
                }

                else {
                    if (transform.type) {
                        if (transform.type === 'number') {
                            if (/^\d+$/.test(entity_value)) {
                                out[new_key] = parseInt(entity_value);
                            }
                        }
                        else if (transform.type === 'date') {
                            // console.log('masuk transform date');
                            // console.log(transform);
                            // console.log('entity value');
                            // console.log(entity_value);
                            let formatted_date_str = entity_value+'';
                            let pattern = "YYYY-MM-DD";
                            var momentObj = null;
                            if (transform.date_format) {
                                if (transform.date_format == 'iso8601') {
                                    pattern = "YYYY-MM-DD'T'HH:mm:ssZZ";
                                }
                            }

                            if (moment.isMoment(entity_value)) {
                                momentObj = moment;
                            } else if (_.isNumber(entity_value)) {
                                // assuming entity_value is unix epoch in milliseconds in seconds.
                                momentObj = moment(entity_value);
                            }
                            if (momentObj) {
                                formatted_date_str = momentObj.format(pattern);
                            }
                            out[new_key] = formatted_date_str;
                        }
                    }
                    else if (_.isArray(entity_value)) {
                        out[new_key] = entity_value;
                    }
                    else if (_.isNumber(entity_value)) {
                        out[new_key] = entity_value+'';
                    }
                    else if (_.isBoolean(entity_value)) {
                        out[new_key] = entity_value;
                    }
                    else if (_.isNull(entity_value)) {
                        // exclude NULL value

                        // out[new_key] = entity_value;
                    }
                    else {
                        // console.log('test key: ', new_key, '; value: ', entity_value, '; test value: ', (entity_value+'').trim(), ';');

                        out[new_key] = (entity_value+'').trim();
                    }
                }
            }
        }
        // console.log('isi out di apply sebelum return');
        // console.log(out);
        return out;
    }
    catch(err) {
        console.log(err);
    }
}

/**

old version of format function

@param {Array | Object} obj - inputs to be formatted, either a list or single item.
@param {String} typeId - (Optional) use formatter for specific type id. if specified will override automatic formatter selection based on instance type.
@param {Object} options - output options
@returns {Array} - formatted objects
*/
function format({obj=null, typeId=null, clazz={}, options={}}={}) {
    var is_array = (obj instanceof Array);
    var item = {};
    if (is_array) {
        if (obj.length > 0) {
            item = obj[0];
        }
    } else {
        item = obj;
    }

    var fn = getFormatterFn({entity:item, typeId:typeId});
    if (!fn) return obj;

    

    var out = [];
    if (is_array) {
        // console.log('obj di if formatter');
        // console.log(obj);
        out = obj.map((item)=>{
            return fn(item, options);
        });
        // console.log('out di if formatter');
        // console.log(out);
    } else {
        if (item) {
            out = fn(item, options);
        }
        // console.log('out di else formatter');
        // console.log(out);
    }
    return out;
}

/**
generates 'user' output format.
@returns {Object} - formatted user
*/
function formatUser(user, options) {
    if (DEBUG_LOG) console.log('formatUser', 'options:', options);
    const desc = {
        "id": {},
        "username": {},
        "email": {},
        "pin": {},
        "type": {type:'number'},
        "fullname": {rename:'name'},
        "nama_toko": {},
        "password_changed": {},
        "valdoAccount": {rename:'valdo_account'},
        "retailerId": {type:'number', rename:'retailer_id'},
        "distributorId": {type:'number', rename:'distributor_id'},
        "salesId": {type:'number', rename:'sales_id'},
        "onbehalf": {},
        "reference": {},
        "phone": {},
        "roles":{},
        "banks":{},
        "retailer": formatRetailer,
        "distributor": formatDistributor,
        "sales": formatSales,
        "pickup": formatPickup
    };
    if (options && options.dev) {
        Object.assign(desc, {
            "id": {type:"string", rename:null},
            "password": {type:"string"},
            "accessToken": {type:"string", rename:'access_token'},
            "accessTokenExpiry": {type:"string", rename:null, rename:'access_token_expiry'}
        });
    }
    if (options && options.excludes) {
        options.excludes.forEach(item => {
            delete desc[item];
        });
    }
    var out = apply(desc, user);
    return out;
}

function formatRetailer(entity, options) {
    const desc = {
        "id": {type:"number"},
        "outletCode": {rename:'outlet_code'},
        "name": {},
        "le_code_dry": {},
        "le_code_ice": {},
        "le_code_ufs": {}
    };
    var out = apply(desc, entity);
    return out;
}

function formatInformasi(entity, options) {
    const desc = {
        "id": {type:"number"},
        "createdAt": {type:'date'},
        "updatedAt": {type:'date'},
        "title": {},
        "content": {},
        "image": {}
    };
    var out = apply(desc, entity);
    return out;
}

function formatTransaction(entity, options) {
    const desc = {
        "id": {},
        "toUserId": {rename: 'to_user_id'},
        "fromUserId": {rename: 'from_user_id'},
        "invoiceId": {rename:'invoice_id'},
        "retailerId": {rename: 'retailer_id'},
        "distributorId": {rename: 'distributor_id'},
        "currencyCode": {rename: 'currency_code'},
        "type": {type:'number'},
        "amount": {},
        "balance":{type:"number"},
        "description":{},
        "dk":{},
        "valdoTxId": {},
        "historyid": {},
        'transactDate': {type:'number', rename: 'transact_date'},
        'date_time': {type:"number"}
        // 'transactDate': {type:'date', date_format:'iso8601', rename: 'transact_date'}
        // '__derive__transactDate': {name: 'transact_date_iso8601'}
    };
    var out = apply(desc, entity);
    return out;
}

function formatDistributor(entity, options) {
    const desc = {
        "id": {type:'number'},
        "distributorCode": {rename:'dt_code'},
        "name": {}
    };
    var out = apply(desc, entity);
    return out;
}

function formatSales(entity, options) {
    const desc = {
        "sales_id": {type:'number'},
        "sales_code": {}
    };
    var out = apply(desc, entity);
    return out;
}

function formatPickup(entity, options) {
    const desc = {
        "id": {type:'number'},
        "pickup_code": {}
    };
    var out = apply(desc, entity);
    return out;
}

function formatInvoice(entity, options) {
    const desc = {
        // "id": {},
        "invoice_id":{rename:'id'},
        "distributorCode": {rename: 'dt_code'},
        "outletCode": {rename:'outlet_code'},
        "le_code": {},
        "outlet_name": {},
        "cash_memo_total_amount":{rename:'total_amount'},
        "cash_memo_balance_amount":{rename:'balance_amount'},
        "cashmemo_type":{},
        "invoice_payment_status_paid":{rename:'payment_status_paid'},
        "invoice_payment_status_unpaid":{rename:'payment_status_unpaid'},
        "invoice_sales_date":{rename:'sales_date', type: 'number'},
        "invoice_due_date":{rename:'due_date', type: 'number'},
        "invoice_payment_status_paid":{},
        "transact_date":{rename:'paid_date', type: 'number'},
        "is_paid":{},
        "is_refunded":{},
        "has_refund":{},
        // "approval_date": {},
        "status": {},
        "distributor":formatDistributorItem,
        "retur": {},
        "transactions": {},
        "paid_amount": {},
        "products":formatInvoiceItem
    };

    applyExcluded({desc:desc, options:options});
    // console.log('isi desc di formatInvoice');
    // console.log(desc);
    var out = apply(desc, entity);
    return out;
}

/**
remove attributes in descriptor.

@param {Object} desc - descriptor
@param {Object} options - format options
*/
function applyExcluded({desc=null, options=null}={}) {
    if (options) {
        if (options.attributes) {
            if (_.isArray(options.attributes)) {
                options.attributes.forEach(item => {
                    var re = /\!(\w+)/;
                    var match = item.match(re);
                    if (match) {
                        var name = match[1];
                        delete desc[name];
                    }
                });
            }
        }
    }
}

function formatInvoiceItem(entity) {
    if (DEBUG_LOG) console.log('formatInvoiceItem. entity:', entity);
    const desc = {
        "product": {rename:'id'},
        "product_name": {rename:'name'},
        "quantity": {},
        "product_price_cs": {rename:'price_cs'},
        "product_price_dz": {rename:'price_dz'},
        "product_price_pc": {rename:'price_pc'}
    };
    var out = apply(desc, entity);
    return out;
}

function formatDistributorItem(entity) {
    if (DEBUG_LOG) console.log('formatDistributorItem. entity:', entity);
    const desc = {
        "dt_code": {},
        "name": {},
    };
    var out = apply(desc, entity);
    return out;
}

// function formatInvoiceItem(entity) {
//     if (DEBUG_LOG) console.log('formatInvoiceItem. entity:', entity);
//     const desc = {
//         "product": {rename:'id'},
//         "product_name": {rename:'name'},
//         "quantity": {},
//         "product_price_cs": {rename:'price_cs'},
//         "product_price_dz": {rename:'price_dz'},
//         "product_price_pc": {rename:'price_pc'}
//     };
//     var out = apply(desc, entity);
//     return out;
// }

function formatRetailBalance(entity) {
    const desc = {
        "balance": {type:"string", rename:null},
    };
    var out = apply(desc, entity);
    return out;
}

function IDRformat(angka) {
    var temprev = parseInt(angka, 10);
    var positive = true;
    
    if(temprev < 0) {
        temprev = temprev * -1;
        positive = false;
    }

    var rev = temprev.toString().split('').reverse().join('');
    var rev2 = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }

    if(!positive) {
        return '(IDR ' + rev2.split('').reverse().join('') + ')';
    }

    // return 'IDR ' + rev2.split('').reverse().join('');
    return rev2.split('').reverse().join('');
}

var obj = {};
obj.computeOptions = computeOptions;
obj.format = format;
obj.formatUser = formatUser;
obj.formatRetailer = formatRetailer;
obj.formatInformasi = formatInformasi;
obj.formatDistributor = formatDistributor;
obj.formatTransaction = formatTransaction;
obj.formatInvoice = formatInvoice;
obj.formatRetailBalance = formatRetailBalance;
obj.IDRformat = IDRformat;
export default obj


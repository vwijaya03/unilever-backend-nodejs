import moment from 'moment'
import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import Sequelize from 'sequelize'

//
import { orm } from '@/server/services/mariadb'
import { MakePaymentError } from '@/server/error/Error'
import { User } from '@/models/orm/index'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Invoice } from '@/models/orm/index'
import { Transaction } from '@/models/orm/index'
import { TransactionQuery } from '@/models/orm/transaction'

import { UserNotFoundError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { BalanceNotEnoughError } from '@/server/error/Error'
import { InternalServerError } from '@/server/error/Error'
import { InvoiceAlreadyPaidError } from '@/server/error/Error'

import { MoneyAccess as moneyaccess } from '@/moneyaccess/index'
import { MoneyAccessTransferError } from '@/moneyaccess/error/index'
import { MoneyAccessInquiryError } from '@/moneyaccess/error/index'
import { MoneyAccessError } from '@/server/error/Error'

import { Transaction as TransactionResponseAdapter } from '@/moneyaccess/adapter/index'

import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_invoice from '@/server/services/invoice'
import service_generator from '@/server/services/generator'
import service_formatter from '@/server/services/formatter'
import base_repo from '@/server/repositories/base'
import test_users from '@/test/data/users'
import common_util from '@/server/utils/util'

const DEBUG_LOG = true;
const LOG_TAG = '[service:transaction] '
const log = service_logger.getLogger({name: 'service', service:'transaction'});

function updateInvoiceRejectStatus({status=null, invoiceId=null}={}) {
    return Invoice.update(
        { approval_date: Date(), status: status },
        { where: { invoice_id: invoiceId }}
    )
    .then(bankaccount => {
        return {code: 201, message: 'Request retur ditolak.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

/**
retrieve one transaction

@param {Object} filter
@param {Object} options
@returns {Transaction}
*/
function getOne(filter, options) {
    return get(filter, options).then((result_set)=>{
        // console.log('get-one result-set:', result_set);
        if (_.isEmpty(result_set)) {
            return null;
        } else {
            var k = Object.keys(result_set);
            return result_set[k[0]];
        }
    });
}

/**
search transactions

@param {String} targetUserId - target user id
@param {Boolean} is_send - flag for send type
@param {Boolean} is_receive - flag for receive type
@param {String} retailerId - the retailer id
@param {Moment} transactDateStart - start of transact date window.
@param {Moment} transactDateEnd - end of transact date window.
@param {Integer} type - 0 for payment, 1 for topup
@returns {Array.<Transaction>}
*/
function get({
    targetUserId=null,
    isSend=null,
    isReceive=null,
    retailerId=null,
    type=null,
    transactDateStart=null,
    transactDateEnd=null,
    startPage=1,
    itemsPerPage=15,
    page=null
    }={}) {

    log.debug(sprintf(LOG_TAG + ' get transactions. target-user-id:%1$s is-send:%2$s is-receive:%3$s type:%4$s', targetUserId, isSend, isReceive, type));

    var where = {};

    if (targetUserId) {
        log.debug('applying filter target-user-id');
        TransactionQuery.userId({filter:where, userId:targetUserId, isSend:isSend, isReceive:isReceive});
    }

    if (transactDateStart && transactDateEnd) {
        log.debug('applying date filter');
        const startTs = transactDateStart.unix()*1000; // in millis
        const endTs = transactDateEnd.unix()*1000; // in millis

        // start of debugging
        if (DEBUG_LOG) {
            log.debug('get. start-date', 'unix-millis:', startTs, 'formatted:', moment(startTs).format('LLL'));
            log.debug('get. end-date', 'unix-millis:', endTs, 'formatted:', moment(endTs).format('LLL'));
        }
        // end of debugging

        TransactionQuery.transactDate({filter:where, start:startTs, end:endTs});
    }
    if (retailerId) {
        // console.log('retailer id');
        TransactionQuery.retailerId({filter:where, retailerId:retailerId});
    }
    if (type) {
        TransactionQuery.type({filter:where, type:type});
    }

    // normalize startPage to 1 if less than 1.

    startPage = (startPage < 1 ) ? 1 : startPage;
    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;
    // console.log('offset page');
    // console.log(offset);

    const order = [
        ['transactDate', 'DESC']
    ];

    var include = null;

    // Eager loading Retailer and Distributor
    // Reference: http://docs.sequelizejs.com/en/latest/docs/models-usage/#eager-loading
    ///var include = [Retailer, Distributor];

    // var include = null;
    // var include = [ {model:Retailer, as:'retailer' }, {model:Distributor, as:'distributor'} ];

    var simulated_mobnum = '8804112345678900';
    simulated_mobnum = '08113001688';

    var p = Promise.resolve({});

    // get user object
    p = p.then(p_result => {

        log.debug('get user with id:', targetUserId);

        var getUserWhere = {};
        getUserWhere.id = targetUserId;
        return base_repo.getOne({entity:User, where:getUserWhere, options:{throwIfEmpty:true}})
        .then(user => {
            p_result.user = user;
            return p_result;
        });
    });

    if (transactDateStart && transactDateEnd) {
        p = p.then(p_result => {
            var {user:user} = p_result;
            // console.log('cek isi user');
            // console.log(user.valdoAccount);
            var trans_type;

            if(type == 0)
                trans_type = "transfer";
            else if(type == 1)
                trans_type = "topup";
            else if(type == 2)
                trans_type = "topup-reversal";
            else if(type == 3)
                trans_type = "disbursment";
            else if(type == 4)
                trans_type = "pickup";
            else if(type == 5)
                trans_type = "transfer_cl";
            else if(type == 6)
                trans_type = "ppob";
            else if(type == 7)
                trans_type = "cashback";

            log.info(LOG_TAG + 'retrieving moneyaccess:history for user vaccount:', user.valdoAccount);
            return moneyaccess.history({vacc_number:user.valdoAccount, type:trans_type, pagenum:startPage, item_per_page:itemsPerPage, startDate:transactDateStart, endDate:transactDateEnd, usertype:user.type})
            .then((api_response) => {
                // console.log('isi pagination responsenya');
                // console.log(api_response);
                // var pagination = {pagination: api_response[0].pagination, pagenumber}
                var transactionsList = TransactionResponseAdapter.parse({response:api_response});
                log.debug('valdo history', transactionsList);
                p_result.moneyaccessTransactionsList = transactionsList;
                // console.log('isi p_result transaction');
                // console.log(p_result);
                return p_result;
            });
        });
    }

    p = p.then(p_result => {

        var {moneyaccessTransactionsList:moneyaccessTransactionsList} = p_result;
        console.log('isi p_result MA');
        console.log(p_result);
        return base_repo.get({entity:Transaction, where:where, limit:limit, offset:offset, order:order, include:include, options:{}})
        .then(result_obj => {
            let { result_list:result_list, total:total } = result_obj;

            log.debug('combine both lists');

            // var txdb_list_transIdMap = common_util.list2obj(result_list, 'valdoTxId');
            var maccess_list_transIdMap = common_util.list2obj(moneyaccessTransactionsList, 'valdoTxId');
            // console.log('isi maccess_list_transIdMap');
            // console.log(moneyaccessTransactionsList);
            // log.debug('txdb map1:', txdb_list_transIdMap);
            log.debug('maccess map2:', maccess_list_transIdMap);

            var other_transactions_list = [];

            moneyaccessTransactionsList.map( (val, index) => {
                
                moneyaccessTransactionsList[index].date_time = Number(moneyaccessTransactionsList[index].transactDate);
                moneyaccessTransactionsList[index].historyid = val.historyid;
                console.log('isi history id '+val.historyid);
                if(val.type == 'transfer')
                {
                    moneyaccessTransactionsList[index].type = 0; 
                    moneyaccessTransactionsList[index].description = val.description;
                    moneyaccessTransactionsList[index].balance = val.saldo;
                    moneyaccessTransactionsList[index].dk = val.dk;
                }    
                if(val.type == 'topup')
                {
                    moneyaccessTransactionsList[index].type = 1;
                    moneyaccessTransactionsList[index].description = val.description;
                    moneyaccessTransactionsList[index].balance = val.saldo;
                    moneyaccessTransactionsList[index].dk = val.dk;
                }
                    
                if(val.type == 'topup-reversal')
                {
                    moneyaccessTransactionsList[index].type = 2;
                    moneyaccessTransactionsList[index].description = val.description;
                    moneyaccessTransactionsList[index].balance = val.saldo;
                    moneyaccessTransactionsList[index].dk = val.dk;
                }
                if(val.type == 'disbursment')
                {
                    moneyaccessTransactionsList[index].type = 3;
                    moneyaccessTransactionsList[index].description = val.description;
                    moneyaccessTransactionsList[index].balance = val.saldo;
                    moneyaccessTransactionsList[index].dk = val.dk;
                }
                if(val.type == 'pickup')
                {
                    moneyaccessTransactionsList[index].type = 4; 
                    moneyaccessTransactionsList[index].description = val.description;
                    moneyaccessTransactionsList[index].balance = val.saldo;
                    moneyaccessTransactionsList[index].dk = val.dk;
                }
                if(val.type == 'transfer_cl')
                {
                    moneyaccessTransactionsList[index].type = 5; 
                    moneyaccessTransactionsList[index].description = val.description;
                    moneyaccessTransactionsList[index].balance = val.saldo;
                    moneyaccessTransactionsList[index].dk = val.dk;
                }
                if(val.type == 'ppob')
                {
                    moneyaccessTransactionsList[index].type = 6; 
                    moneyaccessTransactionsList[index].description = val.description;
                    moneyaccessTransactionsList[index].balance = val.saldo;
                    moneyaccessTransactionsList[index].dk = val.dk;
                }
                if(val.type == 'cashback')
                {
                    moneyaccessTransactionsList[index].type = 7; 
                    moneyaccessTransactionsList[index].description = val.description;
                    moneyaccessTransactionsList[index].balance = val.saldo;
                    moneyaccessTransactionsList[index].dk = val.dk;
                }
            })

            // find records in itprovent internal db with matching transid.
            // store transactions that happen outside of ITProvent system.
            // for example: moneyaccess transaction history records with transid that do not match with the transid in itprovent db records.

            // Object.keys(maccess_list_transIdMap).forEach(transId => {
            //     // var matching_record = txdb_list_transIdMap[transId];
            //     // if (matching_record) {
            //     var obj = maccess_list_transIdMap[transId];
            //     // console.log('isi object cok');
            //     // console.log(obj);
            //     obj.date_time = obj.transactDate;
            //     if(maccess_list_transIdMap[transId].type == 'transfer')
            //     {
            //         obj.type = 0; 
            //         obj.description = maccess_list_transIdMap[transId].description;
            //         obj.balance = maccess_list_transIdMap[transId].saldo;
            //         obj.dk = maccess_list_transIdMap[transId].dk;
            //     }    
            //     if(maccess_list_transIdMap[transId].type == 'topup')
            //     {
            //         obj.type = 1;
            //         obj.description = maccess_list_transIdMap[transId].description;
            //         obj.balance = maccess_list_transIdMap[transId].saldo;
            //         obj.dk = maccess_list_transIdMap[transId].dk;
            //     }
                    
            //     if(maccess_list_transIdMap[transId].type == 'topup-reversal')
            //     {
            //         obj.type = 2;
            //         obj.description = maccess_list_transIdMap[transId].description;
            //         obj.balance = maccess_list_transIdMap[transId].saldo;
            //         obj.dk = maccess_list_transIdMap[transId].dk;
            //     }
            //     if(maccess_list_transIdMap[transId].type == 'disbursment')
            //     {
            //         obj.type = 3;
            //         obj.description = maccess_list_transIdMap[transId].description;
            //         obj.balance = maccess_list_transIdMap[transId].saldo;
            //         obj.dk = maccess_list_transIdMap[transId].dk;
            //     }
            //     if(maccess_list_transIdMap[transId].type == 'pickup')
            //     {
            //         obj.type = 4; 
            //         obj.description = maccess_list_transIdMap[transId].description;
            //         obj.balance = maccess_list_transIdMap[transId].saldo;
            //         obj.dk = maccess_list_transIdMap[transId].dk;
            //     }
            //     if(maccess_list_transIdMap[transId].type == 'transfer_cl')
            //     {
            //         obj.type = 5; 
            //         obj.description = maccess_list_transIdMap[transId].description;
            //         obj.balance = maccess_list_transIdMap[transId].saldo;
            //         obj.dk = maccess_list_transIdMap[transId].dk;
            //     }
            //     if(maccess_list_transIdMap[transId].type == 'ppob')
            //     {
            //         obj.type = 6; 
            //         obj.description = maccess_list_transIdMap[transId].description;
            //         obj.balance = maccess_list_transIdMap[transId].saldo;
            //         obj.dk = maccess_list_transIdMap[transId].dk;
            //     }
            //     if(maccess_list_transIdMap[transId].type == 'cashback')
            //     {
            //         obj.type = 7; 
            //         obj.description = maccess_list_transIdMap[transId].description;
            //         obj.balance = maccess_list_transIdMap[transId].saldo;
            //         obj.dk = maccess_list_transIdMap[transId].dk;
            //     }
            // });

            // Object.keys(txdb_list_transIdMap).forEach(transId => {
            //     var matching_record = maccess_list_transIdMap[transId];
            //     if (matching_record) {
            //         var obj = txdb_list_transIdMap[transId];

                    // console.log(LOG_TAG + 'FOUND MATCH! rec1:', matching_record, 'rec2:', obj);

                    // copy saldo information from moneyaccess response.
                    // obj.balance = matching_record.saldo;
                // } 
                // else 
                // {
                //     other_transactions_list.push(txdb_list_transIdMap[transId]);
                // }
            // });

            // combine both lists and sort by transact date.
//            var combinedList = [].concat(result_list);
//            console.log(LOG_TAG + 'sorting list.');
//            combinedList.sort(function (left, right) {
//                return moment.utc(left.transact_date).diff(moment.utc(right.transact_date));
//            });

            log.debug('done.');
            var out = {};

            out.total_transactions = moneyaccessTransactionsList.length;
            out.result_list = moneyaccessTransactionsList;
            // out.other_transactions_list = other_transactions_list;
            // console.log('Money Access List');
            // console.log(moneyaccessTransactionsList);
            return out;
        });

    });

    return p;
}

/**
Get balance
@param {User} user - the User object
@return {Promise} will resolve to: {balance:<Number>}
*/
function getBalance({user=null}={}) {
    log.debug(sprintf('get balance for username:%1$s', user.username));

    if (! user) {
        return {balance:0};
    }

    // get latest balance from remote server.
    var from_vaccount = user.valdoAccount;
    var mobnum = from_vaccount;

    if (! from_vaccount || ! mobnum) {
        return {balance:0}
    }

    return moneyaccess.inquiry({mobnum:mobnum, vaccount:from_vaccount, usertype: user.type}).then(p2_result => {
        log.debug(sprintf('get-balance username:%1$s result:%2$j', user.username, p2_result));
        var resp = p2_result;
        var balanceStr = resp.balance;
        var balanceNumber = Number(balanceStr);
        var effective_balanceNumber = Number(resp.effective_balance);
        var intransit_balanceNumber = Number(resp.intransit_balance);
        var credit_limitNumber = Number(resp.credit_limit);
        var current_credit_limitNumber = Number(resp.current_credit_limit);
        return {balance: balanceNumber, effective_balance: effective_balanceNumber, intransit_balance: intransit_balanceNumber, credit_limit: credit_limitNumber, current_credit_limit: current_credit_limitNumber};
    }).catch(MoneyAccessInquiryError, err => {
        log.error(sprintf('failed to invoke moneyaccess:inquiry for username:%1$s exception:$2$j', user.username, err));
        // wraps the third party error into our error.
        throw new InternalServerError('failed to get balance. err:' + err, err.code);
    });
}

/**
Perform normal invoice payment from source vaccount to destination vaccount.
@param {User} user -
@param {String} to_vaccount -
*/
function makePayment({requestId=null, transId=null, mobnum=null, from_vaccount=null, to_vaccount=null, amount=null, pin=null, usertype=null, invoiceId=null}={}) {
    let msg = sprintf('request-id:%1$s txid:%2$s transferring money from:%3$s to:%4$s amount:$5$s', requestId, transId, from_vaccount, to_vaccount, amount);
    log.info(msg);
    var args = {transId:transId, mobnum:mobnum, from:from_vaccount, to:to_vaccount, amount:amount, pin:pin, usertype: usertype};

    return service_user.getOneUser({vaccount: from_vaccount}).then(pengirim => {

        console.log('isi pengirim');
        console.log(pengirim.dataValues.valdo_account);
        var nama_pengirim = pengirim.fullname;
        
        return service_user.getOneUser({vaccount: to_vaccount}).then(penerima => {

            var nama_penerima = penerima.fullname;

            args.desc = "Pembayaran "+invoiceId+" Rp. "+service_formatter.IDRformat(amount)+" dari "+nama_pengirim+ " ("+from_vaccount+")"+" ke "+nama_penerima+" ("+to_vaccount+")";

            console.log('transaction je es make payment.', args);

            return moneyaccess.transfer(args).then(p_result => {
                let msg = sprintf('request-id:%1$s txid:%2$s transfer result:%3$j', requestId, transId, p_result);
                log.info(msg);
                return p_result;
            });
        });
        
    })
    .catch(MoneyAccessTransferError, err => {
        let msg = sprintf('request-id:%1$s txid:%2$s failed to make-payment. exception:%3$s', requestId, transId, err.message);
        log.error(msg);
        throw new MakePaymentError(err.message, err.code);
    });
}

/**
Perform normal invoice payment from source vaccount to destination vaccount.
@param {String} requestId - request id
@param {Transaction} transaction - Sequelize transaction object
@param {User} user - sender User
@param {String} to_vaccount - recipient vaccount
@param {Number} amount - saldo amount
@param {String} pin - the pin
*/
function makeNormalPayment({requestId=null, transaction=null, user=null, recipientUsername=null, amount=null, pin=null}={}) {
    log.debug(sprintf(LOG_TAG + ' make normal payment. recipientUsername:%1$s amount:%2$s', recipientUsername, amount));

    // get and check saldo balance
    return Promise.resolve({})
    .then(p_result => {
        return getBalance({user:user})
        .then(p2_result => {
            var {balance:balance} = p2_result;
            if (balance < amount) {
                throw new BalanceNotEnoughError();
            }
            p_result.balance = balance;
            return p_result;
        });
    })
    .then(p_result => {
        return base_repo.getOne({entity:User, where:{username:recipientUsername}, options:{throwIfEmpty:true}})
    })
    .then(p_result => {
        var recipient_user = p_result;
        var to_vaccount = recipient_user.valdoAccount;
        var from_vaccount = user.valdoAccount;
        var usertype = user.type;
        return makePayment(
        {
            requestId: requestId,
            mobnum:from_vaccount,
            from_vaccount:from_vaccount,
            to_vaccount:to_vaccount,
            amount:amount,
            pin:pin,
            usertype: usertype
        });
    });
}

/**
Perform invoice payment
@param {String} requestId - request id
@param {Transaction} transaction - Sequelize transaction object
@param {Principal} principal - the principal entity making this request.
@param {Invoice} invoice - the invoice object
@param {String} pin - secret transaction pin
@param {Number} amount - amount of money

@throws InsufficientBalanceError - when not enough money in the pocket.
@throws MakePaymentError - can't make a payment for whatever reasons.
*/
function makePaymentInvoice({requestId=null, transaction=null, user=null, principalRetailer=null, invoiceId=null, distributor=null, pin=null, amount=null, dt_code=null}={}) {

    log.info(sprintf('request-id:%1$s make-invoice-payment. username:%2$s invoice-id:%3$s payment:%4$s', requestId, user.username, invoiceId, amount));

    // return orm.query('select count(invoice.invoice_id) as total_id from invoice where invoice.invoice_id = '+invoiceId+ ' AND invoice.dt_code = '+dt_code, { type: orm.QueryTypes.SELECT})
    // .then(function(data){
    //     console.log('isi data sebelum if');
    //     console.log(data[0].total_id);
    //     if(data[0].total_id == 0 || data[0] == null || data == null)
    //     {
    //         console.log('masuk if');
    //         console.log(data);

    //         throw new Error('Data invoice tidak ditemukan.');
    //     }
    // })

    var from_vaccount = user.valdoAccount;
    var invoice_code = '';

    return Promise.resolve({

    })
    .then(p_result => {
        return base_repo.getOne(
            {
                entity:Invoice, where:{invoice_id: invoiceId, dt_code:dt_code},
                options:{throwIfEmpty:true, transaction:transaction, lock:transaction.LOCK.UPDATE}
            })
            .then(invoice => {
            p_result.invoice = invoice;
            return p_result;
        });
    })
    /**
    Verify if principal-retailer owns invoice.
    **/
    .then(p_result => {

        const {invoice:invoice} = p_result;

        if (! invoice || ! principalRetailer) {
            throw new Error('invalid request');
        }

        if(invoice.cash_memo_balance_amount < amount){
            throw new Error('Jumlah yang di inputkan terlalu banyak, kekurangan yang belum di bayar: Rp. '+invoice.cash_memo_balance_amount);
        }

        console.log('isi p result dari get one invoice');
        console.log(p_result);

        var expectedOutletCode = principalRetailer.outletCode?principalRetailer.outletCode.trim():null;
        var invoiceLeCode = invoice.le_code?invoice.le_code.trim():null;
        var actualOutletCode = invoice.outletCode?invoice.outletCode.trim():null;

        // if (! expectedOutletCode || ! actualOutletCode) {
        //     throw new Error('cant resolve outlet codes.');
        // } else {
        //     if ( expectedOutletCode != actualOutletCode ) {
        //         var msg = sprintf('request-id:%1$s bearer-token does not own invoice. principal-outletcode:%2$s invoice-outletcode:%3$s', requestId, principalRetailer.outletCode, invoice.outletCode);
        //         throw new Error(msg);
        //     }
        // }
        return p_result;
    })
    /**
     Check if invoice is already paid.
     */
    .then(p_result => {

        const {invoice:invoice} = p_result;

        return service_invoice.isPaidFull({invoiceId:invoice.invoice_id, dt_code:dt_code})
        .then(p2_result => {
            var is_paid_full = p2_result;
            if (is_paid_full == "1") {
                throw new InvoiceAlreadyPaidError();
            }
            return p_result;
        });
    })
    // get and check saldo balance
    .then(p_result => {
        return getBalance({user:user}).then(p2_result => {
            var {balance:balance} = p2_result;
            if (balance < amount) {
                throw new BalanceNotEnoughError();
            }
            p_result.balance = balance;
            return p_result;
        });
    })
    // transfer money
    .then(p_result => {
        var {balance:balance, invoice:invoice} = p_result;

        var distributor_id = distributor.id;

        // get distributor user.

        return Promise.resolve({})
        .then(p2_result => {
            return base_repo.getOne({entity:User, where:{distributorId:distributor_id, type:1}, options:{throwIfEmpty:true}})
            .then(user => {
                p2_result.user = user;
                return p2_result;
            });
        })
        .then(p2_result => {
            var { user:user } = p2_result;
            var msg = sprintf('request-id:%1$s resolved payment to-username:%2$s', requestId, user.username);
            log.debug(msg);
            p2_result.to_vaccount = user.valdoAccount;
            return p2_result;
        })
        .then(p2_result => {

            const { to_vaccount:to_vaccount } = p2_result;

            console.log('to vaccount');
            console.log(to_vaccount);

            console.log('from vaccount');
            console.log(from_vaccount);
            if (to_vaccount === from_vaccount) {
                throw new MakePaymentError('you cant send money to yourself.');
            }
            var mobnum = from_vaccount;

            // generate transaction id
            var transId = service_generator.getPayInvoiceTransId({userId:user.id});
            // log.info('transaction-id:', transId);

            invoice_code = distributor.distributorCode+invoiceId;

            return makePayment({requestId:requestId, transId:transId, mobnum:mobnum, from_vaccount:from_vaccount, to_vaccount:to_vaccount, amount:amount, pin:pin, usertype: user.type, invoiceId: invoice_code})
            .then(moneyAccessApiResponse => {
                var msg = sprintf('request-id:%1$s txid:%2$s raw-moneyaccess-resp:%3$j', requestId, transId, moneyAccessApiResponse);
                log.info(msg);
                p2_result.moneyAccessApiResponse = moneyAccessApiResponse;
                return p2_result;
            });
        })
        .then(p2_result => {

            // update invoice record.
            // console.log('isi distributor');
            // console.log(distributor.distributorCode);
            const {moneyAccessApiResponse:{transid:transid}} = p2_result;

            log.info(sprintf('request-id:%1$s txid:%2$s updating invoice db record for invoice-id:%3$s', requestId, transid, invoiceId));

            return Invoice.update(
              { cash_memo_balance_amount: invoice.cash_memo_balance_amount - amount },
              {
                fields: ['cash_memo_balance_amount'],
                where: {invoice_id: invoiceId, dt_code: distributor.distributorCode},
                logging: console.log,
                validate: true,
                limit: 1,
                transaction: transaction
              }
            ).then(p3_result => {
                const [affectedCount, affectedRows] = p3_result;
                log.debug(sprintf('request-id:%1$s affectedCount:%2$s', requestId, affectedCount));
                return p2_result;
            });
        })
        .then(p2_result => {
            var { moneyAccessApiResponse:moneyAccessApiResponse } = p2_result;
            return moneyAccessApiResponse;
        })
        .catch(ResourceNotFoundError, err => {
            var msg = sprintf('request-id:%1$s failed to pay invoice-id:%2$s. unable to find distributor-user.', requestId, invoiceId, err);
            log.error(msg);
            throw new MakePaymentError(msg, err.code);
        })
        .catch(err => {
            var msg = sprintf('request-id:%1$s failed to pay invoice-id:%2$s', requestId, invoiceId);
            log.error(msg, err);
            throw new MakePaymentError(msg);
        });
    });
}

function makeReturInvoice({requestId=null, transaction=null, user=null, principalRetailer=null, invoiceId=null, distributor=null, pin=null, amount=null}={}) {

    log.info(sprintf('request-id:%1$s make-invoice-payment. username:%2$s invoice-id:%3$s payment:%4$s', requestId, user.username, invoiceId, amount));

    var from_vaccount = user.valdoAccount;

    return Promise.resolve({})
    .then(p_result => {
        return base_repo.getOne(
            {
                entity:Invoice, where:{invoice_id: invoiceId},
                options:{throwIfEmpty:true, transaction:transaction, lock:transaction.LOCK.UPDATE}
            })
            .then(invoice => {
            p_result.invoice = invoice;
            return p_result;
        });
    })
    /**
    Verify if principal-retailer owns invoice.
    **/
    .then(p_result => {
        const {invoice:invoice} = p_result;
        // if (! invoice || ! principalRetailer) {
        //     throw new Error('invalid request');
        // }
        // var expectedOutletCode = principalRetailer.outletCode?principalRetailer.outletCode.trim():null;
        if (! invoice || ! distributor) {
            throw new Error('invalid request');
        }
        var expectedDistCode = distributor.distributorCode?distributor.distributorCode.trim():null;

        var actualDistCode = invoice.distributorCode?invoice.distributorCode.trim():null;
        if (! expectedDistCode || ! actualDistCode) {
            throw new Error('cant resolve outlet codes.');
        } else {
            if ( expectedDistCode != actualDistCode ) {
                var msg = sprintf('request-id:%1$s bearer-token does not own invoice. principal-outletcode:%2$s invoice-outletcode:%3$s', requestId, distributor.outletCode, invoice.distributorCode);
                throw new Error(msg);
            }
        }
        return p_result;
    })
    /**
     Check if invoice is already paid.
     */
    .then(p_result => {

        const {invoice:invoice} = p_result;

        return service_invoice.isPaidFull({invoiceId:invoice.invoice_id})
        .then(p2_result => {
            var is_paid_full = p2_result;
            if (is_paid_full) {
                throw new InvoiceAlreadyPaidError();
            }
            return p_result;
        });
    })
    // get and check saldo balance
    .then(p_result => {
        return getBalance({user:user}).then(p2_result => {
            var {balance:balance} = p2_result;
            amount = Math.abs(p_result.invoice.cash_memo_total_amount);
            console.log('isi amon');
            console.log(amount);
            if (balance < amount) {
                throw new BalanceNotEnoughError();
            }
            p_result.balance = balance;
            return p_result;
        });
    })
    // transfer money
    .then(p_result => {
        var {balance:balance, invoice:invoice} = p_result;
        console.log(obj);
        var distributor_id = distributor.id;
        // var distributor_id = distributor.id;
        console.log('bawah nya var dist id');
        // get distributor user.
        return Promise.resolve({})
        .then(p2_result => {
            console.log('p2 result tahap 1');
            return base_repo.getOne({entity:User, where:{distributorId:distributor_id}, options:{throwIfEmpty:true}})
            .then(user => {
                p2_result.user = user;
                return p2_result;
            });
        })
        .then(p2_result => {
            console.log('p2 result tahap 2');
            var { user:user } = p2_result;
            var msg = sprintf('request-id:%1$s resolved payment to-username:%2$s', requestId, user.username);
            log.debug(msg);
            p2_result.to_vaccount = user.valdoAccount;
            return p2_result;
        })
        .then(p2_result => {
            console.log('setelah p2');
            console.log(p_result.invoice.outletCode);
            // const { to_vaccount:to_vaccount } = p2_result;

            // if (to_vaccount === from_vaccount) {
            //     throw new MakePaymentError('you cant send money to yourself.');
            // }
            return Promise.resolve()
            .then(() => {
                return Retailer.findOne({
                    where: {
                        outlet_code: p_result.invoice.outletCode,
                    },
                }); 
            })
            .then(response => {
                console.log('isi response');
                console.log(response.id);
                return User.findOne({
                    where: {
                        retailer_id: response.id,
                    },
                });
            }).then(user_data => {
                console.log('user data');
                console.log(user_data);
               
                var mobnum = p2_result.valdoAccount;

                // generate transaction id
                var transId = service_generator.getReturInvoiceTransId({userId:user.id});
                // log.info('transaction-id:', transId);
                amount = Math.abs(p_result.invoice.cash_memo_total_amount);
                console.log('atas nya make payment');
                return makePayment({requestId:requestId, transId:transId, mobnum:mobnum, from_vaccount:from_vaccount, to_vaccount:user_data.valdoAccount, amount:amount, pin:pin, usertype: user.type})
                .then(moneyAccessApiResponse => {
                    var msg = sprintf('request-id:%1$s txid:%2$s raw-moneyaccess-resp:%3$j', requestId, transId, moneyAccessApiResponse);
                    log.info(msg);
                    p2_result.moneyAccessApiResponse = moneyAccessApiResponse;
                    p2_result.user_data = user_data;
                    return p2_result;
                }); 
            });
        })
        .then(p2_result => {

            // update invoice record.
            const {moneyAccessApiResponse:{transid:transid}} = p2_result;

            log.info(sprintf('request-id:%1$s txid:%2$s updating invoice db record for invoice-id:%3$s', requestId, transid, invoiceId));

            return Invoice.update(
              { invoice_payment_status_paid: invoice.cash_memo_total_amount },
              {
                fields: ['invoice_payment_status_paid'],
                where: {invoice_id: invoiceId},
                logging: console.log,
                validate: true,
                limit: 1,
                transaction: transaction
              }
            ).then(p3_result => {
                const [affectedCount, affectedRows] = p3_result;
                log.debug(sprintf('request-id:%1$s affectedCount:%2$s', requestId, affectedCount));
                return p2_result;
            });
        })
        .then(p2_result => {
            console.log('di service');
            console.log(p2_result);
            var { moneyAccessApiResponse:moneyAccessApiResponse } = p2_result;
            // console.log('isi dari p2 res');
            // console.log(p2_result);
            return p2_result;
        })
        .catch(ResourceNotFoundError, err => {
            var msg = sprintf('request-id:%1$s failed to pay invoice-id:%2$s. unable to find distributor-user.', requestId, invoiceId, err);
            log.error(msg);
            throw new MakePaymentError(msg, err.code);
        })
        .catch(err => {
            var msg = sprintf('request-id:%1$s failed to pay invoice-id:%2$s', requestId, invoiceId);
            log.error(msg, err);
            throw new MakePaymentError(msg);
        });
    });
}
/**
Writes transaction record to DB
@returns { Promise }
*/
function writeTxRecord(
{
    requestId=null,
    valdoTxId=null,
    fromUserId=null,
    toUserId=null,
    dt_code=null,
    invoiceId=null,
    invoice_code=null,
    retailerId=null,
    distributorId=null,
    type=null,
    amount=null,
    currencyCode=null,
    transactDate=null
}={}) {
    console.log('transactDate', transactDate);
    if (! transactDate) {
        transactDate = moment().unix() * 1000;
    }

    var args = {
        valdoTxId: valdoTxId,
        fromUserId: fromUserId,
        toUserId: toUserId,
        dt_code: dt_code,
        invoiceId: invoiceId,
        invoice_code: invoice_code,
        retailerId: retailerId,
        distributorId: distributorId,
        type: type,
        amount: amount,
        currencyCode: 'IDR',
        transactDate: transactDate
    };

    log.debug(sprintf('request-id:%1$s creating transaction record in db.', requestId));

    return Transaction.create(args).then(tx => {
        log.info(sprintf('request-id:%1$s done writing transaction record in db.', requestId));
        return tx;
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error(sprintf('request-id:%1$s error:%2$s', requestId, err_message));
        throw new DbRecordCreationError(err_message);
    });
}

function writeReturTxRecord(
{
    requestId=null,
    valdoTxId=null,
    fromUserId=null,
    toUserId=null,
    invoiceId=null,
    retailerId=null,
    distributorId=null,
    type=null,
    amount=null,
    currencyCode=null,
    transactDate=null,
    status=null
}={}) {

    if (! transactDate) {
        transactDate = moment().unix() * 1000;
    }
    Invoice.update(
        { approval_date: Date(), status: status },
        { where: { invoice_id: invoiceId }}
    );
    var args = {
        valdoTxId: valdoTxId,
        fromUserId: fromUserId,
        toUserId: toUserId,
        invoiceId: invoiceId,
        retailerId: retailerId,
        distributorId: distributorId,
        type: type,
        amount: amount,
        currencyCode: 'IDR',
        transactDate: transactDate
    };

    log.debug(sprintf('request-id:%1$s creating transaction record in db.', requestId));

    return Transaction.create(args).then(tx => {
        log.info(sprintf('request-id:%1$s done writing transaction record in db.', requestId));
        return tx;
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error(sprintf('request-id:%1$s error:%2$s', requestId, err_message));
        throw new DbRecordCreationError(err_message);
    });
}

var obj = {}
obj.get = get
obj.getOne = getOne
obj.makePayment = makePayment
obj.makeNormalPayment = makeNormalPayment
obj.makePaymentInvoice = makePaymentInvoice
obj.writeTxRecord = writeTxRecord
obj.getBalance = getBalance
obj.updateInvoiceRejectStatus = updateInvoiceRejectStatus
obj.makeReturInvoice = makeReturInvoice
obj.writeReturTxRecord = writeReturTxRecord
export default obj


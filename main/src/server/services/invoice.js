import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import test_invoices from '@/test/data/invoices'
import { Invoice } from '@/models/orm/index'
import { Transaction } from '@/models/orm/index'
import { Products } from '@/models/orm/index'
import { InvoiceQuery } from '@/models/orm/invoice'
import { ResourceNotFoundError } from '@/server/error/Error'
import { orm } from '@/server/services/mariadb'
import base_repo from '@/server/repositories/base'
import util from '@/server/utils/util'
import moment from 'moment'
import Sequelize from 'sequelize'
import async from 'async';

const LOG_TAG = '[service-invoice] ';

function getCheckProcessInvoice({dt_code=null}={})
{
    return orm.query('select count(tasks.id) as total_tasks from tasks where tasks.dt_code = "'+dt_code+'" and tasks.status = 0 ', { type: orm.QueryTypes.SELECT})
    .then(function(result){
        
        if(result[0].total_tasks > 0) {
            return {code: 403, message: 'Proses invoice sedang berjalan, silahkan tunggu sebentar'};
        } else {
            return {code: 200, message: 'Lanjut'};
        }
    });
}

function getTask({dt_code=null, email=null, startPage=1, itemsPerPage=15, sorting=null, sortingBy=null, usertype=null}={})
{
    // normalize startPage to 1 if less than 1.
    startPage = (startPage < 1 ) ? 1 : startPage;

    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    var orderByColumnQuery = '';
    var orderByAscOrDescQuery = '';

    if(sorting == null)
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }

    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by tasks.createdAt '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'sales_date')
    {
        orderByColumnQuery = 'order by tasks.createdAt '+orderByAscOrDescQuery;
    }

    if(usertype == 1) 
    {
        return orm.query('select users.fullname as created_by, tasks.id, tasks.original_name, tasks.file_name, tasks.status, tasks.total_row, tasks.processed_row, tasks.updated_row, tasks.createdAt, tasks.updatedAt, tasks.total_upload from tasks inner join users on users.user_id = tasks.user_id where tasks.dt_code = "'+dt_code+'" '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
        .then(function(tasks){

            tasks.forEach(result => {
                var milisecond_created_at = new Date(String(result.createdAt));
                var milisecond_updated_at = new Date(String(result.updatedAt));

                result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);
                    
                result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
            });

            return orm.query('select count(tasks.id) as total_tasks from tasks inner join users on users.user_id = tasks.user_id where tasks.dt_code = "'+dt_code+'" ', { type: orm.QueryTypes.SELECT})
            .then(function(total_tasks){
                
                return {list:tasks, total:total_tasks[0].total_tasks};
            });
        });
    }
    else 
    {
        if(dt_code != null)
        {
            return orm.query('select users.fullname as created_by, tasks.id, tasks.original_name, tasks.file_name, tasks.status, tasks.total_row, tasks.processed_row, tasks.updated_row, tasks.action, tasks.createdAt, tasks.updatedAt, tasks.total_upload from tasks inner join users on users.user_id = tasks.user_id where tasks.dt_code = "'+dt_code+'" '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
            .then(function(tasks){

                tasks.forEach(result => {
                    var milisecond_created_at = new Date(String(result.createdAt));
                    var milisecond_updated_at = new Date(String(result.updatedAt));

                    result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);
                    
                    result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
                });

                return orm.query('select count(tasks.id) as total_tasks from tasks inner join users on users.user_id = tasks.user_id where tasks.dt_code = "'+dt_code+'" ', { type: orm.QueryTypes.SELECT})
                .then(function(total_tasks){
                    
                    return {list:tasks, total:total_tasks[0].total_tasks};
                });
            });
        }
        else if(email != null)
        {
            return orm.query('select users.fullname as created_by, tasks.id, tasks.original_name, tasks.file_name, tasks.status, tasks.total_row, tasks.processed_row, tasks.updated_row, tasks.action, tasks.createdAt, tasks.updatedAt, tasks.total_upload from tasks inner join users on users.user_id = tasks.user_id where users.email = "'+email+'" '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
            .then(function(tasks){

                tasks.forEach(result => {
                    var milisecond_created_at = new Date(String(result.createdAt));
                    var milisecond_updated_at = new Date(String(result.updatedAt));

                    result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);
                    
                    result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
                });

                return orm.query('select count(tasks.id) as total_tasks from tasks inner join users on users.user_id = tasks.user_id where users.email = "'+email+'" ', { type: orm.QueryTypes.SELECT})
                .then(function(total_tasks){
                    
                    return {list:tasks, total:total_tasks[0].total_tasks};
                });
            });
        }
        else
        {
            return orm.query('select users.fullname as created_by, tasks.id, tasks.original_name, tasks.file_name, tasks.status, tasks.total_row, tasks.processed_row, tasks.updated_row, tasks.action, tasks.createdAt, tasks.updatedAt, tasks.total_upload from tasks inner join users on users.user_id = tasks.user_id '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
            .then(function(tasks){

                tasks.forEach(result => {
                    var milisecond_created_at = new Date(String(result.createdAt));
                    var milisecond_updated_at = new Date(String(result.updatedAt));

                    result.createdAt = milisecond_created_at.getFullYear()+"-"+("0" + (milisecond_created_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_created_at.getDate()).slice(-2)+" "+("0" + milisecond_created_at.getHours()).slice(-2)+":"+("0" + milisecond_created_at.getMinutes()).slice(-2)+":"+("0" + milisecond_created_at.getSeconds()).slice(-2);

                    result.updatedAt = milisecond_updated_at.getFullYear()+"-"+("0" + (milisecond_updated_at.getMonth() + 1)).slice(-2)+"-"+("0" + milisecond_updated_at.getDate()).slice(-2)+" "+("0" + milisecond_updated_at.getHours()).slice(-2)+":"+("0" + milisecond_updated_at.getMinutes()).slice(-2)+":"+("0" + milisecond_updated_at.getSeconds()).slice(-2);
                });

                return orm.query('select count(tasks.id) as total_tasks from tasks inner join users on users.user_id = tasks.user_id', { type: orm.QueryTypes.SELECT})
                .then(function(total_tasks){
                    
                    return {list:tasks, total:total_tasks[0].total_tasks};
                });
            });
        }
    }
}

function getDataFromExcelDump({filename=null}={})
{
    //select count(*) as total_excel_dump from (select * from excel_dump group by dt_code, invoice_id) as invoice 
    return orm.query("select * from excel_dump where file_name = '"+filename+"' group by dt_code, invoice_id", { type: orm.QueryTypes.SELECT }
        ).then(result => {
          return result;  
    });
}

function enhanceGetDataFromExcelDump({filename=null}={})
{
    //select count(*) as total_excel_dump from (select * from excel_dump group by dt_code, invoice_id) as invoice 
    return orm.query("select id from excel_dump where file_name = '"+filename+"' group by dt_code, invoice_id", { type: orm.QueryTypes.SELECT }
        ).then(result => {
          return result;  
    });
}

function checkExcelDumpToInvoice({invoice_id=null, dt_code=null}={})
{
    return orm.query("select * from invoice where invoice_id = '"+invoice_id+"' and dt_code = '"+dt_code+"'", { type: orm.QueryTypes.SELECT }
        ).then(result => {
          return result;  
    });
}

function insertInvoiceFromExcelDump({filename=null}={})
{
    return orm.query(`
        insert into invoice(file_name, dt_code, le_code, outlet_code, cash_memo_total_amount, cash_memo_balance_amount, cashmemo_type, invoice_id, invoice_sales_date, invoice_due_date, invoice_payment_status_paid, invoice_payment_status_unpaid, status)

        select a.file_name, a.dt_code, a.le_code, a.outlet_code, a.cash_memo_total_amount, a.cash_memo_balance_amount, a.cashmemo_type, a.invoice_id, a.invoice_sales_date, a.invoice_due_date, SUBSTRING_INDEX(a.invoice_payment_status, "/", 1) as invoice_payment_status_paid, SUBSTRING_INDEX(a.invoice_payment_status, "/", -1) as invoice_payment_status_unpaid, 0 as status
        from excel_dump as a
        where REPLACE(CONCAT(a.dt_code, a.invoice_id),' ', '') NOT IN (select REPLACE(CONCAT(b.dt_code, b.invoice_id),' ', '') from invoice as b)
        AND a.file_name = '`+filename+`'
        GROUP BY a.dt_code, a.invoice_id

    `, { type: orm.QueryTypes.INSERT })
    .then( () => {
          return "invoice done";  
    });
}

function insertProductFromExcelDump({filename=null}={})
{
    return orm.query(`
        insert into products(file_name, dt_code, invoice_id, product, product_name, quantity, product_price_cs, product_price_dz, product_price_pc, createdAt, updatedAt)

        select a.file_name, a.dt_code, a.invoice_id, a.product, a.product_name, a.quantity, a.product_price_cs, a.product_price_dz, a.product_price_pc, CURRENT_TIMESTAMP() as createdAt, CURRENT_TIMESTAMP() as updatedAt from excel_dump as a

        where REPLACE(CONCAT(a.dt_code, a.invoice_id),' ', '') NOT IN (select REPLACE(CONCAT(b.dt_code, b.invoice_id),' ', '') from products as b)
        AND a.file_name = '`+filename+`'

    `, { type: orm.QueryTypes.INSERT })
    .then( () => {
          return "product done";  
    }); 
}

function enhanceCheckExcelDumpToInvoice({filename=null}={})
{
    return orm.query(`
        select *,

        case 
            when ( Q.cash_memo_balance_amount > ed_balance_amount and Q.cash_memo_balance_amount is not null) 
                then 2 
            when ( Q.cash_memo_balance_amount <= ed_balance_amount and Q.cash_memo_balance_amount is not null) then 1 
            else 0 
        end as result

        from 
        (
            select 

            (select cash_memo_balance_amount from invoice as sq1 where sq1.dt_code = a.dt_code and sq1.invoice_id = a.invoice_id group by sq1.dt_code, sq1.invoice_id limit 1 ) as cash_memo_balance_amount,
            a.invoice_id as ed_invoice_id,
            a.dt_code as ed_dt_code,
            a.cash_memo_balance_amount as ed_balance_amount

            from excel_dump as a
            where REPLACE(CONCAT(a.dt_code, a.invoice_id),' ', '') IN (select REPLACE(CONCAT(b.dt_code, b.invoice_id),' ', '') from invoice as b)
            AND a.file_name = '`+filename+`'
            group by a.dt_code, a.invoice_id
        ) as Q
    `, { type: orm.QueryTypes.SELECT })
    .then(result => {
        return result;  
    });
}

function getProductFromExcelDump({invoice_id=null, dt_code=null}={})
{
    return orm.query("select * from excel_dump where invoice_id = '"+invoice_id+"' and dt_code = '"+dt_code+"'", { type: orm.QueryTypes.SELECT }
        ).then(result => {
          return result;  
    });   
}

function postUploadInvoice({records=null}={}) {
    // console.log(records.length);
    var array_args = [];
    var filtered_array_args = [];
    var array_map = [];
    var index = 0;

    // return Promise.map(records, (records, index)  => {
    //     // return base_repo.getOne({entity:Invoice, where:{invoice_id:invoice_id, dt_code: invoices.dt_codes[index]}, options:{throwIfEmpty:true}})
    //     // .catch(ResourceNotFoundError, err => {
    //     //     return err;
    //     // });
    // });
    async.eachSeries(records, function(eachRecord, callbackEach) {

        var invoice_payment_status = eachRecord[12].split('/');
        // console.log('invoice_id : '+eachRecord[8]);
        // console.log('dt_code : '+eachRecord[0]);
        // console.log('');
        console.log(eachRecord[0]);

        orm.query('SELECT id, cash_memo_balance_amount FROM invoice WHERE invoice_id = '+"'"+eachRecord[8]+"'"+' and dt_code = '+"'"+eachRecord[0]+"'"+'', { type: orm.QueryTypes.SELECT }
        ).then(result => {
            console.log(result);
            if(result[0] != null && result != null && result[0].cash_memo_balance_amount >= eachRecord[6])
            {
                console.log('masuk if');
                // Invoice.update(
                //     { 
                //         cash_memo_balance_amount: eachRecord[6]
                //     },
                //     { 
                //         where: 
                //         { 
                //             invoice_id: eachRecord[8],
                //             dt_code: eachRecord[0]
                //         }
                //     }
                // );
            }
            else if(result[0] != null && result != null && result[0].cash_memo_balance_amount <= eachRecord[6])
            {
                console.log('masuk else if');
            }
            else
            {
                console.log('masuk else');
                Invoice.create({distributorCode: eachRecord[0], le_code: eachRecord[1], outletCode: eachRecord[2], cash_memo_total_amount: eachRecord[5], cash_memo_balance_amount: eachRecord[6], cashmemo_type: eachRecord[7], invoice_id: eachRecord[8], invoice_sales_date: eachRecord[10], invoice_due_date: eachRecord[11], invoice_payment_status_paid: invoice_payment_status[0], invoice_payment_status_unpaid: invoice_payment_status[1]});
            }
        });

        setImmediate( () => {
            index++;
            callbackEach();
        });
        
    },function(errEach) {
        //console.log(errEach);
        // Done
    });

    return {code: 201, message: 'Upload berhasil, data sedang di proses'};
}

/**
@param {Number} invoiceId - the invoice id
@throws {ResourceNotFound} - if invoice not found
@returns {Promise.<Boolean>} resolved value is a boolean
*/
function isPaidFull({invoiceId=null, dt_code=null}={}) {
    console.log('isPaidFull. invoiceId:', invoiceId);
    return Promise.resolve()
    .then(()=>{
        return base_repo.getOne({entity:Invoice, where:{invoice_id:invoiceId, dt_code:dt_code}, options:{throwIfEmpty:true}}).then(invoice => {
            var is_paid = invoice.is_paid;
            console.log(sprintf(LOG_TAG + ' is invoice %1$s already paid? %2$s', invoiceId, is_paid));
            if(is_paid == "1")
            {
                return "1";
            }
            else
            {
                return "0";
            }
        });
    });
}

/**
@param {String} outletCode - the retailer outlet code
@returns {Promise.<Moment>} - resolved value is due date of the oldest unpaid invoice.
*/
function getOldestUnpaidDate({outletCode=null, distributorCode=null}={}) {
    console.log(LOG_TAG + ' get oldest unpaid invoice', outletCode);
    var where = {};
    var limit = 1;
    InvoiceQuery.isUnpaid({filter:where});
    // outletCode memiliki prioritas pertama.
    if (outletCode) {
        InvoiceQuery.outletCode({filter:where, outletCode:outletCode});
    }
    else if (distributorCode) {
        InvoiceQuery.distributorCode({filter:where, distributorCode:distributorCode});
    }
    var order = [
        ['invoice_due_date', 'ASC']
    ];

    return base_repo.get({entity:Invoice, where:where, order:order, limit:limit, options:{}})
    .then(result_obj => {
        let {result_list:result_list} = result_obj;
        console.log('### oldest unpaid invoices =>', result_list);
        var first_item = result_list.shift();
        if (first_item) {
            var duedate = moment(first_item.invoice_due_date);
            console.log('oldest unpaid invoice due date', duedate.format('LLL'));
            return duedate;
        }
        return null;
    });
}

/**
@param {String} outletCode - the retailer outlet code
@param {Moment} dueDateStart - start of due date window.
@param {Moment} dueDateEnd - end of due date window.
@returns {Promise} resolved value is an object {result_list:<Array>, count:<Integer>}
*/
function get({
    outletCode=null,
    distributorCode=null,
    leCodeFromSales=null,
    isPaid=false,
    isUnpaid=false,
    startDate=null,
    endDate=null,
    // dueDateEnd=moment(),
    // dueDateStart=moment().subtract(30,'day'),
    flag_start_due_date_unpaid=false,
    startPage=1, // starts from 1 not zero.
    itemsPerPage=15,
    sorting=null,
    sortingBy=null,
    filterBy=null, 
    leCodeDry=null, 
    leCodeIce=null, 
    leCodeUFS=null,
    usertype=null,
    platform=null
    }={}) {

    console.log(sprintf(LOG_TAG+' get invoices. outletCode:%1$s distributorCode:%2$s', outletCode, distributorCode));

//    var now = moment();
//    var now_human = now.format('YYYY-MM-DD');
//    var now_date = new Date(now.unix() * 1000);

    // normalize startPage to 1 if less than 1.
    startPage = (startPage < 1 ) ? 1 : startPage;

    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    var filter = {};
    var table = Invoice;
    var defaultQuery;
    var defaultQueryisPaid = 'where invoice.cash_memo_balance_amount = 0';
    var defaultQueryisUnPaid = 'where invoice.cash_memo_balance_amount > 0';
    var whereDateQuery;
    var whereOutletCodeQuery;
    var whereDistributorCodeQuery;
    var whereQuery;
    var orderByColumnQuery = '';
    var orderByAscOrDescQuery = '';
    var whereLeCodeDry = '';
    var whereLeCodeIce = '';
    var whereLeCodeUFS = '';
    var whereLeCodeDryIceUFS = '';
    var whereLeCodeFromSales = '';
    var groupByInvoiceCode = '';

    if(isPaid == true)
    {
        defaultQuery = 'where invoice.cash_memo_balance_amount = 0'
    }
    else if(isUnpaid == true)
    {
        defaultQuery = 'where invoice.cash_memo_balance_amount > 0'
    }
    else
    {
        defaultQuery = 'where invoice.cash_memo_balance_amount >= 0';
    }

    if(sorting == null)
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }

    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by invoice.invoice_sales_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'sales_date')
    {
        orderByColumnQuery = 'order by invoice.invoice_sales_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'paid_date')
    {
        orderByColumnQuery = 'order by transact_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'due_date')
    {
        orderByColumnQuery = 'order by invoice.invoice_due_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'invoice_id')
    {
        orderByColumnQuery = 'order by invoice.invoice_id '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'outlet_code')
    {
        orderByColumnQuery = 'order by invoice.outlet_code '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'is_paid')
    {
        orderByColumnQuery = 'order by is_paid '+orderByAscOrDescQuery;
    }

    if(filterBy != null)
    {
        if(filterBy == 'sales_date')
        {
            if(startDate && endDate){
                whereDateQuery = 'and invoice.invoice_sales_date >= '+"'"+startDate+"'"+' and invoice.invoice_sales_date <= '+"'"+endDate+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }
        }
        else if(filterBy == 'due_date')
        {
            if(startDate && endDate){
                whereDateQuery = 'and invoice.invoice_due_date >= '+"'"+startDate+"'"+' and invoice.invoice_due_date <= '+"'"+endDate+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }
            
        }
        else if(filterBy == 'paid_date')
        {
            if(startDate && endDate){
                whereDateQuery = 'and transactions.transact_date >= '+"'"+startDate+"'"+' and transactions.transact_date <= '+"'"+endDate+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }
            
        }
    }

    if(usertype == 0) {

        if(leCodeDry)
            whereLeCodeDry = "invoice.le_code = "+"'"+leCodeDry+"'";

        if(leCodeIce){
            if(leCodeDry){
                whereLeCodeIce = "or invoice.le_code = "+"'"+leCodeIce+"'";
            } else {
                whereLeCodeIce = "invoice.le_code = "+"'"+leCodeIce+"'";
            }
            
        }

        if(leCodeUFS){
            if(leCodeDry || leCodeIce){
                whereLeCodeUFS = "or invoice.le_code = "+"'"+leCodeUFS+"'";
            } else {
                whereLeCodeUFS = "invoice.le_code = "+"'"+leCodeUFS+"'";
            }
            
        }

        if(whereLeCodeDry == null && whereLeCodeIce == null && whereLeCodeUFS == null){
           
        } else {
            whereLeCodeDryIceUFS = 'and('+whereLeCodeDry+' '+whereLeCodeIce+' '+whereLeCodeUFS+')';
        }

        // groupByInvoiceCode = '';
    } 

    if (outletCode) {
        whereOutletCodeQuery = 'and invoice.outlet_code = '+outletCode;
    }
    if (distributorCode) {
        whereDistributorCodeQuery = 'and invoice.dt_code = '+distributorCode;
    }

    if(whereDateQuery == null)
        whereDateQuery = '';
    if(whereOutletCodeQuery == null)
        whereOutletCodeQuery = '';
    if(whereDistributorCodeQuery == null)
        whereDistributorCodeQuery = '';

    whereQuery = whereDateQuery+' '+whereOutletCodeQuery+' '+whereDistributorCodeQuery+' '+whereLeCodeDryIceUFS;
    
    // whereQuery = whereDateQuery +' '+whereOutletCodeQuery +' '+whereDistributorCodeQuery+ ' '+whereLeCodeDry+ ' '+whereLeCodeIce+ ' '+whereLeCodeUFS+' '+whereLeCodeFromSales;

    if(whereQuery == null)
    {
        whereQuery = '';
    }

    if(platform == 'web')
    {
        if(itemsPerPage == 0)
        {
            return orm.query('select ret.name as outlet_name, dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' and users.status = "0"'+'group by invoice.id '+orderByColumnQuery, { type: orm.QueryTypes.SELECT})
            .then(function(projects){
                // console.log(projects);
                var invoices = {};
                projects.forEach(result => {
                    var milisecond_transact_date = new Date(String(result.transact_date));
                    var milisecond_sales_date = new Date(String(result.invoice_sales_date));
                    var milisecond_due_date = new Date(String(result.invoice_due_date));

                    result.transact_date = milisecond_transact_date.getTime();
                    result.invoice_sales_date = milisecond_sales_date.getTime();
                    result.invoice_due_date = milisecond_due_date.getTime();
                    result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                    result.is_paid = (result.is_paid == 1) 
                    result.is_refunded = (result.is_refunded == 1) 
                    result.has_refund = (result.has_refund == 1) 
                    // processItem({item:result});
                });
                // var list = util.obj2list(invoices);
                // console.log('isi project');
                // console.log(projects.length);
                return Promise.resolve()
                .then(response => { 
                    return orm.query('select count(*) as total_items from (select count(invoice.invoice_id) as total_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code '+defaultQuery+' '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                    .then(function(response){
                        return {list:projects, total:response[0].total_items};
                    }); 
                });
            });
        }

        return orm.query('select ret.name as outlet_name, dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' and users.status = "0" '+'group by invoice.id '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
        .then(function(projects){
            // console.log('isi project');
            // console.log(projects);
            var invoices = {};
            projects.forEach(result => {
                var milisecond_transact_date = new Date(String(result.transact_date));
                var milisecond_sales_date = new Date(String(result.invoice_sales_date));
                var milisecond_due_date = new Date(String(result.invoice_due_date));

                result.transact_date = milisecond_transact_date.getTime();
                result.invoice_sales_date = milisecond_sales_date.getTime();
                result.invoice_due_date = milisecond_due_date.getTime();
                result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                result.is_paid = (result.is_paid == 1) 
                result.is_refunded = (result.is_refunded == 1) 
                result.has_refund = (result.has_refund == 1) 
                // processDistributor({item:result});
            });
            // var list = util.obj2list(invoices);
            //console.log('isi project');
            //console.log(projects);
            return Promise.resolve()
            .then(response => { 
                return orm.query('select count(*) as total_items from (select count(invoice.invoice_id) as total_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id '+defaultQuery+' '+whereQuery+' and users.status = "0" group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                .then(function(response){
                    return orm.query('select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id where invoice.cash_memo_balance_amount > 0 '+whereQuery+' and users.status = "0" group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                    .then(function(total_unpaid_items){
                        return {list:projects, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
                    }); 
                }); 
            });
        });
    }
    else if(platform == 'android')
    {
        if(usertype == 4)
        {
            if(leCodeFromSales == null)
                leCodeFromSales = '';

            return orm.query('select * from ((select ret.name as outlet_name, dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisUnPaid+' '+whereQuery+' and users.status = "0" '+'group by invoice.id '+'order by invoice.invoice_due_date asc'+' limit '+limit+' offset '+offset+') UNION (select ret.name as outlet_name, dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisPaid+' '+whereQuery+' and users.status = "0" '+'group by invoice.id '+'order by invoice.invoice_due_date desc'+' limit '+limit+' offset '+offset+')) as DataCountPaidAndUnPaid inner join distributors ON distributors.dt_code = DataCountPaidAndUnPaid.distributorCode inner join users ON users.distributor_id = distributors.distributor_id where users.type = 0 and (users.nama_toko like "%'+leCodeFromSales+'%" COLLATE utf8_general_ci or DataCountPaidAndUnPaid.le_code like "%'+leCodeFromSales+'%")', { replacements: { search_by_nama_toko_or_le_code: leCodeFromSales}, type: orm.QueryTypes.SELECT})
            .then(function(projects){
                // console.log('isi project');
                // console.log(projects);
                var invoices = {};
                projects.forEach(result => {
                    var milisecond_transact_date = new Date(String(result.transact_date));
                    var milisecond_sales_date = new Date(String(result.invoice_sales_date));
                    var milisecond_due_date = new Date(String(result.invoice_due_date));

                    result.transact_date = milisecond_transact_date.getTime();
                    result.invoice_sales_date = milisecond_sales_date.getTime();
                    result.invoice_due_date = milisecond_due_date.getTime();
                    result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                    result.is_paid = (result.is_paid == 1) 
                    result.is_refunded = (result.is_refunded == 1) 
                    result.has_refund = (result.has_refund == 1) 
                    // processDistributor({item:result});
                });
                // var list = util.obj2list(invoices);
                // console.log('isi project');
                // console.log(projects);
                return Promise.resolve()
                .then(response => { 
                    return orm.query('select count(le_code) as total_items from ((select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisUnPaid+' '+whereQuery+' and users.status = "0" '+'group by invoice.id '+'order by invoice.invoice_due_date asc'+' limit '+limit+' offset '+offset+') UNION (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisPaid+' '+whereQuery+' and users.status = "0" '+'group by invoice.id '+'order by invoice.invoice_due_date desc'+' limit '+limit+' offset '+offset+')) as DataCountPaidAndUnPaid inner join distributors ON distributors.dt_code = DataCountPaidAndUnPaid.distributorCode inner join users ON users.distributor_id = distributors.distributor_id where users.type = 0 and (users.nama_toko like "%'+leCodeFromSales+'%" COLLATE utf8_general_ci or DataCountPaidAndUnPaid.le_code like "%'+leCodeFromSales+'%")', { replacements: { search_by_nama_toko_or_le_code: leCodeFromSales}, type: orm.QueryTypes.SELECT})
                    .then(function(response){
                        return orm.query('select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id where invoice.cash_memo_balance_amount > 0 and users.status = "0" '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                        .then(function(total_unpaid_items){
                            return {list:projects, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
                        }); 
                    }); 
                });
            });
        }
        else
        {
            return orm.query('(select ret.name as outlet_name, dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisUnPaid+' '+whereQuery+' and users.status = "0" '+'group by invoice.id '+'order by invoice.invoice_due_date asc'+' limit '+limit+' offset '+offset+') UNION (select ret.name as outlet_name, dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisPaid+' '+whereQuery+' and users.status = "0" '+'group by invoice.id '+'order by invoice.invoice_due_date desc'+' limit '+limit+' offset '+offset+')', { type: orm.QueryTypes.SELECT})
            .then(function(projects){
                // console.log('isi project');
                // console.log(projects);
                var invoices = {};
                projects.forEach(result => {
                    var milisecond_transact_date = new Date(String(result.transact_date));
                    var milisecond_sales_date = new Date(String(result.invoice_sales_date));
                    var milisecond_due_date = new Date(String(result.invoice_due_date));

                    result.transact_date = milisecond_transact_date.getTime();
                    result.invoice_sales_date = milisecond_sales_date.getTime();
                    result.invoice_due_date = milisecond_due_date.getTime();
                    result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                    result.is_paid = (result.is_paid == 1) 
                    result.is_refunded = (result.is_refunded == 1) 
                    result.has_refund = (result.has_refund == 1) 
                    // processDistributor({item:result});
                });
                // var list = util.obj2list(invoices);
                //console.log('isi project');
                //console.log(projects);
                return Promise.resolve()
                .then(response => { 
                    return orm.query('select count(le_code) as total_items from ((select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id '+defaultQueryisUnPaid+' '+whereQuery+' and users.status = "0" '+'group by invoice.id '+'order by invoice.invoice_due_date asc'+') UNION (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id '+defaultQueryisPaid+' '+whereQuery+' and users.status = "0" '+'group by invoice.id '+'order by invoice.invoice_due_date desc'+')) as DataCountPaidAndUnPaid', { type: orm.QueryTypes.SELECT})
                    .then(function(response){
                        return orm.query('select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code left join retailers as ret ON ret.`le_code_dry` = invoice.`le_code` or ret.`le_code_ice` = invoice.`le_code` or ret.`le_code_ufs` = invoice.`le_code` left join users as users ON users.retailer_id = ret.retailer_id where invoice.cash_memo_balance_amount > 0 and users.status = "0" '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                        .then(function(total_unpaid_items){
                            return {list:projects, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
                        }); 
                    }); 
                });
            });
        }
        
    }
    else if(platform == 'ios')
    {

    }
    else
    {

    }
    
    // return base_repo.get({entity:Invoice, 
    //     where:filter, 
    //     limit:1, 
    //     offset:offset, 
    //     options:{}, 
    //     include: [{ attributes: ['transact_date'], model: Transaction, required: false}], 
    //     order:[sortingBy]})
    // .then(result_obj => {
    //     // console.log('isi object');
    //     // console.log(result_obj);
    //     let {result_list:result_list, total:total} = result_obj;

    //     // console.log('num rows:', result_list.length);
    //     var invoices = {};
    //     result_list.forEach(result => {
    //         var invoice_id = result.invoice_id;
    //         var invoice = invoices[invoice_id];
    //         if (!invoice) {
    //             invoice = invoices[invoice_id] = result;
    //         }
    //         processItem({item:invoice});
    //     });
    //     // console.log('sebelum masuk util');
    //     // console.log(invoices);
    //     // convert to object map to list
    //     var list = util.obj2list(invoices);

    //     // console.log('sesudah masuk util');
    //     // console.log(list);

    //     console.log('num invoices:', list.length, 'total-result-set:', total);
    //     return {list:list, total:total};
    // });
}

/**
@param {Number} id - the invoice id
@param {Object} options - the options
@returns {Promise.<Invoice>} resolved value is to invoice object.
*/
function getOne({id=null, dt_code=null, options=null}) {

    // return base_repo.getOne({entity:Invoice, where:{invoice_id:id}, options:options})
    // .then(p_result => {

        return orm.query('select invoice.invoice_id, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, invoice.invoice_details, transactions.transact_date from invoice left outer join transactions on invoice.invoice_id = transactions.invoice_id where invoice.invoice_id = "'+id+'" AND invoice.dt_code = '+dt_code, { type: orm.QueryTypes.SELECT})
        .then(function(data){
            if(data.length <= 0)
            {
                throw new Error('invoice tidak ditemukan.');
            }
            data.forEach(result => {
                var milisecond_transact_date = new Date(String(result.transact_date));
                var milisecond_sales_date = new Date(String(result.invoice_sales_date));
                var milisecond_due_date = new Date(String(result.invoice_due_date));

                if(isNaN(milisecond_transact_date))
                {
                    delete result.transact_date;
                }
                else
                {
                    result.transact_date = milisecond_transact_date.getTime();
                }
                
                result.invoice_sales_date = milisecond_sales_date.getTime();
                result.invoice_due_date = milisecond_due_date.getTime();

                result.is_paid = (result.is_paid == 1) 
                result.is_refunded = (result.is_refunded == 1) 
                result.has_refund = (result.has_refund == 1)
                 
                // processItem({item:result});
            });
            // processItemVersiBaru({invoice_id:id});
            return orm.query('select * from products where invoice_id = "'+id+'" and dt_code = '+dt_code, { type: orm.QueryTypes.SELECT})
            .then(products => {
                if(products <= 0)
                {
                    throw new Error('produk tidak ditemukan.');
                }
                var check_last_number_invoice_id = id.substr(-1);
                // console.log('last number invoice id');
                // console.log(check_last_number_invoice_id);
                return orm.query('select transaction_id as id, amount, DATE_FORMAT(convert_tz(transact_date, @@session.time_zone,"+07:00"),"%Y-%m-%d %H:%i:%s") AS date_time from transactions where invoice_code = "'+dt_code+id+'"', { type: orm.QueryTypes.SELECT})
                .then(transactions => {

                    if(check_last_number_invoice_id == 0)
                    {
                        var check_invoice_id = id.substr(0, id.length - 1);
                        return Promise.resolve()
                        .then(() => {
                            return Invoice.findAll({
                                attributes: ['invoice_id', 'cash_memo_total_amount', 'cash_memo_balance_amount'],
                                where: {
                                    invoice_id: {
                                      $like: '%'+check_invoice_id+'1'+'%'
                                    },
                                    dt_code: dt_code
                                },
                            }); 
                        })
                        .then(response => {
                            var item = data;
                            item[0].products = products;
                            item[0].retur = response;
                            item[0].transactions = transactions;
                            // console.log('cek item');
                            // console.log(item);
                            return item[0];
                        });
                        
                    }
                    else
                    {
                        var item = data;
                        item[0].products = products;
                        item[0].transactions = transactions;
                        return item[0];
                    }
                });
            });
            
        // });
        
    })
    .catch(ResourceNotFoundError, err => {
        return err;
    });
}

/**
Processes each invoice object.
@param {Invoice} item - the invoice
*/
function processItem({item=null}={}) {

    // console.log('processItem. item:', item);
    var details = item.invoice_details;
    var products = [];
    try {
        products = JSON.parse(details);
        
    } catch(err) {
        console.warn('error parsing details json.', err);
        console.warn('raw json:', details);
    }
    // console.log('products:', products);
    item.products = products;
}

function processDistributor({item=null}={}) {

    console.log('processDistributor. item:', item);
    var details = item.distributor;
    var distributors = [];
    console.log('processDistributor');
    console.log(details);
    try {
        distributors = JSON.parse(details);
        
    } catch(err) {
        console.warn('error parsing details json.', err);
        console.warn('raw json:', details);
    }
    // console.log('products:', products);
    item.distributor = distributors;
}

/**
Gets the sum amount of unpaid invoices
@param {Number} invoiceId
@returns {Promise} - resolved to: {sum:<Number>}
*/
function getSumUnpaidAmount({le_code_dry=null,le_code_ice=null,le_code_ufs=null}) {
    console.log(LOG_TAG + 'getSumUnpaidAmount');
    return Promise.resolve(true)
    .then(p_result => {
        // InvoiceQuery.outletCode({filter:filter, outletCode:outletCode});
        // InvoiceQuery.isUnpaid({filter:filter});
        return Invoice.sum('cash_memo_balance_amount', {where: {cash_memo_balance_amount: {$gte:0},$or: [
                            {
                                le_code: le_code_dry
                            },
                            {
                                le_code: le_code_ice
                            },
                            {
                                le_code: le_code_ufs
                            }
                        ]}}).then(function(sum) {
            console.log("Sum of unpaid: " + sum);
            return {sum:sum};
        });
    });
}

function request_retur({invoice_id=null}={}) {
    console.log('retur id');
    console.log(invoice_id);
    return Invoice.update(
        { approval_date: Date(), status: 0 },
        { where: { invoice_id: invoice_id }}
    )
    .then(bankaccount => {
        return {code: 201, message: 'Request retur berhasil.'};
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
}

function getReturOld({
    outletCode=null,
    distributorCode=null,
    isPaid=false,
    isUnpaid=false,
    dueDateEnd=null,
    dueDateStart=null,
    // dueDateEnd=moment(),
    // dueDateStart=moment().subtract(30,'day'),
    flag_start_due_date_unpaid=false,
    startPage=1, // starts from 1 not zero.
    itemsPerPage=15,
    sorting=null,
    sortingBy=null
    }={}) {

    console.log(sprintf(LOG_TAG+' get invoices. outletCode:%1$s distributorCode:%2$s', outletCode, distributorCode));

//    var now = moment();
//    var now_human = now.format('YYYY-MM-DD');
//    var now_date = new Date(now.unix() * 1000);

    // normalize startPage to 1 if less than 1.
    startPage = (startPage < 1 ) ? 1 : startPage;

    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    var filter = {};
    InvoiceQuery.queryRetur({filter:filter, cashmemo_type:'Sales Return'});
    if(sorting == null)
    {
        sorting = 'desc';
    }
    if(sortingBy == null)
    {
        sortingBy = 'invoice_sales_date';
    }
    if (dueDateStart && dueDateEnd) {
        const due_date_max_ts = dueDateEnd.unix()*1000; // in millis
        const due_date_min_ts = dueDateStart.unix()*1000; // in millis
        InvoiceQuery.duedate({filter:filter, start:due_date_min_ts, end:due_date_max_ts});
    }

    if (outletCode) {
        InvoiceQuery.outletCode({filter:filter, outletCode:outletCode});
    }
    if (isPaid) {
        InvoiceQuery.isPaid({filter:filter});
    }
    if (isUnpaid) {
        InvoiceQuery.isUnpaid({filter:filter});
    }
    if (distributorCode) {
        InvoiceQuery.distributorCode({filter:filter, distributorCode:distributorCode});
    }
    console.log('isi filter nya');
    console.log('invoice:get. filter:', filter);
    
    return base_repo.get({entity:Invoice, where:filter, limit:limit, order:[[sortingBy, sorting]], offset:offset, options:{}})
    .then(result_obj => {

        let {result_list:result_list, total:total} = result_obj;

        // console.log('num rows:', result_list.length);
        var invoices = {};
        result_list.forEach(result => {
            var invoice_id = result.invoice_id;
            var invoice = invoices[invoice_id];
            if (!invoice) {
                invoice = invoices[invoice_id] = result;
            }
            processItem({item:invoice});
        });
        // convert to object map to list
        var list = util.obj2list(invoices);
        console.log('num invoices:', list.length, 'total-result-set:', total);
        return {list:list, total:total};
    });
}

function getRetur({
    outletCode=null,
    distributorCode=null,
    leCodeFromSales=null,
    isPaid=false,
    isUnpaid=false,
    startDate=null,
    endDate=null,
    // dueDateEnd=moment(),
    // dueDateStart=moment().subtract(30,'day'),
    flag_start_due_date_unpaid=false,
    startPage=1, // starts from 1 not zero.
    itemsPerPage=15,
    sorting=null,
    sortingBy=null,
    filterBy=null, 
    leCodeDry=null, 
    leCodeIce=null, 
    leCodeUFS=null,
    usertype=null,
    platform=null
    }={}) {

    console.log(sprintf(LOG_TAG+' get invoices. outletCode:%1$s distributorCode:%2$s', outletCode, distributorCode));

//    var now = moment();
//    var now_human = now.format('YYYY-MM-DD');
//    var now_date = new Date(now.unix() * 1000);

    // normalize startPage to 1 if less than 1.
    startPage = (startPage < 1 ) ? 1 : startPage;

    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    var filter = {};
    var table = Invoice;
    var defaultQuery;
    var defaultQueryisPaid = 'where invoice.cash_memo_balance_amount = 0';
    var defaultQueryisUnPaid = 'where invoice.cash_memo_balance_amount > 0';
    var whereDateQuery;
    var whereOutletCodeQuery;
    var whereDistributorCodeQuery;
    var whereQuery;
    var orderByColumnQuery = '';
    var orderByAscOrDescQuery = '';
    var whereLeCodeDry = '';
    var whereLeCodeIce = '';
    var whereLeCodeUFS = '';
    var whereLeCodeDryIceUFS = '';
    var whereLeCodeFromSales = '';
    var groupByInvoiceCode = '';

    defaultQuery = 'where invoice.cash_memo_balance_amount < 0';

    if(sorting == null)
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }

    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by invoice.invoice_sales_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'sales_date')
    {
        orderByColumnQuery = 'order by invoice.invoice_sales_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'paid_date')
    {
        orderByColumnQuery = 'order by transactions.transact_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'due_date')
    {
        orderByColumnQuery = 'order by invoice.invoice_due_date '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'invoice_id')
    {
        orderByColumnQuery = 'order by invoice.invoice_id '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'outlet_code')
    {
        orderByColumnQuery = 'order by invoice.outlet_code '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'is_paid')
    {
        orderByColumnQuery = 'order by is_paid '+orderByAscOrDescQuery;
    }

    if(filterBy != null)
    {
        if(filterBy == 'sales_date')
        {
            if(startDate && endDate){
                whereDateQuery = 'and invoice.invoice_sales_date >= '+"'"+startDate+"'"+' and invoice.invoice_sales_date <= '+"'"+endDate+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }
        }
        else if(filterBy == 'due_date')
        {
            if(startDate && endDate){
                whereDateQuery = 'and invoice.invoice_due_date >= '+"'"+startDate+"'"+' and invoice.invoice_due_date <= '+"'"+endDate+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }
            
        }
        else if(filterBy == 'paid_date')
        {
            if(startDate && endDate){
                whereDateQuery = 'and transactions.transact_date >= '+"'"+startDate+"'"+' and transactions.transact_date <= '+"'"+endDate+" 23:59:59'";
            }
            else{
                throw new Error('Masukkan tanggal awal dan tanggal akhir.');
            }
            
        }
    }

    if(usertype == 0) {

        if(leCodeDry)
            whereLeCodeDry = "invoice.le_code = "+"'"+leCodeDry+"'";

        if(leCodeIce){
            if(leCodeDry){
                whereLeCodeIce = "or invoice.le_code = "+"'"+leCodeIce+"'";
            } else {
                whereLeCodeIce = "invoice.le_code = "+"'"+leCodeIce+"'";
            }
            
        }

        if(leCodeUFS){
            if(leCodeIce){
                whereLeCodeUFS = "or invoice.le_code = "+"'"+leCodeUFS+"'";
            } else {
                whereLeCodeUFS = "invoice.le_code = "+"'"+leCodeUFS+"'";
            }
            
        }

        if(whereLeCodeDry == null && whereLeCodeIce == null && whereLeCodeUFS == null){
           
        } else {
            whereLeCodeDryIceUFS = 'and('+whereLeCodeDry+' '+whereLeCodeIce+' '+whereLeCodeUFS+')';
        }

        // groupByInvoiceCode = '';
    } 

    if (outletCode) {
        whereOutletCodeQuery = 'and invoice.outlet_code = '+outletCode;
    }
    if (distributorCode) {
        whereDistributorCodeQuery = 'and invoice.dt_code = '+distributorCode;
    }

    if(whereDateQuery == null)
        whereDateQuery = '';
    if(whereOutletCodeQuery == null)
        whereOutletCodeQuery = '';
    if(whereDistributorCodeQuery == null)
        whereDistributorCodeQuery = '';

    whereQuery = whereDateQuery+' '+whereOutletCodeQuery+' '+whereDistributorCodeQuery+' '+whereLeCodeDryIceUFS;
    
    // whereQuery = whereDateQuery +' '+whereOutletCodeQuery +' '+whereDistributorCodeQuery+ ' '+whereLeCodeDry+ ' '+whereLeCodeIce+ ' '+whereLeCodeUFS+' '+whereLeCodeFromSales;

    if(whereQuery == null)
    {
        whereQuery = '';
    }

    if(platform == 'web')
    {
        if(itemsPerPage == 0)
        {
            return orm.query('select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' '+'group by invoice.id '+orderByColumnQuery, { type: orm.QueryTypes.SELECT})
            .then(function(projects){
                // console.log(projects);
                var invoices = {};
                projects.forEach(result => {
                    var milisecond_transact_date = new Date(String(result.transact_date));
                    var milisecond_sales_date = new Date(String(result.invoice_sales_date));
                    var milisecond_due_date = new Date(String(result.invoice_due_date));

                    result.transact_date = milisecond_transact_date.getTime();
                    result.invoice_sales_date = milisecond_sales_date.getTime();
                    result.invoice_due_date = milisecond_due_date.getTime();
                    result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                    result.is_paid = (result.is_paid == 1) 
                    result.is_refunded = (result.is_refunded == 1) 
                    result.has_refund = (result.has_refund == 1) 
                    // processItem({item:result});
                });
                // var list = util.obj2list(invoices);
                // console.log('isi project');
                // console.log(projects.length);
                return Promise.resolve()
                .then(response => { 
                    return orm.query('select count(*) as total_items from (select count(invoice.invoice_id) as total_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code '+defaultQuery+' '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                    .then(function(response){
                        return {list:projects, total:response[0].total_items};
                    }); 
                });
            });
        }

        return orm.query('select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' '+'group by invoice.id '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
        .then(function(projects){
            // console.log('isi project');
            // console.log(projects);
            var invoices = {};
            projects.forEach(result => {
                var milisecond_transact_date = new Date(String(result.transact_date));
                var milisecond_sales_date = new Date(String(result.invoice_sales_date));
                var milisecond_due_date = new Date(String(result.invoice_due_date));

                result.transact_date = milisecond_transact_date.getTime();
                result.invoice_sales_date = milisecond_sales_date.getTime();
                result.invoice_due_date = milisecond_due_date.getTime();
                result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                result.is_paid = (result.is_paid == 1) 
                result.is_refunded = (result.is_refunded == 1) 
                result.has_refund = (result.has_refund == 1) 
                // processDistributor({item:result});
            });
            // var list = util.obj2list(invoices);
            console.log('isi project');
            console.log(projects);
            return Promise.resolve()
            .then(response => { 
                return orm.query('select count(*) as total_items from (select count(invoice.invoice_id) as total_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code '+defaultQuery+' '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                .then(function(response){
                    return orm.query('select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code where invoice.cash_memo_balance_amount > 0 '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                    .then(function(total_unpaid_items){
                        return {list:projects, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
                    }); 
                }); 
            });
        });
    }
    else if(platform == 'android')
    {
        if(usertype == 4)
        {
            if(leCodeFromSales == null)
                leCodeFromSales = '';

            return orm.query('select * from ((select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisUnPaid+' '+whereQuery+' '+'group by invoice.id '+'order by invoice.invoice_due_date asc'+' limit '+limit+' offset '+offset+') UNION (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisPaid+' '+whereQuery+' '+'group by invoice.id '+'order by invoice.invoice_due_date desc'+' limit '+limit+' offset '+offset+')) as DataCountPaidAndUnPaid inner join distributors ON distributors.dt_code = DataCountPaidAndUnPaid.distributorCode inner join users ON users.distributor_id = distributors.distributor_id where users.type = 0 and (users.nama_toko like "%'+leCodeFromSales+'%" COLLATE utf8_general_ci or DataCountPaidAndUnPaid.le_code like "%'+leCodeFromSales+'%")', { replacements: { search_by_nama_toko_or_le_code: leCodeFromSales}, type: orm.QueryTypes.SELECT})
            .then(function(projects){
                // console.log('isi project');
                // console.log(projects);
                var invoices = {};
                projects.forEach(result => {
                    var milisecond_transact_date = new Date(String(result.transact_date));
                    var milisecond_sales_date = new Date(String(result.invoice_sales_date));
                    var milisecond_due_date = new Date(String(result.invoice_due_date));

                    result.transact_date = milisecond_transact_date.getTime();
                    result.invoice_sales_date = milisecond_sales_date.getTime();
                    result.invoice_due_date = milisecond_due_date.getTime();
                    result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                    result.is_paid = (result.is_paid == 1) 
                    result.is_refunded = (result.is_refunded == 1) 
                    result.has_refund = (result.has_refund == 1) 
                    // processDistributor({item:result});
                });
                // var list = util.obj2list(invoices);
                // console.log('isi project');
                // console.log(projects);
                return Promise.resolve()
                .then(response => { 
                    return orm.query('select count(le_code) as total_items from ((select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisUnPaid+' '+whereQuery+' '+'group by invoice.id '+'order by invoice.invoice_due_date asc'+' limit '+limit+' offset '+offset+') UNION (select dist.name, dist.dt_code, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQueryisPaid+' '+whereQuery+' '+'group by invoice.id '+'order by invoice.invoice_due_date desc'+' limit '+limit+' offset '+offset+')) as DataCountPaidAndUnPaid inner join distributors ON distributors.dt_code = DataCountPaidAndUnPaid.distributorCode inner join users ON users.distributor_id = distributors.distributor_id where users.type = 0 and (users.nama_toko like "%'+leCodeFromSales+'%" COLLATE utf8_general_ci or DataCountPaidAndUnPaid.le_code like "%'+leCodeFromSales+'%")', { replacements: { search_by_nama_toko_or_le_code: leCodeFromSales}, type: orm.QueryTypes.SELECT})
                    .then(function(response){
                        return orm.query('select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code where invoice.cash_memo_balance_amount > 0 '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                        .then(function(total_unpaid_items){
                            return {list:projects, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
                        }); 
                    }); 
                });
            });
        }
        else
        {
            return orm.query('select dist.name, dist.dt_code, invoice.status, invoice.invoice_id, invoice.le_code, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, MAX(transactions.transact_date) as transact_date, SUM(transactions.amount) as paid_amount, transactions.invoice_code from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code inner join distributors as dist ON dist.dt_code = invoice.dt_code '+defaultQuery+' '+whereQuery+' '+'group by invoice.id '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
            .then(function(projects){
                // console.log('isi project');
                // console.log(projects);
                var invoices = {};
                var default_status = -1;
                projects.forEach(result => {
                    var milisecond_transact_date = new Date(String(result.transact_date));
                    var milisecond_sales_date = new Date(String(result.invoice_sales_date));
                    var milisecond_due_date = new Date(String(result.invoice_due_date));

                    result.transact_date = milisecond_transact_date.getTime();
                    result.invoice_sales_date = milisecond_sales_date.getTime();
                    result.invoice_due_date = milisecond_due_date.getTime();
                    result.distributor = JSON.parse(`{"dt_code": "${result.dt_code}", "name": "${result.name}"}`);

                    result.is_paid = (result.is_paid == 1) 
                    result.is_refunded = (result.is_refunded == 1) 
                    result.has_refund = (result.has_refund == 1) 
                    result.status = Number(default_status);
                    // processDistributor({item:result});
                });
                // var list = util.obj2list(invoices);
                console.log('isi project');
                console.log(projects);
                return Promise.resolve()
                .then(response => { 
                    return orm.query('select count(*) as total_items from (select count(invoice.invoice_id) as total_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code '+defaultQuery+' '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                    .then(function(response){
                        return orm.query('select count(*) as total_unpaid_items from (select count(invoice.invoice_id) as total_unpaid_items from invoice left outer join transactions on REPLACE(CONCAT(invoice.dt_code, invoice.invoice_id), " ", "") = transactions.invoice_code where invoice.cash_memo_balance_amount > 0 '+whereQuery+' group by invoice.id) as T', { type: orm.QueryTypes.SELECT})
                        .then(function(total_unpaid_items){
                            return {list:projects, total:response[0].total_items, total_unpaid:total_unpaid_items[0].total_unpaid_items};
                        }); 
                    }); 
                });
            });
        }
        
    }
    else if(platform == 'ios')
    {

    }
    else
    {

    }
}

function getOneRetur({id=null, dt_code=null, options=null}) {

    // return base_repo.getOne({entity:Invoice, where:{invoice_id:id}, options:options})
    // .then(p_result => {

        return orm.query('select invoice.invoice_id, invoice.dt_code as distributorCode, invoice.outlet_code as outletCode, invoice.cash_memo_total_amount, invoice.cash_memo_balance_amount, invoice.cashmemo_type, invoice.invoice_payment_status_paid, invoice.invoice_payment_status_unpaid, invoice.invoice_sales_date, invoice.invoice_due_date, IF(invoice.cash_memo_balance_amount = 0, true, false) as is_paid, false as has_refund, false as is_refunded, invoice.invoice_details, transactions.transact_date from invoice left outer join transactions on invoice.invoice_id = transactions.invoice_id where invoice.invoice_id = "'+id+'" AND invoice.dt_code = '+dt_code, { type: orm.QueryTypes.SELECT})
        .then(function(data){
            if(data.length <= 0)
            {
                throw new Error('invoice tidak ditemukan.');
            }
            data.forEach(result => {
                var milisecond_transact_date = new Date(String(result.transact_date));
                var milisecond_sales_date = new Date(String(result.invoice_sales_date));
                var milisecond_due_date = new Date(String(result.invoice_due_date));

                if(isNaN(milisecond_transact_date))
                {
                    delete result.transact_date;
                }
                else
                {
                    result.transact_date = milisecond_transact_date.getTime();
                }
                
                result.invoice_sales_date = milisecond_sales_date.getTime();
                result.invoice_due_date = milisecond_due_date.getTime();

                result.is_paid = (result.is_paid == 1) 
                result.is_refunded = (result.is_refunded == 1) 
                result.has_refund = (result.has_refund == 1)
                 
                // processItem({item:result});
            });
            // processItemVersiBaru({invoice_id:id});
            return orm.query('select * from products where invoice_id = "'+id+'" and dt_code = '+dt_code, { type: orm.QueryTypes.SELECT})
            .then(products => {
                if(products <= 0)
                {
                    throw new Error('produk tidak ditemukan.');
                }

                var item = data;
                item[0].products = products;
                return item[0];
            });
            
        // });
        
    })
    .catch(ResourceNotFoundError, err => {
        return err;
    });
}

function getAllReturRequest({
    outletCode=null,
    distributorCode=null,
    isPaid=false,
    isUnpaid=false,
    dueDateEnd=null,
    dueDateStart=null,
    // dueDateEnd=moment(),
    // dueDateStart=moment().subtract(30,'day'),
    flag_start_due_date_unpaid=false,
    startPage=1, // starts from 1 not zero.
    itemsPerPage=15,
    status=null
    }={}) {

    console.log(sprintf(LOG_TAG+' get invoices. outletCode:%1$s distributorCode:%2$s', outletCode, distributorCode));

//    var now = moment();
//    var now_human = now.format('YYYY-MM-DD');
//    var now_date = new Date(now.unix() * 1000);

    // normalize startPage to 1 if less than 1.
    startPage = (startPage < 1 ) ? 1 : startPage;

    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    var filter = {};
    InvoiceQuery.queryAllReturRequest({filter:filter, cashmemo_type:'Sales Return'});
    console.log('di service');
    console.log(status);
    if(status)
    {

        InvoiceQuery.querySortReturRequest({filter:filter, cashmemo_type:'Sales Return', status: status});
    }

    if (dueDateStart && dueDateEnd) {
        const due_date_max_ts = dueDateEnd.unix()*1000; // in millis
        const due_date_min_ts = dueDateStart.unix()*1000; // in millis
        InvoiceQuery.duedate({filter:filter, start:due_date_min_ts, end:due_date_max_ts});
    }

    if (outletCode) {
        InvoiceQuery.outletCode({filter:filter, outletCode:outletCode});
    }
    if (isPaid) {
        InvoiceQuery.isPaid({filter:filter});
    }
    if (isUnpaid) {
        InvoiceQuery.isUnpaid({filter:filter});
    }
    if (distributorCode) {
        InvoiceQuery.distributorCode({filter:filter, distributorCode:distributorCode});
    }
    // console.log('invoice:get. filter:', filter);
    
    return base_repo.get({entity:Invoice, where:filter, limit:limit, offset:offset, options:{}})
    .then(result_obj => {

        let {result_list:result_list, total:total} = result_obj;

        // console.log('num rows:', result_list.length);
        var invoices = {};
        result_list.forEach(result => {
            var invoice_id = result.invoice_id;
            var invoice = invoices[invoice_id];
            if (!invoice) {
                invoice = invoices[invoice_id] = result;
            }
            processItem({item:invoice});
        });
        // convert to object map to list
        var list = util.obj2list(invoices);
        // console.log('num invoices:', list.length, 'total-result-set:', total);
        return {list:list, total:total};
    });
}

var obj = {}
obj.get = get
obj.getOne = getOne
obj.getTask = getTask
obj.getCheckProcessInvoice = getCheckProcessInvoice
obj.getDataFromExcelDump = getDataFromExcelDump
obj.enhanceGetDataFromExcelDump = enhanceGetDataFromExcelDump
obj.checkExcelDumpToInvoice = checkExcelDumpToInvoice
obj.enhanceCheckExcelDumpToInvoice = enhanceCheckExcelDumpToInvoice
obj.getProductFromExcelDump = getProductFromExcelDump
obj.getOneRetur = getOneRetur
obj.getOldestUnpaidDate = getOldestUnpaidDate
obj.isPaidFull = isPaidFull
obj.getSumUnpaidAmount = getSumUnpaidAmount;
obj.getRetur = getRetur;
obj.request_retur = request_retur;
obj.getAllReturRequest = getAllReturRequest;
obj.postUploadInvoice = postUploadInvoice;
obj.insertInvoiceFromExcelDump = insertInvoiceFromExcelDump;
obj.insertProductFromExcelDump = insertProductFromExcelDump;
export default obj


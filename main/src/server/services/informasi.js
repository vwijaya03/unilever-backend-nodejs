import _ from 'lodash'
import { sprintf } from 'sprintf-js'
import crypto from 'crypto'
import Promise from 'bluebird'
import Sequelize from 'sequelize'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { orm } from '@/server/services/mariadb'
import { Informasi } from '@/models/orm/index'
import base_repo from '@/server/repositories/base'

import service_logger from '@/server/services/logger'

import test_users from '@/test/data/users'
import properties from '@/properties'
import moment from 'moment'

const LOG_TAG = '[services:user] '
const log = service_logger.getLogger({name: 'service', service:'informasi dan promosi'});

function getOne({id=null}={}) {
    return orm.query('select * from informasis where informasis.id = '+id, { type: orm.QueryTypes.SELECT})
    .then(function(response){
        if (response == null || response == undefined) {
            return {};
        }
        return response[0];
    }); 
}

/**
@param {Object} filter
@param {Object} options
@param {Boolean} options.throwIfEmpty - throw error if resultset is empty
@throws { UserNotFoundError } - if throwIfEmpty is true and empty result set.
@returns {Promise} - resolved to user object
*/
function get(filter, options) {
    options = options || {};
    log.debug('[service-informasi-dan-promosi] get ', filter);
    return Promise.resolve()
    .then(()=>{
        // var users = _.find(test_users, filter);
        if (filter) {
            return Informasi.findAll(
                {
                    where: filter
                }
            );
        } else {
            return Informasi.findAll();
        }
    })
    .then(result_set => {
        // console.log('############', filter, result_set);
        if (!result_set || Object.keys(result_set).length == 0) {
            if (options.throwIfEmpty) {
                throw new UserNotFoundError();
            }
        }
        return result_set;
    });
}

function createInformasiDanPromosi(
{
    title=null,
    content=null,
    image=null,
    createdAt=null,
    updatedAt=null
}={}) {

    var args =
    {
    title:            title,
    content:          content,
    image:            image,
    createdAt:        createdAt,
    updatedAt:        updatedAt
    }

    log.debug('execute create informasi dan promosi record in db.', args);
    
    return Informasi.create(args).then(informasi => {
        return informasi;
    })
    .catch(Sequelize.UniqueConstraintError, err => {
        var err_message = 'unique constraint error';
        try {
            err_message = err.errors[0].message;
        } catch (err2) {
        }
        log.error('unique constraint error:', err_message);
        throw new DbRecordCreationError(err_message);
    });
    
}

var obj = {}

obj.get = get
obj.createInformasiDanPromosi = createInformasiDanPromosi
obj.getOne = getOne

export default obj


/**

References:

How to enable CORS:
https://github.com/swagger-api/swagger-ui/blob/master/README.md#cors-support
https://github.com/expressjs/cors


**/


import { extend } from  'util'
import express from 'express'
import bodyParser from 'body-parser'
import Promise from 'bluebird'
import multer from 'multer'
import path from 'path'

Promise.config({
    warnings: true,
    longStackTraces: true,
    cancellation: true,
    monitoring: false
});

import http from 'http'
import bunyan from 'bunyan'
import debounce_promise from 'debounce-promise'
import mariasql from 'mariasql'
import { sprintf } from "sprintf-js"
import formidable from 'express-formidable'
import swagger_middleware from 'swagger-express-middleware'
import SwaggerExpress from 'swagger-express-mw'

// internal components

import util_logging from '@/server/utils/logging'
import config_logging from '@/config/logging'
import config_api from '@/config/api'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_mariadb from '@/server/services/mariadb'
import service_test from '@/server/services/test'
import repository_user from '@/server/repositories/user'
import errors from '@/errors'
import PromiseRequest from '@/server/utils/promise_request'
import singleton from '@/singleton'
import cors from 'cors'


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Promise.promisifyAll(mariasql.prototype);

Promise.onPossiblyUnhandledRejection(function(error){
    log.error('onPossiblyUnhandledRejection', error);
    process.exit(1);
});

process.on('uncaughtException', function (err) {
    util_logging.logError(sprintf('%1$s uncaughtException:%2$j', (new Date).toUTCString(), err));
    log.error('stack:', err.stack);
    process.exit(1);
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const log = service_logger.getLogger({name: 'server', service:'server'});
const LOGTAG = "server";

var join = Promise.join;

var app = express();
var default_swagger_port = 10010;
var config_swagger = {
    appRoot: __dirname // required config
};

// Untuk bisa langsung akses /apk/versi_android.apk
app.use(express.static('static_file'))

// configures swagger-express
SwaggerExpress.create(config_swagger, function(err, swaggerExpress) {
    if (err) {
        throw err;
    }

    // install middleware
    var port = process.env.PORT || default_swagger_port;
    var upload = multer({dest: path.resolve('/tmp', './')});

    app.use(cors());
    app.use(upload.any());
    // app.use(formidable.parse());
    //app.use(upload.fields([{name: "file"}]));
    app.listen(port);
    swaggerExpress.register(app);
});

/**
 * @returns Promise
 */
function initExpress(){
    log.info('initializing express..');
    return new Promise((resolve, reject) => {
        try {
            // app.use(formidable.parse());
            var port = process.env.PORT || config_api.port;
            var router = express.Router();
            // reference: http://stackoverflow.com/questions/25260818/rest-with-express-js-nested-router

            // app.use('/', require('./routes/main'));
            //app.listen(port);

            // util_logging.logState(sprintf('express is listening on port: %1$s', port));
            // util_logging.logInfo(sprintf('[done] express is initialized.'));
        } catch(err) {
            reject(err);
            return;
        }
        resolve(true);
    });
}

/**
 * @returns Promise
 */
function init() {
    return Promise.resolve()
    .then(function(){
        log.info('initializing server..');
    })
    .then(function(){
        return Promise.join(
            Promise.resolve(false),
            service_mariadb.init(),
            function(){
                log.info('[promises-join] all services initialized.');
            }
        );
    })
    .then(function(){
        return Promise.join(
            repository_user.init(),
            function(){
                log.info('[promises-join] all repositories initialized.');
            }
        );
    })
    .then(function(){
        return initExpress();
    })
//    .then(function(){
//        return service_test.init();
//    })
    .then(function(){
        log.info('[done] server initialized.');
    });
}

/**
 * @returns Promise
 */
function run() {
    return Promise.resolve()
    .then(function(){
        return init();
    })
    .then(function(){
        log.info('[server is ready]');
    })
    .catch(function(err){
        log.error(sprintf('failed to initialize server. exception:%1$j', err.message));
        throw err;
    });
};

var obj = {
    run: run
}
export default obj

export { app }
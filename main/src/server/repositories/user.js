import path from 'path'
import clc from 'cli-color'
import Promise from 'bluebird'
import { sprintf } from 'sprintf-js'

//

import config_deployment from '@/config/deployment'
import util_logging from '@/server/utils/logging'
import service_logger from '@/server/services/logger'
import service_mariadb from '@/server/services/mariadb'
import singleton from '@/singleton'

const log = service_logger.getLogger({name: 'repository', service:'user'});

var join = Promise.join;

function init(){
    return Promise.resolve()
    .then(function(){
        log.info('initializing user repository..');
    })
    .then(function(){
        log.info('[done] user repository initialized.');
    });
}


/**
@returns Promise
*/
function getUserInfoMariaAsync(user_id) {
    log.debug(sprintf('getUserInfoMariaAsync. hashedUserId:%1$s', hashedUserId));
    var table = 'users';
    var colName = 'user_id';
    var colValue = user_id;
    return service_mariadb.getSingleRow(table, colName, colValue)
    .then(function(row){
        log.debug('getUserInfoMariaAsync', hashedUserId, row);
        return row;
    })
    .catch(function(err){
        log.error(sprintf('getUserInfoMariaAsync. err:%1$s', err.message));
        return null;
    });
}

function processQueryResult(connection, query, callback) {
    return new Promise(function(resolve, reject) {
        query
        .on('result',
            function(row) {
                connection.pause();
                console.log('row: ',row);
                callback(row, function() {
                    connection.resume();
                });
            }
        )
        .on('end', function() {
            resolve(true);
        });
    });
}

////////////////////////////////////////////////////////////////////////////////////////

var obj = {
    init: init
}

export default obj


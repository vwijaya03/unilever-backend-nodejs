import _ from 'lodash'
import util from 'util'
import { sprintf } from 'sprintf-js'
import Sequelize from 'sequelize'
import { ResourceNotFoundError } from '@/server/error/Error'
import { orm } from '@/server/services/mariadb'
import Promise from 'bluebird'

import service_logger from '@/server/services/logger'

const log = service_logger.getLogger({name: 'repository', service:'base'});

/**
retrieves one entity from db
@param {Object} entity - sequelize model entity.
@param {Object} where - sql where
@param {Object} options - options for sequelize
@returns {Sequelize.Model} - entity
*/
function getOne({entity=null, where=null, options=null}={}) {
    if (! entity) {
        log.warn('please provide entity class ', entity);
        return Promise.resolve(null);
    }
    return get({entity:entity, where:where, options:options}).then(result_obj => {
        let {result_list:result_list} = result_obj;
        // log.debug('got entity:', result_set);
        if (!result_list || _.isEmpty(result_list)) {
            return null;
        } else {
            return result_list.shift();
        }
    });
}

/**
@param {Sequelize.Model} entity
@param {Object} the where filter
@param {Object} options
@param {Boolean} options.throwIfEmpty - throw error if resultset is empty
@throws { ResourceNotFoundError } - if throwIfEmpty is true and empty
result set.
@returns {Promise} - resolved to {result_list:<Array>, total:<Integer>}
*/
function get({entity=null, where=null, order=null, limit=null, offset=null, include=null, options=null, attributes=null}={}) {
    if (!entity) {
        return Promise.resolve(null);
    }
    options = options || {};
    log.debug('get. where:', where, 'attributes:', attributes);
    return Promise.resolve()
    .then(() => {
        let args = {};
        if (order) {
            args.order = order;
        }
        if (limit) {
            args.limit = limit;
        }
        if (offset) {
            args.offset = offset;
        }
        if (attributes && _.isArray(attributes)) {
            args.attributes = attributes;
        }
        if (include) {
            args.include = include;
        }
        if (where) {
            args.where = where;
        }
        if (options && options.transaction) {
            args.transaction = options.transaction;
        }
        if (options && options.lock) {
            args.lock = options.lock;
        }
        return {args:args};
    })
    .then(p_result => {
        let {args:args} = p_result;
        return entity.count({where:args.where}).then(total => {
            p_result.total = total;
            return p_result;
        });
    })
    .then(p_result => {
        let {args:args} = p_result;
        return entity.findAll(args).then(result_list => {
            p_result.result_list = result_list;
            return p_result;
        });
    })
    .then(p_result => {
        let {args:args, total:total, result_list:result_list} = p_result;

        log.debug('total:', total);
        
        if (! (_.isArray(result_list))) {
            result_list = [result_list];
        }
        if (! result_list || result_list.length == 0) {
            if (options.throwIfEmpty) {
                throw new ResourceNotFoundError();
            }
        }
        return {total:total, result_list:result_list};
    });
}

/**
@param {String} id
@returns {Promise} - resolved to a boolean
*/
function isExist(entity, id, options) {
    return Promise.resolve()
    .then(()=>{
        var entity = get(entity, {id:id}, options);
        return entity;
    })
    .then(entity => {
        if (entity && entity.length > 0) return Promise.resolve(true);
        return Promise.resolve(false);
    });
}

function create(entity, attributes, options) {
    return entity.create(attributes, options);
}

var obj = {}
obj.get = get
obj.getOne = getOne
obj.create = create
obj.isExist = isExist
export default obj
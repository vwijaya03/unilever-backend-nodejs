import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'
import moment from 'moment'

// money-access errors
import { MoneyAccessRegisterError } from '@/moneyaccess/error/index'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { DOBInvalidFormat } from '@/server/error/Error'
import { UsernameAlreadyExistsError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePasswordError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Pickup } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import { UserQuery } from '@/models/orm/user'

import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_bank_account from '@/server/services/bankaccount'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import service_otp from '@/server/services/otp'

import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'
import notp from 'notp'

const acl_otp = acl_factory.getDefault().addRoles(['all']);

function generate_otp(req, res) {
    const token = req.swagger.params.token.value? req.swagger.params.token.value:'xxx';
    const to = req.swagger.params.to.value;
    const type = req.swagger.params.type.value;
    const acl = acl_otp;
    const requested_resource = {};
    var datetime = new Date;
    var value_time = datetime.getTime().toString()
    var token_user = token;
    var _to = '';

    var p =  common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var d = new Date,
        
        dformat = [ (d.getMonth()+1).padLeft(),
                    d.getDate().padLeft(),
                    d.getFullYear()].join('/')+
                    ' ' +
                  [ d.getHours().padLeft(),
                    d.getMinutes().padLeft(),
                    d.getSeconds().padLeft()].join(':');

        var args = {};
        var otp_service = new service_otp();
        
        args.token_user = token;
        args.to = ((principal.user)? (((type == "email")? principal.user.email:((type == "sms")? principal.user.phone:""))):to);
        args.type = type;
        args.ipaddress =  req.connection.remoteAddress ||  req.socket.remoteAddress || req.headers['x-forwarded-for'];
        args.createdAt = dformat;
        args.updatedAt = dformat;

        _to = args.to;
        
        return (otp_service.addOtp(args).then(api_response => {
            return [principal, api_response];
        }))
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        /*res.json(api_response);*/
        res.json({
            "otp": api_response.otp,
            "type": type,
            "to": _to,
            "createdAt": api_response.createdAt.toISOString().replace(/T/, ' ').replace(/\..+/, '')
        });
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        /*console.log('generic error: ', err);*/
        res.status(400).json({code: 0, message:'failed to get otp.' + err});
    });
}

function verify_otp(req, res)
{
    const acl = acl_otp;
    const requested_resource = {};

    const param_token = req.swagger.params.token.value;
    const param_otp = req.swagger.params.otp.value;

    var token = _.isUndefined(param_token)?'':param_token;
    var otp = _.isUndefined(param_otp)?'':param_otp;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        return (new service_otp()).verifyOtp(token, otp).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        const iso8601format = "YYYY-MM-DD";
        var data = service_formatter.format({obj:api_response, typeId: 'otp'})
        data.forEach(result => {

            var dateCreatedAt = new Date(result.createdAt);
            var formattedCreatedAt = dateCreatedAt.getFullYear() +'-'+ ("0" + (dateCreatedAt.getMonth() + 1)).slice(-2) +'-'+ ("0" + dateCreatedAt.getDate()).slice(-2) +' '+dateCreatedAt.getHours()+ ':' +dateCreatedAt.getMinutes()+ ':'+dateCreatedAt.getSeconds();

            var dateUpdatedAt = new Date(result.updatedAt);
            var formattedUpdatedAt = dateUpdatedAt.getFullYear() +'-'+ ("0" + (dateUpdatedAt.getMonth() + 1)).slice(-2) +'-'+ ("0" + dateUpdatedAt.getDate()).slice(-2) +' '+dateUpdatedAt.getHours()+ ':' +dateUpdatedAt.getMinutes()+ ':'+dateUpdatedAt.getSeconds();

            result.createdAt = formattedCreatedAt;
            result.updatedAt = formattedUpdatedAt;

        });

        var out = {};
        if (data.length > 0){
            /*out.result = data[0];*/
            out.result = {"OTP":"Valid"};
        } else {
            out.result = {"OTP":"Invalid"}
        }
        
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to show version.' + err});
    });
}

export { generate_otp }
export { verify_otp }
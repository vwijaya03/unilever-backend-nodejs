import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'
import moment from 'moment'

// money-access errors
import { MoneyAccessRegisterError } from '@/moneyaccess/error/index'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { DOBInvalidFormat } from '@/server/error/Error'
import { UsernameAlreadyExistsError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePasswordError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Pickup } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import { UserQuery } from '@/models/orm/user'

import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_bank_account from '@/server/services/bankaccount'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import service_informasi from '@/server/services/informasi'
import service_notification from '@/server/services/notification'

import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'

const acl_informasi_dan_promosi = acl_factory.getDefault().addRoles(['all']);

const log = service_logger.getLogger({name: 'controller', service:'informasi dan promosi'});

function get_informasi_dan_promosi(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_informasi_dan_promosi;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        // var args = {};
        // args.user_id = principal.user.id;
        // console.log('get_informasi_dan_promosi.', args);

        return service_informasi.get().then(api_response => {
            console.log(api_response);
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        const iso8601format = "YYYY-MM-DD";
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        // var out = service_formatter.format({obj:api_response});
        var data = service_formatter.format({obj:api_response, typeId: 'informasi'})
        data.forEach(result => {

            var dateCreatedAt = new Date(result.createdAt);
            var formattedCreatedAt = dateCreatedAt.getFullYear() +'-'+ ("0" + (dateCreatedAt.getMonth() + 1)).slice(-2) +'-'+ ("0" + dateCreatedAt.getDate()).slice(-2) +' '+dateCreatedAt.getHours()+ ':' +dateCreatedAt.getMinutes()+ ':'+dateCreatedAt.getSeconds();

            var dateUpdatedAt = new Date(result.updatedAt);
            var formattedUpdatedAt = dateUpdatedAt.getFullYear() +'-'+ ("0" + (dateUpdatedAt.getMonth() + 1)).slice(-2) +'-'+ ("0" + dateUpdatedAt.getDate()).slice(-2) +' '+dateUpdatedAt.getHours()+ ':' +dateUpdatedAt.getMinutes()+ ':'+dateUpdatedAt.getSeconds();

            result.createdAt = formattedCreatedAt;
            result.updatedAt = formattedUpdatedAt;

        });

        var out = {};
        out.promotions = data;
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to add bank account.' + err});
    });
}

function get_detail_informasi_dan_promosi(req, res)
{
    const token = req.swagger.params.token.value;
    const id = req.swagger.params.id.value;
    const acl = acl_informasi_dan_promosi;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        return service_informasi.getOne({id: id}).then(api_response => {
            console.log(api_response);
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        // var out = service_formatter.format({obj:api_response});
        var data = service_formatter.format({obj:api_response, typeId: 'informasi'})

        var dateCreatedAt = new Date(data.createdAt);
        var formattedCreatedAt = dateCreatedAt.getFullYear() +'-'+ ("0" + (dateCreatedAt.getMonth() + 1)).slice(-2) +'-'+ ("0" + dateCreatedAt.getDate()).slice(-2) +' '+dateCreatedAt.getHours()+ ':' +dateCreatedAt.getMinutes()+ ':'+dateCreatedAt.getSeconds();

        var dateUpdatedAt = new Date(data.updatedAt);
        var formattedUpdatedAt = dateUpdatedAt.getFullYear() +'-'+ ("0" + (dateUpdatedAt.getMonth() + 1)).slice(-2) +'-'+ ("0" + dateUpdatedAt.getDate()).slice(-2) +' '+dateUpdatedAt.getHours()+ ':' +dateUpdatedAt.getMinutes()+ ':'+dateUpdatedAt.getSeconds();

        data.createdAt = formattedCreatedAt;
        data.updatedAt = formattedUpdatedAt;


        var out = {};
        out.promotion = data;
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to add bank account.' + err});
    });
}

Number.prototype.padLeft = function(base,chr){
   var  len = (String(base || 10).length - String(this).length)+1;
   return len > 0? new Array(len).join(chr || '0')+this : this;
}

function add_informasi_dan_promosi(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_informasi_dan_promosi;
    const requested_resource = {};

    const param_title = req.swagger.params.title;
    const param_content = req.swagger.params.content;
    const param_image = req.swagger.params.image;

    var title = _.isUndefined(param_title.value)?'':param_title.value;
    var content = _.isUndefined(param_content.value)?'':param_content.value;
    var image = _.isUndefined(param_image.value)?'':param_image.value;
    var result_message = JSON.stringify({
        title: title,
        body: content
    })

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var d = new Date,
        dformat = [ (d.getMonth()+1).padLeft(),
                    d.getDate().padLeft(),
                    d.getFullYear()].join('/')+
                    ' ' +
                  [ d.getHours().padLeft(),
                    d.getMinutes().padLeft(),
                    d.getSeconds().padLeft()].join(':');

        var args = {};
        args.title = title;
        args.content = content;
        args.image = image;
        args.createdAt = dformat;
        args.updatedAt = dformat;

        console.log('add_bank_account.', args);

        return service_informasi.createInformasiDanPromosi(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var d = new Date,
        dformat = [ (d.getMonth()+1).padLeft(),
                    d.getDate().padLeft(),
                    d.getFullYear()].join('/')+
                    ' ' +
                  [ d.getHours().padLeft(),
                    d.getMinutes().padLeft(),
                    d.getSeconds().padLeft()].join(':');

        var args = {};
        args.to = "/topics/promotion-notification";
        args.name = "data";
        args.message = result_message;
        args.state = 0;
        args.createdAt = dformat;
        args.updatedAt = dformat;

        console.log('add_notification.', args);

        return (new service_notification()).addNotification(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'Gagal menambahkan informasi dan promosi.' + err});
    });
}

export { add_informasi_dan_promosi }
export { get_informasi_dan_promosi }
export { get_detail_informasi_dan_promosi }
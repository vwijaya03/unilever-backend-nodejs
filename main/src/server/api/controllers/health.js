import util from 'util'

function status(req, res) {
  var hello = util.format('Hello there stranger...');
  res.json(hello);
}

export { status }

import util from 'util'
import path from 'path'
import { sprintf } from 'sprintf-js'
import _ from 'lodash'
import Promise from 'bluebird'
import moment from 'moment'
import queryString from 'query-string'
import Sequelize from 'sequelize'

//

import properties from '@/properties.js'

import { ResourceNotFoundError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { InternalServerError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { InvoiceAlreadyPaidError } from '@/server/error/Error'
import { BalanceNotEnoughError } from '@/server/error/Error'
import { MakePaymentError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { MoneyAccessError } from '@/server/error/Error'
import { PinError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { User } from '@/models/orm/index'
import { Invoice } from '@/models/orm/index'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { TransactionQuery } from '@/models/orm/transaction'
import { orm } from '@/server/services/mariadb'

import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_invoice from '@/server/services/invoice'
import service_formatter from '@/server/services/formatter'
import service_transaction from '@/server/services/transaction'
import service_notification from '@/server/services/notification'
import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'
import base_repo from '@/server/repositories/base'

/////////////////////////////////////////////////////////////////////////////////////////////

const log = service_logger.getLogger({name: 'transaction', service:'controller'});
const DEBUG_LOG = true;
const LOG_TAG = '[ctrl:trans] ';

const acl_get_transactions = acl_factory.getDefault().addRoles(['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center']);
const acl_get_one_transaction = acl_factory.getDefault().addRoles(['internal', 'retailer']);
const acl_make_payment = acl_factory.getDefault().addRoles(['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center']);
// const acl_make_payment = acl_factory.getDefault().addRoles(['retailer', 'pickup_agent']);
const acl_get_balance = acl_factory.getDefault().addRoles(['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center']);
const acl_get_summary = acl_factory.getDefault().addRoles(['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center']);

function retur_approval(req, res) {

    log.debug(LOG_TAG + 'retur_approval');

    // start of required parameters
    var amount = 0;
    var pin = req.swagger.params.pin.value;
    var dryrun = _.isUndefined(param_dryrun)?false:param_dryrun.value;
    // end of required parameters

    // start of optional parameters
    const param_dryrun = req.swagger.params.dryrun;
    const param_recipient_username = req.swagger.params.recipient_username;
    const param_invoiceId = req.swagger.params.invoice_id;
    const param_outletCode = req.swagger.params.outlet_code;
    const param_distributorCode = req.swagger.params.dt_code;
    const param_requestId = req.swagger.params.request_id;
    const param_status = req.swagger.params.status.value;
    // end of optional parameters

    var requestId = _.isUndefined(param_requestId.value)?null:param_requestId.value;
    var recipientUsername = _.isUndefined(param_recipient_username.value)?null:param_recipient_username.value;
    var invoiceId = _.isUndefined(param_invoiceId.value)?null:param_invoiceId.value;
    var outletCode = _.isUndefined(param_outletCode.value)?null:param_outletCode.value;
    var distributorCode = _.isUndefined(param_distributorCode.value)?null:param_distributorCode.value;


    var is_invoice_payment = (invoiceId);
    var is_normal_payment = (! invoiceId && recipientUsername);

    log.info('requestId:', requestId);
    log.debug('recipientUsername:', recipientUsername);
    log.debug('invoiceId:', invoiceId);
    log.debug('outletCode:', outletCode);
    log.debug('distributorCode:', distributorCode);
    log.debug('amount:', Number(amount));
    log.debug('dryrun:', dryrun);

    const token = req.swagger.params.token.value;
    const acl = acl_make_payment;
    const requested_resource = {};

    if(param_status == 2)
    {
        var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
        .then(p_result => {
            return service_transaction.updateInvoiceRejectStatus({
                status:param_status,
                invoiceId:invoiceId
            }).then(api_response => {
                // console.log(api_response);
                return [api_response];
            });   
        })
        .then(p_result => {
            const [api_response] = p_result;
            // console.log(api_response);
            // console.log('got api response: ', common_util.stringifyOnce(api_response));
            res.json(api_response);
        });
        return common_handler.exceptions(p, req, res)
        .catch(err => {
            console.log('generic error: ', err);
            res.status(400).json({code: 0, message:'failed to update status. ' + err});
        });
    }

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        if (dryrun) {
            var is_allow_dryrun = acl_factory.getDefault().isAllowed(principal);
            dryrun = is_allow_dryrun;
        }
        if (! principal.user) {
            throw new Error('bearer-token does not have associated user');
        }

        return {principal:principal};
    })

    // RESOLVE PRINCIPAL-USER-RETAILER, INVOICE-RETAILER, INVOICE-DISTRIBUTOR (WITHOUT TRANSACTION)

    .then(p_result => {
        const {principal:principal} = p_result;

        var expected_pin = service_user.getHashedPin(principal.user.salt, pin);

        if(expected_pin != principal.user.pin)
        {
            throw new PinError();
        }

        return Promise.resolve({})
        .then(p2_result => {
            return principal.user.getRetailer().then(retailer => {
                var p2_result = {principalRetailer:retailer};
                return p2_result;
            });
        })
        .then(p2_result => {
            if (! recipientUsername) {
                return p2_result;
            }
            return base_repo.getOne({entity:User, where:{username: recipientUsername}, options:{throwIfEmpty:false}})
            .then(recipientUser => {
                p2_result.recipientUser = recipientUser;
                return p2_result;
            });
        })
        .then(p2_result => {
            if (! invoiceId) {
                return p2_result;
            }
            return base_repo.getOne({entity:Invoice, where:{invoice_id: invoiceId}, options:{throwIfEmpty:true}})
            .then(invoice => {
                p2_result.invoice = invoice;
                return p2_result;
            });
        })
        .then(p2_result => {
            var {
                principalRetailer:principalRetailer,
                recipientUser:recipientUser,
                invoice:invoice
            } = p2_result;

            log.debug('principal-retailer.outlet-code:', principalRetailer?principalRetailer.outletCode:null);
            log.debug('invoice.outlet-code:', invoice?invoice.outletCode:null);
            log.debug('invoice.distributor-code:', invoice?invoice.distributorCode:null);
            log.debug('recipient-username:', recipientUser?recipientUser.username:null);

            return (() => {
                if (invoice) {
                    return Promise.join(
                        invoice.getRetailer(),
                        invoice.getDistributor()
                    );
                } else {
                    return Promise.resolve([]);
                }
            })()
            .then(p3_result => {
                var [retailer, distributor] = p3_result;
                p2_result.retailer = retailer;
                p2_result.distributor = distributor;
                return p2_result;
            });
        })
        .then(p2_result => {
            var {distributor:distributor} = p2_result;
            if (! recipientUsername && distributor) {
                log.debug(sprintf('request-id:%1$s resolving distributor-user as recipient..', requestId));
                var distributorId = distributor.id;
                return base_repo.getOne({entity:User, where:{distributorId:distributorId}, options:{throwIfEmpty:false}})
                .then(user => {
                    if (!user) {
                        var msg = sprintf('request-id:%1$s unable to get one distributor-user for distributor-id:%2$s, invoice-id:%3$s', requestId, distributorId, invoiceId);
                        throw new ResourceNotFoundError(msg);
                    }
                    p2_result.recipientUser = user;
                    log.debug(sprintf('request-id:%1$s got distributor-user:%2$s', requestId, user.username));
                    return p2_result;
                });
            }
        })
        .then(p2_result => {
            var {
                principalRetailer:principalRetailer,
                recipientUser:recipientUser,
                invoice:invoice,
                retailer:retailer,
                distributor:distributor
            } = p2_result;

            log.debug(sprintf('request-id:%1$s has-retailer?%2$s  has-distributor?%3$s', requestId, retailer != null, distributor != null));

            /**
             set values to be passed-on to the next 1st-level then.
             **/
            p_result.principalRetailer = principalRetailer;
            p_result.recipientUser = recipientUser;
            p_result.invoice = invoice;
            p_result.retailer = retailer;
            p_result.distributor = distributor;
            return p_result;
        });
    })

    .then(p_result => {
        var {principal:principal,
         principalRetailer:principalRetailer, invoice:invoice, retailer:retailer, distributor:distributor} = p_result;
//        if (principalRetailer.outletCode != invoice.outletCode) {
//            throw new Error('invoice is not owned by token-bearer');
//        }
        return p_result;
    })

    // PERFORM PAYMENT (WITH TRANSACTION)

    .then(p_result => {
        var {principal:principal, principalRetailer:principalRetailer, invoice:invoice, retailer:retailer, distributor:distributor} = p_result;

        if (dryrun) {
            log.info(sprintf('request-id:%1$s is dryrun. skip actual payment', requestId));
            return Promise.resolve(true)
        }

        const user = principal.user;

        return orm.transaction({
            isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ,
            lock: Sequelize.Transaction.LOCK.UPDATE
        }, function(transaction) {
            if (is_invoice_payment) {

                log.info(sprintf('request-id:%1$s, Payment-type:INVOICE. username:%2$s invoiceId:%3$s', requestId, user.username, invoiceId));

                return service_transaction.makeReturInvoice({
                    requestId:requestId,
                    transaction:transaction,
                    user:user,
                    principalRetailer:principalRetailer,
                    invoiceId:invoiceId,
                    distributor:distributor,
                    amount:Number(amount),
                    pin:pin
                }).then(paymentResult => {
                    console.log('payment results');
                    console.log(paymentResult);
                    p_result.paymentResult = paymentResult;
                    return p_result;
                });
            }
            else if (is_normal_payment) {
                log.info(sprintf('request-id:%1$s Payment-type:NORMAL. username:%2$s', requestId, user.username));

                return service_transaction.makeNormalPayment({
                    requestId:requestId,
                    transaction:transaction,
                    user:user,
                    recipientUsername:recipientUsername,
                    amount:Number(amount),
                    pin:pin
                }).then(paymentResult => {
                    p_result.paymentResult = paymentResult;
                    return p_result;
                });
            }
        });

        throw new Error('invalid request. please check your parameters.');
    })

    // CREATES TRANSACTION RECORD

    .then(p_result => {
        var {
            principal:principal,
            retailer:retailer,
            distributor:distributor,
            paymentResult:paymentResult,
            recipientUser:recipientUser
        } = p_result;

        let msg = sprintf('request-id:%1$s Creating transaction record. has_retailer?%2$s has_distributor?%3$s', requestId, null != retailer, null != distributor);
        log.info(msg);

        // log.debug('payment-result:', paymentResult);

        // extract value of valdo-transaction-id from payment result.
        // var {transid:valdoTxId} = paymentResult;

        var distributorId = (! distributor)?null:distributor.id;
        var retailerId = (! retailer)?null:retailer.id;
        var fromUserId = principal.user.id;
        var toUserId = (! recipientUser)?null:recipientUser.id;

        // log.debug('valdo-tx-id:', valdoTxId);

        // create record in database

        var writeRecordArgs =
        {
            requestId: requestId,
            valdoTxId: paymentResult.moneyAccessApiResponse.transid,
            fromUserId:fromUserId,
            toUserId:paymentResult.user_data.id,
            invoiceId:invoiceId,
            retailerId:retailerId,
            distributorId:distributorId,
            type:0,
            amount:Number(paymentResult.moneyAccessApiResponse.amount),
            currencyCode:null,
            status: param_status
        };

        log.info(sprintf('request-id:%1$s write-db-record-args:%2$j', requestId, writeRecordArgs));

        return service_transaction.writeReturTxRecord(writeRecordArgs)
        .then(p2_result => {
            return p_result;
        });

    })

    // CREATE AND SEND RESPONSE

    .then(p_result => {
        log.info(sprintf('request-id:%1$s payment is successful.', requestId));

        var {principal:principal, paymentResult:paymentResult} = p_result;
        var {desc:message} = paymentResult; // extract Valdo-MoneyAccess response.

        // if the message from valdo-moneyaccess is not defined then we create our own message.
        if (! message) {
            message = sprintf('Request retur diterima.');
        }
        if (res) {
            res.json({message:message});
        }
        return true;
    });

    return common_handler.exceptions(p, req, res)
    .catch(
        ResourceNotFoundError,
        InvoiceAlreadyPaidError,
        BalanceNotEnoughError,
        MakePaymentError, err => {
        if (res) {
            res.status(404).json({code: err.code, message:err.message});
        }
    })
    .catch(PinError, err => {
        res.status(400).json({code: err.code, message:'Pin salah.'});
    })
    .catch(err => {
        if (res) {
            res.status(400).json({code: 0, message:'failed to make a payment. '+err});
        }
    });
}

function get_one_transaction() {
    log.debug('get_one_transaction.');

    const token = req.swagger.params.token.value;
    const param_invoiceId = req.swagger.params.invoiceId;
    const acl = acl_get_one_transaction;
    const requested_resource = {};

    //

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        if (!principal.user) {
            throw new Error('no associated user with this principal.');
        }
        return principal.user.getRetailer().then(retailer => {
            return [principal, retailer];
        });
    })
    .then(p_result => {
        var [principal, retailer] = p_result;
        if (param_startFromOldestUnpaid && param_startFromOldestUnpaid.value) {
            const outletCode = retailer.outletCode;
            var args = {outletCode:outletCode};
            return service_invoice.getOldestUnpaidDate(args).then(oldest_unpaid_date => {
                return [principal, retailer, oldest_unpaid_date];
            });
        } else {
            return [principal, retailer, null];
        }
    })
    .then(p_result => {
        var [principal, retailer, oldest_unpaid_date] = p_result;
        if (DEBUG_LOG) log.debug('oldest-unpaid-due-date: ', oldest_unpaid_date);
        const outletCode = retailer.outletCode;

        var startDueDate = null;
        var endDueDate = null;

        if (oldest_unpaid_date) {
            startDueDate = oldest_unpaid_date;
        } else {
            if (! param_startDueDate) {
                startDueDate = moment();
            } else {
                startDueDate = common_util.parseDateString({pattern:param_startDueDate.value, now:null});
            }
        }

        endDueDate = common_util.parseDateString({pattern:param_endDueDate.value, now:startDueDate});

        if (startDueDate) {
            if (DEBUG_LOG) log.debug('startduedate', startDueDate.format('LLL'));
        }
        if (endDueDate) {
            if (DEBUG_LOG) log.debug('endduedate', endDueDate.format('LLL'));
        }

        var args = {outletCode:outletCode,isPaid:isPaid, isUnpaid:isUnpaid};
        if (startDueDate) {
            args.dueDateStart = startDueDate;
        }
        if (endDueDate) {
            args.dueDateEnd = endDueDate;
        }
        return service_invoice.get(args).then(invoices_list => {
            return [invoices_list];
        });
    })
    .then(p_result => {
        const [invoices_list] = p_result;
        var message = sprintf('get invoices. [%s]');
        // log.debug(invoices_list);
        var out = service_formatter.format(invoices_list);
        res.json({invoices:out});
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        log.debug('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get invoices.' + err});
    });

}

function get_moneyaccess_transactions(req, res) {
    log.debug('get_moneyaccess_transactions.');

    const param_startDate = req.swagger.params.start_date;
    const param_endDate = req.swagger.params.end_date;
    const param_trans_type = req.swagger.params.trans_type;

    const param_username = req.swagger.params.username;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;

    var username = _.isUndefined(param_username.value)?null:param_username.value;
    var transType = _.isUndefined(param_trans_type.value)?null:param_trans_type.value;
    var startDate = _.isUndefined(param_startDate.value)?null:param_startDate.value;
    var endDate = _.isUndefined(param_endDate.value)?null:param_endDate.value;
    var page = _.isUndefined(param_page.value)?1:param_page.value;
    var itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;

    // normalize startPage to 1
    page = (page < 1 ) ? 1 : page;

    //

    const token = req.swagger.params.token.value;
    const acl = acl_get_transactions;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;

        var principal_username = principal.user.username;

        // check if username is allowed being overridden
        if (username && (principal_username != username)) {
            var is_allow_override_username = acl_factory.getDefault().isAllowed(principal);
            if (! is_allow_override_username) {
                throw new UnauthorizedRequestError('you are not authorized to view transactions of other users.');
            }
        }

        if (username) {
            return base_repo.getOne({entity:User, where:{username:username}, options:{throwIfEmpty:true}})
            .then(user => {
                return {principal:principal, user:user};
            });
        } else {
            if (! principal.user) {
                throw new Error('no associated user with this principal. please specify username.');
            }
            return {principal:principal, user:principal.user};
        }
    })
    .then(p_result => {
        var {principal:principal, user:user} = p_result;

        var startDateMoment = null;
        var endDateMoment = null;

        if (startDate && endDate) {
            // parses string start-date and string end-date into moment objects.
            var dates = common_util.getStartEndDates({startDate:startDate, endDate:endDate});
            startDateMoment = dates.start;
            endDateMoment = dates.end;
            log.debug('# Date filter range:');
            if (DEBUG_LOG) log.debug('start-date:', startDateMoment.format('LLL'));
            if (DEBUG_LOG) log.debug('end-date:', endDateMoment.format('LLL'));
        }

        //

        var args = {};

        if (username && user) {
            args.targetUserId = user.id;
        }
        if (startDateMoment) {
            args.transactDateStart = startDateMoment;
        }
        if (endDateMoment) {
            args.transactDateEnd = endDateMoment;
        }
        if (type) {
            args.type = type;
        }
        if (isSend) {
            args.isSend = isSend;
        }
        if (isReceive) {
            args.isReceive = isReceive;
        }
        if (page) {
            args.page = page;
        }
        if (itemsPerPage) {
            args.itemsPerPage = itemsPerPage;
        }

        return service_transaction.get(args).then(transactions_result => {
            p_result.transactions_result = transactions_result;
            return p_result;
        });
    })
    .then(p_result => {
        const {principal:principal, transactions_result:transactions_result} = p_result;
        const {total:total_transactions, result_list:transactions_list} = transactions_result;

        // create a set of retailer ids and a set of distributor ids.

        // TODO: retrieve retailers and distributors from caches

        // get retailers and distributors from database
        return Promise.resolve()
        .then(() => {
            // get users
            return Promise.map(users_id_set, user_id => {
                if (_.isNull(user_id) ||  _.isUndefined(user_id)) {
                    return null;
                }
                return base_repo.getOne({entity:User, where:{id:user_id}}).then(user => {
                    const _retailerId = user.retailerId;
                    const _distributorId = user.distributor_id;

                    // collects retailer-id and distributor-id
                    if (_retailerId) {
                        retailers_id_set.add(_retailerId);
                    }
                    if (_distributorId) {
                        distributors_id_set.add(_distributorId);
                    }

                    return user;
                });
            }).then(users => {
                var p2_result = {users:users};
                return p2_result;
            });
        })
    });
    return common_handler.exceptions(p, req, res)
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: err.code, message:err.message});
    })
    .catch(err => {
        log.debug(LOG_TAG + 'generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get invoices.' + err});
    });
}

function get_transactions(req, res) {
    log.debug('get_transactions');

    const param_startDate = req.swagger.params.start_date;
    const param_endDate = req.swagger.params.end_date;
    const param_type = req.swagger.params.type;

    const param_user_id = req.swagger.params.user_id;
    const param_email = req.swagger.params.email;
    const param_is_send = req.swagger.params.is_send;
    const param_is_receive = req.swagger.params.is_receive;

    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;

    var user_id = _.isUndefined(param_user_id.value)?null:param_user_id.value;
    var email = _.isUndefined(param_email.value)?null:param_email.value;
    var isSend = _.isUndefined(param_is_send.value)?true:param_is_send.value;
    var isReceive = _.isUndefined(param_is_receive.value)?true:param_is_receive.value;
    var type = _.isUndefined(param_type.value)?null:param_type.value;
    var startDate = _.isUndefined(param_startDate.value)?null:param_startDate.value;
    var endDate = _.isUndefined(param_endDate.value)?null:param_endDate.value;
    var page = _.isUndefined(param_page.value)?1:param_page.value;
    var itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;

    // normalize startPage to 1
    page = (page < 1 ) ? 1 : page;

    //

    const token = req.swagger.params.token.value;
    const acl = acl_get_transactions;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        
        // var principal_username = principal.user.username;
        var principal_user_id = principal.user.id;

        // check if username is allowed being overridden
        if (user_id && (principal_user_id != user_id)) {
            var is_allow_override_username = acl_factory.getDefault().isAllowed(principal);
            if (! is_allow_override_username) {
                throw new UnauthorizedRequestError('you are not authorized to view transactions of other users.');
            }
        }

        if (email) {
            return base_repo.getOne({entity:User, where:{email:email}, options:{throwIfEmpty:true}})
            .then(user => {
                return {principal:principal, user:user};
            });
        }
        if (user_id) {
            return base_repo.getOne({entity:User, where:{id:user_id}, options:{throwIfEmpty:true}})
            .then(user => {
                return {principal:principal, user:user};
            });
        } else {
            if (! principal.user) {
                throw new Error('no associated user with this principal. please specify username.');
            }
            user_id = principal.user.id;

            return {principal:principal, user:principal.user};
        }
    })
    .then(p_result => {
        var {principal:principal, user:user} = p_result;

        var args = {};
        var startDateMoment = null;
        var endDateMoment = null;

        if(!startDate && !endDate)
        {
            var now = new Date();
            var now_minus_seven_day = new Date();
            var data_minus_seven_day = now_minus_seven_day.getDate() - 30;
            now_minus_seven_day.setDate(data_minus_seven_day);

            var res_now = now.toISOString().slice(0,10);
            var res_minus_seven_day = now_minus_seven_day.toISOString().slice(0,10);

            var dates = common_util.getStartEndDates({startDate:res_minus_seven_day, endDate:res_now});
            startDateMoment = dates.start;
            endDateMoment = dates.end;
            log.debug('# Get transactions date filter range:');
            log.debug('start-date:', startDateMoment.format('LLL'));
            log.debug('end-date:', endDateMoment.format('LLL'));
            args.transactDateStart = startDateMoment;
            args.transactDateEnd = endDateMoment;
        }

        if (startDate && endDate) {
            // parses string start-date and string end-date into moment objects.
            var dates = common_util.getStartEndDates({startDate:startDate, endDate:endDate});
            startDateMoment = dates.start;
            endDateMoment = dates.end;
            log.debug('# Get transactions date filter range:');
            log.debug('start-date:', startDateMoment.format('LLL'));
            log.debug('end-date:', endDateMoment.format('LLL'));
        }

        if (user_id && user) {
            args.targetUserId = user.id;
        }
        if (email && user) {
            args.targetUserId = user.id;
        }
        if (startDateMoment) {
            args.transactDateStart = startDateMoment;
        }
        if (endDateMoment) {
            args.transactDateEnd = endDateMoment;
        }
        if (type) {
            args.type = type;
        }
        if (isSend) {
            args.isSend = isSend;
        }
        if (isReceive) {
            args.isReceive = isReceive;
        }
        if (page) {
            args.startPage = page;
        }
        if (itemsPerPage) {
            args.itemsPerPage = itemsPerPage;
        }
        // console.log('isi args');
        // console.log(args);
        return service_transaction.get(args).then(transactions_result => {
            p_result.transactions_result = transactions_result;
            return p_result;
        });
    })
    .then(p_result => {

        const {principal:principal, transactions_result:transactions_result} = p_result;
        // console.log('Check P Result');
        // console.log(p_result);
        // console.log('Check Principal');
        // console.log(principal);
        // console.log('Check Transaction Result');
        // console.log(transactions_result);
        const {
            total_transactions: total_transactions,
            result_list: transactions_list,
            other_transactions_list: other_transactions_list
        } = transactions_result;
        

        // create a set of retailer ids and a set of distributor ids.

        let retailers_id_set = new Set();
        let distributors_id_set = new Set();
        let users_id_set = new Set();

        transactions_list.forEach(item => {
            // console.log('lihat item');
            // console.log(item);
            if (item._source != 'moneyaccess') {
                var retailer_id = item.retailerId;
                var distributor_id = item.distributorId;
                var from_user_id = item.fromUserId;
                var to_user_id = item.toUserId;
                retailers_id_set.add(retailer_id);
                distributors_id_set.add(distributor_id);
                users_id_set.add(from_user_id);
                users_id_set.add(to_user_id);
            }
        });
        // console.log('lihat transaction list');
        // console.log(transactions_list);
        // TODO: retrieve retailers and distributors from caches

        // get retailers and distributors from database
        return Promise.resolve()
        .then(() => {
            // get users
            return Promise.map(users_id_set, user_id => {
                if (_.isNull(user_id) ||  _.isUndefined(user_id)) {
                    return null;
                }
                return base_repo.getOne({entity:User, where:{id:user_id}}).then(user => {
                    // console.log('check _retailerId');
                    // console.log(user);
                    const _retailerId = user.retailerId;
                    const _distributorId = user.distributor_id;

                    // collects retailer-id and distributor-id
                    if (_retailerId) {
                        retailers_id_set.add(_retailerId);
                    }
                    if (_distributorId) {
                        distributors_id_set.add(_distributorId);
                    }

                    return user;
                });
            }).then(users => {
                var p2_result = {users:users};
                return p2_result;
            });
        })
        .then(p2_result => {
            // get retailers
            return Promise.map(retailers_id_set, retailer_id => {
                if (_.isNull(retailer_id) ||  _.isUndefined(retailer_id)) {
                    return null;
                }
                return base_repo.getOne({entity:Retailer, where:{id:retailer_id}}).then(retailer => {
                    return retailer;
                });
            }).then(retailers => {
                p2_result.retailers = retailers;
                return p2_result;
            });
        }).then(p2_result => {
            // get distributors
            return Promise.map(distributors_id_set, distributor_id => {
                if (_.isNull(distributor_id) || _.isUndefined(distributor_id)) {
                    return null;
                }
                return base_repo.getOne({entity:Distributor, where:{id:distributor_id}}).then(distributor => {
                    return distributor;
                });
            }).then(distributors => {
                p2_result.distributors = distributors;
                return p2_result;
            });
        }).then(p2_result => {
            var {users:users, retailers:retailers, distributors:distributors} = p2_result;

            log.debug(LOG_TAG + 'users. size:', users.length);
            log.debug(LOG_TAG + 'retailers. size:', retailers.length);
            log.debug(LOG_TAG + 'distributors. size:', distributors.length);

            var message = sprintf(LOG_TAG + 'transactions. count:%1$s', total_transactions);
            log.debug(message);

            // format responses

            var formatted_retailers = common_util.list2obj(service_formatter.format({obj:retailers}), 'id');
            var formatted_distributors = common_util.list2obj(service_formatter.format({obj:distributors}), 'id');
            var formatted_users = common_util.list2obj(service_formatter.format( {obj:users, options:{excludes:['pin']}} ), 'id');
            var formatted_transactions = service_formatter.format({typeId:'transaction', obj:transactions_list});
            var formatted_other_transactions = service_formatter.format({typeId:'transaction', obj:other_transactions_list});


            // create paging info

            // var paging = common_handler.paging(
            //     {list:transactions_list, page:page, totalItems:total_transactions, itemsPerPage:itemsPerPage}
            // );
            // console.log('isi paging');
            // console.log(paging);

            // prepare output response

            var resp = {};
            // resp = Object.assign(resp, paging);

            resp.size = transactions_list['itempage'];
            resp.page = transactions_list['pagenumber'];
            resp.items_per_page = itemsPerPage;
            resp.total_pages = transactions_list['totalpage'];
            resp.total_items = transactions_list['totalitem'];

            resp.transactions = formatted_transactions;
            resp.other_transactions = formatted_other_transactions;
            resp.users = formatted_users;
            resp.retailers = formatted_retailers;
            resp.distributors = formatted_distributors;

            // console.log('isi resp');
            // console.log(resp);

            res.json(resp);
        });
    });
    return common_handler.exceptions(p, req, res)
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: err.code, message:err.message});
    })
    .catch(err => {
        log.debug(LOG_TAG + 'generic error: ', err);

        if(err['code'] == '68000')
        {
            var resp = {};
            resp.size = 0;
            resp.page = 0;
            resp.items_per_page = 0;
            resp.total_pages = 0;
            resp.total_items = 0;

            resp.transactions = [];
            resp.other_transactions = [];
            resp.users = {};
            resp.retailers = {};
            resp.distributors = {};
            return res.status(200).json(resp);
        }
        // res.status(400).json({code: 0, message:'failed to get invoices.' + err});
        res.status(400).json({code: 0, message:'' + err});
    });
}

function make_payment(req, res) {

    log.debug(LOG_TAG + 'make_payment');

    // start of required parameters
    var amount = req.swagger.params.amount.value;
    var pin = req.swagger.params.pin.value;
    var dryrun = _.isUndefined(param_dryrun)?false:param_dryrun.value;
    // end of required parameters

    // start of optional parameters
    const param_dryrun = req.swagger.params.dryrun;
    const param_recipient_username = req.swagger.params.recipient_username;
    const param_invoiceId = req.swagger.params.invoice_id;
    const param_outletCode = req.swagger.params.outlet_code;
    const param_distributorCode = req.swagger.params.dt_code;
    const param_requestId = req.swagger.params.request_id;
    // end of optional parameters

    var requestId = _.isUndefined(param_requestId.value)?null:param_requestId.value;
    var recipientUsername = _.isUndefined(param_recipient_username.value)?null:param_recipient_username.value;
    var invoiceId = _.isUndefined(param_invoiceId.value)?null:param_invoiceId.value;
    var outletCode = _.isUndefined(param_outletCode.value)?null:param_outletCode.value;
    var distributorCode = _.isUndefined(param_distributorCode.value)?null:param_distributorCode.value;


    var is_invoice_payment = (invoiceId);
    var is_normal_payment = (! invoiceId && recipientUsername);

    log.info('requestId:', requestId);
    log.debug('recipientUsername:', recipientUsername);
    log.debug('invoiceId:', invoiceId);
    log.debug('outletCode:', outletCode);
    log.debug('distributorCode:', distributorCode);
    log.debug('amount:', Number(amount));
    log.debug('dryrun:', dryrun);

    const token = req.swagger.params.token.value;
    const acl = acl_make_payment;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})

    .then(p_result => {
        const [principal, is_allowed] = p_result;
        if (dryrun) {
            var is_allow_dryrun = acl_factory.getDefault().isAllowed(principal);
            dryrun = is_allow_dryrun;
        }
        if (! principal.user) {
            throw new Error('bearer-token does not have associated user');
        }

        return {principal:principal};
    })

    // RESOLVE PRINCIPAL-USER-RETAILER, INVOICE-RETAILER, INVOICE-DISTRIBUTOR (WITHOUT TRANSACTION)

    .then(p_result => {
        const {principal:principal} = p_result;

        var expected_pin = service_user.getHashedPin(principal.user.salt, pin);

        if(expected_pin != principal.user.pin)
        {
            throw new PinError();
        }

        return Promise.resolve({})
        .then(p2_result => {
            return principal.user.getRetailer().then(retailer => {
                var p2_result = {principalRetailer:retailer};
                return p2_result;
            });
        })
        .then(p2_result => {
            if (! recipientUsername) {
                return p2_result;
            }
            return base_repo.getOne({entity:User, where:{username: recipientUsername}, options:{throwIfEmpty:false}})
            .then(recipientUser => {
                p2_result.recipientUser = recipientUser;
                return p2_result;
            });
        })
        .then(p2_result => {
            if (! invoiceId) {
                return p2_result;
            }
            return base_repo.getOne({entity:Invoice, where:{invoice_id: invoiceId, dt_code: distributorCode}, options:{throwIfEmpty:true}})
            .then(invoice => {
                p2_result.invoice = invoice;
                return p2_result;
            });
        })
        .then(p2_result => {
            var {
                principalRetailer:principalRetailer,
                recipientUser:recipientUser,
                invoice:invoice
            } = p2_result;

            log.debug('principal-retailer.outlet-code:', principalRetailer?principalRetailer.outletCode:null);
            log.debug('invoice.outlet-code:', invoice?invoice.outletCode:null);
            log.debug('invoice.distributor-code:', invoice?invoice.distributorCode:null);
            log.debug('recipient-username:', recipientUser?recipientUser.username:null);

            return (() => {
                if (invoice) {
                    return Promise.join(
                        invoice.getRetailer(),
                        invoice.getDistributor()
                    );
                } else {
                    return Promise.resolve([]);
                }
            })()
            .then(p3_result => {
                var [retailer, distributor] = p3_result;
                p2_result.retailer = retailer;
                p2_result.distributor = distributor;
                return p2_result;
            });
        })
        .then(p2_result => {
            var {distributor:distributor} = p2_result;
            if (! recipientUsername && distributor) {
                log.debug(sprintf('request-id:%1$s resolving distributor-user as recipient..', requestId));
                var distributorId = distributor.id;
                return base_repo.getOne({entity:User, where:{distributorId:distributorId, type:1}, options:{throwIfEmpty:false}})
                .then(user => {
                    if (!user) {
                        var msg = sprintf('request-id:%1$s unable to get one distributor-user for distributor-id:%2$s, invoice-id:%3$s', requestId, distributorId, invoiceId);
                        throw new ResourceNotFoundError(msg);
                    }
                    p2_result.recipientUser = user;
                    log.debug(sprintf('request-id:%1$s got distributor-user:%2$s', requestId, user.username));
                    return p2_result;
                });
            }
        })
        .then(p2_result => {
            var {
                principalRetailer:principalRetailer,
                recipientUser:recipientUser,
                invoice:invoice,
                retailer:retailer,
                distributor:distributor
            } = p2_result;

            log.debug(sprintf('request-id:%1$s has-retailer?%2$s  has-distributor?%3$s', requestId, retailer != null, distributor != null));

            /**
             set values to be passed-on to the next 1st-level then.
             **/
            p_result.principalRetailer = principalRetailer;
            p_result.recipientUser = recipientUser;
            p_result.invoice = invoice;
            p_result.retailer = retailer;
            p_result.distributor = distributor;
            return p_result;
        });
    })

    .then(p_result => {
        var {principal:principal,
         principalRetailer:principalRetailer, invoice:invoice, retailer:retailer, distributor:distributor} = p_result;
//        if (principalRetailer.outletCode != invoice.outletCode) {
//            throw new Error('invoice is not owned by token-bearer');
//        }
        return p_result;
    })

    // PERFORM PAYMENT (WITH TRANSACTION)

    .then(p_result => {
        var {principal:principal, principalRetailer:principalRetailer, invoice:invoice, retailer:retailer, distributor:distributor} = p_result;

        if (dryrun) {
            log.info(sprintf('request-id:%1$s is dryrun. skip actual payment', requestId));
            return Promise.resolve(true)
        }

        const user = principal.user;

        return orm.transaction({
            isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ,
            lock: Sequelize.Transaction.LOCK.UPDATE
        }, function(transaction) {
            if (is_invoice_payment) {

                log.info(sprintf('request-id:%1$s, Payment-type:INVOICE. username:%2$s invoiceId:%3$s', requestId, user.username, invoiceId));

                return service_transaction.makePaymentInvoice({
                    requestId:requestId,
                    dt_code:distributorCode,
                    transaction:transaction,
                    user:user,
                    principalRetailer:principalRetailer,
                    invoiceId:invoiceId,
                    distributor:distributor,
                    amount:Number(amount),
                    pin:pin
                }).then(paymentResult => {
                    //console.log('isi payment result');
                    //console.log(paymentResult);
                    p_result.paymentResult = paymentResult;
                    return p_result;
                });
            }
            else if (is_normal_payment) {
                log.info(sprintf('request-id:%1$s Payment-type:NORMAL. username:%2$s', requestId, user.username));

                return service_transaction.makeNormalPayment({
                    requestId:requestId,
                    transaction:transaction,
                    user:user,
                    recipientUsername:recipientUsername,
                    amount:Number(amount),
                    pin:pin
                }).then(paymentResult => {
                    p_result.paymentResult = paymentResult;
                    return p_result;
                });
            }
        });

        throw new Error('invalid request. please check your parameters.');
    })

    // CREATES TRANSACTION RECORD

    .then(p_result => {
        // console.log('isi p result');
        // console.log(p_result.paymentResult.trans_date);
        var {
            principal:principal,
            retailer:retailer,
            distributor:distributor,
            paymentResult:paymentResult,
            recipientUser:recipientUser
        } = p_result;

        let msg = sprintf('request-id:%1$s Creating transaction record. has_retailer?%2$s has_distributor?%3$s', requestId, null != retailer, null != distributor);
        log.info(msg);

        // log.debug('payment-result:', paymentResult);

        // extract value of valdo-transaction-id from payment result.
        var {transid:valdoTxId} = paymentResult;

        var distributorId = (! distributor)?null:distributor.id;
        var retailerId = principal.user.retailer_id;
        var fromUserId = principal.user.id;
        var toUserId = (! recipientUser)?null:recipientUser.id;

        // log.debug('valdo-tx-id:', valdoTxId);
        // console.log('isi distributor');
        // console.log(distributor.distributorCode);

        var invoice_id = invoiceId;
        var invoice_code = distributor.distributorCode+invoiceId;
        // create record in database

        var writeRecordArgs =
        {
            requestId: requestId,
            valdoTxId: valdoTxId,
            fromUserId:fromUserId,
            toUserId:toUserId,
            dt_code:distributor.distributorCode,
            invoiceId:invoice_id,
            invoice_code:invoice_code,
            retailerId:retailerId,
            distributorId:distributorId,
            type:0,
            amount:Number(amount),
            currencyCode:null,
            transactDate: p_result.paymentResult.trans_date
        };

        log.info(sprintf('request-id:%1$s write-db-record-args:%2$j', requestId, writeRecordArgs));

        return service_transaction.writeTxRecord(writeRecordArgs)
        .then(p2_result => {
            return p_result;
        });

    })

    // CREATE AND SEND RESPONSE

    .then(p_result => {
        log.info(sprintf('request-id:%1$s payment is successful.', requestId));

        var {principal:principal, paymentResult:paymentResult} = p_result;
        var {desc:message} = paymentResult; // extract Valdo-MoneyAccess response.

        var d = new Date,
        dformat = [ (d.getMonth()+1).padLeft(),
                    d.getDate().padLeft(),
                    d.getFullYear()].join('/')+
                    ' ' +
                  [ d.getHours().padLeft(),
                    d.getMinutes().padLeft(),
                    d.getSeconds().padLeft()].join(':');

        var payment_message = JSON.stringify({
            title: 'Pembayaran Berhasil',
            body: 'Pembayaran invoice '+invoiceId+' dengan jumlah Rp. '+Number(amount)+' berhasil.'
        });

        var args = {};

        args.to = principal.user.fcm_id;
        args.type = 'notification';
        args.message = payment_message;
        args.state = 0;
        args.createdAt = dformat;
        args.updatedAt = dformat;

        console.log('add_notification.', args);

        return (new service_notification()).addNotification(args).then(api_response => {
            // if the message from valdo-moneyaccess is not defined then we create our own message.
            if (! message) {
                message = sprintf('request-id:%1$s sent payment from username:%1$s amount %2$s to invoiceId:%3$s', requestId, principal.user.username, Number(amount), invoiceId);
            }
            if (res) {
                res.json({message:message});
            }
            return true;
        });
    });

    return common_handler.exceptions(p, req, res)
    .catch(
        ResourceNotFoundError,
        InvoiceAlreadyPaidError,
        BalanceNotEnoughError,
        MakePaymentError, err => {
        if (res) {
            res.status(404).json({code: err.code, message:err.message});
        }
    })
    .catch(PinError, err => {
        res.status(400).json({code: err.code, message:'Pin salah.'});
    })
    .catch(err => {
        if (res) {
            res.status(400).json({code: 0, message:'failed to make a payment. '+err});
        }
    });
}

function get_balance(req, res) {
    log.debug('get_balance.');

    const token = req.swagger.params.token.value;
    const acl = acl_get_balance;
    const requested_resource = {};

    const param_user_id = req.swagger.params.user_id;

    var user_id = _.isUndefined(param_user_id.value)?null:param_user_id.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        if (user_id) {
            // check if allowed
            var acl_override_username = acl_get_balance;
            var is_allow_override = acl_override_username.isAllowed(principal);
            if (! is_allow_override) {
                throw new UnauthorizedRequestError('cant access balance of random users.');
            } else {
                // get user from db
                return base_repo.getOne({entity:User, where:{id:user_id}, options:{throwIfEmpty:true}}).then(user => {
                    return {principal:principal, user:user};
                });
            }
        }

        if (! principal.user) {
            // static internal tokens does not have user.
            throw new Error('no associated user with this principal.');
        }
        return {principal:principal, user:principal.user};
    })
    .then(p_result => {
        var {user:user} = p_result;
        return service_transaction.getBalance({user:user}).then(p2_result => {
            var resp = p2_result;
            var log_message = sprintf('get balance. resp:%1$j', resp);
            log.debug(log_message);
            res.json(resp);
        });
    });
    return common_handler.exceptions(p, req, res)
    .catch(
        InternalServerError,
        ResourceNotFoundError, err => {
        res.status(400).json({code: err.code, message:'failed to get balance. ' + err.message});
    })
    .catch(err => {
        log.debug('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get balance. ' + err});
    });
}

function get_summary(req, res) {
    log.debug('get_summary.');

    const token = req.swagger.params.token.value;
    const acl = acl_get_summary;
    const requested_resource = {};

    const param_user_id = req.swagger.params.user_id;

    var user_id = _.isUndefined(param_user_id.value)?null:param_user_id.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        if (user_id) {
            // check if allowed
            var acl_override_username = acl_get_balance;
            var is_allow_override = acl_override_username.isAllowed(principal);
            if (! is_allow_override) {
                throw new UnauthorizedRequestError('cant access transaction summary of random users.');
            } else {
                // get user from db
                return base_repo.getOne({entity:User, where:{id:user_id}, options:{throwIfEmpty:true}}).then(user => {
                    return {principal:principal, user:user};
                });
            }
        }

        if (! principal.user) {
            // static internal tokens does not have user.
            throw new Error('no associated user with this principal.');
        }
        return {principal:principal, user:principal.user};
    })
    .then(p_result => {
        // get retailer from user.

        var {user:user} = p_result;
        return user.getRetailer().then(p2_result => {
            var retailer = p2_result;
            if (! retailer) {
                throw new Error('token bearer does not have retailer');
            }
            p_result.retailer = retailer;
            return p_result;
        });
    })
    .then(p_result => {
        // get fields as part of the summary

        var {user:user, retailer:retailer} = p_result;
        var outletCode = retailer.outletCode;

        return Promise.join(
            service_transaction.getBalance({user:user}),
            service_invoice.getSumUnpaidAmount({le_code_dry: retailer.le_code_dry, le_code_ice: retailer.le_code_ice, le_code_ufs: retailer.le_code_ufs})
        ).then(p2_result => {
            var [{balance:balance}, {sum:unpaid_total}]= p2_result;
            var log_message = sprintf('unpaid_total:%1$s balance:%2$s', unpaid_total, balance);
            log.debug(log_message);
            p_result.balance = balance;

            if(isNaN(unpaid_total))
            {
                p_result.unpaid_total = 0;
            }
            else
            {
                p_result.unpaid_total = unpaid_total;
            }
            
            return p_result;
        });
    })
    .then(p_result => {
        var {balance:balance, unpaid_total:unpaid_total} = p_result;
        res.json({balance:balance, unpaid_total:unpaid_total});
    });

    return common_handler.exceptions(p, req, res)
    .catch(
        InternalServerError,
        ResourceNotFoundError, err => {
        res.status(400).json({code: err.code, message:'failed to get balance. ' + err.message});
    })
    .catch(err => {
        log.error('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get balance. ' + err});
    });
}

export { get_transactions }
export { get_one_transaction }
export { get_balance }
export { make_payment }
export { get_summary }
export { retur_approval }
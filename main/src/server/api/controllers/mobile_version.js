import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'
import moment from 'moment'

// money-access errors
import { MoneyAccessRegisterError } from '@/moneyaccess/error/index'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { DOBInvalidFormat } from '@/server/error/Error'
import { UsernameAlreadyExistsError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePasswordError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Pickup } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import { UserQuery } from '@/models/orm/user'

import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_bank_account from '@/server/services/bankaccount'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import service_mobile_version from '@/server/services/mobile_version'

import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'
import fs from 'fs'
import ApkReader from 'adbkit-apkreader'
import path from 'path'

const acl_mobile_version = acl_factory.getDefault().addRoles(['all']);

function get_mobile_version(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_mobile_version;
    const requested_resource = {};

    if(token == '2b3e6865-bdae-4881-ba71-ea9c5f123e8d') {
        return (new service_mobile_version()).getOne().then(api_response => {
            console.log(api_response);
            return [api_response];
        })
        .then(p_result => {
            const [api_response] = p_result;
            const iso8601format = "YYYY-MM-DD";
            var data = service_formatter.format({obj:api_response, typeId: 'version'})
            data.forEach(result => {

                var dateCreatedAt = new Date(result.createdAt);
                var formattedCreatedAt = dateCreatedAt.getFullYear() +'-'+ ("0" + (dateCreatedAt.getMonth() + 1)).slice(-2) +'-'+ ("0" + dateCreatedAt.getDate()).slice(-2) +' '+dateCreatedAt.getHours()+ ':' +dateCreatedAt.getMinutes()+ ':'+dateCreatedAt.getSeconds();

                var dateUpdatedAt = new Date(result.updatedAt);
                var formattedUpdatedAt = dateUpdatedAt.getFullYear() +'-'+ ("0" + (dateUpdatedAt.getMonth() + 1)).slice(-2) +'-'+ ("0" + dateUpdatedAt.getDate()).slice(-2) +' '+dateUpdatedAt.getHours()+ ':' +dateUpdatedAt.getMinutes()+ ':'+dateUpdatedAt.getSeconds();

                result.force = result.force == 1;
                result.createdAt = formattedCreatedAt;
                result.updatedAt = formattedUpdatedAt;

            });

            var out = {};
            if (data.length > 0){
                out.version = data;
            } else {
                out.version = []
            }
            
            res.json(out);
        });
        return common_handler.exceptions(p, req, res)
        .catch(err => {
            console.log('generic error: ', err);
            res.status(400).json({code: 0, message:'failed to show version.' + err});
        });
    } else {
        return res.json({msg: 'not found'});
    }
}

function add_mobile_version(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_mobile_version;
    const requested_resource = {};

    const param_file = req.swagger.params.file;
    const param_force = req.swagger.params.force;
    const param_update_description = req.swagger.params.update_description;

    var file = _.isUndefined(param_file.value)?'':param_file.value;
    var force = _.isUndefined(param_force.value)?'':param_force.value;
    var update_description = _.isUndefined(param_update_description.value)?'':param_update_description.value;
    var path_dir = __dirname+'/../../../static_file/apk';
    var manifest;
    var p =  common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        if (path.extname(file.originalname) == ".apk") {
            try {
                var stats = fs.statSync(path_dir);
                if (!stats.isDirectory()) {
                    fs.unlinkSync(path_dir);
                    fs.mkdirSync(path_dir);
                }
            } catch(err) {
                fs.mkdirSync(path_dir);
            } finally {
                return p_result;
            }
        } else {
            return Promise.reject(new Error("file extension not '.apk'"));
        }
    })
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return ApkReader.open(file.path)
        .then(reader => reader.readManifest()).then((m) => {
            manifest = m
            path_dir = path_dir + '/' + manifest.versionName + '.apk';
            fs.renameSync(file.path, path_dir);
            return Promise.resolve(p_result)
        })
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var d = new Date,
        
        dformat = [ (d.getMonth()+1).padLeft(),
                    d.getDate().padLeft(),
                    d.getFullYear()].join('/')+
                    ' ' +
                  [ d.getHours().padLeft(),
                    d.getMinutes().padLeft(),
                    d.getSeconds().padLeft()].join(':');

        var args = {};
        args.version = manifest.versionName;
        args.download_link = '/apk/download.html';
        args.force = force;
        args.update_description = update_description;
        args.createdAt = dformat;
        args.updatedAt = dformat;
        return (new service_mobile_version()).addMobileVersion(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        res.json(api_response);
    })  
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to add mobile version.' + err});
    });
}

export { get_mobile_version }
export { add_mobile_version }
import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'

import { UserRegistrationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Distributor } from '@/models/orm/index'
import service_user from '@/server/services/user'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'
import base_repo from '@/server/repositories/base'

const acl_default = acl_factory.getDefault().roles('all');
const acl_make_payment = acl_factory.getDefault().resources({
    username: [
        { principal_roles:{ contains:['retailer'] } }
    ]
});

function get_distributors(req, res) {
    console.log('get_distributors');

    const token = req.swagger.params.token.value;
    const acl = acl_default;
    const requested_resource = null;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        console.log('retrieving all distributors.. entity:', Distributor);
        var args = {entity:Distributor};
        return base_repo.get(args).then(result_obj => {
            let {result_list:result_list} = result_obj;
            return [principal, result_list];
        });
    })
    .then(p_result => {
        const [principal, result_list] = p_result;
        var output_options = service_formatter.computeOptions(principal);
        console.log('result_list:', result_list);
        var out = service_formatter.format({obj:result_list, options:output_options});
        console.log('out:', out);
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get user info ' + err});
    });
}

function get_one_distributor(req, res) {
    console.log('get_one_distributor');

    const token = req.swagger.params.token.value;
    const acl = acl_default;
    const requested_resource = null;

    const distributorCode = req.swagger.params.dt_code.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        console.log('retrieving one distributor. entity:', Distributor);
        var args = {entity:Distributor, where:{distributorCode:distributorCode}, options:{throwIfEmpty:true}};
        return base_repo.getOne(args).then(distributor => {
            return [principal, distributor];
        });
    })
    .then(p_result => {
        const [principal, distributor] = p_result;
        var output_options = service_formatter.computeOptions(principal);
        var out = service_formatter.format({obj:distributor, options:output_options});
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: 0, message:'distributor not found.' + err});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get user info. ' + err});
    });
}


export { get_distributors }
export { get_one_distributor }
import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'

// money-access errors
import { MoneyAccessRegisterError } from '@/moneyaccess/error/index'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { DOBInvalidFormat } from '@/server/error/Error'
import { UsernameAlreadyExistsError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePasswordError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Pickup } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import { UserRetailerCode } from '@/models/orm/index'
import { orm } from '@/server/services/mariadb'
import { UserQuery } from '@/models/orm/user'

import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_bank_account from '@/server/services/bankaccount'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import { MoneyAccess } from '@/moneyaccess/index'
import { MoneyAccessErrors } from '@/moneyaccess/index'

import usertypes from '@/models/usertypes'
import { USERTYPES } from '@/models/usertypes'

import { ACCESS_TOKEN_WEBADMIN } from '@/properties'

import { RETAILER_CODE } from '@/models/usertypes'
import { DISTRIBUTOR_CODE } from '@/models/usertypes'
import { DISTRIBUTOR_MANAGER_CODE } from '@/models/usertypes'
import { SUPER_ADMIN_CODE } from '@/models/usertypes'
import { UNILEVER_ADMIN_CODE } from '@/models/usertypes'
import { PICKUP_AGENT_CODE } from '@/models/usertypes'

import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'

const acl_register_fcm = acl_factory.getDefault().addRoles(['all']);

function register_fcm_id(req, res) {
    const token = req.swagger.params.token.value;
    const fcm_id = req.swagger.params.fcm_id.value;
    const acl = acl_register_fcm;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var args = {
            user_id:principal.user.id,
            fcm_id:fcm_id
        };
        return service_user.registerFcm(args).then(api_response => {
            console.log('api respon');
            console.log(api_response);
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'Register FCM failed. ' + err});
    });
}

export { register_fcm_id }
import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'
import moment from 'moment'

// money-access errors
import { MoneyAccessRegisterError } from '@/moneyaccess/error/index'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { DOBInvalidFormat } from '@/server/error/Error'
import { UsernameAlreadyExistsError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePasswordError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Pickup } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import { UserQuery } from '@/models/orm/user'

import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_bank_account from '@/server/services/bankaccount'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import service_informasi from '@/server/services/informasi'
import service_notification from '@/server/services/notification'

import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'
import fs from 'fs'
import path from "path"
import cheerio from "cheerio"

const acl_informasi_dan_promosi = acl_factory.getDefault().addRoles(['all']);

const log = service_logger.getLogger({name: 'controller', service:'informasi dan promosi'});

function get_faqs(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_informasi_dan_promosi;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var faq_dir = path.join(__dirname, "..", "..", "..", "static_file", "faq")

        /*
        */

        var result = []

        fs.readdirSync(faq_dir).forEach(file => {
            var faq_file = path.join(faq_dir, file)

            var content = fs.readFileSync(faq_file).toString();
            var $ = cheerio.load(content);
            var faq = {
                title: $('title').text(),
                id: $('order').text(),
                url: "/faq/"+file
            }
            result.push(faq)

        })
        result = result.sort(function(a,b) {return (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0);} ); 
        return Promise.resolve([principal, result])
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        var data = service_formatter.format({obj:api_response, typeId: "faqs"})
        data.forEach(result => {


        });

        var out = {};
        out.faqs = data;
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to add bank account.' + err});
    });
}


export { get_faqs }

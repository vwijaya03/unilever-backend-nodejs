import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'

// money-access errors
import { MoneyAccessRegisterError } from '@/moneyaccess/error/index'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { DOBInvalidFormat } from '@/server/error/Error'
import { UsernameAlreadyExistsError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePasswordError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Pickup } from '@/models/orm/index'
import { Sales } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import { Transaction } from '@/models/orm/index'
import { BankAccount } from '@/models/orm/index'
import { UserRetailerCode } from '@/models/orm/index'
import { orm } from '@/server/services/mariadb'
import { orm_a } from '@/server/services/mariadb'
import { UserQuery } from '@/models/orm/user'

import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_bank_account from '@/server/services/bankaccount'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import service_otp from '@/server/services/otp'
import { MoneyAccess } from '@/moneyaccess/index'
import { MoneyAccessErrors } from '@/moneyaccess/index'

import usertypes from '@/models/usertypes'
import { USERTYPES } from '@/models/usertypes'

import { ACCESS_TOKEN_WEBADMIN } from '@/properties'
import { allowed_user_status } from '@/properties'

const acl_all = acl_factory.getDefault().addRoles(['all']);

function migrate_retailer(req, res)
{
    return orm.query(`
        SHOW COLUMNS FROM unilever.retailers LIKE 'old_retailer_id'
    `, {type: orm.QueryTypes.RAW})
    .then((column_exist) => {
        
        if(column_exist[0].length > 0) {

            return orm.query(`
                ALTER TABLE retailers
                DROP COLUMN old_retailer_id
            `, {type: orm.QueryTypes.RAW})
            .then(add_retailer(res));
        } else {
            add_retailer(res);
        }
    });
}

function add_retailer(res){
                
    return orm.query(`
        ALTER TABLE retailers
        ADD old_retailer_id varchar(255) NULL DEFAULT NULL
    `, {type: orm.QueryTypes.RAW})
    .then(function(){
        
        return orm_a.query(`
            select * from retailers where le_code <> 'NULL'
        `, {type: orm_a.QueryTypes.SELECT})
        .then(function(retailers){

            let old_retailers = retailers;

            old_retailers.map( (data, idx) => {
                return orm.query(`
                    select * from retailers where le_code_dry = '${data.le_code}'
                `, {type: orm.QueryTypes.SELECT})
                .then(function(new_retailers){
                    if(new_retailers.length == 0) {
                        return orm.query(`
                            INSERT INTO retailers(name, address, le_code_dry, old_retailer_id) 
                            VALUES ('${data.name}', '${data.address}', '${data.le_code}', '${data.retailer_id}')
                        `, {type: orm.QueryTypes.INSERT}).then(result => {

                        });
                    }
                });
            });

            res.json({message: 'retailer done'});
        });
    });
}

function migrate_distributor(req, res) 
{
    return orm.query(`
        SHOW COLUMNS FROM unilever.distributors LIKE 'old_distributor_id'
    `, {type: orm.QueryTypes.RAW})
    .then((column_exist) => {
        
        if(column_exist[0].length > 0) {

            return orm.query(`
                ALTER TABLE distributors
                DROP COLUMN old_distributor_id
            `, {type: orm.QueryTypes.RAW})
            .then(add_distributor(res));
        } else {
            add_distributor(res);
        }
    });
}

function add_distributor(res){
                
    return orm.query(`
        ALTER TABLE distributors
        ADD old_distributor_id varchar(255) NULL DEFAULT NULL
    `, {type: orm.QueryTypes.RAW})
    .then(function(){
        
        return orm_a.query(`
            select * from distributors where dt_code <> 'NULL'
        `, {type: orm_a.QueryTypes.SELECT})
        .then(function(distributors){

            let old_distributors = distributors;

            old_distributors.map( (data, idx) => {
                return orm.query(`
                    select * from distributors where dt_code = '${data.dt_code}'
                `, {type: orm.QueryTypes.SELECT})
                .then(function(new_distributors){
                    if(new_distributors.length == 0) {
                        return orm.query(`
                            INSERT INTO distributors(dt_code, name, old_distributor_id) 
                            VALUES ('${data.dt_code}', '${data.name}', '${data.distributor_id}')
                        `, {type: orm.QueryTypes.INSERT}).then(result => {

                        });
                    }
                });
            });

            res.json({message: 'distributor done'});
        });
    });
}

function migrate_pickup(req, res) 
{
    return orm.query(`
        SHOW COLUMNS FROM unilever.pickup LIKE 'old_pickup_id'
    `, {type: orm.QueryTypes.RAW})
    .then((column_exist) => {
        
        if(column_exist[0].length > 0) {

            return orm.query(`
                ALTER TABLE pickup
                DROP COLUMN old_pickup_id
            `, {type: orm.QueryTypes.RAW})
            .then(add_pickup(res));
        } else {
            add_pickup(res);
        }
    });
}

function add_pickup(res){
                
    return orm.query(`
        ALTER TABLE pickup
        ADD old_pickup_id varchar(255) NULL DEFAULT NULL
    `, {type: orm.QueryTypes.RAW})
    .then(function(){
        
        return orm_a.query(`
            select * from pickup where pickup_code <> 'NULL'
        `, {type: orm_a.QueryTypes.SELECT})
        .then(function(pickup){

            let old_pickup = pickup;

            old_pickup.map( (data, idx) => {
                return orm.query(`
                    select * from pickup where pickup_code = '${data.pickup_code}'
                `, {type: orm.QueryTypes.SELECT})
                .then(function(new_pickup){
                    if(new_pickup.length == 0) {
                        return orm.query(`
                            INSERT INTO pickup(pickup_code, old_pickup_id) 
                            VALUES ('${data.pickup_code}', '${data.id}')
                        `, {type: orm.QueryTypes.INSERT}).then(result => {

                        });
                    }
                });
            });

            res.json({message: 'pickup done'});
        });
    });
}

function migrate_invoice(req, res) 
{
    return orm_a.query(`
        select * from invoice limit 40
    `, {type: orm_a.QueryTypes.SELECT})
    .then(function(invoice){

        let old_invoice = invoice;

        old_invoice.map( (data, idx) => {
            return orm.query(`
                select * from invoice where invoice_id = '${data.invoice_id}' and dt_code = '${data.dt_code}'
            `, {type: orm.QueryTypes.SELECT})
            .then(function(new_invoice){
                if(new_invoice.length == 0) {
                    return orm.query(`
                        INSERT INTO invoice(dt_code, le_code, outlet_code, cash_memo_total_amount, cash_memo_balance_amount, cashmemo_type, invoice_id, invoice_sales_date, invoice_due_date, invoice_payment_status_paid, invoice_payment_status_unpaid)

                        VALUES ('${data.dt_code}', '${data.le_code}', '${data.outlet_code}', '${data.cash_memo_total_amount}', '${data.cash_memo_balance_amount}', '${data.cashmemo_type}', '${data.invoice_id}', '${data.invoice_sales_date}', '${data.invoice_due_date}', '${data.invoice_payment_status_paid}', '${data.invoice_payment_status_unpaid}')
                    `, {type: orm.QueryTypes.INSERT}).then(result => {

                    });
                }
            });
        });

        res.json({message: 'invoice done'});
    });
}

function migrate_user(req, res)
{
    return orm.query(`
        SELECT COLUMN_NAME, COLUMN_TYPE, COLUMN_DEFAULT, IS_NULLABLE
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_SCHEMA='unilever' AND TABLE_NAME='users' and (COLUMN_NAME = 'old_user_id' or COLUMN_NAME = 'old_valdo_account')
    `, {type: orm.QueryTypes.RAW})
    .then((column_exist) => {
        if(column_exist[0].length == 1) {
            if(column_exist[0][0].COLUMN_NAME == 'old_user_id') {
                return orm.query(`
                    ALTER TABLE users
                    DROP COLUMN old_user_id
                `, {type: orm.QueryTypes.RAW})
                .then(add_user(res, 'old_user_id'));
            } else {
                return orm.query(`
                    ALTER TABLE users
                    DROP COLUMN old_valdo_account
                `, {type: orm.QueryTypes.RAW})
                .then(add_user(res, 'old_valdo_account'));
            }
        } if(column_exist[0].length == 2) {
            return orm.query(`
                ALTER TABLE users
                DROP COLUMN old_user_id,
                DROP COLUMN old_valdo_account
            `, {type: orm.QueryTypes.RAW})
            .then(add_user(res));
        } else {
            add_user(res, 'all');
        }
    });
}

function add_user(res, table){
    
    var alter_query = '';
    var role_query = '';
    var insert_role_query = '';

    var formattedValdoAccount = '';
    var kode_perusahaan = '01';

    var str = "";
    var pad = "0000000";

    var unique_number = "";   
    var increment_max = 0;

    var users_alpha_array = [];
    var exist_email_alpha_in_bravo_array = [];

    var obj2 = {};
    var obj_retailer = [];
    var obj_pickup = [];
    var obj_dist = [];

    var retailer_array = [];
    var pickup_array = [];
    var distributor_array = [];

    if(table == 'old_user_id') {
        alter_query = `
            ALTER TABLE users
            ADD old_user_id varchar(50) NULL DEFAULT NULL
        `;
    } else if(table == 'old_valdo_account') {
        alter_query = `
            ALTER TABLE users
            ADD old_valdo_account varchar(50) NULL DEFAULT NULL
        `;
    } else {
        alter_query = `
            ALTER TABLE users
            ADD old_user_id varchar(50) NULL DEFAULT NULL,
            ADD old_valdo_account varchar(50) NULL DEFAULT NULL
        `;
    }

    return orm.query(alter_query, {type: orm.QueryTypes.RAW})
    .then(function(){
        
        return orm_a.query(`
            select * from users where user_id >= 182
        `, {type: orm_a.QueryTypes.SELECT})
        .then(function(users){
            return users;
            // res.json({message: 'user done'});
        })
        .then(users_alpha => {
            users_alpha.map( (data, idx) => { 
                users_alpha_array.push('"'+data.email+'"');
            });

            var obj = {};
            obj.users_alpha = users_alpha;
            obj.users_alpha_email = users_alpha_array;

            return obj;
        })
        .then(obj => {
            return Promise.map(obj.users_alpha, (data, index) => {
                return orm.query(`
                    select * from users where email = '${data.email}'
                `, {type: orm.QueryTypes.SELECT});
            })
            .then(users_bravo => {
                obj.users_bravo = users_bravo;

                return obj;
            });
        })
        .then(obj => {
            return Promise.map(obj.users_bravo, (data_bravo, index) => {
                if(data_bravo[index] !== undefined) {
                    exist_email_alpha_in_bravo_array.push('"'+data_bravo[index].email+'"');
                }
            })
            .then(() => {
                obj.exist_email_alpha_in_bravo = exist_email_alpha_in_bravo_array;
                return obj;
            });
        })
        .then(obj => {  
            
            if(obj.exist_email_alpha_in_bravo.length == 0) {
                obj.exist_email_alpha_in_bravo = '""';
            }

            return orm_a.query(`
                select * from users where user_id >= 182 and email not in (`+obj.exist_email_alpha_in_bravo+`)
            `, {type: orm_a.QueryTypes.SELECT})
            .then(function(users){
                obj.filtered_user_alpha = users;
                return obj;
            });
        })
        .then(obj => {
            return Promise.map(obj.filtered_user_alpha, (val, index) => {
                if(val.type == 0) {
                    role_query = `select * from retailers where old_retailer_id = '${val.retailer_id}'`;

                    return orm.query(role_query, {type: orm.QueryTypes.SELECT})
                    .then(function(retailer){
                        //console.log('isi retailer = '+val.retailer_id+' '+retailer);
                        if(retailer.length != 0) {
                            User.find({
                                where: {
                                    type: 0,
                                    email: val.email
                                }
                            })
                            .then(exist_retailer => {
                                if(exist_retailer == null) {
                                    User.create({
                                        password:           val.password,
                                        password_changed:   "false",
                                        pin:                val.pin,
                                        email:              val.email,
                                        fullname:           val.fullname,
                                        phone:              val.phone,
                                        salt:               val.salt,
                                        type:               val.type,
                                        retailer_id:        retailer[0].retailer_id,
                                        accessToken:        val.access_token,
                                        accessTokenExpiry:  val.access_token_expiry,
                                        valdo_account:      100100,
                                        status:             0
                                    })
                                    .then(result => {
                                        console.log(val.user_id, val.valdo_account);
                                        unique_number = pad.substring(0, pad.length - result.id.toString().length) + result.id.toString();

                                        formattedValdoAccount = "1"+kode_perusahaan+unique_number;
                                        orm.query(`
                                            update users
                                            set valdo_account = '${formattedValdoAccount}', old_user_id = '${val.user_id}', old_valdo_account = '${val.valdo_account}'
                                            where user_id = '${result.id}'
                                        `, {type: orm.QueryTypes.UPDATE});
                                    });
                                }
                            });
                        }
                        
                    });
                } else if(val.type == 1) {
                    role_query = `select * from distributors where old_distributor_id = '${val.distributor_id}'`;

                    return orm.query(role_query, {type: orm.QueryTypes.SELECT})
                    .then(function(distributor){

                        if(distributor.length != 0) {
                            User.find({
                                where: {
                                    type: 1,
                                    email: val.email
                                }
                            })
                            .then(exist_distributor => {
                                if(exist_distributor == null) {
                                    User.create({
                                        password:           val.password,
                                        password_changed:   "false",
                                        pin:                val.pin,
                                        email:              val.email,
                                        fullname:           val.fullname,
                                        phone:              val.phone,
                                        salt:               val.salt,
                                        type:               val.type,
                                        distributorId:      distributor[0].distributor_id,
                                        accessToken:        val.access_token,
                                        accessTokenExpiry:  val.access_token_expiry,
                                        valdo_account:      100100,
                                        status:             0
                                    })
                                    .then(result => {
                                        // console.log(val.user_id, val.valdo_account);
                                        unique_number = pad.substring(0, pad.length - result.id.toString().length) + result.id.toString();

                                        formattedValdoAccount = "2"+kode_perusahaan+unique_number;
                                        orm.query(`
                                            update users
                                            set valdo_account = '${formattedValdoAccount}', old_user_id = '${val.user_id}', old_valdo_account = '${val.valdo_account}'
                                            where user_id = '${result.id}'
                                        `, {type: orm.QueryTypes.UPDATE});
                                    });
                                }
                            });
                        }
                        
                    });
                } else if(val.type == 2) {
                    role_query = `select * from pickup where old_pickup_id = '${val.pickup_id}'`;

                    return orm.query(role_query, {type: orm.QueryTypes.SELECT})
                    .then(function(pickup){

                        if(pickup.length != 0) {
                            User.find({
                                where: {
                                    type: 2,
                                    email: val.email
                                }
                            })
                            .then(exist_pickup => {
                                if(exist_pickup == null) {
                                    User.create({
                                        password:           val.password,
                                        password_changed:   "false",
                                        pin:                val.pin,
                                        email:              val.email,
                                        fullname:           val.fullname,
                                        phone:              val.phone,
                                        salt:               val.salt,
                                        type:               val.type,
                                        pickupId:           pickup[0].id,
                                        accessToken:        val.access_token,
                                        accessTokenExpiry:  val.access_token_expiry,
                                        valdo_account:      100100,
                                        status:             0
                                    })
                                    .then(result => {
                                        //console.log(val.user_id, val.valdo_account);
                                        unique_number = pad.substring(0, pad.length - result.id.toString().length) + result.id.toString();

                                        formattedValdoAccount = "3"+kode_perusahaan+unique_number;
                                        orm.query(`
                                            update users
                                            set valdo_account = '${formattedValdoAccount}', old_user_id = '${val.user_id}', old_valdo_account = '${val.valdo_account}'
                                            where user_id = '${result.id}'
                                        `, {type: orm.QueryTypes.UPDATE});
                                    });
                                }
                            });
                        }
                        
                    });
                }

                return "done";
            })
            .then( () => {
                res.json({message: 'users done'}); 
            });
        });
    });
}

function migrate_user_banking(req, res)
{
    return orm_a.query(`
        select * from users_bankings where user_id >= 182
    `, {type: orm_a.QueryTypes.SELECT})
    .then(ub => {

        return Promise.map(ub, (data_ub, index) => {
            BankAccount.find({
                where: {
                    user_id: data_ub.user_id,
                    no_rekening: data_ub.no_rekening
                }
            })
            .then(exist_bank_account => {
                if(exist_bank_account == null) {
                    BankAccount.create({
                        user_id: data_ub.user_id,
                        name: data_ub.name,
                        no_rekening: data_ub.no_rekening,
                        bank: data_ub.bank
                    });
                }

                return "done";
            });
        })
        .then( () => {
            return orm.query(`
                select users_bankings.id, users_bankings.no_rekening, users_bankings.user_id as bank_user_id, users.user_id, users.old_user_id, users.fullname, users.email from users_bankings
                inner join users on users.old_user_id = users_bankings.user_id
            `, {type: orm_a.QueryTypes.SELECT})
            .then(result => {
                return Promise.map(result, (data, index) => {
                    orm.query(`
                        update users_bankings
                        set user_id = '${data.user_id}'
                        where user_id = '${data.old_user_id}' and no_rekening = '${data.no_rekening}'
                    `, {type: orm.QueryTypes.UPDATE});
                });
            })
            .then( () => {
                res.json({message: 'Users bankings done'});
            });
        });
    });
}

function migrate_transactions(req, res)
{
    const old_user_id = req.swagger.params.old_user_id.value;
    const old_dt_code = req.swagger.params.old_dt_code.value;

    return orm.query(`
        select user_id, retailer_id from users where old_user_id = '`+old_user_id+`'
        union
        select old_distributor_id, dt_code from distributors where dt_code = '`+old_dt_code+`'
    `, {type: orm_a.QueryTypes.SELECT})
    .then(user_bravo => {
        var obj = {};
        obj.user_bravo = user_bravo;
        return obj;
    })
    .then(obj => {
        return orm_a.query(`
            select * from transactions
            where from_user_id = '`+old_user_id+`' and distributor_id = '`+obj.user_bravo[1].user_id+`'
        `, {type: orm_a.QueryTypes.SELECT})
        .then(t_alpha => {
            
            t_alpha.map( (t_alpha_map, index) => {
                orm.query(`
                    select * from transactions where valdo_tx_id = '`+t_alpha_map.valdo_tx_id+`'
                `, {type: orm_a.QueryTypes.SELECT})
                .then(exist_t_bravo => {

                    orm.query(`
                        select user_id from users where old_user_id = '`+t_alpha_map.to_user_id+`'
                    `, {type: orm_a.QueryTypes.SELECT})
                    .then(result => {
                        
                        if(exist_t_bravo.length == 0) {
                            Transaction.create({
                                fromUserId: obj.user_bravo[0].user_id,
                                toUserId: result[0].user_id,
                                dt_code: old_dt_code,
                                invoiceId: t_alpha_map.invoice_id,
                                invoice_code: old_dt_code+t_alpha_map.invoice_id,
                                retailerId: obj.user_bravo[0].retailer_id,
                                distributorId: obj.user_bravo[1].user_id,
                                type: t_alpha_map.type,
                                amount: t_alpha_map.amount,
                                currencyCode: t_alpha_map.currency_code,
                                transactDate: t_alpha_map.transact_date,
                                valdoTxId: t_alpha_map.valdo_tx_id,
                                updated_at: t_alpha_map.updated_at
                            });
                        }
                    });
                    
                });
            });
        });
    })
    .then( () => {
        res.json({message: 'transactions done'});
    });
}

function getToUserId(user_id)
{
    return new Promise(function(resolve){
        return orm.query(`
            select user_id from users where old_user_id = '`+user_id+`'
        `, {type: orm.QueryTypes.SELECT})
        .then(result => {
            return result;
        });
    });
    // Promise.resolve()
    // .then( () => {
        
    // })
    // .then(result => {
    //     console.log(result);
    // });

}

export { migrate_retailer }
export { migrate_distributor }
export { migrate_pickup }
export { migrate_invoice }
export { migrate_user }
export { migrate_user_banking }
export { migrate_transactions }
import util from 'util'
import path from 'path'
import { sprintf } from 'sprintf-js'
import _ from 'lodash'
import moment from 'moment'
import formidable from 'formidable'
import multer from 'multer'
import properties from '@/properties.js'
import service_excel from '@/server/services/excel'

import { ACCESS_TOKEN_WEBADMIN } from '@/properties'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Invoice } from '@/models/orm/index'
import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_invoice from '@/server/services/invoice'
import service_test from '@/server/services/test'
import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'
import service_formatter from '@/server/services/formatter'

import { ResourceNotFoundError } from '@/server/error/Error'

/////////////////////////////////////////////////////////////////////////////////////////////

const log = service_logger.getLogger({name: 'controller', service:'test'});
const DEBUG_LOG = true;

const acl_default = acl_factory.getDefault();

function init(req, res) {
    log.debug('init');

    const token = req.swagger.params.token.value;
    const acl = acl_default;
    const requested_resource = null;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return service_test.init();
    })
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        log.error('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get roles.' + err});
    });
}

function reset_paid_invoices(req, res) {
    log.debug('reset_paid_invoices');

    const token = req.swagger.params.token.value;
    const acl = acl_default;
    const requested_resource = null;

    const param_invoice_id = req.swagger.params.invoice_id;

    var invoiceId = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        var args = {};
        if (invoiceId) {
            args.invoiceId = invoiceId;
        }
        return service_test.reset_paid_invoices(args);
    })
    .then(p_result => {
        res.json({message:'done'});
    })
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        log.error('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to reset paid status.' + err});
    });
}

export { init }
export { reset_paid_invoices }

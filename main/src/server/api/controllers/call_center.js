import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'
import moment from 'moment'

// money-access errors
import { MoneyAccessRegisterError } from '@/moneyaccess/error/index'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { DOBInvalidFormat } from '@/server/error/Error'
import { UsernameAlreadyExistsError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePasswordError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Logs } from '@/models/orm/index'
import { Pickup } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import { UserQuery } from '@/models/orm/user'

import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_bank_account from '@/server/services/bankaccount'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import service_informasi from '@/server/services/informasi'
import service_notification from '@/server/services/notification'
import service_call_center from '@/server/services/call_center'
import { MoneyAccess } from '@/moneyaccess/index'

import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'

const acl_call_center = acl_factory.getDefault().addRoles(['call_center']);

const log = service_logger.getLogger({name: 'controller call center', service:'call center'});

function call_center_reset_password(req, res)
{
    const log_action = "call center reset password";

    const token = req.swagger.params.token.value;
    const acl = acl_call_center;
    const requested_resource = {};
    const param_fullname = req.swagger.params.fullname;
    const param_pob = req.swagger.params.pob;
    const param_dob = req.swagger.params.dob;

    var fullname = _.isUndefined(param_fullname.value)?'':param_fullname.value;
    var pob = _.isUndefined(param_pob.value)?'':param_pob.value;
    var dob = _.isUndefined(param_dob.value)?'':param_dob.value;

    Logs.create({action: log_action, description: "param req fullname: "+fullname+", pob: "+pob+", dob: "+dob});
    
    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        if(fullname == null || fullname == "") {
            throw new Error('Nama lengkap harus di isi');
        } if(pob == null || pob == "") {
            throw new Error('Tempat lahir harus di isi');
        } if(dob == null || dob == "") {
            throw new Error('Tanggal lahir harus di isi');
        }
        var args = {};
        args.fullname = fullname;
        args.pob = pob;
        args.dob = dob;
        // console.log('get_informasi_dan_promosi.', args);

        return service_user.reset_password_by_call_center(args).then(api_response => {
            console.log(api_response);
            return [principal, api_response];
        });
    })
    .then(result => {
        var [principal, api_response] = result;
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'Gagal reset password.' + err});
    });
}

function call_center_reset_pin(req, res)
{
    const log_action = "call center reset pin";

    const token = req.swagger.params.token.value;
    const acl = acl_call_center;
    const requested_resource = {};
    const param_fullname = req.swagger.params.fullname;
    const param_pob = req.swagger.params.pob;
    const param_dob = req.swagger.params.dob;

    var fullname = _.isUndefined(param_fullname.value)?'':param_fullname.value;
    var pob = _.isUndefined(param_pob.value)?'':param_pob.value;
    var dob = _.isUndefined(param_dob.value)?'':param_dob.value;

    Logs.create({action: log_action, description: "param req fullname: "+fullname+", pob: "+pob+", dob: "+dob});

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        Logs.create({action: log_action, description: "call center user: "+principal.user.fullname});

        if(fullname == null || fullname == "") {
            throw new Error('Nama lengkap harus di isi');
        } if(pob == null || pob == "") {
            throw new Error('Tempat lahir harus di isi');
        } if(dob == null || dob == "") {
            throw new Error('Tanggal lahir harus di isi');
        }
        var args = {};
        args.fullname = fullname;
        args.pob = pob;
        args.dob = dob;
        // console.log('get_informasi_dan_promosi.', args);

        return service_user.getOneUserByCallCenter(args).then(user => {
            return [principal, user];
        });
    })
    .then(result => {
        var [principal, user] = result;
        
        var args = {
            vaccount:user.valdoAccount,
            usertype:user.type
        };

        Logs.create({action: log_action, description: "isi args dari getOneUserByCallCenter, vaccount: "+user.valdoAccount+', usertype: '+user.type});

        return MoneyAccess.resetpin(args).then(api_response => {
            Logs.create({action: log_action, description: "isi response dari money access vaccount: "+api_response.userinfo.vaccount});
            return [principal, user, api_response];
        });
    })
    .then(result => {
        var [principal, user, api_response] = result;

        var args = {};
        args.pin = api_response.userinfo.new_pin;
        args.user_salt = user.salt;
        args.user_id = user.id;
        args.email = user.email;

        return service_user.resetpin(args).then(api_response => {
            // console.log(api_response);
            return api_response;
        });  
    })
    .then(result => {
        res.json(result);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'Gagal reset password.' + err});
    });
}

function call_center_get_customer_data(req, res)
{
    const log_action = "call center get customer data";

    const token = req.swagger.params.token.value;
    const acl = acl_call_center;
    const requested_resource = {};
    const param_fullname = req.swagger.params.fullname;
    const param_pob = req.swagger.params.pob;
    const param_dob = req.swagger.params.dob;

    var fullname = _.isUndefined(param_fullname.value)?'':param_fullname.value;
    var pob = _.isUndefined(param_pob.value)?'':param_pob.value;
    var dob = _.isUndefined(param_dob.value)?'':param_dob.value;

    Logs.create({action: log_action, description: "param req fullname: "+fullname+", pob: "+pob+", dob: "+dob});
    
    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        if(fullname == null || fullname == "") {
            throw new Error('Nama lengkap harus di isi');
        } if(pob == null || pob == "") {
            throw new Error('Tempat lahir harus di isi');
        } if(dob == null || dob == "") {
            throw new Error('Tanggal lahir harus di isi');
        }
        var args = {};
        args.fullname = fullname;
        args.pob = pob;
        args.dob = dob;
        // console.log('get_informasi_dan_promosi.', args);

        return service_user.getOneCustomerByCallCenter(args).then(user => {
            return user;
        });
    })
    .then(result => {
        var out = {};
        out.fullname = result.fullname;
        out.nama_toko = result.nama_toko;
        out.phone = result.phone;
        out.tanggal_lahir = result.tanggal_lahir;
        out.tempat_lahir = result.tempat_lahir;
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'Gagal mendapatkan data customer.' + err});
    });
}

export { call_center_reset_password }
export { call_center_reset_pin }
export { call_center_get_customer_data }

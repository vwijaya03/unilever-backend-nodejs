import util from 'util'
import { sprintf } from 'sprintf-js'
import _ from 'lodash'
import moment from 'moment'
import formidable from 'formidable'
import multer from 'multer'
import properties from '@/properties.js'
import fs from 'fs'
import path from 'path'
import async from 'async'

import { UnauthorizedRequestError } from '@/server/error/Error'

import { ACCESS_TOKEN_WEBADMIN } from '@/properties'
import { ACCESS_TOKEN_INTERNAL_DEV } from '@/properties'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Invoice } from '@/models/orm/index'
import { Products } from '@/models/orm/index'
import { Task } from '@/models/orm/index'
import base_repo from '@/server/repositories/base'
import service_invoice from '@/server/services/invoice'
import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'
import service_logger from '@/server/services/logger'
import service_formatter from '@/server/services/formatter'

import { ResourceNotFoundError } from '@/server/error/Error'

/////////////////////////////////////////////////////////////////////////////////////////////

const log = service_logger.getLogger({name: 'controller', service:'invoice'});
const DEBUG_LOG = true;
const app_version = properties.version;
const public_www_dir = properties.public_www_dir;
const tmpDir = properties.tmp_dir;
const publicDir = properties.public_dir;
const uploadDir = path.resolve(public_www_dir, '.');

log.info('public_www_dir:', public_www_dir);
log.info('tmpDir:', tmpDir);
log.info('publicDir:', publicDir);
log.info('uploadDir:', uploadDir);

const acl_default = acl_factory.getDefault();
const acl_get_retur = acl_factory.getDefault().addRoles(['all']);
const acl_get_invoices = acl_factory.getDefault().addRoles(['retailer','distributor','unilever_admin', 'distributor_manager', 'sales', 'super_admin']);
const acl_get_one_invoice = acl_factory.getDefault().addRoles(['retailer', 'sales', 'super_admin']);
const acl_get_one_retur = acl_factory.getDefault().addRoles(['retailer', 'sales', 'super_admin']);
const acl_request_retur = acl_factory.getDefault().addRoles('all');
const acl_check_invoices = acl_factory.getDefault().addRoles('all');

function enhance_upload(req, res) {

    const token = req.swagger.params.token.value;
    const param_filename = req.swagger.params.filename;
    const param_dt_code = req.swagger.params.dt_code;
    const acl = acl_check_invoices;
    const requested_resource = {};

    const exec = require('child_process').execSync;

    var d = new Date();
    var current_datetime = d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();

    var filename = _.isUndefined(param_filename.value)?null:param_filename.value;
    var dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    var path_dir = __dirname+'/../../../static_file/data-invoice/'+filename;
    var dest_dir = __dirname+'/../../../static_file/data-invoice/'+"remove_line_"+current_datetime+"_"+filename;

    var total_invoice = 0;
    var action_invoice = "";
    var status = 0;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {

        const [principal, is_allowed] = p_result;

        if (!principal.user) {
            throw new Error('no associated user with this principal.');
        }
        if(principal.user.type == 1 || principal.user.type == 3) {
            return principal.user.getDistributor().then(distributor => {
                return [principal, distributor];
            });
        }
        else
        {
            if(dt_code == null || dt_code == '')
            {
                throw new Error('Distributor Code tidak boleh kosong');
            }
            else 
            {
                return principal.user.getDistributor().then(distributor => {
                    return [principal, distributor];
                });
            }
        }
    })
    .then(p_result => {

        var remove_2_lines_from_bottom = "head --lines=-2 '" + path_dir + "' > '" +dest_dir+"'";

        exec(remove_2_lines_from_bottom, (error, stdout, stderr) => {
            //console.log('rmv 2 line');
            //console.log(error);
        });

        exec("rm -rf '"+path_dir+"'", (error, stdout, stderr) => {
            //console.log('exec rm');
            //console.log(error);
        });

        return p_result;
    })
    .then(p_result => {

        var sql_command = `mysql -u nodebackend -psecretpass --local-infile=1 unilever -e "LOAD DATA LOCAL INFILE '`+dest_dir+`' INTO TABLE excel_dump FIELDS TERMINATED BY ',' IGNORE 3 LINES (@col1, @col2, @col3, @col4, @col5, @col6, @col7, @col8, @col9, @col10, @col11, @col12, @col13, @col14, @col15, @col16, @col17, @col18, @col19)  SET session_id='123', file_name='`+'remove_line_'+current_datetime+'_'+filename+`', dt_code=@col1, le_code=@col2, outlet_code=@col3, cash_memo_total_amount=@col6, cash_memo_balance_amount=@col7, cashmemo_type=@col8, invoice_id=@col9, invoice_sales_date=@col11, invoice_due_date=@col12, invoice_payment_status=@col13, product=@col14, product_name=@col15, quantity=@col16, product_price_cs=@col17, product_price_dz=@col18, product_price_pc=@col19"`;

        exec(sql_command, (error, stdout, stderr) => {
            //console.log('exec sql command');
            //console.log(error);
        });

        return p_result;
    })
    .then(p_result => {
        var [principal, distributor] = p_result;

        var user_id = principal.user.id;

        var out = {code: 200, message: 'Upload success.'};
        res.json(out);

        if(principal.user.type == 1 || principal.user.type == 3) {
            dt_code = distributor.distributorCode;
        }

        return Task.create({
            dt_code: dt_code,
            original_name: filename,
            file_name: 'remove_line_'+current_datetime+'_'+filename,
            status: status,
            total_row: total_invoice,
            processed_row: 0,
            updated_row: 0,
            action: action_invoice,
            created: current_datetime,
            user_id: user_id,
            total_upload: total_invoice

        })
        .then(result => {
            var task_id = result.id;
            return task_id;
        }); 
        
    }).then(task_id => {
        //console.log("isi task id");
        //console.log(task_id);

        return service_invoice.enhanceGetDataFromExcelDump({filename: 'remove_line_'+current_datetime+"_"+filename}).then(excel_dump => {
            var result = {};
            result.excel_dump = excel_dump;
            result.task_id = task_id;

            return result;
        });
    })
    .then(result => {
        var result_task_id = result.task_id;
        var total_row = result.excel_dump.length;

        Task.update({total_row: result.excel_dump.length, total_upload: result.excel_dump.length},{where: {id: result_task_id}});

        return service_invoice.insertInvoiceFromExcelDump({filename: 'remove_line_'+current_datetime+"_"+filename})
        .then(insert_invoice => {
            service_invoice.insertProductFromExcelDump({filename: 'remove_line_'+current_datetime+"_"+filename});

            var result = {};
            result.task_id = result_task_id;
            result.total_row = total_row;

            return result;
        });
    })
    .then(result => {
        return service_invoice.enhanceCheckExcelDumpToInvoice({filename: 'remove_line_'+current_datetime+"_"+filename}).then(data => {
            result.data = data;
            return result;
        });
    })
    .then(result_invoice => {

        var processed_invoice = 0;
        var updated_invoice = 1;

        result_invoice.data.map( (data, idx) => {
            if(data.result == 2) {
                action_invoice += " invoice_id: "+data.ed_invoice_id+" dt_code: "+data.ed_dt_code+" dari cash memo balance amount "+data.cash_memo_balance_amount+" menjadi "+data.ed_balance_amount+" ,";
                    
                Task.update({action: action_invoice, updated_row:updated_invoice}, {where: {id: result_invoice.task_id}});

                Invoice.update({cash_memo_balance_amount: data.ed_balance_amount}, {where: {invoice_id: data.ed_invoice_id, dt_code: data.ed_dt_code}});

                updated_invoice++;
            }

            processed_invoice++;
            
            if(processed_invoice == Math.floor(result_invoice.total_row * 0.2)) {
                Task.update({processed_row: processed_invoice}, {where: {id: result_invoice.task_id }});
            } else if(processed_invoice == Math.floor(result_invoice.total_row * 0.4)) {
                Task.update({processed_row: processed_invoice}, {where: {id: result_invoice.task_id }});
            } else if(processed_invoice == Math.floor(result_invoice.total_row * 0.6)) {
                Task.update({processed_row: processed_invoice}, {where: {id: result_invoice.task_id }});
            } else if(processed_invoice == Math.floor(result_invoice.total_row * 0.8)) {
                Task.update({processed_row: processed_invoice}, {where: {id: result_invoice.task_id }});
            } else if(processed_invoice == result_invoice.total_row) {
                Task.update({processed_row: processed_invoice}, {where: {id: result_invoice.task_id }});
            }

        });

        Task.update({status: 1}, {where: {id: result_invoice.task_id }}); 
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to upload invoice. ' + err});
    });
}

function upload(req, res) {

    const token = req.swagger.params.token.value;
    const param_filename = req.swagger.params.filename;
    const param_dt_code = req.swagger.params.dt_code;
    const acl = acl_check_invoices;
    const requested_resource = {};

    const exec = require('child_process').execSync;

    var d = new Date();
    var current_datetime = d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();

    var filename = _.isUndefined(param_filename.value)?null:param_filename.value;
    var dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    var path_dir = __dirname+'/../../../static_file/data-invoice/'+filename;
    var dest_dir = __dirname+'/../../../static_file/data-invoice/'+"remove_line_"+current_datetime+"_"+filename;

    var total_invoice = 0;
    var processed_invoice = 0;
    var action_invoice = "";
    var status = 0;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {

        const [principal, is_allowed] = p_result;

        if (!principal.user) {
            throw new Error('no associated user with this principal.');
        }
        if(principal.user.type != 1) {
            if(dt_code == null || dt_code == '')
            {
                throw new Error('Distributor Code tidak boleh kosong');
            }
            else 
            {
                return principal.user.getDistributor().then(distributor => {
                    return [principal, distributor];
                });
            }
        }
        else
        {
            return principal.user.getDistributor().then(distributor => {
                return [principal, distributor];
            });
        }
    })
    .then(p_result => {

        var remove_2_lines_from_bottom = "head --lines=-2 '" + path_dir + "' > '" +dest_dir+"'";

        exec(remove_2_lines_from_bottom, (error, stdout, stderr) => {
            console.log('rmv 2 line');
            //console.log(error);
        });

        exec("rm -rf '"+path_dir+"'", (error, stdout, stderr) => {
            console.log('exec rm');
            //console.log(error);
        });

        return p_result;
    })
    .then(p_result => {

        var sql_command = `mysql -u nodebackend -psecretpass --local-infile=1 unilever -e "LOAD DATA LOCAL INFILE '`+dest_dir+`' INTO TABLE excel_dump FIELDS TERMINATED BY ',' IGNORE 3 LINES (@col1, @col2, @col3, @col4, @col5, @col6, @col7, @col8, @col9, @col10, @col11, @col12, @col13, @col14, @col15, @col16, @col17, @col18, @col19)  SET session_id='123', file_name='`+'remove_line_'+current_datetime+'_'+filename+`', dt_code=@col1, le_code=@col2, outlet_code=@col3, cash_memo_total_amount=@col6, cash_memo_balance_amount=@col7, cashmemo_type=@col8, invoice_id=@col9, invoice_sales_date=@col11, invoice_due_date=@col12, invoice_payment_status=@col13, product=@col14, product_name=@col15, quantity=@col16, product_price_cs=@col17, product_price_dz=@col18, product_price_pc=@col19"`;

        exec(sql_command, (error, stdout, stderr) => {
            console.log('exec sql command');
            console.log(error);
        });

        return p_result;
    })
    .then(p_result => {
        var [principal, distributor] = p_result;

        var user_id = principal.user.id;

        if(principal.user.type == 1) {
            dt_code = distributor.distributorCode;
        }

        return Task.create({
            dt_code: dt_code,
            original_name: filename,
            file_name: 'remove_line_'+current_datetime+'_'+filename,
            status: status,
            total_row: total_invoice,
            processed_row: processed_invoice,
            action: action_invoice,
            created: current_datetime,
            user_id: user_id,
            total_upload: total_invoice

        })
        .then(result => {
            var task_id = result.id;
            return task_id;
        }); 
        
    }).then(task_id => {
        //console.log("isi task id");
        //console.log(task_id);

        return service_invoice.getDataFromExcelDump({filename: 'remove_line_'+current_datetime+"_"+filename}).then(excel_dump => {
            var result = {};
            result.excel_dump = excel_dump;
            result.task_id = task_id;

            return result;
        });
    })
    .then(result => {
        var result_task_id = result.task_id;

        Task.update(
            {
                total_row: result.excel_dump.length,
                total_upload: result.excel_dump.length
            },
            { 
                where: 
                { 
                    id: result_task_id
                }
            }
        );

        async.eachSeries(result.excel_dump, function(excel_dump_invoices, callbackEach) {

            service_invoice.checkExcelDumpToInvoice({invoice_id: excel_dump_invoices.invoice_id, dt_code: excel_dump_invoices.dt_code}).then(result => {

                if(result != null && result.length != 0 && result[0].cash_memo_balance_amount >= excel_dump_invoices.cash_memo_balance_amount)
                {
                    // rewrite
                    //console.log('isi balance amount');
                    //console.log(excel_dump_invoices.cash_memo_balance_amount);

                    action_invoice += " invoice_id: "+excel_dump_invoices.invoice_id+" dt_code: "+excel_dump_invoices.dt_code+" dari cash memo balance amount "+result[0].cash_memo_balance_amount+" menjadi "+excel_dump_invoices.cash_memo_balance_amount+" ,";
                    //console.log('action invoice');
                    //console.log(action_invoice);
                    Task.update(
                        {
                            action: action_invoice
                        },
                        { 
                            where: 
                            { 
                                id: result_task_id
                            }
                        }
                    );

                    Invoice.update(
                        {
                            cash_memo_balance_amount: excel_dump_invoices.cash_memo_balance_amount
                        },
                        { 
                            where: 
                            { 
                                invoice_id: excel_dump_invoices.invoice_id,
                                dt_code: excel_dump_invoices.dt_code
                            }
                        }
                    );
                }
                else if(result != null && result.length != 0 && result[0].cash_memo_balance_amount <= excel_dump_invoices.cash_memo_balance_amount)
                {
                    // skip
                }
                else 
                {
                    var invoice_payment_status_array = excel_dump_invoices.invoice_payment_status.split('/');

                    Invoice.create({session_id: excel_dump_invoices.file_name, distributorCode: excel_dump_invoices.dt_code, le_code: excel_dump_invoices.le_code, outletCode: excel_dump_invoices.outlet_code, cash_memo_total_amount: excel_dump_invoices.cash_memo_total_amount, cash_memo_balance_amount: excel_dump_invoices.cash_memo_balance_amount, cashmemo_type: excel_dump_invoices.cashmemo_type, invoice_id: excel_dump_invoices.invoice_id, invoice_sales_date: excel_dump_invoices.invoice_sales_date, invoice_due_date: excel_dump_invoices.invoice_due_date, invoice_payment_status_paid: invoice_payment_status_array[0], invoice_payment_status_unpaid: invoice_payment_status_array[1]});

                    service_invoice.getProductFromExcelDump({invoice_id: excel_dump_invoices.invoice_id, dt_code: excel_dump_invoices.dt_code}).then(resultProductExcelDump => {

                        // console.log('isi invoice_id: '+excel_dump_invoices.invoice_id+" dt_code: "+excel_dump_invoices.dt_code);
                        // console.log('isi resultProductExcelDump');
                        // console.log(resultProductExcelDump);

                        async.eachSeries(resultProductExcelDump, function(productExcelDump, callbackEach) {
                            
                            Products.create({dt_code: productExcelDump.dt_code, invoice_id: productExcelDump.invoice_id, product: productExcelDump.product, product_name: productExcelDump.product_name, quantity: productExcelDump.quantity, product_price_cs: productExcelDump.product_price_cs, product_price_dz: productExcelDump.product_price_dz, product_price_pc: productExcelDump.product_price_pc});

                            setImmediate( () => {
                                callbackEach();
                            });
                        });        
                    });
                }

            });
            setImmediate( () => {
                callbackEach();
            });

            processed_invoice++;

            Task.update(
                {
                    processed_row: processed_invoice
                },
                { 
                    where: 
                    { 
                        id: result.task_id
                    }
                }
            );
        });     
        
        Task.update(
            {
                status: 1
            },
            { 
                where: 
                { 
                    id: result.task_id
                }
            }
        );

        var out = {code: 200, message: 'Upload success.'};
        res.json(out); 
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to upload invoice. ' + err});
    });
}

function check_process_invoice(req, res) {

    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const acl = acl_get_invoices;
    const requested_resource = {};

    var dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;

        if (!principal.user) {
            throw new Error('no associated user with this principal.');
        }
        if(principal.user.type == 1 || principal.user.type == 3) {
            return principal.user.getDistributor().then(distributor => {
                return [principal, distributor];
            });
        }
        else
        {
            if(dt_code == null || dt_code == '')
            {
                throw new Error('Distributor Code tidak boleh kosong');
            }
            else 
            {
                return principal.user.getDistributor().then(distributor => {
                    return [principal, distributor];
                });
            }
        }
        
    })
    .then(p_result => {

        var [principal, distributor] = p_result;

        if(principal.user.type == 1 || principal.user.type == 3) {
            dt_code = distributor.distributorCode;
        }

        return service_invoice.getCheckProcessInvoice({dt_code:dt_code}).then(result => {
            return result;
        });
    })
    .then(result => {

        if(result.code == "403") {
            var out = result;
            res.status(403);
            res.json(out);
        } else {
            var out = result;
            res.status(200);
            res.json(out);
        }
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get task. ' + err});
    });
}

function task(req, res) {

    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const param_email = req.swagger.params.email;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;
    const acl = acl_get_invoices;
    const requested_resource = {};

    var dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    var email = _.isUndefined(param_email.value)?null:param_email.value;
    var startPage = _.isUndefined(param_page.value)?0:param_page.value;
    var itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
    var sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    var sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;

    // normalize startPage to 1
    startPage = (startPage < 1 ) ? 1 : startPage;
    
    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;

        if (!principal.user) {
            throw new Error('no associated user with this principal.');
        }
        else
        {
            return principal.user.getDistributor().then(distributor => {
                return [principal, distributor];
            });
        }
        
    })
    .then(p_result => {

        var [principal, distributor] = p_result;
        
        if(principal.user.type == 1 || principal.user.type == 3) {
            dt_code = distributor.distributorCode;
        }

        return service_invoice.getTask({dt_code:dt_code, email:email, startPage:startPage, itemsPerPage:itemsPerPage, sorting:sorting, sortingBy:sortingBy, usertype: principal.user.type}).then(tasks => {
            return tasks;
        });
    })
    .then(tasks => {

        let {list:result_tasks, total:total_tasks} = tasks;

        var total_pages = Math.ceil(total_tasks / itemsPerPage);

        var out = {};
        out.page = startPage;
        out.total_pages = total_pages;
        out.size = result_tasks.length;
        out.total_tasks = total_tasks;
        out.items_per_page = itemsPerPage;
        out.tasks = result_tasks;

        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get task. ' + err});
    });
}

function get_one_invoice(req, res) {
    console.log('get_one_invoice.');

    const token = req.swagger.params.token.value;
    const invoice_id = req.swagger.params.invoice_id.value;
    const dt_code = req.swagger.params.dt_code.value;
    const acl = acl_get_one_invoice;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        // get the retailer code.
        if (!principal.user) {
            throw new Error('no associated user with this principal.');
        }
        if(principal.user.type == 4)
        {
            return "sales";
        }
        else
        {
            return principal.user.getRetailer().then(retailer => {
                return [principal, retailer];
            });
        }
        
    })
    .then(p_result => {
        var [principal, retailer] = p_result;
        console.log('isi p_result');
        console.log(p_result);
        const outlet_code = retailer.outletCode;
        console.log('base_repo:getOne ', 'outlet-code:', outlet_code, 'invoice-id: ',invoice_id);
        return service_invoice.getOne({id:invoice_id, dt_code:dt_code, options:{throwIfEmpty:true}}).then(invoice => {
            return invoice;
        });
    })
    .then(p_result => {
        const invoice = p_result;
        // console.log('invoice: ', invoice);
        var formatter_options = {};
        formatter_options.attributes = ['!product'];
        var out = service_formatter.format({obj: invoice, typeId: 'invoice', options:formatter_options});
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: 0, message:'invoice not found. ' + err});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get invoices. ' + err});
    });

}

function get_invoices(req, res) {
    const token = req.swagger.params.token.value;
    const platform = req.swagger.params.platform.value;
    const param_outlet_code = req.swagger.params.outlet_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_paid = req.swagger.params.paid;
    const param_unpaid = req.swagger.params.unpaid;
    // const param_startDueDate = req.swagger.params.start_due_date;
    // const param_endDueDate = req.swagger.params.end_due_date;
    const param_startFromOldestUnpaid = req.swagger.params.start_from_oldest_unpaid;

    const param_startDate = req.swagger.params.start_date;
    const param_endDate = req.swagger.params.end_date;
    
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_show_products = req.swagger.params.show_products;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;
    const param_filter_by = req.swagger.params.filter_by;

    const acl = acl_get_invoices;
    const requested_resource = {};
    //

    var isUnpaid = param_unpaid.value?param_unpaid.value:false;
    var isPaid = param_paid.value?param_paid.value:false;
    var outletCode = _.isUndefined(param_outlet_code.value)?null:param_outlet_code.value;
    // var outletCode;
    var distributorCode = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    var leCode = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    var startPage = _.isUndefined(param_page.value)?0:param_page.value;
    var itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
    var startFromOldestUnpaid = _.isUndefined(param_startFromOldestUnpaid.value)?false:param_startFromOldestUnpaid.value;

    // var startDueDate = _.isUndefined(param_startDueDate.value)?null:param_startDueDate.value.trim();
    // var endDueDate = _.isUndefined(param_endDueDate.value)?null:param_endDueDate.value.trim();
    var sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;

    var sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    var filterBy = _.isUndefined(param_filter_by.value)?null:param_filter_by.value;

    var startDate = _.isUndefined(param_startDate.value)?null:param_startDate.value.trim();
    var endDate = _.isUndefined(param_endDate.value)?null:param_endDate.value.trim();

    var leCodeDry = '';
    var leCodeIce = '';
    var leCodeUFS = '';

    var tipe_user = '';
    //var is_show_products = _.isUndefined(param_show_products.value)?false:param_show_products.value;

    // normalize startPage to 1
    startPage = (startPage < 1 ) ? 1 : startPage;

    console.log('### distributorCode:', distributorCode);
    console.log('### outletCode:', outletCode);

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        // console.log('p_result:', p_result);
        const [principal, is_allowed] = p_result;

        console.log('principal: ', principal);

        var admin_roles = ['unilever_admin', 'super_admin'];
        var distributor_role = 'distributor';

        var is_distributor = principal.roles.indexOf('distributor') >= 0;
        var is_distributor_manager = principal.roles.indexOf('distributor_manager') >= 0;
        var is_retailer = principal.roles.indexOf('retailer') >= 0;
        var is_sales = principal.roles.indexOf('sales') >= 0;

        if (is_distributor) {
            return principal.user.getDistributor().then(distributor => {
                if (! distributor) {
                    throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter distributorCode with the value
                // from the token holder.
                distributorCode = distributor.distributorCode;
                return {principal:principal};
            });
        }
        else if (is_sales) {
            return principal.user.getDistributor().then(sales => {
                if (! sales) {
                    throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter distributorCode with the value
                // from the token holder.
                console.log('isi sales nya');
                console.log(sales);
                distributorCode = sales.distributorCode;
                return {principal:principal};
            });
        } else if (is_retailer) {
            console.log('masuk retailer');
            // Note: the sequelize orm getter method returns a Promise object.
            return principal.user.getRetailer().then(retailer => {
                if (! retailer) {
                    throw new Error('principal user does not have associated retailer. please specify outlet_code parameter.');
                }
                console.log('isi retailer di invoice');
                console.log(retailer);

                leCodeDry = retailer.le_code_dry;
                leCodeIce = retailer.le_code_ice;
                leCodeUFS = retailer.le_code_ufs;

                return {principal:principal};
                // NOTE: this is important. we override the parameter outletCode with the value
                // from the token holder.
                
                // Hapus ini
                // outletCode = retailer.outletCode;
                
                // return principal.user.getDistributor().then(distributor => {
                //     if (! distributor) {
                //         throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                //     }
                //     // NOTE: this is important. we override the parameter distributorCode with the value
                //     // from the token holder.
                //     // distributorCode = distributor.distributorCode;
                //     return {principal:principal};
                // });
            });
        } else if (is_distributor_manager) {
            return principal.user.getDistributor().then(distributor => {
                if (! distributor) {
                    throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter distributorCode with the value
                // from the token holder.
                distributorCode = distributor.distributorCode;
                return {principal:principal};
            });
        }
        if (distributorCode) {
            // only certain roles can explicitly specify the dt_code.
            var is_allow_dt_code_override = acl_factory.getDefault()
                .addRoles(admin_roles)
                .isAllowed(principal);
            if (is_allow_dt_code_override) {
                return {principal:principal};
            } else {
                throw new UnauthorizedRequestError('you are unauthorized to view random distributor invoices.');
            }
        }
        if (outletCode) {
            console.log('masuk if outlet code');
            // only certain roles can explicitly specify the outlet_code.
            var is_allow_outlet_code_override = acl_factory.getDefault()
                .addRoles(admin_roles)
                .addRoles(distributor_role)
                .isAllowed(principal);
            if (is_allow_outlet_code_override) {
                // check if retailer with given outletCode exists
                return base_repo.getOne({entity:Retailer, where:{outletCode:outletCode}, options:{throwIfEmpty:true}}).then(retailer => {
                    return {principal:principal};
                });
            } else {
                throw new UnauthorizedRequestError('you are unauthorized to view random retailers invoices.');
            }
        }

        return {principal:principal};

        // throw new Error('please specify outlet_code or dt_code parameter.');
    })
    .then(p_result => {
        console.log('isi p result sebelum date');
        console.log(p_result.principal.retailer_id);
        let {principal:principal} = p_result;
        if (startFromOldestUnpaid) {
            var args = {outletCode:outletCode, distributorCode:distributorCode};
            return service_invoice.getOldestUnpaidDate(args).then(oldest_unpaid_date => {
                p_result.oldest_unpaid_date = oldest_unpaid_date;
                return p_result;
            });
        } else {
            return p_result;
        }
    })
    .then(p_result => {
        let {principal:principal, oldest_unpaid_date:oldest_unpaid_date} = p_result;

        const iso8601format = "YYYY-MM-DD";

        var startDueDateMoment = null;
        var endDueDateMoment = null;

        console.log('isi principal');
        console.log(principal.user.dataValues.type);
        tipe_user = principal.user.dataValues.type;
        // Note: start-from-oldest-unpaid-date overrides start-due-date parameter.

        // if (oldest_unpaid_date) {
        //     if (DEBUG_LOG) console.log('start from oldest-unpaid-due-date: ', oldest_unpaid_date);
        //     startDueDateMoment = oldest_unpaid_date;
        // } else {
        //     if (startDueDate) {
        //         var is_format_match = /^\d{4}-\d{2}-\d{2}$/.test(startDueDate);
        //         if (is_format_match) {
        //             startDueDateMoment = moment.utc(startDueDate, iso8601format);
        //         } else {
        //             startDueDateMoment = common_util.parseDateString({pattern:startDueDate, now:null});
        //         }
        //     }
        // }

        // if (startDueDateMoment) {
        //     if (endDueDate) {
        //         var is_match_end = /^\d{4}-\d{2}-\d{2}$/.test(endDueDate);
        //         if (is_match_end) {
        //             endDueDateMoment = moment.utc(endDueDate, iso8601format);
        //         } else {
        //             endDueDateMoment = common_util.parseDateString({pattern:endDueDate, now:startDueDateMoment});
        //         }
        //     }
        // }
        
        var args = {outletCode:outletCode, distributorCode:distributorCode, leCodeFromSales: leCode, isPaid:isPaid, isUnpaid:isUnpaid, startPage:startPage, itemsPerPage:itemsPerPage, sorting:sorting, sortingBy:sortingBy, filterBy:filterBy, leCodeDry:leCodeDry, leCodeIce:leCodeIce, leCodeUFS:leCodeUFS, usertype: principal.user.dataValues.type, platform: platform};
        console.log('isi args');
        console.log(args);
        // if (startDueDateMoment && endDueDateMoment) {
        //     args.dueDateStart = startDueDateMoment;
        //     args.dueDateEnd = endDueDateMoment;

        //     console.log('# Date filter range:');
        //     if (DEBUG_LOG) console.log('start-duedate:', startDueDateMoment.format('LLL'));
        //     if (DEBUG_LOG) console.log('end-duedate:', endDueDateMoment.format('LLL'));
        // }
        
        if(startDate != null)
        {
            if(!startDate.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/))
            {
                throw new Error('Format tanggal tidak sesuai.');
            }
            else
            {
                args.startDate = startDate;
            }
        }

        if(endDate != null)
        {
            if(!endDate.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/))
            {
                throw new Error('Format tanggal tidak sesuai.');
            }
            else
            {
                args.endDate = endDate;
            }
        }

        return service_invoice.get(args).then(result_obj => {
            return result_obj;
        });
    })
    .then(p_result => {
        // console.log('di controller');
        // console.log(p_result.list[0].transaction);
        // console.log('di atas list');
        let {list:invoices_list, total:total_items, total_unpaid:total_unpaid_items} = p_result;
        // console.log('total item nya');
        // console.log(p_result);
        // console.log('isi invoices list');
        // console.log(invoices_list);
        // invoices_list.forEach(invoice => {
        //     if (invoice.is_paid) {
        //         if(invoice.transaction != null)
        //         {
        //             var transact_date = new Date(invoice.transaction.dataValues.transact_date);
        //             invoice.paid_date = transact_date.getTime();
        //         }
        //     }
            
        // });
        // console.log('isi invoices list');
        // console.log(invoices_list);
        var formatter_options = {};
        formatter_options.attributes = ['!product'];
        var invoices = service_formatter.format({obj:invoices_list, typeId: 'invoice', options:formatter_options});

        // console.log('isi invoices');
        // console.log(invoices);
        // compute total pages
        if(itemsPerPage != 0)
        {
            var total_pages = Math.ceil(total_items / itemsPerPage);

            var out = {};
            out.page = startPage;
            out.total_pages = total_pages;
            out.size = invoices.length;
            out.total_items = total_items;
            out.total_unpaid_items = total_unpaid_items;
            out.items_per_page = itemsPerPage;
            out.invoices = invoices;

            res.json(out);  
        }
        else
        {
            var out = {};
            out.total_items = total_items;
            out.total_unpaid_items = total_unpaid_items;
            out.invoices = invoices;
            res.json(out);
        } 
    });
    return common_handler.exceptions(p, req, res)
    .catch(UnauthorizedRequestError, err => {
        res.status(400).json({code: 0, message: err.message});
    })
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: 0, message:'resource not found. ' + err.message});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get invoices.' + err});
    });
}

function get_retur(req, res) {
    const token = req.swagger.params.token.value;
    const platform = req.swagger.params.platform.value;
    const param_outlet_code = req.swagger.params.outlet_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_paid = req.swagger.params.paid;
    const param_unpaid = req.swagger.params.unpaid;
    // const param_startDueDate = req.swagger.params.start_due_date;
    // const param_endDueDate = req.swagger.params.end_due_date;
    const param_startFromOldestUnpaid = req.swagger.params.start_from_oldest_unpaid;

    const param_startDate = req.swagger.params.start_date;
    const param_endDate = req.swagger.params.end_date;
    
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_show_products = req.swagger.params.show_products;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;
    const param_filter_by = req.swagger.params.filter_by;

    const acl = acl_get_invoices;
    const requested_resource = {};
    //

    var isUnpaid = param_unpaid.value?param_unpaid.value:false;
    var isPaid = param_paid.value?param_paid.value:false;
    var outletCode = _.isUndefined(param_outlet_code.value)?null:param_outlet_code.value;
    // var outletCode;
    var distributorCode = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    var leCode = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    var startPage = _.isUndefined(param_page.value)?0:param_page.value;
    var itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
    var startFromOldestUnpaid = _.isUndefined(param_startFromOldestUnpaid.value)?false:param_startFromOldestUnpaid.value;

    // var startDueDate = _.isUndefined(param_startDueDate.value)?null:param_startDueDate.value.trim();
    // var endDueDate = _.isUndefined(param_endDueDate.value)?null:param_endDueDate.value.trim();
    var sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;

    var sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    var filterBy = _.isUndefined(param_filter_by.value)?null:param_filter_by.value;

    var startDate = _.isUndefined(param_startDate.value)?null:param_startDate.value.trim();
    var endDate = _.isUndefined(param_endDate.value)?null:param_endDate.value.trim();

    var leCodeDry = '';
    var leCodeIce = '';
    var leCodeUFS = '';

    var tipe_user = '';
    //var is_show_products = _.isUndefined(param_show_products.value)?false:param_show_products.value;

    // normalize startPage to 1
    startPage = (startPage < 1 ) ? 1 : startPage;

    console.log('### distributorCode:', distributorCode);
    console.log('### outletCode:', outletCode);

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {

        const [principal, is_allowed] = p_result;

        console.log('principal: ', principal);

        var admin_roles = ['unilever_admin', 'super_admin'];
        var distributor_role = 'distributor';

        var is_distributor = principal.roles.indexOf('distributor') >= 0;
        var is_distributor_manager = principal.roles.indexOf('distributor_manager') >= 0;
        var is_retailer = principal.roles.indexOf('retailer') >= 0;
        var is_sales = principal.roles.indexOf('sales') >= 0;

        if (is_distributor) {
            return principal.user.getDistributor().then(distributor => {
                if (! distributor) {
                    throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter distributorCode with the value
                // from the token holder.
                distributorCode = distributor.distributorCode;
                return {principal:principal};
            });
        }
        else if (is_sales) {
            return principal.user.getDistributor().then(sales => {
                if (! sales) {
                    throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter distributorCode with the value
                // from the token holder.

                distributorCode = sales.distributorCode;
                return {principal:principal};
            });
        } else if (is_retailer) {

            // Note: the sequelize orm getter method returns a Promise object.
            return principal.user.getRetailer().then(retailer => {
                if (! retailer) {
                    throw new Error('principal user does not have associated retailer. please specify outlet_code parameter.');
                }
                console.log('isi retailer di invoice');
                console.log(retailer);

                leCodeDry = retailer.le_code_dry;
                leCodeIce = retailer.le_code_ice;
                leCodeUFS = retailer.le_code_ufs;
                // NOTE: this is important. we override the parameter outletCode with the value
                // from the token holder.

                // Hapus ini
                // outletCode = retailer.outletCode;
                return principal.user.getDistributor().then(distributor => {
                    if (! distributor) {
                        throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                    }
                    // NOTE: this is important. we override the parameter distributorCode with the value
                    // from the token holder.
                    // distributorCode = distributor.distributorCode;
                    return {principal:principal};
                });
            });
        } else if (is_distributor_manager) {
            return principal.user.getDistributor().then(distributor => {
                if (! distributor) {
                    throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter distributorCode with the value
                // from the token holder.
                distributorCode = distributor.distributorCode;
                return {principal:principal};
            });
        }
        if (distributorCode) {
            // only certain roles can explicitly specify the dt_code.
            var is_allow_dt_code_override = acl_factory.getDefault()
                .addRoles(admin_roles)
                .isAllowed(principal);
            if (is_allow_dt_code_override) {
                return {principal:principal};
            } else {
                throw new UnauthorizedRequestError('you are unauthorized to view random distributor invoices.');
            }
        }
        if (outletCode) {

            // only certain roles can explicitly specify the outlet_code.
            var is_allow_outlet_code_override = acl_factory.getDefault()
                .addRoles(admin_roles)
                .addRoles(distributor_role)
                .isAllowed(principal);
            if (is_allow_outlet_code_override) {
                // check if retailer with given outletCode exists
                return base_repo.getOne({entity:Retailer, where:{outletCode:outletCode}, options:{throwIfEmpty:true}}).then(retailer => {
                    return {principal:principal};
                });
            } else {
                throw new UnauthorizedRequestError('you are unauthorized to view random retailers invoices.');
            }
        }

        return {principal:principal};

        // throw new Error('please specify outlet_code or dt_code parameter.');
    })
    .then(p_result => {
        
        let {principal:principal} = p_result;
        if (startFromOldestUnpaid) {
            var args = {outletCode:outletCode, distributorCode:distributorCode};
            return service_invoice.getOldestUnpaidDate(args).then(oldest_unpaid_date => {
                p_result.oldest_unpaid_date = oldest_unpaid_date;
                return p_result;
            });
        } else {
            return p_result;
        }
    })
    .then(p_result => {
        let {principal:principal, oldest_unpaid_date:oldest_unpaid_date} = p_result;

        const iso8601format = "YYYY-MM-DD";

        var startDueDateMoment = null;
        var endDueDateMoment = null;

        tipe_user = principal.user.dataValues.type;

        var args = {outletCode:outletCode, distributorCode:distributorCode, leCodeFromSales: leCode, isPaid:isPaid, isUnpaid:isUnpaid, startPage:startPage, itemsPerPage:itemsPerPage, sorting:sorting, sortingBy:sortingBy, filterBy:filterBy, leCodeDry:leCodeDry, leCodeIce:leCodeIce, leCodeUFS:leCodeUFS, usertype: principal.user.dataValues.type, platform: platform};
        
        if(startDate != null)
        {
            if(!startDate.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/))
            {
                throw new Error('Format tanggal tidak sesuai.');
            }
            else
            {
                args.startDate = startDate;
            }
        }

        if(endDate != null)
        {
            if(!endDate.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/))
            {
                throw new Error('Format tanggal tidak sesuai.');
            }
            else
            {
                args.endDate = endDate;
            }
        }

        return service_invoice.getRetur(args).then(result_obj => {
            return result_obj;
        });
    })
    .then(p_result => {

        let {list:invoices_list, total:total_items, total_unpaid:total_unpaid_items} = p_result;

        var formatter_options = {};
        formatter_options.attributes = ['!product'];
        var invoices = service_formatter.format({obj:invoices_list, typeId: 'invoice'});

        if(itemsPerPage != 0)
        {
            var total_pages = Math.ceil(total_items / itemsPerPage);

            var out = {};
            out.page = startPage;
            out.total_pages = total_pages;
            out.size = invoices.length;
            out.total_items = total_items;
            out.total_unpaid_items = total_unpaid_items;
            out.items_per_page = itemsPerPage;
            out.invoices = invoices;

            res.json(out);  
        }
        else
        {
            var out = {};
            out.total_items = total_items;
            out.total_unpaid_items = total_unpaid_items;
            out.invoices = invoices;
            res.json(out);
        } 
    });
    return common_handler.exceptions(p, req, res)
    .catch(UnauthorizedRequestError, err => {
        res.status(400).json({code: 0, message: err.message});
    })
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: 0, message:'resource not found. ' + err.message});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get invoices.' + err});
    });
}

function get_all_retur_request(req, res) {
    console.log('get_all_retur_request');

    const token = req.swagger.params.token.value;
    const param_outlet_code = req.swagger.params.outlet_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_paid = req.swagger.params.paid;
    const param_unpaid = req.swagger.params.unpaid;
    const param_startDueDate = req.swagger.params.start_due_date;
    const param_endDueDate = req.swagger.params.end_due_date;
    const param_startFromOldestUnpaid = req.swagger.params.start_from_oldest_unpaid;

    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_show_products = req.swagger.params.show_products;

    const acl = acl_get_retur;
    const requested_resource = {};
    const param_status = req.swagger.params.status;

    //

    var isUnpaid = param_unpaid.value?param_unpaid.value:false;
    var isPaid = param_paid.value?param_paid.value:false;
    var outletCode = _.isUndefined(param_outlet_code.value)?null:param_outlet_code.value;
    var distributorCode = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    var startPage = _.isUndefined(param_page.value)?0:param_page.value;
    var status = _.isUndefined(param_status.value)?0:param_status.value;
    var itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
    var startFromOldestUnpaid = _.isUndefined(param_startFromOldestUnpaid.value)?false:param_startFromOldestUnpaid.value;

    var startDueDate = _.isUndefined(param_startDueDate.value)?null:param_startDueDate.value.trim();
    var endDueDate = _.isUndefined(param_endDueDate.value)?null:param_endDueDate.value.trim();

    //var is_show_products = _.isUndefined(param_show_products.value)?false:param_show_products.value;

    // normalize startPage to 1
    startPage = (startPage < 1 ) ? 1 : startPage;

    console.log('### distributorCode:', distributorCode);
    console.log('### outletCode:', outletCode);

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        console.log('p_result:', p_result);
        const [principal, is_allowed] = p_result;

        console.log('principal: ', principal);

        var admin_roles = ['unilever_admin', 'super_admin'];
        var distributor_role = 'distributor';

        var is_distributor = principal.roles.indexOf('distributor') >= 0;
        var is_distributor_manager = principal.roles.indexOf('distributor_manager') >= 0;
        var is_retailer = principal.roles.indexOf('retailer') >= 0;

        if (is_distributor) {
            return principal.user.getDistributor().then(distributor => {
                if (! distributor) {
                    throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter distributorCode with the value
                // from the token holder.
                distributorCode = distributor.distributorCode;
                return {principal:principal};
            });
        } else if (is_retailer) {
            // Note: the sequelize orm getter method returns a Promise object.
            return principal.user.getRetailer().then(retailer => {
                if (! retailer) {
                    throw new Error('principal user does not have associated retailer. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter outletCode with the value
                // from the token holder.
                outletCode = retailer.outletCode;
                return {principal:principal};
            });
        } else if (is_distributor_manager) {
            return principal.user.getDistributor().then(distributor => {
                if (! distributor) {
                    throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter distributorCode with the value
                // from the token holder.
                distributorCode = distributor.distributorCode;
                return {principal:principal};
            });
        }
        if (distributorCode) {
            // only certain roles can explicitly specify the dt_code.
            var is_allow_dt_code_override = acl_factory.getDefault()
                .addRoles(admin_roles)
                .isAllowed(principal);
            if (is_allow_dt_code_override) {
                return {principal:principal};
            } else {
                throw new UnauthorizedRequestError('you are unauthorized to view random distributor invoices.');
            }
        }
        if (outletCode) {
            // only certain roles can explicitly specify the outlet_code.
            var is_allow_outlet_code_override = acl_factory.getDefault()
                .addRoles(admin_roles)
                .addRoles(distributor_role)
                .isAllowed(principal);
            if (is_allow_outlet_code_override) {
                // check if retailer with given outletCode exists
                return base_repo.getOne({entity:Retailer, where:{outletCode:outletCode}, options:{throwIfEmpty:true}}).then(retailer => {
                    return {principal:principal};
                });
            } else {
                throw new UnauthorizedRequestError('you are unauthorized to view random retailers invoices.');
            }
        }

        return {principal:principal};

        // throw new Error('please specify outlet_code or dt_code parameter.');
    })
    .then(p_result => {
        let {principal:principal} = p_result;
        if (startFromOldestUnpaid) {
            var args = {outletCode:outletCode, distributorCode:distributorCode};
            return service_invoice.getOldestUnpaidDate(args).then(oldest_unpaid_date => {
                p_result.oldest_unpaid_date = oldest_unpaid_date;
                return p_result;
            });
        } else {
            return p_result;
        }
    })
    .then(p_result => {
        let {principal:principal, oldest_unpaid_date:oldest_unpaid_date} = p_result;

        const iso8601format = "YYYY-MM-DD";

        var startDueDateMoment = null;
        var endDueDateMoment = null;

        // Note: start-from-oldest-unpaid-date overrides start-due-date parameter.

        if (oldest_unpaid_date) {
            if (DEBUG_LOG) console.log('start from oldest-unpaid-due-date: ', oldest_unpaid_date);
            startDueDateMoment = oldest_unpaid_date;
        } else {
            if (startDueDate) {
                var is_format_match = /^\d{4}-\d{2}-\d{2}$/.test(startDueDate);
                if (is_format_match) {
                    startDueDateMoment = moment.utc(startDueDate, iso8601format);
                } else {
                    startDueDateMoment = common_util.parseDateString({pattern:startDueDate, now:null});
                }
            }
        }

        if (startDueDateMoment) {
            if (endDueDate) {
                var is_match_end = /^\d{4}-\d{2}-\d{2}$/.test(endDueDate);
                if (is_match_end) {
                    endDueDateMoment = moment.utc(endDueDate, iso8601format);
                } else {
                    endDueDateMoment = common_util.parseDateString({pattern:endDueDate, now:startDueDateMoment});
                }
            }
        }

        var args = {outletCode:outletCode, distributorCode:distributorCode, isPaid:isPaid, isUnpaid:isUnpaid, startPage:startPage, itemsPerPage:itemsPerPage};

        if (startDueDateMoment && endDueDateMoment) {
            args.dueDateStart = startDueDateMoment;
            args.dueDateEnd = endDueDateMoment;

            console.log('# Date filter range:');
            if (DEBUG_LOG) console.log('start-duedate:', startDueDateMoment.format('LLL'));
            if (DEBUG_LOG) console.log('end-duedate:', endDueDateMoment.format('LLL'));
        }

        if(status)
        {
            args.status = status;
        }
        console.log(args);
        return service_invoice.getAllReturRequest(args).then(result_obj => {
            return result_obj;
        });
    })
    .then(p_result => {
        let {list:invoices_list, total:total_items} = p_result;

        invoices_list.forEach(invoice => {
            if (invoice.is_paid) {
                invoice.paid_date = 0;
            }
        });

        var formatter_options = {};
        formatter_options.attributes = ['!product'];
        var invoices = service_formatter.format({obj:invoices_list, options:formatter_options});

        // compute total pages
        var total_pages = Math.ceil(total_items / itemsPerPage);

        var out = {};
        out.page = startPage;
        out.total_pages = total_pages;
        out.size = invoices.length;
        out.total_items = total_items;
        out.items_per_page = itemsPerPage;
        out.invoices = invoices;

        console.log('cek status');
        console.log(status);

        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(UnauthorizedRequestError, err => {
        res.status(400).json({code: 0, message: err.message});
    })
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: 0, message:'resource not found. ' + err.message});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get invoices.' + err});
    });
}

function get_retur_old(req, res) {
    console.log('get_retur');

    const token = req.swagger.params.token.value;
    const param_outlet_code = req.swagger.params.outlet_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_paid = req.swagger.params.paid;
    const param_unpaid = req.swagger.params.unpaid;
    const param_startDueDate = req.swagger.params.start_due_date;
    const param_endDueDate = req.swagger.params.end_due_date;

    const param_startPaidDate = req.swagger.params.start_paid_date;
    const param_endPaidDate = req.swagger.params.end_paid_date;

    const param_startFromOldestUnpaid = req.swagger.params.start_from_oldest_unpaid;

    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_show_products = req.swagger.params.show_products;

    const acl = acl_get_retur;
    const requested_resource = {};
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;

    //

    var isUnpaid = param_unpaid.value?param_unpaid.value:false;
    var isPaid = param_paid.value?param_paid.value:false;
    var outletCode = _.isUndefined(param_outlet_code.value)?null:param_outlet_code.value;
    var distributorCode = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    var startPage = _.isUndefined(param_page.value)?0:param_page.value;
    var itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
    var startFromOldestUnpaid = _.isUndefined(param_startFromOldestUnpaid.value)?false:param_startFromOldestUnpaid.value;

    var startDueDate = _.isUndefined(param_startDueDate.value)?null:param_startDueDate.value.trim();
    var endDueDate = _.isUndefined(param_endDueDate.value)?null:param_endDueDate.value.trim();

    var startPaidDate = _.isUndefined(param_startPaidDate.value)?null:param_startPaidDate.value.trim();
    var endPaidDate = _.isUndefined(param_endPaidDate.value)?null:param_endPaidDate.value.trim();

    var sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    var sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    //var is_show_products = _.isUndefined(param_show_products.value)?false:param_show_products.value;

    // normalize startPage to 1
    startPage = (startPage < 1 ) ? 1 : startPage;

    console.log('### distributorCode:', distributorCode);
    console.log('### outletCode:', outletCode);

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        console.log('p_result:', p_result);
        const [principal, is_allowed] = p_result;

        console.log('principal: ', principal);

        var admin_roles = ['unilever_admin', 'super_admin'];
        var distributor_role = 'distributor';

        var is_distributor = principal.roles.indexOf('distributor') >= 0;
        var is_distributor_manager = principal.roles.indexOf('distributor_manager') >= 0;
        var is_retailer = principal.roles.indexOf('retailer') >= 0;

        if (is_distributor) {
            return principal.user.getDistributor().then(distributor => {
                if (! distributor) {
                    throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter distributorCode with the value
                // from the token holder.
                distributorCode = distributor.distributorCode;
                return {principal:principal};
            });
        } else if (is_retailer) {
            // Note: the sequelize orm getter method returns a Promise object.
            return principal.user.getRetailer().then(retailer => {
                if (! retailer) {
                    throw new Error('principal user does not have associated retailer. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter outletCode with the value
                // from the token holder.
                outletCode = retailer.outletCode;
                return principal.user.getDistributor().then(distributor => {
                    if (! distributor) {
                        throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                    }
                    // NOTE: this is important. we override the parameter distributorCode with the value
                    // from the token holder.
                    distributorCode = distributor.distributorCode;
                    return {principal:principal};
                });
            });
        } else if (is_distributor_manager) {
            return principal.user.getDistributor().then(distributor => {
                if (! distributor) {
                    throw new Error('principal user does not have associated distributor. please specify outlet_code parameter.');
                }
                // NOTE: this is important. we override the parameter distributorCode with the value
                // from the token holder.
                distributorCode = distributor.distributorCode;
                return {principal:principal};
            });
        }
        if (distributorCode) {
            // only certain roles can explicitly specify the dt_code.
            var is_allow_dt_code_override = acl_factory.getDefault()
                .addRoles(admin_roles)
                .isAllowed(principal);
            if (is_allow_dt_code_override) {
                return {principal:principal};
            } else {
                throw new UnauthorizedRequestError('you are unauthorized to view random distributor invoices.');
            }
        }
        if (outletCode) {
            // only certain roles can explicitly specify the outlet_code.
            var is_allow_outlet_code_override = acl_factory.getDefault()
                .addRoles(admin_roles)
                .addRoles(distributor_role)
                .isAllowed(principal);
            if (is_allow_outlet_code_override) {
                // check if retailer with given outletCode exists
                return base_repo.getOne({entity:Retailer, where:{outletCode:outletCode}, options:{throwIfEmpty:true}}).then(retailer => {
                    return {principal:principal};
                });
            } else {
                throw new UnauthorizedRequestError('you are unauthorized to view random retailers invoices.');
            }
        }

        return {principal:principal};

        // throw new Error('please specify outlet_code or dt_code parameter.');
    })
    .then(p_result => {
        let {principal:principal} = p_result;
        if (startFromOldestUnpaid) {
            var args = {outletCode:outletCode, distributorCode:distributorCode};
            return service_invoice.getOldestUnpaidDate(args).then(oldest_unpaid_date => {
                p_result.oldest_unpaid_date = oldest_unpaid_date;
                return p_result;
            });
        } else {
            return p_result;
        }
    })
    .then(p_result => {
        let {principal:principal, oldest_unpaid_date:oldest_unpaid_date} = p_result;

        const iso8601format = "YYYY-MM-DD";

        var startDueDateMoment = null;
        var endDueDateMoment = null;

        // Note: start-from-oldest-unpaid-date overrides start-due-date parameter.

        if (oldest_unpaid_date) {
            if (DEBUG_LOG) console.log('start from oldest-unpaid-due-date: ', oldest_unpaid_date);
            startDueDateMoment = oldest_unpaid_date;
        } else {
            if (startDueDate) {
                var is_format_match = /^\d{4}-\d{2}-\d{2}$/.test(startDueDate);
                if (is_format_match) {
                    startDueDateMoment = moment.utc(startDueDate, iso8601format);
                } else {
                    startDueDateMoment = common_util.parseDateString({pattern:startDueDate, now:null});
                }
            }
        }

        if (startDueDateMoment) {
            if (endDueDate) {
                var is_match_end = /^\d{4}-\d{2}-\d{2}$/.test(endDueDate);
                if (is_match_end) {
                    endDueDateMoment = moment.utc(endDueDate, iso8601format);
                } else {
                    endDueDateMoment = common_util.parseDateString({pattern:endDueDate, now:startDueDateMoment});
                }
            }
        }

        if(startPaidDate != null)
        {
            if(!startPaidDate.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/))
            {
                throw new Error('Format tanggal tidak sesuai.');
            }
            else
            {
                
                args.startPaidDate = startPaidDate;
            }
        }

        if(endPaidDate != null)
        {
            if(!endPaidDate.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/))
            {
                throw new Error('Format tanggal tidak sesuai.');
            }
            else
            {
                args.endPaidDate = endPaidDate;
            }
        }

        var args = {outletCode:outletCode, distributorCode:distributorCode, isPaid:isPaid, isUnpaid:isUnpaid, startPage:startPage, itemsPerPage:itemsPerPage, sorting:sorting, sortingBy:sortingBy};

        if (startDueDateMoment && endDueDateMoment) {
            args.dueDateStart = startDueDateMoment;
            args.dueDateEnd = endDueDateMoment;

            console.log('# Date filter range:');
            if (DEBUG_LOG) console.log('start-duedate:', startDueDateMoment.format('LLL'));
            if (DEBUG_LOG) console.log('end-duedate:', endDueDateMoment.format('LLL'));
        }

        return service_invoice.getRetur(args).then(result_obj => {
            return result_obj;
        });
    })
    .then(p_result => {
        let {list:invoices_list, total:total_items} = p_result;

        // console.log('cek isi invoice nya');
        // console.log(p_result.list);
        invoices_list.forEach(invoice => {
            if (invoice.is_paid) {
                invoice.paid_date = 0;
            }
        });

        var formatter_options = {};
        formatter_options.attributes = ['!product'];
        var invoices = service_formatter.format({obj:invoices_list, options:formatter_options});

        // compute total pages
        var total_pages = Math.ceil(total_items / itemsPerPage);

        var out = {};
        out.page = startPage;
        out.total_pages = total_pages;
        out.size = invoices.length;
        out.total_items = total_items;
        out.items_per_page = itemsPerPage;
        out.invoices = invoices;

        console.log('cek status');
        console.log(out.invoices.status);

        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(UnauthorizedRequestError, err => {
        res.status(400).json({code: 0, message: err.message});
    })
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: 0, message:'resource not found. ' + err.message});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get invoices.' + err});
    });
}

function request_retur(req, res) {
    const token = req.swagger.params.token.value;
    const invoice_id = req.swagger.params.invoice_id.value;
    const acl = acl_request_retur;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        // get the retailer code.
        if (!principal.user) {
            throw new Error('no associated user with this principal.');
        }
        return principal.user.getRetailer().then(retailer => {
            return [principal, retailer];
        });
    })
    .then(p_result => {
        var [principal, retailer] = p_result;
        const outlet_code = retailer.outletCode;
        console.log('base_repo:getOne ', 'outlet-code:', outlet_code, 'invoice-id: ',invoice_id);
        return service_invoice.request_retur({invoice_id:invoice_id}).then(invoice => {
            return invoice;
        });
    })
    .then(p_result => {
        const out = p_result;
        // console.log('invoice: ', invoice);
        // var out = service_formatter.format({obj: invoice});
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: 0, message:'invoice not found. ' + err});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get retur. ' + err});
    });
}

function get_one_retur(req, res) {
    console.log('get_one_retur.');

    const token = req.swagger.params.token.value;
    const invoice_id = req.swagger.params.invoice_id.value;
    const dt_code = req.swagger.params.dt_code.value;
    const acl = acl_get_one_retur;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        // get the retailer code.
        if (!principal.user) {
            throw new Error('no associated user with this principal.');
        }
        return principal.user.getRetailer().then(retailer => {
            return [principal, retailer];
        });
    })
    .then(p_result => {
        var [principal, retailer] = p_result;
        const outlet_code = retailer.outletCode;

        return service_invoice.getOneRetur({id:invoice_id, dt_code:dt_code, options:{throwIfEmpty:true}}).then(invoice => {
            return invoice;
        });
    })
    .then(p_result => {
        const invoice = p_result;
        console.log('invoice: ', invoice);
        var out = service_formatter.format({obj: invoice, typeId: 'invoice'});
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: 0, message:'invoice not found. ' + err});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get retur. ' + err});
    });

}

function check_invoices(req, res) {
    console.log('check_invoices.');

    const token = req.swagger.params.token.value;
    const acl = acl_check_invoices;
    const requested_resource = {};

    const invoices = req.swagger.params.invoices.value;
    console.log('invoices:', invoices.invoices[0]);

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        
        return Promise.map(invoices.invoices, (invoice_id, index)  => {
            return base_repo.getOne({entity:Invoice, where:{invoice_id:invoice_id, dt_code: invoices.dt_codes[index]}, options:{throwIfEmpty:true}})
            .catch(ResourceNotFoundError, err => {
                return err;
            });
        });
    })
    .then(p_result => {
        console.log('check result:', p_result);
        var result_list = p_result;

        var list2 = result_list.map( (result, idx) => {
            console.log('isi result');
            console.log(result);

            console.log('isi idx');
            console.log(invoices.balance_amounts[idx]);
            var has_error = false;
            if (result instanceof ResourceNotFoundError) {
                has_error = true;
            }
            var invoice_id = invoices[idx];
            var result_item = {id:invoice_id};

            // if (has_error) {
            //     var errorObj = {};
            //     errorObj.code = result.code;
            //     errorObj.message = result.message;
            //     result_item.error = errorObj;
            // } 
            if(result.dataValues != null && result.dataValues.cash_memo_balance_amount >= invoices.balance_amounts[idx])
            {
                
                const invoice = result;
                const is_paid = invoice.is_paid;
                const unique_id = invoice.id;
                const has_refund = invoice.has_refund;
                const is_refunded = invoice.is_refunded;
                result_item.unique_id = unique_id;
                result_item.code = 0;
                if (has_refund) {
                    result_item.is_refunded = is_refunded;
                } else {
                    result_item.is_paid = is_paid;
                    // result_item.unique_id = unique_id;
                }
            }
            else if(result.dataValues != null && result.dataValues.cash_memo_balance_amount <= invoices.balance_amounts[idx]){
                const invoice = result;
                const is_paid = invoice.is_paid;
                const unique_id = invoice.id;
                const has_refund = invoice.has_refund;
                const is_refunded = invoice.is_refunded;
                result_item.unique_id = unique_id;
                result_item.code = 1;
                if (has_refund) {
                    result_item.is_refunded = is_refunded;
                } else {
                    result_item.is_paid = is_paid;
                    // result_item.unique_id = unique_id;
                }
            }
            else
            {
                const invoice = result;
                const is_paid = invoice.is_paid;
                const unique_id = invoice.id;
                const has_refund = invoice.has_refund;
                const is_refunded = invoice.is_refunded;
                result_item.unique_id = unique_id;
                result_item.code = 2;
                if (has_refund) {
                    result_item.is_refunded = is_refunded;
                } else {
                    result_item.is_paid = is_paid;
                    // result_item.unique_id = unique_id;
                }
            }
            console.log('isi result item');
            console.log(result_item);
            return result_item;
        });

        var out = {};
        out.result = list2;
        console.log('out:', out);
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to check invoices.' + err});
    });
}

/**
@returns {Promise}
*/
function validateFileType(buffer) {
    const file_type = fileType(buffer);
    console.log('### detected file_type :', file_type);
    if (!file_type) {
        return false;
    } else {
        if (file_type.mime.indexOf('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') != 0) {
            return false;
        }
    }
    return true;
}

var options = {
    tmpDir: tmpDir,
    publicDir: publicDir,
    uploadDir: uploadDir,

    // publicDir: __dirname + '/public',
    // uploadDir: __dirname + '/public/files',

    uploadUrl: '/_msg_atts/files/',
    // maxPostSize: 11 000 000 000, // 11 GB
    maxPostSize: 10000000, // 10MB
    minFileSize: 1, // 1 Bytes
    maxFileSize: 10000000, // 10MB
    acceptFileTypes: /.+/i,
    // Files not matched by this regular expression force a download dialog,
    // to prevent executing any scripts in the context of the service domain:
    inlineFileTypes: /\.(xlsx|xls)$/i,
    accessControl: {
        allowOrigin: '*',
        allowMethods: 'OPTIONS, HEAD, GET, POST, PUT, DELETE',
        allowHeaders: 'Content-Type, Content-Range, Content-Disposition'
    },
    /* Uncomment and edit this section to provide the service via HTTPS:
    ssl: {
        key: fs.readFileSync('/Applications/XAMPP/etc/ssl.key/server.key'),
        cert: fs.readFileSync('/Applications/XAMPP/etc/ssl.crt/server.crt')
    },
    */
    nodeStatic: {
        cache: 3600 // seconds to cache served files
    }
}

function very_old_upload_invoice_document(req, res) {
    console.log('upload invoice doc.');

    var token = req.swagger.params.token;
    var file = req.swagger.params.file;

    var setNoCacheHeaders = function () {
        res.setHeader('Pragma', 'no-cache');
        res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate');
        // res.setHeader('Content-Disposition', 'inline; filename="files.json"');
    };

    // console.log('file:', file);

    // save to tmp path
    // service_excel.parse(file);
    // var msg = util.format('upload invoice document is successful');

    // this sends back a JSON response which is a single string

    var handler = new UploadHandler(req, res,
    () => {
        console.log('handler callback');
        res.json({ code:0, message: 'done upload'});
    });

    switch (req.method) {
        case 'OPTIONS':
            res.end();
            break;
        case 'HEAD':
        case 'GET':
//            if (req.url === '/') {
//                setNoCacheHeaders();
//                if (req.method === 'GET') {
//                    handler.get();
//                } else {
//                    res.end();
//                }
//            } else {
//                fileServer.serve(req, res);
//            }
            // console.log('req:', req);
            var filePath = req.files[0].path;
            console.log('excel file path:', filePath);

            // parse excel file
            break;
        case 'POST':
            console.log('### POST ###');
            var p = service_excel.parse(filePath);
            p.then(()=>{
                res.json({ message: 'done upload!'});
            });
            break;
        case 'DELETE':
            handler.destroy();
            break;
        default:
            res.statusCode = 405;
            res.end();
    }

}

var FileInfo = function (file) {
    this.name = file.name;
    this.size = file.size;
    this.type = file.type;
    this.deleteType = 'DELETE';
};

FileInfo.prototype.setName = function (name) {
    this.name = name;
};
FileInfo.prototype.validate = function () {
    if (options.minFileSize && options.minFileSize > this.size) {
        this.error = 'File is too small';
    } else if (options.maxFileSize && options.maxFileSize < this.size) {
        this.error = 'File is too big';
    } else if (!options.acceptFileTypes.test(this.name)) {
        this.error = 'Filetype not allowed';
    }
    return {failed: !this.error, code: this.error};
};
FileInfo.prototype.safeName = function () {
    // Prevent directory traversal and creating hidden system files:
    this.name = path.basename(this.name).replace(/^\.+/, '');
    // Prevent overwriting existing files:
    while (_existsSync(options.uploadDir + '/' + this.name)) {
        this.name = this.name.replace(nameCountRegexp, nameCountFunc);
    }
};
FileInfo.prototype.initUrls = function (req) {
    if (!this.error) {
        console.log('[initUrls]', this);
        var that = this,
            baseUrl = (true || options.ssl ? 'https:' : 'http:') +
                '//' + req.headers.host + options.uploadUrl;
        this.url = this.deleteUrl = baseUrl + encodeURIComponent(this.name);
        Object.keys(options.imageVersions).forEach(function (version) {
            if (_existsSync(
                    options.uploadDir + '/' + version + '/' + that.name
                )) {
                that[version + 'Url'] = baseUrl + version + '/' +
                    encodeURIComponent(that.name);
            }
        });
    }
};

var UploadHandler = function (req, res, callback) {
    this.req = req;
    this.res = res;
    this.callback = callback;
};

UploadHandler.prototype.destroy = function () {
    console.log('@@@ [destroy]');
    var handler = this,
        fileName;
    if (handler.req.url.slice(0, options.uploadUrl.length) === options.uploadUrl) {
        fileName = path.basename(decodeURIComponent(handler.req.url));
        if (fileName[0] !== '.') {
            fs.unlink(options.uploadDir + '/' + fileName, function (ex) {
                Object.keys(options.imageVersions).forEach(function (version) {
                    fs.unlink(options.uploadDir + '/' + version + '/' + fileName);
                });
                handler.callback({success: !ex});
            });
            return;
        }
    }
    handler.callback({success: false});
};

UploadHandler.prototype.get = function () {
    var handler = this;
    var files = [];
    fs.readdir(options.uploadDir, function (err, list) {
        // console.log(list);
        if (!list) {
            handler.callback({}, false);
            return;
        }
        list.forEach(function (name) {
            var stats = fs.statSync(options.uploadDir + '/' + name),
                fileInfo;
            if (stats.isFile() && name[0] !== '.') {
                fileInfo = new FileInfo({
                    name: name,
                    size: stats.size
                });
                fileInfo.initUrls(handler.req);
                files.push(fileInfo);
            }
        });
        handler.callback({files: files});
    });
};

UploadHandler.prototype.post = function () {
    var form = new formidable.IncomingForm();
    var tmpFiles = [];
    var handler = this;
    var files = [],
    map = {},
    counter = 1,
    redirect,
    finish = function() {
        console.log('[finish]', '\nfiles:',files, '\ntmp-files:', tmpFiles);
        // clean tmp files
        const removedTmpFiles = tmpFiles.splice(0, tmpFiles.length);
        Promise.map(removedTmpFiles, item => {
            console.log('### [finish] unlinking file:', item);
            return fsUnlinkAsync(item);
        });
        counter -= 1;
        if (!counter) {
            files.forEach(function (fileInfo) {
                fileInfo.initUrls(handler.req);
            });
            handler.callback({files: files}, redirect);
        }
    };

    form.uploadDir = options.tmpDir;
    form.on('fileBegin', function (name, file) {
        console.log('[on-file-begin]', 'name', name, 'file', file);
        tmpFiles.push(file.path);
        var fileInfo = new FileInfo(file, handler.req, true);
        fileInfo.safeName();
        map[path.basename(file.path)] = fileInfo;
        files.push(fileInfo);
    }).on('field', function (name, value) {
        if (name === 'redirect') {
            redirect = value;
        }
    }).on('file', function (name, file) {
        console.log('[on-file]', name);
        var fileInfo = map[path.basename(file.path)];
        fileInfo.size = file.size;
        const validation = fileInfo.validate();
        if (! validation.failed) {
            console.log('[validate] failed.', validation.code, file.path);
            fs.unlink(file.path);
            return;
        }
    }).on('aborted', function () {
        console.log('[on-aborted] tmp-files:', this.openedFiles);
        const removedTmpFiles = tmpFiles.splice(0, tmpFiles.length);
        if (removedTmpFiles.length > 0) {
            Promise.map(removedTmpFiles, item => {
                console.log('### unlinking file:', item);
                return fsUnlinkAsync(item);
            });
        }
        // TODO return error codes to client

    }).on('error', function (e) {
        console.log('[on-error]', e);
    }).on('progress', function (bytesReceived, bytesExpected) {
        console.log('[on-progress] bytes-received:', bytesReceived, 'bytes-expected', bytesExpected);

        // const file_type = fileType(buffer);
        // console.log('file_type:', file_type);
        // ctx.emit('aborted');

        if (this.openedFiles.length > 0 ) {
            const file = this.openedFiles[0];
            var ctx = this;

            isFileExists(file.path).then(is_exist => {
                if (!is_exist) {
                    return Promise.resolve();
                }
                readFile(file.path).then(img_buffer => {
                    const is_valid = validateFileType(img_buffer);
                    if (! is_valid) {
                        ctx.emit('aborted');
                        ctx._error(new Error('invalid-file-type'));
                        handler.req.connection.destroy();
                    }
                }).catch((err) => {
                    console.log('failed reading file. err:', err);
                });
            });
        } else {
            console.log('no-opened-files!!!');
        }

        if (bytesReceived > options.maxPostSize) {
            console.log('destroying connection');
            handler.req.connection.destroy();
        }
    }).on('part', function(part) {
        console.log('[on-part]', part);
        part.addListener('data', function() {
        });
    }).on('end', function(){
        console.log('[on-end] tmp-files:', tmpFiles);
            // finish();
            // res.json({ code:0, message: 'done upload'});
        }
    ).parse(handler.req);
}

function unused_upload_invoice(req, res) {

    const token = req.swagger.params.token.value;
    const param_file = req.swagger.params.file;

    var file = _.isUndefined(param_file.value)?'':param_file.value;
    var path_dir = __dirname+'/../../../static_file/data-invoice';
    var extension = path.extname(file.originalname);
    var original_filename = file.originalname;

    Promise.resolve()
    .then(() => {
        if(token == ACCESS_TOKEN_INTERNAL_DEV) {
            if (path.extname(file.originalname) == ".csv") {
                
                try {
                    var stats = fs.statSync(path_dir);
                    if (!stats.isDirectory()) {
                        fs.unlinkSync(path_dir);
                        fs.mkdirSync(path_dir);
                    }
                } catch(err) {
                    fs.mkdirSync(path_dir);
                } finally {
                    var d = new Date();
                    path_dir = path_dir + '/' + original_filename.slice(0, -(extension.length))+"_"+d.getDate()+"_"+(d.getMonth()+1)+"_"+d.getFullYear()+"_"+d.getHours()+"_"+d.getMinutes()+"_"+d.getSeconds()+extension;
                    fs.renameSync(file.path, path_dir);

                    var inputStream = fs.createReadStream(path_dir, 'utf8');
                    
                    var records = [];
                    inputStream
                    .pipe(CsvReadableStream({ parseNumbers: true, parseBooleans: true, trim: true }))
                    .on('data', function (row) {
                        records.push(row);
                    })
                    .on('end', function (data) {
                        records.splice(0, 3);
                        records.splice(records.length-2, records.length-1);

                        var out = service_invoice.postUploadInvoice({records:records});
                        res.json(out);
                    });

                    
                    // excelParser.parse({
                    //     inFile: path_dir,
                    //     worksheet: 1,
                    //     skipEmpty: true
                    // },function(err, records){
                    //     if(err){
                    //         console.log(err);
                    //         res.json({code: 400, message: 'Terjadi kesalahan'});
                    //     } else {
                            // var out = service_invoice.postUploadInvoice({records:records});
                            // res.json(out);
                    //     }                                
                    // });
                }
            } else {
                return Promise.reject(new Error("file extension bukan '.csv'"));
            }
            
        } else {
            throw new Error('Token Invalid');
        }
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message: ""+err});
    });
}

export { very_old_upload_invoice_document }
export { unused_upload_invoice }
export { check_invoices }
export { get_invoices }
export { get_retur }
export { get_one_retur }
export { get_all_retur_request }
export { request_retur }
export { get_one_invoice }
export { upload }
export { task }
export { check_process_invoice }
import util from 'util'
import Promise from 'bluebird'
import _ from 'lodash'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import service_auth from '@/server/services/auth'

function verify({token=null, acl=null, resources=null} = {}) {
    return Promise.resolve()
    .then(function(){
        var p_check_auth = service_auth.checkAuth(
        {   token:token,
            acl:acl,
            resource:resources
        });
        return p_check_auth;
    });
}

function exceptions(incoming_p, req, res) {
    return incoming_p
    .catch(AccessTokenExpiredError, err => {
        res.status(400).json({code: err.code, message:err.message});
    })
    .catch(UnauthorizedRequestError, err => {
        res.status(400).json({code: err.code, message:err.message});
    });
}

function paging({list=null, totalItems=0, page=0, itemsPerPage=1}={}) {
    list = list || [];

    var totalPages = Math.ceil(totalItems / itemsPerPage);
    var out = {};

    out.size = list.length;
    out.page = page;
    out.items_per_page = itemsPerPage;
    out.total_pages = totalPages;
    out.total_items = totalItems;

    return out;
}

var obj = {}
obj.verify = verify
obj.exceptions = exceptions
obj.paging = paging

export default obj
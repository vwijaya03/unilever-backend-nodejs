import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'

import { ResourceNotFoundError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { InvoiceAlreadyPaidError } from '@/server/error/Error'
import { BalanceNotEnoughError } from '@/server/error/Error'
import { MakePaymentError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { User } from '@/models/orm/index'
import { Invoice } from '@/models/orm/index'
import service_user from '@/server/services/user'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_invoice from '@/server/services/invoice'
import service_formatter from '@/server/services/formatter'
import service_transaction from '@/server/services/transaction'
import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'
import base_repo from '@/server/repositories/base'

var acl_default = AclDefault;

var acl_make_payment = acl_factory.getDefault().addRoles(['retailer', 'pickup_agent']);

function make_payment(req, res) {
    console.log('make_payment');

    var token = req.swagger.params.token.value;
    var invoiceId = req.swagger.params.invoice_id.value;
    var outletCode = req.swagger.params.outlet_code.value;
    var distributorCode = req.swagger.params.dt_code.value;
    var amount = req.swagger.params.amount.value;
    var pin = req.swagger.params.pin.value;

    console.log('invoiceId:', invoiceId);
    console.log('outletCode:', outletCode);
    console.log('distributorCode:', distributorCode);
    console.log('amount:', amount);

    const acl = acl_make_payment;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource});
    p = p.then(p_result => {
        const [principal, is_allowed] = p_result;
        return base_repo.getOne({entity:Invoice, where:{invoice_id:invoiceId}}).then(invoice => {
            return {principal:principal, invoice:invoice};
        });
    }).then(p_result => {
        const {principal:principal, invoice:invoice} = p_result;
        if (!invoice) {
            throw new ResourceNotFoundError('invoice not found');
        }
        // check if already paid in full
        return service_invoice.isPaidFull({invoiceId:invoice.invoice_id})
        .then(is_paid_full => {
            if (is_paid_full) {
                throw new InvoiceAlreadyPaidError();
            }
            p_result.invoice = invoice;
            return p_result;
        });
    }).then(p_result => {
        var {principal:principal, invoice:invoice} = p_result;
        return service_transaction.makePayment({principal:principal, invoice:invoice, pin:pin, amount:amount})
        .then(p2_result => {
            // TODO: create record in database?
            // service_transaction.createTxRecord();
            return p2_result;
        })
        .then(p2_result => {
            console.info('# payment is successful. ', p2_result);
            var {desc:message} = p2_result; // extract Valdo-MoneyAccess response.

            // if the message from valdo-moneyaccess is not defined then we create our own message.
            if (! message) {
                message = sprintf('sent payment from username:%3$s amount %1$s to invoice#:%2$s', amount, invoiceId, principal.user.username);
            }
            res.json({message:message});
            return true;
        });
    });
    return common_handler.exceptions(p, req, res)
    .catch(
        ResourceNotFoundError,
        InvoiceAlreadyPaidError,
        BalanceNotEnoughError,
        MakePaymentError, err => {
        res.status(404).json({code: err.code, message:err.message});
    })
    .catch(err => {
        res.status(400).json({code: 0, message:'failed to make a payment. '+err});
    });
}


export { make_payment }

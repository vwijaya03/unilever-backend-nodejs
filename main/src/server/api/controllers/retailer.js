import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'

import { ACCESS_TOKEN_INTERNAL_DEV } from '@/properties'

import { UserRegistrationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import service_user from '@/server/services/user'
import service_retailer from '@/server/services/retailer'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'
import base_repo from '@/server/repositories/base'

import fs from 'fs'
import path from 'path'
import excelParser from 'excel-parser'

var acl_default = acl_factory.getDefault();

var acl_get_all_retailers = acl_factory.getDefault().addRoles(['all']);
var acl_get_one_retailer = acl_factory.getDefault().addRoles(['all']);

function upload_retailer(req, res) {

    const token = req.swagger.params.token.value;
    const param_file = req.swagger.params.file;

    var file = _.isUndefined(param_file.value)?'':param_file.value;
    var path_dir = __dirname+'/../../../static_file/data-excel-retailer';
    var extension = path.extname(file.originalname);
    var original_filename = file.originalname;

    Promise.resolve()
    .then(() => {
        if(token == ACCESS_TOKEN_INTERNAL_DEV) {
            try {
                var stats = fs.statSync(path_dir);
                if (!stats.isDirectory()) {
                    fs.unlinkSync(path_dir);
                    fs.mkdirSync(path_dir);
                }
            } catch(err) {
                fs.mkdirSync(path_dir);
            } finally {
                path_dir = path_dir + '/' + original_filename.slice(0, -(extension.length)) + extension;
                fs.renameSync(file.path, path_dir);

                excelParser.parse({
                    inFile: path_dir,
                    worksheet: 1,
                    skipEmpty: true
                },function(err, records){
                    if(err){
                        console.log(err);
                        res.json({code: 400, message: 'Terjadi kesalahan'});
                    } else {
                        var out = service_retailer.postUploadRetailer({records:records});
                        res.json(out);
                    }                                
                });
            }
        } else {
            throw new Error('Token Invalid');
        }
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message: ""+err});
    });
}

function get_one_retailer(req, res) {
    console.log('get_one_retailer');

    const token = req.swagger.params.token.value;
    const acl = acl_get_one_retailer;
    const requested_resource = null;

    const retailer_id = req.swagger.params.retailer_id.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        console.log('retrieving one retailer');
        var args = { entity:Retailer, where:{id: retailer_id}, options:{throwIfEmpty:true} };
        return base_repo.getOne(args).then(retailer => {
            return [principal, retailer];
        });
    })
    .then(p_result => {
        const [principal, retailer] = p_result;
        console.log('got one retailer:', retailer);
        var output_options = service_formatter.computeOptions(principal);
        var out = service_formatter.format({obj:retailer, options:output_options});
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(ResourceNotFoundError, err => {
        res.status(400).json({code: 0, message:'retailer not found ' + err});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get one retailer ' + err});
    });
}

function get_retailers(req, res) {
    console.log('get_retailers');

    const token = req.swagger.params.token.value;
    const acl = acl_get_all_retailers;
    const requested_resource = null;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        console.log('retrieving all retailers');
        var args = {entity:Retailer};
        return base_repo.get(args).then(result_obj => {
            let {result_list:result_list} = result_obj;
            return [principal, result_list];
        });
    })
    .then(p_result => {
        const [principal, result_list] = p_result;
        var output_options = service_formatter.computeOptions(principal);
        console.log('result_list:', result_list);
        var out = service_formatter.format({obj:result_list, options:output_options});
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get retailers ' + err});
    });
}

export { get_retailers }
export { get_one_retailer }
export { upload_retailer }
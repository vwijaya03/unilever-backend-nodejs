import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'
import MoneyAccess from '@/moneyaccess/moneyaccess'

import { ACCESS_TOKEN_INTERNAL_DEV } from '@/properties'
import { UserRegistrationError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { PinError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import User from '@/models/orm/user'
import service_user from '@/server/services/user'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import service_logger from '@/server/services/logger'
import service_notification from '@/server/services/notification'
import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'

const DEBUG_LOG = true;
const LOG_TAG = '[ctrl:vaccount] ';
const log = service_logger.getLogger({name: 'controller', service:'user'});

const acl_default = acl_factory.getDefault();
const acl_history = acl_default;
const acl_register = acl_default;
const acl_transfer = acl_factory.getDefault().addRoles(['retailer','distributor','unilever_admin', 'distributor_manager', 'sales', 'pickup_agent']);
const acl_inquiry = acl_default;
const acl_pull_money = acl_factory.getDefault().addRoles(['retailer','distributor','unilever_admin', 'distributor_manager', 'sales']);
const acl_account_inquiry_name = acl_factory.getDefault().addRoles(['retailer','distributor','unilever_admin', 'distributor_manager', 'sales', 'pickup_agent']);

function vaccount_check_owner_bank_account(req, res) {
    const token = req.swagger.params.token.value;
    const param_accountnumber = req.swagger.params.accountnumber;
    const acl = acl_account_inquiry_name;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var args = {};
        args.accountnumber = _.isUndefined(param_accountnumber.value)?null:param_accountnumber.value;
        args.usertype = principal.user.type;
        console.log('check_owner_bank_account.', args);
        return MoneyAccess.account_inquiry_name(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        // console.log('isi principal user');
        // console.log(principal.user.type);
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to pull money.' + err});
    });
}

function vaccount_pull_money(req, res) {

    const token = req.swagger.params.token.value;
    const param_credit_accountno = req.swagger.params.credit_accountno;
    const param_value_date = req.swagger.params.value_date;
    const param_value_amount = req.swagger.params.value_amount;
    const param_destination_bankcode = req.swagger.params.destination_bankcode;
    const param_beneficiary_name = req.swagger.params.beneficiary_name;
    const param_preferred_transfer_method_id = req.swagger.params.preferred_transfer_method_id;
    const param_remark1 = req.swagger.params.remark1;
    const param_extended_payment_detail = req.swagger.params.extended_payment_detail;
    const param_beneficiary_email_address = req.swagger.params.beneficiary_email_address;
    const param_payment_method = req.swagger.params.payment_method;
    const param_reserved_field1 = req.swagger.params.reserved_field1;
    const param_reserved_field2 = req.swagger.params.reserved_field2;
    const param_pin = req.swagger.params.pin;

    const acl = acl_pull_money;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var d = new Date();

        var pin = _.isUndefined(param_pin.value)?null:param_pin.value;
        var expected_pin = service_user.getHashedPin(principal.user.salt, pin);

        console.log('pin hashed');
        console.log(expected_pin);
        
        if(expected_pin != principal.user.pin)
        {
            throw new PinError();
        }

        var args = {};
        args.credit_accountno = _.isUndefined(param_credit_accountno.value)?null:param_credit_accountno.value;
        args.value_date = _.isUndefined(param_value_date.value)?null:param_value_date.value;
        args.value_amount = _.isUndefined(param_value_amount.value)?null:param_value_amount.value;
        args.destination_bankcode = _.isUndefined(param_destination_bankcode.value)?null:param_destination_bankcode.value;
        args.beneficiary_name = _.isUndefined(param_beneficiary_name.value)?null:param_beneficiary_name.value;
        args.preferred_transfer_method_id = _.isUndefined(param_preferred_transfer_method_id.value)?null:param_preferred_transfer_method_id.value;
        args.remark1 = _.isUndefined(param_remark1.value)?null:param_remark1.value;

        args.extended_payment_detail = _.isUndefined(param_extended_payment_detail.value)?null:param_extended_payment_detail.value;

        args.desc = "Penarikan Dana Rp. "+service_formatter.IDRformat(args.value_amount)+" ke No.Rek.Mandiri "+args.credit_accountno+" oleh "+principal.user.fullname+"("+principal.user.valdoAccount+")";

        args.beneficiary_email_address = _.isUndefined(param_beneficiary_email_address.value)?null:param_beneficiary_email_address.value;
        args.payment_method = _.isUndefined(param_payment_method.value)?null:param_payment_method.value;
        args.reserved_field1 = _.isUndefined(param_reserved_field1.value)?null:param_reserved_field1.value;
        args.reserved_field2 = _.isUndefined(param_reserved_field2.value)?null:param_reserved_field2.value;
        args.vacc_number = _.isUndefined(principal.user.valdoAccount)?null:principal.user.valdoAccount;
        args.pin = pin;
        args.transid = d.getTime();
        args.usertype = principal.user.type;
        console.log('pull_money.', args);
        return MoneyAccess.dopayment(args).then(api_response => {
            return [principal, api_response];
        })
        .then(p_result => {
            const [principal, api_response] = p_result;
            // console.log('got api response: ', common_util.stringifyOnce(api_response));
            // console.log(principal.user);
            res.json(api_response);
        });
    })
    // .then(p_result => {
    //     const [principal, api_response] = p_result;
    //     // console.log('got api response: ', common_util.stringifyOnce(api_response));
    //     console.log(principal.user);
    //     res.json(api_response);
    // });
    return common_handler.exceptions(p, req, res)
    .catch(PinError, err => {
        res.status(400).json({code: err.code, message:'Pin salah.'});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to pull money.' + err});
    });
}

function vaccount_get_history(req, res) {

    if (DEBUG_LOG) console.log(LOG_TAG + 'vaccount_get_history');

    const param_mobnum = req.swagger.params.mobnum;
    const param_page = req.swagger.params.page;
    const param_type = req.swagger.params.type;
    const param_start_date = req.swagger.params.start_date;
    const param_end_date = req.swagger.params.end_date;
    const param_is_simulation = req.swagger.params.is_simulation;

    var page = _.isUndefined(param_page.value)?null:param_page.value;
    var type = _.isUndefined(param_page.value)?null:param_type.value;
    var is_simulation = _.isUndefined(param_is_simulation.value)?null:param_is_simulation.value;

    const token = req.swagger.params.token.value;
    const acl = acl_history;
    const requested_resource = {};

    var mobnum = _.isUndefined(param_mobnum.value)?null:param_mobnum.value;

    console.log(LOG_TAG + 'mobnum:', mobnum);

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, retailer] = p_result;
        var paging = {page:page};

        var p2 = Promise.resolve({});

        if (is_simulation) {
            // SIMULATE USING DIFFERENT TEST-VACCOUNT.
            // ONLY FOR DEVELOPMENT.
            // get the topup history for different accounts.
            var topup_mobnum = '8804112345678900';
            p2 = p2.then(p2_result => {
                return MoneyAccess.history({mobnum:topup_mobnum, paging:paging, type:type, usertype:principal.user.type}).then(api_response => {
                    p2_result.simulated_result = api_response;
                    return p2_result;
                });
            });
        }
        p2 = p2.then(p2_result => {
            var {simulated_result:simulated_result} = p2_result;
            return MoneyAccess.history({mobnum:mobnum, paging:paging, type:type, usertype:principal.user.type}).then(api_response => {
                p2_result.normal_result = api_response;
                return p2_result;
            });
        })
        .then(p2_result => {
            // combine all results
            var {normal_result:normal_result, simulated_result:simulated_result} = p2_result;
            console.log('simulated result: ', simulated_result);
            console.log('normal_result: ', normal_result);

            var api_response = simulated_result;

            if (! normal_result.failed) {
            }

            return {principal:principal, retailer:retailer, api_response};
        });

        return p2;

    })
    .then(p_result => {
        const {principal:principal, retailer:retailer, api_response} = p_result;
        console.log('got api response: ', common_util.stringifyOnce(api_response));
        //console.log('api response: ', JSON.stringify(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get vaccount history.' + err});
    });
}

function vaccount_register_user(req, res) {

    if (DEBUG_LOG) console.log('vaccount_register_user');

    const token = req.swagger.params.token.value;

    const param_mobnum = req.swagger.params.mobnum;
    const param_fname = req.swagger.params.fname;
    const param_lname = req.swagger.params.lname;
    const param_pob = req.swagger.params.pob;
    const param_dob = req.swagger.params.dob;
    const param_email = req.swagger.params.email;
    const param_pin = req.swagger.params.pin;

    const acl = acl_register;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var mobnum = param_mobnum.value;

        var args = {};
        args.mobnum = _.isUndefined(param_mobnum.value)?null:param_mobnum.value;
        args.fname = _.isUndefined(param_fname.value)?null:param_fname.value;
        args.lname = _.isUndefined(param_lname.value)?null:param_lname.value;
        args.email = _.isUndefined(param_email.value)?null:param_email.value;
        args.pin = _.isUndefined(param_pin.value)?null:param_pin.value;
        args.dob = _.isUndefined(param_dob.value)?null:param_dob.value;
        args.pob = _.isUndefined(param_pob.value)?null:param_pob.value;

        console.log('register new user', args);
        return MoneyAccess.register(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get vaccount history.' + err});
    });

}

function vaccount_pickup_transfer_money(req, res) {

    if (DEBUG_LOG) console.log('vaccount_pickup_transfer_money');

    const token = req.swagger.params.token.value;
    const param_mobnum = req.swagger.params.mobnum;
    const param_from = req.swagger.params.vaccount_from;
    const param_to = req.swagger.params.vaccount_to;
    const param_amount = req.swagger.params.amount;
    const param_pin = req.swagger.params.pin;

    const acl = acl_transfer;
    const requested_resource = {};

    console.log('param_mobnum', param_mobnum);

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var mobnum = param_mobnum.value;
        var tanggal = new Date();
        var vaccount = _.isUndefined(param_to.value)?null:param_to.value;
        var pickup_name = principal.user.fullname;

        return service_user.getOneUser({vaccount: vaccount}).then(response => {
            
            var args = {};
            // args.mobnum = _.isUndefined(param_mobnum.value)?null:param_mobnum.value;
            args.vaccount_pickup = _.isUndefined(param_from.value)?null:param_from.value;
            args.vaccount_toko = _.isUndefined(param_to.value)?null:param_to.value;
            args.amount = _.isUndefined(param_amount.value)?null:param_amount.value;
            args.pin = _.isUndefined(param_pin.value)?null:param_pin.value;
            args.usertype = principal.user.type;
            
            args.desc = "PickUp TRANSFER Rp. "+service_formatter.IDRformat(param_amount.value)+" dari "+pickup_name+ " ("+principal.user.valdoAccount+")"+" ke "+response.fullname+" ("+param_to.value+")";
            args.transId = tanggal.getTime();

            console.log('pickup transfer.', args);
            log.debug('isi pickup transfer');
            log.debug(args);
            return MoneyAccess.pickup_transfer(args).then(api_response => {
                return [principal, api_response];
            });
        });
    })
    .then(p_result => {

        const [principal, api_response] = p_result;
        console.log('got api response: ', common_util.stringifyOnce(api_response));

        return service_user.getOneUser({vaccount: param_to.value}).then(retailer => {
            // console.log('isi retailer');
            // console.log(retailer);
            // console.log('isi fcm id');
            // console.log(retailer.fcm_id);

            var d = new Date,
            dformat = [ (d.getMonth()+1).padLeft(),
                        d.getDate().padLeft(),
                        d.getFullYear()].join('/')+
                        ' ' +
                      [ d.getHours().padLeft(),
                        d.getMinutes().padLeft(),
                        d.getSeconds().padLeft()].join(':');

            var pickup_transfer_message = JSON.stringify({
                title: 'Transfer Pickup Berhasil',
                body: 'Pickup transfer ke VA '+param_to.value+' dengan jumlah Rp. '+param_amount.value+' berhasil.'
            });

            var args = {};
            var fcm = "";
            if(retailer.fcm_id == null){
                args.to = fcm;
            }
            else{
                args.to = retailer.fcm_id;
            }
            
            args.type = 'notification';
            args.message = pickup_transfer_message;
            args.state = 0;
            args.createdAt = dformat;
            args.updatedAt = dformat;

            console.log('add_notification.', args);

            return (new service_notification()).addNotification(args).then(notification => {
                log.debug('isi api response');
                log.debug(api_response);
                res.json(api_response);
            });
        });
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to transfer.' + err});
    });
}

function vaccount_transfer_money(req, res) {

    if (DEBUG_LOG) console.log('vaccount_transfer_money');

    const token = req.swagger.params.token.value;
    const param_mobnum = req.swagger.params.mobnum;
    const param_from = req.swagger.params.vaccount_from;
    const param_to = req.swagger.params.vaccount_to;
    const param_amount = req.swagger.params.amount;

    const acl = acl_transfer;
    const requested_resource = {};

    console.log('param_mobnum', param_mobnum);

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var mobnum = param_mobnum.value;
        var tanggal = new Date();

        var args = {};
        args.mobnum = _.isUndefined(param_mobnum.value)?null:param_mobnum.value;
        args.from = _.isUndefined(param_from.value)?null:param_from.value;
        args.to = _.isUndefined(param_to.value)?null:param_to.value;
        args.amount = _.isUndefined(param_amount.value)?null:param_amount.value;
        args.usertype = principal.user.type;
        args.transId = tanggal.getTime();

        console.log('transfer.', args);
        return MoneyAccess.transfer(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to transfer.' + err});
    });

}

function vaccount_inquiry(req, res) {

    if (DEBUG_LOG) console.log('vaccount_inquiry');

    const token = req.swagger.params.token.value;
    const param_mobnum = req.swagger.params.mobnum;
    const param_vaccount = req.swagger.params.vaccount;

    const acl = acl_inquiry;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var args = {};
        args.mobnum = _.isUndefined(param_mobnum.value)?null:param_mobnum.value;
        args.vaccount = _.isUndefined(param_vaccount.value)?null:param_vaccount.value;
        args.usertype = principal.user.type;

        console.log('inquiry.', args);
        return MoneyAccess.inquiry(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to inquiry.' + err});
    });

}

function vaccount_custlist(req, res) {

    if (DEBUG_LOG) console.log('vaccount_custlist');

    const token = req.swagger.params.token.value;
    const acl = acl_default;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var args = {};
        return MoneyAccess.custlist(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get customers list.' + err});
    });

}

function vaccount_updatecust(req, res) {

    if (DEBUG_LOG) console.log('vaccount_updatecust');

    const token = req.swagger.params.token.value;
    const acl = acl_default;
    const requested_resource = {};

    const param_vaccount = req.swagger.params.vaccount;
    const param_fname = req.swagger.params.fname;
    const param_lname = req.swagger.params.lname;
    const param_pob = req.swagger.params.pob;
    const param_dob = req.swagger.params.dob;
    const param_email = req.swagger.params.email;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var args = {
            vaccount:param_vaccount.value,
            fname: param_fname.value,
            lname: param_lname.value,
            pob: param_pob.value,
            dob: param_dob.value,
            email: param_email.value
        };
        return MoneyAccess.updatecust(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to update customer info. ' + err});
    });

}

function vaccount_resetpin(req, res) {

    if (DEBUG_LOG) console.log('vaccount_resetpin');

    const token = req.swagger.params.token.value;
    const acl = acl_default;
    const requested_resource = {};

    const param_vaccount = req.swagger.params.vaccount;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var args = {
            vaccount:param_vaccount.value
        };
        return MoneyAccess.resetpin(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to reset pin. ' + err});
    });

}

function vaccount_changepin(req, res) {

    if (DEBUG_LOG) console.log('vaccount_changepin');

    const token = req.swagger.params.token.value;
    // const acl = acl_default;
    const acl = acl_pull_money;
    const requested_resource = {};

    const param_vaccount = req.swagger.params.vaccount;
    const param_old_pin = req.swagger.params.old_pin;
    const param_new_pin = req.swagger.params.new_pin;
    const param_retype_new_pin = req.swagger.params.retype_new_pin;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        var args = {
            vaccount: param_vaccount.value,
            old_pin: param_old_pin.value,
            new_pin: param_new_pin.value,
            retype_new_pin: param_retype_new_pin.value
        };
        return MoneyAccess.changepin(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to change pin. ' + err});
    });

}

export { vaccount_custlist }
export { vaccount_updatecust }
export { vaccount_resetpin }
export { vaccount_changepin }

export { vaccount_get_history }
export { vaccount_register_user }
export { vaccount_transfer_money }
export { vaccount_inquiry }
export { vaccount_pull_money }
export { vaccount_check_owner_bank_account }
export { vaccount_pickup_transfer_money }
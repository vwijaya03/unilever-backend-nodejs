import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'
import moment from 'moment'

// money-access errors
import { MoneyAccessRegisterError } from '@/moneyaccess/error/index'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { DOBInvalidFormat } from '@/server/error/Error'
import { UsernameAlreadyExistsError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePasswordError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Pickup } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import { UserQuery } from '@/models/orm/user'

import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_bank_account from '@/server/services/bankaccount'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import service_notification from '@/server/services/notification'

import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'

const acl_notification = acl_factory.getDefault().addRoles(['all']);

function get_notification(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_notification;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        return (new service_notification()).get().then(api_response => {
            console.log(api_response);
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        const iso8601format = "YYYY-MM-DD";
        var data = service_formatter.format({obj:api_response, typeId: 'notifikasi'})
        data.forEach(result => {

            var dateCreatedAt = new Date(result.createdAt);
            var formattedCreatedAt = dateCreatedAt.getFullYear() +'-'+ ("0" + (dateCreatedAt.getMonth() + 1)).slice(-2) +'-'+ ("0" + dateCreatedAt.getDate()).slice(-2) +' '+dateCreatedAt.getHours()+ ':' +dateCreatedAt.getMinutes()+ ':'+dateCreatedAt.getSeconds();

            var dateUpdatedAt = new Date(result.updatedAt);
            var formattedUpdatedAt = dateUpdatedAt.getFullYear() +'-'+ ("0" + (dateUpdatedAt.getMonth() + 1)).slice(-2) +'-'+ ("0" + dateUpdatedAt.getDate()).slice(-2) +' '+dateUpdatedAt.getHours()+ ':' +dateUpdatedAt.getMinutes()+ ':'+dateUpdatedAt.getSeconds();

            result.createdAt = formattedCreatedAt;
            result.updatedAt = formattedUpdatedAt;

        });

        var out = {};
        out.notification = data;
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to show notification.' + err});
    });
}

function add_notification(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_notification;
    const requested_resource = {};

    const param_to = req.swagger.params.to;
    const param_type = req.swagger.params.type;
    const param_title = req.swagger.params.title;
    const param_message = req.swagger.params.message;

    var to = _.isUndefined(param_to.value)?'':param_to.value;
    var type = _.isUndefined(param_type.value)?'':param_type.value;
    var title = _.isUndefined(param_title.value)?'':param_title.value;
    var message = _.isUndefined(param_message.value)?'':param_message.value;
    var result_message = JSON.stringify({
    	title: title,
    	body: message
    })

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        console.log(principal)

        if (principal.user){
            if (to){

            } else {
                switch (type){
                    case "email": {
                        to = principal.user.email
                    } break;
                    case "sms": {
                        to = principal.user.phone
                    } break;
                    case "notification": 
                    case "data": {
                        to = principal.user.fcm_id
                    } break;
                }
            } 
            return Promise.resolve(p_result)
        } else {
            if (to) {
                var multiple_to = to.split(/[\s\,]/gi).filter(Boolean);
                return User.findAll({ where: {valdo_account: multiple_to} })
                .then(users => {
                    if (users.length > 0){
                        multiple_to = [];
                        for (var i = 0; i < users.length; i++){
                            var user = users[i]
                            switch (type){
                                case "email": {
                                    multiple_to.push(user.email)
                                } break;
                                case "sms": {
                                    multiple_to.push(user.phone)
                                } break;
                                case "notification": 
                                case "data": {
                                    multiple_to.push(user.fcm_id)
                                } break;
                            }
                        }
                        to = multiple_to.join(", ")
                    } else {

                    }
                    return Promise.resolve(p_result)
                }) 
            } else {
                throw new Error("Could not determine destination")
            }
        }
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var d = new Date,
        dformat = [ (d.getMonth()+1).padLeft(),
                    d.getDate().padLeft(),
                    d.getFullYear()].join('/')+
                    ' ' +
                  [ d.getHours().padLeft(),
                    d.getMinutes().padLeft(),
                    d.getSeconds().padLeft()].join(':');

        var args = {};

        args.to = to;
        args.type = type;
        args.message = result_message;
        args.state = 0;
        args.createdAt = dformat;
        args.updatedAt = dformat;

        console.log('add_notification.', args);

        return (new service_notification()).addNotification(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'Gagal menambahkan notifikasi.' + err});
    });
}

export { get_notification }
export { add_notification }
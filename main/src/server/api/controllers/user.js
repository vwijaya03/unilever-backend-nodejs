import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'

// money-access errors
import { MoneyAccessRegisterError } from '@/moneyaccess/error/index'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { DOBInvalidFormat } from '@/server/error/Error'
import { UsernameAlreadyExistsError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePasswordError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Pickup } from '@/models/orm/index'
import { Sales } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import { UserRetailerCode } from '@/models/orm/index'
import { orm } from '@/server/services/mariadb'
import { UserQuery } from '@/models/orm/user'

import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_bank_account from '@/server/services/bankaccount'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import service_otp from '@/server/services/otp'
import { MoneyAccess } from '@/moneyaccess/index'
import { MoneyAccessErrors } from '@/moneyaccess/index'

import usertypes from '@/models/usertypes'
import { USERTYPES } from '@/models/usertypes'

import { ACCESS_TOKEN_WEBADMIN } from '@/properties'
import { allowed_user_status } from '@/properties'

import { RETAILER_CODE } from '@/models/usertypes'
import { DISTRIBUTOR_CODE } from '@/models/usertypes'
import { DISTRIBUTOR_MANAGER_CODE } from '@/models/usertypes'
import { SALES_CODE } from '@/models/usertypes'
import { SUPER_ADMIN_CODE } from '@/models/usertypes'
import { UNILEVER_ADMIN_CODE } from '@/models/usertypes'
import { PICKUP_AGENT_CODE } from '@/models/usertypes'

import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'
import GoogleAuth from 'google-auth-library'

const acl_default = acl_factory.getDefault();
// const acl_register_user = acl_factory.getDefault().addRoles(['retailer','distributor','distributor_manager']);
const acl_register_user = acl_factory.getDefault().addRoles(['all']);
const acl_update_user_info = acl_factory.getDefault().addRoles(['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center']);
const acl_get_users = acl_factory.getDefault();
const acl_non_internal = acl_factory.getDefault().addRoles(['retailer','distributor']);
const acl_add_bank_account = acl_factory.getDefault().addRoles(['retailer','distributor']);
const acl_update_bank_account = acl_factory.getDefault().addRoles(['retailer','distributor']);
const acl_destroy_bank_account = acl_factory.getDefault().addRoles(['retailer','distributor']);
const acl_get_bank_account = acl_factory.getDefault().addRoles(['retailer','distributor']);
const acl_get_user_types = acl_factory.getDefault().addRoles(['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center']);
const acl_user_changepassword = acl_factory.getDefault().addRoles(['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center']);
const acl_get_one_user = acl_factory.getDefault().addRoles(['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center']);
const acl_user_changepin = acl_factory.getDefault().addRoles(['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center']);
const acl_user_resetpin = acl_factory.getDefault().addRoles(['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center']);
// const acl_get_one_user = acl_factory.getDefault().resources({
//     username: [
//         { principal_roles:{ contains:['internal'] } },
//         { principal_user:{ matches:['username'] } },
//         { terms:{ has:['self'] } }
//     ]
// });

const log = service_logger.getLogger({name: 'controller', service:'user'});

const USER_TYPE_RETAILER = RETAILER_CODE;
const USER_TYPE_DISTRIBUTOR = DISTRIBUTOR_CODE;
const USER_TYPE_DISTRIBUTOR_MANAGER = DISTRIBUTOR_MANAGER_CODE;
const USER_TYPE_PICKUP = PICKUP_AGENT_CODE;
const USER_TYPE_SALES = SALES_CODE;

var test_usernames = ["dist1", "user1", "user2", "user3", "superadmin1", "superadmin2", "unileveradmin1", "unileveradmin2"];

function user_resetpin(req, res) {

    const token = req.swagger.params.token.value;
    const acl = acl_user_resetpin;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var args = {
            vaccount:principal.user.valdoAccount,
            usertype:principal.user.type
        };
        return MoneyAccess.resetpin(args).then(api_response => {
            console.log('api respon');
            console.log(api_response);
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        
        var args = {};
        args.pin = api_response.userinfo.new_pin;
        args.user_salt = principal.user.salt;
        args.user_id = principal.user.id;
        args.email = principal.user.email;

        console.log(args);

        return service_user.resetpin(args).then(api_response => {
            // console.log(api_response);
            return [principal, api_response];
        });       
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to reset pin. ' + err});
    });
}

function user_changeimei(req, res) {

    const phone = req.swagger.params.phone.value;
    const imei = req.swagger.params.imei.value;
    const otp = req.swagger.params.otp.value;
    const acl = acl_user_changepin;
    const requested_resource = {};

    return (new service_otp()).verifyOtp(null, otp).then(api_response => {
            return api_response;
    })
    .then(result => {
        
        var out = {};
        
        if(_.size(result[0]) > 0) {

            User.update(
                {
                    imei: imei
                },
                { 
                    where: 
                    { 
                        phone: phone
                    }
                }
            );
            out.code = 200;
            out.message = "IMEI berhasil di ubah";
            res.json(out);
        } else {
            out.code = 400;
            out.message = "IMEI gagal di ubah";
            res.json(out);
        }    
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to change pin. ' + err});
    });
}

function user_changepin(req, res) {
    // if (DEBUG_LOG) console.log('vaccount_changepin');
    const token = req.swagger.params.token.value;
    const acl = acl_user_changepin;
    const requested_resource = {};

    const param_old_pin = req.swagger.params.old_pin;
    const param_new_pin = req.swagger.params.new_pin;
    const param_retype_new_pin = req.swagger.params.retype_new_pin;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var args = {
            vaccount: principal.user.valdoAccount,
            old_pin: param_old_pin.value,
            new_pin: param_new_pin.value,
            retype_new_pin: param_retype_new_pin.value,
            usertype:principal.user.type
        };
        return MoneyAccess.changepin(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        
        var args = {};
        args.pin = api_response.userinfo.new_pin;
        args.user_salt = principal.user.salt;
        args.user_id = principal.user.id;

        return service_user.changepin(args).then(api_response => {
            // console.log(api_response);
            return [principal, api_response];
        });       
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to change pin. ' + err});
    });
}

function user_resetpassword(req, res) {
    const email = req.swagger.params.email.value;

    Promise.resolve()
    .then(() => {
        return service_user.reset_password({ 'email': email })
        .then(response => {

            var out = response;
            // console.log('out: ', out, user);
            res.json(out);
        });
    })
    .catch(UserNotFoundError, err => {
        res.status(404).json({code: err.code, message:err.message});
    })
    .catch(err => {
        console.log('error: ', err);
        res.status(400).json({code: 400, message:'reset password failed'});
    });
}

function user_changepassword(req, res) {
    const token = req.swagger.params.token.value;
    const acl = acl_user_changepassword;
    const requested_resource = {};
    const param_old_password = req.swagger.params.oldpassword;
    const param_new_password = req.swagger.params.newpassword;

    var oldpassword = _.isUndefined(param_old_password.value)?'':param_old_password.value;
    var newpassword = _.isUndefined(param_new_password.value)?'':param_new_password.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var expected_oldpassword = service_user.getHashedPassword(principal.user.salt, oldpassword);

        if(expected_oldpassword != principal.user.password)
        {
            throw new ChangePasswordError();
        }

        var args = {};
        args.user_id = principal.user.id;
        args.user_salt = principal.user.salt;
        args.newpassword = newpassword;
        // console.log('user_changepassword.', args);

        return service_user.changepassword(args).then(api_response => {
            //console.log(api_response);
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        // var out = service_formatter.format({obj:api_response});
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(ChangePasswordError, err => {
        res.status(400).json({code: err.code, message:err.message});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to change password.' + err});
    });
}

function get_user_bank_account(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_update_bank_account;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var args = {};
        args.user_id = principal.user.id;
        //console.log('get_user_bank_account.', args);

        return service_bank_account.get(args).then(api_response => {
            //console.log(api_response);
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        // var out = service_formatter.format({obj:api_response});
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to add bank account.' + err});
    });
}

function update_bank_account(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_update_bank_account;
    const requested_resource = {};
    const id_bank_account = req.swagger.params.id_bank_account.value;

    const param_no_rekening = req.swagger.params.no_rekening;
    const param_bank = req.swagger.params.bank;

    var no_rekening = _.isUndefined(param_no_rekening.value)?'':param_no_rekening.value;
    var bank = _.isUndefined(param_bank.value)?'':param_bank.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var args = {};
        args.id_bank_account = id_bank_account;
        args.user_id = principal.user.id;
        args.no_rekening = no_rekening;
        args.bank = bank;
        //console.log('update_bank_account.', args);

        return service_bank_account.updateBankAccount(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to update bank account.' + err});
    });
}

function update_bank_account_android(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_update_bank_account;
    const requested_resource = {};
    const id_bank_account = req.swagger.params.id_bank_account.value;

    const param_no_rekening = req.swagger.params.no_rekening;

    var no_rekening = _.isUndefined(param_no_rekening.value)?'':param_no_rekening.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;

        var args_check_bank_account = {};
        args_check_bank_account.accountnumber = no_rekening;
        args_check_bank_account.usertype = principal.user.type;

        if(args_check_bank_account.accountnumber == null || args_check_bank_account.accountnumber == '') 
        {
            throw new UserRegistrationError("Nomor rekening tidak boleh kosong");
        } 
        else
        {
            console.log(args_check_bank_account);
            return MoneyAccess.account_inquiry_name(args_check_bank_account).then(api_response => {
                return p_result;
            });
        }
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var args = {};
        args.id_bank_account = id_bank_account;
        args.user_id = principal.user.id;
        args.no_rekening = no_rekening;
        //console.log('update_bank_account_android.', args);

        return service_bank_account.updateBankAccountAndroid(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to update bank account.' + err});
    });
}

function destroy_bank_account(req, res) {
    const token = req.swagger.params.token.value;
    const acl = acl_update_bank_account;
    const requested_resource = {};
    const id_bank_account = req.swagger.params.id_bank_account.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var args = {};
        args.id_bank_account = id_bank_account;
        args.user_id = principal.user.id;
        //console.log('destroy_bank_account.', args);

        return service_bank_account.destroyBankAccount(args).then(api_response => {
            //console.log(api_response);
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        // var out = service_formatter.format({obj:api_response});
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to add bank account.' + err});
    });
}

function add_bank_account(req, res)
{
    const token = req.swagger.params.token.value;
    const acl = acl_add_bank_account;
    const requested_resource = {};

    const param_name = req.swagger.params.name;
    const param_no_rekening = req.swagger.params.no_rekening;
    const param_bank = req.swagger.params.bank;

    var name = _.isUndefined(param_name.value)?'':param_name.value;
    var no_rekening = _.isUndefined(param_no_rekening.value)?'':param_no_rekening.value;
    var bank = _.isUndefined(param_bank.value)?'':param_bank.value;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        var args = {};
        args.user_id = principal.user.id;
        args.name = name;
        args.no_rekening = no_rekening;
        args.bank = bank;
        //console.log('add_bank_account.', args);

        return service_bank_account.createBankAccount(args).then(api_response => {
            return [principal, api_response];
        });
    })
    .then(p_result => {
        const [principal, api_response] = p_result;
        // console.log('got api response: ', common_util.stringifyOnce(api_response));
        res.json(api_response);
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to add bank account.' + err});
    });
}

function signin(req, res) {

    const email = req.swagger.params.email.value;
    const password = req.swagger.params.password.value;

    Promise.resolve()
    .then(() => {
        return service_user.getOne({ 'email': email, $or: allowed_user_status}, { throwIfEmpty:true })
        .then(user => {
            var actual_password = service_user.getHashedPassword(user.salt, password);

            log.debug(sprintf('email:%1$s hashed-password:%2$s', email, actual_password));

            // if (test_usernames.indexOf(username) >= 0) {
            //     log.warn(sprintf('%1$s is a test username.', username));
            // }

            var expected_password = user.password;
            if (expected_password != actual_password) {
                throw new SigninError();
            }

            // regenerate access token if nearing expired
            var is_check_token_expiry = true;
            if (is_check_token_expiry) {
                var delta_days_in_seconds = 259200; // 3 days
                if (user) {
                    const expiry_ts = user.accessTokenExpiry; // expiry is seconds
                    const now_seconds = new Date().getTime() / 1000;
                    log.info(sprintf('email:%1$s expiry_ts:%2$s now:%3$s', email, expiry_ts, now_seconds));
                    if (now_seconds > expiry_ts - delta_days_in_seconds) {
                        // renew access token and update db
                        return service_user.renewAccessToken({user:user}).then((new_access_token)=>{
                            var out = {token: new_access_token, user_id: user.id};
                            // console.log('isi out di dalam if expired');
                            // console.log(out);
                            res.json(out);
                        });
                    }
                }
            }

            var out = {token: user.accessToken, user_id: user.id};
            // console.log('out: ', out, user);
            res.json(out);
        });
    })
    .catch(UserNotFoundError, err => {
        res.status(400).json({code: err.code, message:'invalid signin credentials'});
    })
    .catch(SigninError, err => {
        res.status(400).json({code: err.code, message:'invalid signin credentials'});
    })
    .catch(err => {
        console.log('error: ', err);
        res.status(400).json({code: 0, message:'signin failed'});
    });
}

function logout(req, res) {
    console.log('logout');

    const token = req.swagger.params.token.value;
    const acl = acl_get_one_user;
    const requested_resource = null;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then((p_result) => {
        const [principal, is_allowed] = p_result;

        //console.log('isi principal');
        //console.log(principal.user.id);
        
        caller_logout(principal.user.id);

        return "berhasil update fcm id";
        
    })
    .then((result) => {
        var out = {code: 200, msg: 'Logout berhasil'};
        res.json(out);
    });

    return common_handler.exceptions(p, req, res)
    .catch(UserNotFoundError, err => {
        res.status(404).json({code: err.code, message:err.message});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'Gagal logout user ' + err});
    });
}

function caller_logout(user_id)
{
    User.update(
        {
            fcm_id: "",
            accessTokenExpiry: "1970-01-18 05:28:32"
        },
        { 
            where: 
            { 
                id: user_id
            }
        }
    );
}

function get_user_types(req, res) {
    console.log('get_user_types');

    const token = req.swagger.params.token.value;
    const acl = acl_get_user_types;
    const requested_resource = null;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, users_resultset] = p_result;
        var out = USERTYPES;
        res.json(out);
    })
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get roles.' + err});
    });
}

function get_users(req, res) {
    // console.log('get_users');
    const token = req.swagger.params.token.value;
    const acl = acl_get_users;
    const requested_resource = null;

    const param_types = req.swagger.params.types;
    const param_fullname = req.swagger.params.fullname;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;

    var typesNameList = _.isUndefined(param_types.value)?[]:param_types.value.split(',');
    var startPage = _.isUndefined(param_page.value)?0:param_page.value;
    var itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
    var fullname = _.isUndefined(param_fullname.value)?'':param_fullname.value;

    startPage = (startPage < 1 ) ? 1 : startPage;

    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        // retrieve users
        // console.log('retrieving all users..');
        var where = {};
        var typesList;

        if (typesNameList && typesNameList.length > 0) {
            typesList = typesNameList.map(type => {
                return USERTYPES[type];
            });
       
            where = {fullname: {$like: '%'+fullname+'%'}};            
            UserQuery.types({filter:where, typesList:typesList});
        }

        if(fullname && fullname.length > 0)
        {
            where = {fullname: {$like: '%'+fullname+'%'}};            
            UserQuery.types({filter:where, typesList:typesList});
        }

        return base_repo.get({entity:User, where:where, limit:limit, offset:offset}).then(result_obj => {
            return {principal:principal, result_obj:result_obj};
        });
    })
    .then(p_result => {
        const {principal:principal, result_obj:result_obj} = p_result;
        const {total:total, result_list:result_list} = result_obj;

        // now we get retailer and distributor for each user.

        return Promise.map(result_list, user => {
            return service_user.resolveRetailerAndDistributor({user:user});
        }).then(()=>{
            var output_options = service_formatter.computeOptions(principal);
            var out = service_formatter.format({obj:result_list, options:output_options});
            // console.log('out:', out);

            var paging = {};
            var total_pages = Math.ceil(total / itemsPerPage);

            out = Object.assign(out, paging);

            paging.page = startPage;
            paging.total_pages = total_pages;
            paging.total_items = total;
            paging.items_per_page = itemsPerPage;
            paging.size = out.length;
            paging.users = out;

            res.json(paging);
        });

    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get user info ' + err});
    });
}

function get_one_user_by_vaccount(req, res) {

    console.log('get_one_user_by_vaccount');

    const token = req.swagger.params.token.value;
    const vaccount = req.swagger.params.vaccount.value;
    const acl = acl_get_one_user;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then((p_result) => {
        const [principal, is_allowed] = p_result;
        
        return service_user.getOneRetailerName({vaccount: vaccount}, {throwIfEmpty:true}).then(user => {
            return [principal, user];
        });
    })
    .then(p_result => {
        const [principal, user] = p_result;
        if (!user) {
            throw new UserNotFoundError('user not found');
        }

        var out = service_formatter.format({obj:user});
        res.json(out);
    });
    return common_handler.exceptions(p, req, res)
    .catch(UserNotFoundError, err => {
        res.status(404).json({code: err.code, message:err.message});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get user info ' + err});
    });
}

function get_one_user(req, res) {

    console.log('get_one_user');
    const token = req.swagger.params.token.value;
    const id = req.swagger.params.id.value;
    const param_imei = req.swagger.params.imei;
    const acl = acl_get_one_user;
    const requested_resource = {user_id: id};

    var imei = _.isUndefined(param_imei.value)?'':param_imei.value;
    
    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then((p_result) => {
        const [principal, is_allowed] = p_result;
        
        // console.log('dibawah p_result');
        // console.log('isi principal');
        // console.log(principal);
        
        if (id === 'self') {
            if (! principal.id) {
                //console.log('masuk di null');
                return [principal, null];
            }
            else {
                var principal_username = principal.id;
                //console.log('self. get id:', principal_username);
                return service_user.getOne({id: principal_username}).then(user => {
                    return [principal, user];
                });
            }
        }
        return service_user.getOne({id: id}, {throwIfEmpty:true}).then(user => {
            return [principal, user];
        });
    })
    .then(p_result => {
        const [principal, user] = p_result;
        if (!user) {
            throw new UserNotFoundError('user not found');
        }
        
        if(imei.length != 0) {
            if(user.dataValues.imei != imei) {
                caller_logout(user.dataValues.id);
                return res.status(400).json({code: 4001, message: 'Login gagal, IMEI berbeda', phone: principal.user.phone});
            }
            
            if(user.dataValues.imei == null || user.dataValues.imei == "") {
                
                User.update(
                    {
                        imei: imei
                    },
                    { 
                        where: 
                        { 
                            id: user.dataValues.id
                        }
                    }
                );
            }
        }

        return service_user.resolveRetailerAndDistributor({user:user})
        .then(p2_result => {
            var args = {};
            args.user_id = id;
            //console.log('get_user_bank_account.', args);

            return service_bank_account.get_bank_account_only(args).then(bank_accounts => {
                var output_options = service_formatter.computeOptions(principal);

                // inject roles
                user.roles = principal.roles;
                // user.banks = bank_accounts;
                var out = service_formatter.format({obj:user});
                out.banks = bank_accounts;
                // console.log('returning out: ', out);
                // console.log('Isi out nya');
                // console.log(out.banks[0]);
                res.json(out);
            });
            
        });

    });
    return common_handler.exceptions(p, req, res)
    .catch(UserNotFoundError, err => {
        res.status(404).json({code: err.code, message:err.message});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to get user info ' + err});
    });
}

function register_user(req, res) {

    log.debug('register_user');

    var param_usertype = req.swagger.params.usertype;
    var param_outletCode = req.swagger.params.outlet_code;
    var param_distributorCode = req.swagger.params.dt_code;
    var param_pickupCode = req.swagger.params.pickup_code;
    var param_salesCode = req.swagger.params.sales_code;

    var param_LeCodeDry = req.swagger.params.le_code_dry;
    var param_LeCodeIce = req.swagger.params.le_code_ice;
    var param_LeCodeUfs = req.swagger.params.le_code_ufs;

    var param_pin = req.swagger.params.pin;

    var param_reference = req.swagger.params.reference;
    var param_onbehalf = req.swagger.params.onbehalf;
    var param_no_rekening = req.swagger.params.no_rekening;
    var param_imei = req.swagger.params.imei;

    var usertype = _.isUndefined(param_usertype.value)?USER_TYPE_RETAILER:param_usertype.value; // default is retailer
    var outletCode = _.isUndefined(param_outletCode.value)?null:param_outletCode.value;
    var distributorCode = _.isUndefined(param_distributorCode.value)?null:param_distributorCode.value;
    var pickupCode = _.isUndefined(param_pickupCode.value)?null:param_pickupCode.value;
    var salesCode = _.isUndefined(param_salesCode.value)?null:param_salesCode.value;

    var leCodeDry = _.isUndefined(param_LeCodeDry.value)?null:param_LeCodeDry.value;
    var leCodeIce = _.isUndefined(param_LeCodeIce.value)?null:param_LeCodeIce.value;
    var leCodeUfs = _.isUndefined(param_LeCodeUfs.value)?null:param_LeCodeUfs.value;

    // var username = req.swagger.params.username.value;
    var password = req.swagger.params.password.value;

    var firstname = req.swagger.params.fname.value;
    var lastname = req.swagger.params.lname.value;
    var nama_toko = req.swagger.params.nama_toko.value;
    var mobnum = req.swagger.params.mobnum.value;
    var dob = req.swagger.params.dob.value;
    var pob = req.swagger.params.pob.value;
    var ktp = req.swagger.params.ktp.value;
    var email = req.swagger.params.email.value;
    var reference = _.isUndefined(param_reference.value)?null:param_reference.value;
    var onbehalf = _.isUndefined(param_onbehalf.value)?null:param_onbehalf.value;
    var pin = _.isUndefined(param_pin.value)?null:param_pin.value;
    var no_rekening = _.isUndefined(param_no_rekening.value)?null:param_no_rekening.value;
    var imei = _.isUndefined(param_imei.value)?null:param_imei.value;

    var distributorIdFromRetailer = '';

    // username restrictions
    const reserved_usernames = ['self','internal','admin','superuser','superadmin','unilever','valdo','itprovent','dev','qa'];
    const username_minimum_length = 6;

    // log.debug('username:', username);
    log.debug('password:', password);
    log.debug('usertype:', usertype);
    log.debug('mobnum:', mobnum);
    log.debug('email:', email);
    log.debug('outletCode:', outletCode);
    log.debug('pin:', pin);

    const token = req.swagger.params.token.value;
    const acl = acl_register_user;
    const requested_resource = {usertype:usertype};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;

        // check if using test usernames
        // if (test_usernames.indexOf(username) >= 0) {
        //     const msg = sprintf('username %1$s is already taken.', username);
        //     throw new UserRegistrationError(msg);
        // }

        // certain usertypes are allowed for certain certain principal roles.
        var is_authorized = service_auth.canRegisterUserType({principal:principal, usertype:usertype});
        if (! is_authorized) {
            const msg = sprintf('unauthorized to create user with type %1$s', usertype);
            throw new UserRegistrationError(msg);
        }
        // if (reserved_usernames.indexOf(username) >= 0) {
        //     const msg = sprintf('username %1$s is already taken.', username);
        //     throw new UserRegistrationError(msg);
        // }
        // if (username.length < username_minimum_length) {
        //     const msg = sprintf('username minimum length is %1$d', username_minimum_length);
        //     throw new UserRegistrationError(msg);
        // }

        if (! usertypes.isValidType(usertype)) {
            throw new UserRegistrationError('invalid user type');
        }
        // if (usertype == USER_TYPE_RETAILER && !outletCode) {
        //     throw new UserRegistrationError('type is retailer but outlet code is not provided');
        // }
        if (usertype == USER_TYPE_DISTRIBUTOR && !distributorCode) {
            throw new UserRegistrationError('type is distributor but distributor code is not provided');
        }
        if (usertype == USER_TYPE_DISTRIBUTOR_MANAGER && !distributorCode) {
            throw new UserRegistrationError('type is distributor manager but distributor code is not provided');
        }
        if (usertype == USER_TYPE_PICKUP && !pickupCode) {
            throw new UserRegistrationError('type is pickup but pickup code is not provided');
        }
        if (usertype == USER_TYPE_SALES && !salesCode) {
            throw new UserRegistrationError('type is sales but sales code is not provided');
        }
        return service_user.isExist(email);
    })
    .then(p_result => {
        var is_user_exists = p_result;
        if (is_user_exists) {
            throw new UserRegistrationError('email already exists', UsernameAlreadyExistsError.code);
        }
        // TODO: check password length
    })
    .then(p_result => {
        console.log(sprintf('checking outlet-code:%1$s and distributor-code:%2$s', outletCode, distributorCode));
        // get the retailer code / distributor code
        return (()=> {
            if (usertype == 0) {
                if(leCodeIce == null && leCodeDry == null && leCodeUfs == null)
                {
                    throw new UserRegistrationError("Le Code kosong semua.");
                }
                
                var whereQuery = {};

                if(leCodeDry)
                {
                    whereQuery['le_code_dry'] = leCodeDry;
                }

                if(leCodeIce)
                {
                    whereQuery['le_code_ice'] = leCodeIce;
                }

                if(leCodeUfs)
                {
                    whereQuery['le_code_ufs'] = leCodeUfs;
                }
                return base_repo.getOne({entity:Retailer, 
                    where:whereQuery, 
                    options:null
                }).then(retailer=>{
                    if (!retailer) {
                        throw new MoneyAccessRegisterError(sprintf('retailer with code %1$s not found', outletCode));
                    }
                    return [retailer, null, null, null];
                });
                // return base_repo.getOne({entity:Distributor, where:{distributorCode:distributorCode}, options:null}).then(distributor=>{
                //     if (!distributor) {
                //         throw new MoneyAccessRegisterError(sprintf('distributor with code %1$s not found', distributorCode));
                //     }

                    
                // });
            } else if (usertype == 1 || usertype == 3) {
                return base_repo.getOne({entity:Distributor, where:{distributorCode:distributorCode}, options:null}).then(distributor=>{
                    if (!distributor) {
                        throw new MoneyAccessRegisterError(sprintf('distributor with code '+distributor.distributorCode+' not found', distributorCode));
                    }
                    return [null, distributor, null, null];
                });
            } else if (pickupCode) {
                return base_repo.getOne({entity:Pickup, where:{pickupCode:pickupCode}, options:null}).then(pickup=>{
                    if (!pickup) {
                        throw new MoneyAccessRegisterError(sprintf('pickup with code %1$s not found', pickupCode));
                    }
                    return [null, null, pickup, null];
                });
            } else if (usertype == 4) {
                return base_repo.getOne({entity:Distributor, where:{distributorCode:distributorCode}, options:null}).then(distributor=>{
                    if (!distributor) {
                        throw new MoneyAccessRegisterError(sprintf('distributor with code %1$s not found', distributorCode));
                    }
                    //console.log('isi distributor');
                    //console.log(distributor.id);
                    return base_repo.getOne({entity:Sales, where:{salesCode:salesCode, distributorId: distributor.id}, options:null}).then(sales=>{
                        console.log('isi sales');
                        console.log(sales);
                        if (!sales) {
                            throw new MoneyAccessRegisterError(sprintf('sales with distributor code %1$s not found', distributor.distributorCode));
                        }
                        return [null, distributor, null, sales];
                    });
                });
            } else {
                return Promise.resolve([null,null,null,null]);
            }
        })()
        .then(p2_result => {
            //console.log('isi p2 result');
            //console.log(p2_result);
            var [retailer, distributor, pickup, sales] = p2_result;
            if (outletCode || distributorCode || pickupCode || salesCode) {
                if (!retailer && !distributor && !pickup && !sales) {
                    throw new MoneyAccessRegisterError('cant find retailer or distributor or pickup or sales');
                }
            }
            return p2_result;
        })
        .then(p2_result => {

            var args = {};
            args.accountnumber = no_rekening;
            args.usertype = usertype;

            if(usertype == 11 || usertype == 3 || usertype == 4 || usertype == 10 || usertype == 9 || usertype == 2 || usertype == 0) {
                return p2_result;
            } else {
                if(args.accountnumber == null || args.accountnumber == '') 
                {
                    throw new UserRegistrationError("Nomor rekening tidak boleh kosong");
                } 
                else
                {
                    return MoneyAccess.account_inquiry_name(args).then(api_response => {
                        return p2_result;
                    });
                }
            }
        });
    })
    .then(p_result => {
        var [retailer, distributor, pickup, sales] = p_result;

        // console.log('retailer and distributor: ', p_result);
        // console.log('isi retailer');
        // console.log(p_result);

        if(usertype == 0)
            distributorIdFromRetailer = retailer.distributor_id;

        var retailerId = retailer?retailer.id:null;
        var distributorId = distributor?distributor.id:null;
        var pickupId = pickup?pickup.id:null;
        var salesId = sales?sales.id:null;
        // console.log('cek pickupId');        
        // console.log(pickupId);

        if(!dob.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/))
        {
            throw new DOBInvalidFormat('Invalid format date of birth.', DOBInvalidFormat.code);
        }

        var code_role = '';

        if(usertype == 1 || usertype == 3)
            code_role = 2;
        else if(usertype == 0)
            code_role = 1;
        else if(usertype == 2)
            code_role = 301;
        else if(usertype == 2)
            code_role = 4;
        else if(usertype == 11)
            code_role = 11;
        
        var formattedValdoAccount = '';
        
        return service_user.checkVANumber({imei: imei}).then(result => {

            if(result[1].count_imei > 0) {
                return res.status(400).json({code: 4001, message: 'IMEI harus unique'});
            }

            var count_user_id = '';
            var kode_perusahaan = '01';

            if(result[0].count_user_id == null)
                count_user_id = 1;
            else
                count_user_id = result[0].count_user_id;

            var str = "" + (count_user_id+1);
            var pad = "0000000";

            if(usertype == 2){
                pad = "00000";
            } else if(usertype == 11) {
                pad = "00000";
            }

            var unique_number = pad.substring(0, pad.length - str.length) + str;

            formattedValdoAccount = code_role+kode_perusahaan+unique_number;
            // console.log('formatted');
            // console.log(formattedValdoAccount);

            // console.log('ma-register: ', p2_result);
            // var resp = p2_result;
            // var userinfo = resp.userinfo;
            // var pin = userinfo.pin;
            // var mobnum = userinfo.mobnum;

            // create record in database
            var createUserArgs = '';

            if(usertype == 0)
            {
                createUserArgs = {
                    fullname:firstname+' '+lastname,
                    nama_toko:nama_toko,
                    password: password,
                    password_changed: 'false',
                    email:email,
                    pin:pin,
                    type:usertype,
                    valdoAccount:formattedValdoAccount,
                    no_rekening:no_rekening,
                    reference:reference,
                    phone: mobnum,
                    dob: dob,
                    pob: pob,
                    ktp: ktp,
                    onbehalf:onbehalf,
                    retailerId:retailerId,
                    distributorId:distributorIdFromRetailer,
                    pickupId:pickupId,
                    salesId:salesId,
                    imei:imei
                };
            }
            else if(usertype == 3)
            {
                createUserArgs = {
                    fullname:firstname+' '+lastname,
                    nama_toko:nama_toko,
                    password: password,
                    password_changed: 'false',
                    email:email,
                    pin:pin,
                    type:usertype,
                    valdoAccount:formattedValdoAccount,
                    no_rekening:no_rekening,
                    reference:reference,
                    phone: mobnum,
                    dob: dob,
                    pob: pob,
                    ktp: ktp,
                    onbehalf:onbehalf,
                    retailerId:retailerId,
                    distributorId:distributorIdFromRetailer,
                    pickupId:pickupId,
                    salesId:salesId,
                    imei:imei,
                    distributor_code: distributorCode
                };

                return service_user.createDistributorManager(createUserArgs)
                .then( () => {
                    res.json({code: 200, message: 'Distributor manager berhasil di tambahkan'});
                })
                .catch(DbRecordCreationError, err => {
                    console.log('db creation error:', err);
                    throw new UserRegistrationError(err.message);
                })
                .catch(DOBInvalidFormat, err => {
                    res.status(400).json({code: err.code, message:err.message});
                })
                .catch(UserRegistrationError, err => {
                    res.status(400).json({code: err.code, message:err.message});
                })
                .catch(err => {
                    console.log(err);
                    res.status(400).json({code: 0, message:'Gagal register user. ' + err.message});
                });
            }
            else if(usertype == 11)
            {
                createUserArgs = {
                    fullname:firstname+' '+lastname,
                    nama_toko:nama_toko,
                    password: password,
                    password_changed: 'false',
                    email:email,
                    pin:'',
                    type:usertype,
                    valdoAccount:"",
                    no_rekening:no_rekening,
                    reference:reference,
                    phone: mobnum,
                    dob: dob,
                    pob: pob,
                    ktp: ktp,
                    onbehalf:onbehalf,
                    retailerId:retailerId,
                    distributorId:null,
                    pickupId:pickupId,
                    salesId:salesId,
                    imei:imei
                };
            }
            else
            {
                createUserArgs = {
                    fullname:firstname+' '+lastname,
                    nama_toko:nama_toko,
                    password: password,
                    password_changed: 'false',
                    email:email,
                    pin:pin,
                    type:usertype,
                    valdoAccount:formattedValdoAccount,
                    no_rekening:no_rekening,
                    reference:reference,
                    phone: mobnum,
                    dob: dob,
                    pob: pob,
                    ktp: ktp,
                    onbehalf:onbehalf,
                    retailerId:retailerId,
                    distributorId:distributorId,
                    pickupId:pickupId,
                    salesId:salesId,
                    imei:imei
                };
            }
            
            //console.log('creating user record in db with args:', createUserArgs);

            return service_user.createUser(createUserArgs).then(entity => {

                if(usertype == 11) {
                    var out = service_formatter.format({obj:entity});
                    res.json(out);
                } else {
                    return MoneyAccess.register({mobnum:Number(formattedValdoAccount), fname:firstname, lname:lastname, email:email, pin:pin, dob:dob, pob:pob, usertype:usertype})
                    .then(data => {
                        var value = entity;
                        console.log('user db record has been created');
                        var out = service_formatter.format({obj:entity});
                        res.json(out);
                    })
                    .catch(MoneyAccessRegisterError, err => {
                        console.log('moneyaccess error:', err);
                        throw new UserRegistrationError(err.message);
                    });
                }
                
            })
            .catch(DbRecordCreationError, err => {
                console.log('db creation error:', err);
                throw new UserRegistrationError(err.message);
            });
            
        });
        
        // console.log('registering new user', mobnum);
        // generate a random pin of length greater than or equal to 6
        // var pin = Math.random().toString().slice(2,2+6);
    })
    .catch(DOBInvalidFormat, err => {
        res.status(400).json({code: err.code, message:err.message});
    })
    .catch(UserRegistrationError, err => {
        res.status(400).json({code: err.code, message:err.message});
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({code: 0, message:'Gagal register user. ' + err.message});
    });

}

function getRetailerAndDistributor(outletCode, distributorCode) {
    return new Promise((resolve, reject) => {
            const promises = [
                base_repo.getOne({entity:Retailer, where:{outletCode:outletCode}, options:null}),
                base_repo.getOne({entity:Distributor, where:{distributorCode:distributorCode}, options:null})
            ];

            Promise.all(promises)
            .then((results) => {
                const retailer = results[0];
                const distributor = results[1];
                if (retailer != null && distributor != null) {
                    resolve({
                        retailer,
                        distributor
                    })
                } else {
                    const errorMessages = [];
                    if (retailer == null) errorMessages.push(`Retailer with ${outletCode} is not available`);
                    if (distributor == null) errorMessages.push(`Distributor with ${distributorCode} is not available`);
                    reject(new Error(errorMessages.join(', ')));
                }

            }).catch((e) => {
                reject(e)
            });

    });

}

function update_user_info(req, res) {
    console.log('update_user_info');

    const param_id = req.swagger.params.id;
    var id = _.isUndefined(param_id.value)?null:param_id.value;

    var param_LeCodeDry = req.swagger.params.le_code_dry;
    var param_LeCodeIce = req.swagger.params.le_code_ice;
    var param_LeCodeUfs = req.swagger.params.le_code_ufs;
    var param_distributorCode = req.swagger.params.dt_code;
    var param_pickupCode = req.swagger.params.pickup_code;
    var param_salesCode = req.swagger.params.sales_code;

    var leCodeDry = _.isUndefined(param_LeCodeDry.value)?null:param_LeCodeDry.value;
    var leCodeIce = _.isUndefined(param_LeCodeIce.value)?null:param_LeCodeIce.value;
    var leCodeUfs = _.isUndefined(param_LeCodeUfs.value)?null:param_LeCodeUfs.value;
    var distributorCode = _.isUndefined(param_distributorCode.value)?null:param_distributorCode.value;
    var pickupCode = _.isUndefined(param_pickupCode.value)?null:param_pickupCode.value;
    var salesCode = _.isUndefined(param_salesCode.value)?null:param_salesCode.value;

    var firstname = req.swagger.params.fname.value;
    var lastname = req.swagger.params.lname.value;
    var nama_toko = req.swagger.params.nama_toko.value;
    var email = req.swagger.params.email.value;
    var mobnum = req.swagger.params.mobnum.value;

    const token = req.swagger.params.token.value;
    const acl = acl_update_user_info;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource});
    p = p.then(p_result => {
        const [principal, is_allowed] = p_result;

        if (id) {
            // check if allowed to override username parameter.
            var acl_override_username = acl_register_user;
            var is_allow_override = acl_override_username.isAllowed(principal);
            if (! is_allow_override) {
                throw new UnauthorizedRequestError();
            }

            // get the user
            base_repo.getOne({entity:User, where:{id:id}, options:{throwIfEmpty:true}})
            .then(user => {
                return {principal:principal, user:user};
            });
        }

        if (! principal.user) {
            throw new Error('no associated user with this principal');
        }

        // the target user we want to update is the same as the principal user (token bearer)
        return {principal:principal, user:principal.user};

    }).then(p_result => {
        const {principal:principal, user:user} = p_result;

        console.log('isi user');
        console.log(user);
        // perform db updates
        if(user.type == 0) {
            User.update(
                { 
                    fullname: firstname+" "+lastname, 
                    email: email,
                    phone: mobnum,
                    nama_toko: nama_toko
                },
                { 
                    where: 
                    { 
                        id: id 
                    }
                }
            );

            if(leCodeDry != null)
            {
                Retailer.update(
                    { 
                        le_code_dry: leCodeDry, 
                    },
                    { 
                        where: 
                        { 
                            id: user.retailerId 
                        }
                    }
                );
            }

            if(leCodeIce != null)
            {
                Retailer.update(
                    { 
                        le_code_ice: leCodeIce, 
                    },
                    { 
                        where: 
                        { 
                            id: user.retailerId 
                        }
                    }
                );
            }

            if(leCodeUfs != null)
            {
                Retailer.update(
                    { 
                        le_code_ufs: leCodeUfs, 
                    },
                    { 
                        where: 
                        { 
                            id: user.retailerId 
                        }
                    }
                );
            }

            return "success retailer";
        }
        else if(user.type == 1) {
            User.update(
                { 
                    fullname: firstname+" "+lastname, 
                    email: email,
                    phone: mobnum
                },
                { 
                    where: 
                    { 
                        id: id 
                    }
                }
            );

            if(distributorCode != null)
            {
                Distributor.update(
                    { 
                        distributorCode: distributorCode
                    },
                    { 
                        where: 
                        { 
                            id: user.distributorId 
                        }
                    }
                );
            }

            return "success distributor";
        }
        else if(user.type == 2) {
            User.update(
                { 
                    fullname: firstname+" "+lastname, 
                    email: email,
                    phone: mobnum
                },
                { 
                    where: 
                    { 
                        id: id 
                    }
                }
            );

            Pickup.update(
                { 
                    pickupCode: pickupCode
                },
                { 
                    where: 
                    { 
                        id: user.pickupId 
                    }
                }
            );

            return "success pickup";
        }
        else if(user.type == 4) { 

            User.update(
                { 
                    fullname: firstname+" "+lastname, 
                    email: email,
                    phone: mobnum
                },
                { 
                    where: 
                    { 
                        id: id 
                    }
                }
            );

            Sales.update(
                { 
                    salesCode: salesCode
                },
                { 
                    where: 
                    { 
                        id: user.salesId 
                    }
                }
            );

            return "success sales";
        }
        else {
            throw new UnauthorizedRequestError();
        }
    })
    .then(p_result => {
        // const changed_fields = p_result;
        // var message = sprintf('Profile user berhasil di ubah.', changed_fields.join(','));
        res.json({code: 201, message: 'Profile user berhasil diubah.'});
    });
    p = common_handler.exceptions(p, req, res)
    .catch(ChangePinError, err => {
        res.status(400).json({code: err.code, message:err.message});
    })
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'failed to update user info.' + err});
    });

    return p;
}

function reset_userpin(req, res) {
    console.log('reset_userpin');

    var token = req.swagger.params.token.value;

    const acl = acl_non_internal;
    const requested_resource = {};

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource});
    p = p.then(p_result => {
        const [principal, is_allowed] = p_result;
        var principal_user = principal.user;
        var username = principal_user.username;
        console.log('getting user with username:', username);
        return base_repo.getOne({ entity:User, where:{username:username}, options:{throwIfEmpty:true} }).then(user => {
            return [principal, user];
        });
    }).then(p_result => {
       const [principal, user] = p_result;
       if (!user) {
           throw new ResourceNotFoundError('cant find user with the associated token');
       }
       var changed_fields = [];
       var vaccount = user.valdoAccount;
       return MoneyAccess.resetpin({vaccount:vaccount})
       .then(p2_result => {
           console.log('ma-resetpin: ', p2_result);
           var resp = p2_result;
           var userinfo = resp.userinfo;
           var new_pin = userinfo.new_pin;

           // perform db updates
           console.log('updating user pin');

           return User.update(
              { pin: new_pin },
              {
                   fields: ['pin'],
                   where: {username: user.username},
                   logging: console.log,
                   validate: true,
                   limit: 1,
              }
           ).then(p_result => {
               const [affectedCount, affectedRows] = p_result;
               console.log('affectedCount', affectedCount);
               if (affectedCount > 0) {
                    return {new_pin: new_pin};
               }
               throw new ChangePinError('');
           });
       });
    })
    .then(p_result => {
        var info = p_result;
        var message = sprintf('user pin has been reset');
        res.json(info);
    });
    p = common_handler.exceptions(p, req, res)
//    .catch(ResourceNotFoundError, err => {
//        res.status(400).json({code: err.code, message:err.message});
//    })
    .catch(ChangePinError, err => {
        res.status(400).json({code: err.code, message:err.message});
    })
    .catch(err => {
        console.log('generic error: ', err);
        var err_message = err.message || '';
        res.status(400).json( {code: 0, message:'failed to update user info. ' + err_message} );
    });

    return p;
}

export { signin }
export { logout }
export { caller_logout }
export { register_user }
export { get_one_user }
export { get_one_user_by_vaccount }
export { get_users }
export { update_user_info }
export { reset_userpin }
export { get_user_types }
export { add_bank_account }
export { get_user_bank_account }
export { update_bank_account }
export { update_bank_account_android }
export { destroy_bank_account }
export { user_changepassword }
export { user_resetpassword }
export { user_changepin }
export { user_resetpin }
export { user_changeimei }
export { google_auth }

// const promises = splitted_outlet_code.map((code, index) => getRetailerAndDistributor(splitted_outlet_code[index], splitted_dt_code[index]));
// Promise.all(promises)
// .then((results) => {
//     console.log('isi retailer');
//     console.log(results[0].retailer);

//     console.log('isi distributor');
//     console.log(results[0].distributor);
// })
// .catch(DOBInvalidFormat, err => {
//     res.status(400).json({code: err.code, message:err.message});
// })
// .catch(UserRegistrationError, err => {
//     res.status(400).json({code: err.code, message:err.message});
// })
// .catch(err => {
//     console.log(err);
//     res.status(400).json({code: 0, message:'Gagal register user. ' + err});
// });

function cara_baru_register_user(req, res) {

    log.debug('register_user');

    var param_usertype = req.swagger.params.usertype;
    var param_outletCode = req.swagger.params.outlet_code;
    var param_distributorCode = req.swagger.params.dt_code;
    var param_pickupCode = req.swagger.params.pickup_code;

    var param_pin = req.swagger.params.pin;

    var param_reference = req.swagger.params.reference;
    var param_onbehalf = req.swagger.params.onbehalf;
    var param_no_rekening = req.swagger.params.no_rekening;

    var usertype = _.isUndefined(param_usertype.value)?USER_TYPE_RETAILER:param_usertype.value; // default is retailer
    var outletCode = _.isUndefined(param_outletCode.value)?null:param_outletCode.value;
    var distributorCode = _.isUndefined(param_distributorCode.value)?null:param_distributorCode.value;
    var pickupCode = _.isUndefined(param_pickupCode.value)?null:param_pickupCode.value;

    var username = req.swagger.params.username.value;
    var password = req.swagger.params.password.value;

    var firstname = req.swagger.params.fname.value;
    var lastname = req.swagger.params.lname.value;
    var mobnum = req.swagger.params.mobnum.value;
    var dob = req.swagger.params.dob.value;
    var pob = req.swagger.params.pob.value;
    var email = req.swagger.params.email.value;
    var reference = _.isUndefined(param_reference.value)?null:param_reference.value;
    var onbehalf = _.isUndefined(param_onbehalf.value)?null:param_onbehalf.value;
    var pin = _.isUndefined(param_pin.value)?null:param_pin.value;
    var no_rekening = _.isUndefined(param_no_rekening.value)?null:param_no_rekening.value;

    // username restrictions
    const reserved_usernames = ['self','internal','admin','superuser','superadmin','unilever','valdo','itprovent','dev','qa'];
    const username_minimum_length = 6;

    log.debug('username:', username);
    log.debug('password:', password);
    log.debug('usertype:', usertype);
    log.debug('mobnum:', mobnum);
    log.debug('email:', email);
    log.debug('outletCode:', outletCode);
    log.debug('pin:', pin);

    const token = req.swagger.params.token.value;
    const acl = acl_register_user;
    const requested_resource = {usertype:usertype};

    let splitted_outlet_code = outletCode.split(',');
    let splitted_dt_code = distributorCode.split(',');

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;

        // check if using test usernames
        if (test_usernames.indexOf(username) >= 0) {
            const msg = sprintf('username %1$s is already taken.', username);
            throw new UserRegistrationError(msg);
        }

        // certain usertypes are allowed for certain certain principal roles.
        var is_authorized = service_auth.canRegisterUserType({principal:principal, usertype:usertype});
        if (! is_authorized) {
            const msg = sprintf('unauthorized to create user with type %1$s', usertype);
            throw new UserRegistrationError(msg);
        }
        if (reserved_usernames.indexOf(username) >= 0) {
            const msg = sprintf('username %1$s is already taken.', username);
            throw new UserRegistrationError(msg);
        }
        if (username.length < username_minimum_length) {
            const msg = sprintf('username minimum length is %1$d', username_minimum_length);
            throw new UserRegistrationError(msg);
        }
        if (! usertypes.isValidType(usertype)) {
            throw new UserRegistrationError('invalid user type');
        }
        if (usertype == USER_TYPE_RETAILER && !outletCode) {
            throw new UserRegistrationError('type is retailer but outlet code is not provided');
        }
        if (usertype == USER_TYPE_DISTRIBUTOR && !distributorCode) {
            throw new UserRegistrationError('type is distributor but distributor code is not provided');
        }
        if (usertype == USER_TYPE_DISTRIBUTOR_MANAGER && !distributorCode) {
            throw new UserRegistrationError('type is distributor manager but distributor code is not provided');
        }
        if (usertype == USER_TYPE_PICKUP && !pickupCode) {
            throw new UserRegistrationError('type is pickup but pickup code is not provided');
        }
        return service_user.isExist(username);
    })
    .then(p_result => {
        var is_user_exists = p_result;
        if (is_user_exists) {
            throw new UserRegistrationError('username already exists', UsernameAlreadyExistsError.code);
        }
        // TODO: check password length
    })
    .then(p_result => {
        console.log(sprintf('checking outlet-code:%1$s and distributor-code:%2$s', outletCode, distributorCode));
        // get the retailer code / distributor code
        return (()=> {
            if(usertype == 0)
            {
                var checkedValdoAccount = [];
                var user_id;
                if(splitted_outlet_code.length != splitted_dt_code.length)
                {
                    throw new UserRegistrationError('Kode Retailer / Kode Distributor ada yang kosong.');
                }
                else  {
                    var createUserArgs = {
                        username:username,
                        fullname:firstname+' '+lastname,
                        password: password,
                        email:email,
                        pin:pin,
                        type:usertype,
                        no_rekening:no_rekening,
                        reference:reference,
                        onbehalf:onbehalf
                    };
                    return Promise.each(splitted_outlet_code, function(value, index) {
                        
                        return new Promise((resolve, reject) => {
                            console.log('isi outlet code');
                            console.log(splitted_outlet_code[index]);
                            const promises = [
                                base_repo.getOne({entity:Retailer, where:{outletCode:splitted_outlet_code[index].replace(" ", "")}, options:null}),
                                base_repo.getOne({entity:Distributor, where:{distributorCode:splitted_dt_code[index].replace(" ", "")}, options:null})
                            ];

                            Promise.all(promises)
                            .then((results) => {
                                const retailer = results[0];
                                const distributor = results[1];
                                let tampungOutletCode = [];
                                let tampungOutletCodeNull = [];
                                if (retailer != null && distributor != null) {
                                    resolve({
                                        retailer,
                                        distributor
                                    })
                                } else {
                                    const errorMessages = [];
                                    console.log('isi retailer');

                                    if (retailer == null) errorMessages.push(`Retailer with ${splitted_outlet_code[index]} is not available`);
                                    if (distributor == null) errorMessages.push(`Distributor with ${splitted_dt_code[index]} is not available`);

                                    // if (retailer == null) errorMessages.push('Retailer with '+splitted_outlet_code[index]+' is not available');
                                    // if (distributor == null) errorMessages.push('Distributor with '+splitted_dt_code[index]+' is not available');
                                    reject(new Error(errorMessages.join(', ')));
                                }

                            })
                            .then( () => {
                                return service_user.createUser(createUserArgs).then(entity => {
                                    console.log('isi entity');
                                    console.log(entity);
                                    var fin_valdo_account = [];
                                    user_id = entity.dataValues.id;
                                    return Promise.each(splitted_outlet_code, function(value, index){
                                        var code=value;
                                        var code_role='1';
                                        var sequential_code='0';

                                        // if(splitted_outlet_code[i] != null)
                                        //     code = splitted_outlet_code[i];
                                        
                                        var formattedValdoAccount = code_role+'1'+sequential_code+code;

                                        var createCheckVANumber = {
                                            code_role:code_role,
                                            dry_code:'1',
                                            sequential_code:sequential_code,
                                            code:code
                                        };

                                        return service_user.checkVANumber(createCheckVANumber).then(count_checkedVA => {
                                            sequential_code = count_checkedVA;
                                            if(sequential_code > 9)
                                            {
                                                throw new UserRegistrationError('Retailer sudah digunakan 9 kali.');
                                            }
                                            checkedValdoAccount.push(count_checkedVA);
                                            
                                        });

                                    })
                                })
                                .then(() => {
                                    console.log('isi checked valdo account');
                                    console.log(checkedValdoAccount);
                                    var promiseAllData = [];
                                    for (var i = 0; i < splitted_outlet_code.length; i++) {
                                        var code_role='1';
                                        var sequential_code='0';    
                                        var formattedValdoAccount = code_role+'1'+checkedValdoAccount[i]+splitted_outlet_code[i];
                                        var data = {
                                                user_id: user_id,
                                                outlet_code: splitted_outlet_code[i].replace(" ", ""),
                                                dt_code: splitted_dt_code[i].replace(" ", ""),
                                                valdo_account: formattedValdoAccount.replace(" ", "")
                                            };
                                            console.log('PROMISE ALL');
                                            console.log(data);
                                        promiseAllData.push(
                                            //create
                                            service_user.createUserRetailerCode(data)

                                        );
                                        
                                    }
                                    return Promise.all(promiseAllData);
                                })
                            })
                            .catch((e) => {
                                reject(e)
                            })
                            .catch(MoneyAccessRegisterError, err => {
                                console.log('moneyaccess error:', err);
                                throw new UserRegistrationError(err.message);
                            });
                        });
                    })

                    
                       /* .then(() => {
                            return MoneyAccess.register({mobnum:Number(formattedValdoAccount), fname:firstname, lname:lastname, email:email, pin:pin, dob:dob, pob:pob, usertype:usertype})
                        */
                        //})
                    .then(data => {
                        // var value = entity;
                        // console.log('user db record has been created');
                        // var out = service_formatter.format({obj:entity});
                        res.json({code: 200, 'message': 'register berhasil'});
                    })
                    .catch(MoneyAccessRegisterError, err => {
                        console.log('moneyaccess error:', err);
                        throw new UserRegistrationError(err.message);
                    });
                }
            }
            else if (usertype == 1) {
                return base_repo.getOne({entity:Distributor, where:{distributorCode:distributorCode}, options:null}).then(distributor=>{
                    if (!distributor) {
                        throw new MoneyAccessRegisterError(sprintf('distributor with code %1$s not found', distributorCode));
                    }
                    return [null, distributor, null];
                });
            } else if (pickupCode) {
                return base_repo.getOne({entity:Pickup, where:{pickupCode:pickupCode}, options:null}).then(pickup=>{
                    if (!pickup) {
                        throw new MoneyAccessRegisterError(sprintf('pickup with code %1$s not found', pickupCode));
                    }
                    return [null, null, pickup];
                });
            } else {
                return Promise.resolve([null,null]);
            }
        })()
        // .then(p2_result => {
        //     var [retailer, distributor, pickup] = p2_result;
        //     if (outletCode || distributorCode || pickupCode) {
        //         if (!retailer && !distributor && !pickup) {
        //             throw new MoneyAccessRegisterError('cant find retailer or distributor or pickup');
        //         }
        //     }
        //     return p2_result;
        // });
    })
    // .then(p_result => {
    //     var [retailer, distributor, pickup] = p_result;

    //     // console.log('retailer and distributor: ', p_result);
    //     console.log('isi retailer');
    //     console.log(retailer);
    //     var retailerId = retailer?retailer.id:null;
    //     var distributorId = distributor?distributor.id:null;
    //     var pickupId = pickup?pickup.id:null;
    //     console.log('cek pickupId');        
    //     console.log(pickupId);

    //     if(!dob.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/))
    //     {
    //         throw new DOBInvalidFormat('Invalid format date of birth.', DOBInvalidFormat.code);
    //     }

    //     var code;
    //     var code_role;
    //     var sequential_code='0';

    //     if(outletCode != null)
    //         code = outletCode;
    //     else if(distributorCode != null)
    //         code = distributorCode;

    //     if(usertype == 1 || usertype == 3)
    //         code_role = 2;
    //     else if(usertype == 0)
    //         code_role = 1;
    //     else if(usertype == 2)
    //     {
    //         code_role = 3;
    //         code = pickupCode;
    //     }
    //     if(code === undefined)
    //         code = '';
        
    //     var createCheckVANumber = {
    //         code_role:code_role,
    //         dry_code:'1',
    //         sequential_code:sequential_code,
    //         code:code
    //     };
    //     var formattedValdoAccount = code_role+'1'+sequential_code+code;
    //     return service_user.checkVANumber(createCheckVANumber).then(entity => {
    //         sequential_code = entity;
    //         if(sequential_code > 9)
    //         {
    //             throw new UserRegistrationError('Retailer sudah digunakan 9 kali.');
    //         }

    //         if(usertype == 3)
    //         {
    //             //formatted valdo account isi nya distributor code disini
    //             formattedValdoAccount = code;
    //             var createUserArgs = {
    //                 username:username,
    //                 fullname:firstname+' '+lastname,
    //                 password: password,
    //                 email:email,
    //                 pin:pin,
    //                 type:usertype,
    //                 valdoAccount:formattedValdoAccount,
    //                 no_rekening:no_rekening,
    //                 reference:reference,
    //                 onbehalf:onbehalf,
    //                 retailerId:retailerId,
    //                 distributorId:distributorId,
    //                 pickupId:pickupId
    //             };
                
    //             console.log('creating user record in db with args:', createUserArgs);

    //             return service_user.createDistributorManager(createUserArgs).then(entity => {
    //                 var value = entity;
    //                 console.log('user db record has been created');
    //                 var out = service_formatter.format({obj:entity});
    //                 res.json(out);
    //             })
    //         }
    //         else
    //         {
    //             formattedValdoAccount = code_role+'1'+sequential_code+code;
    //             // console.log('formatted');
    //             // console.log(formattedValdoAccount);
    //             var sequential_code

    //             // console.log('ma-register: ', p2_result);
    //             // var resp = p2_result;
    //             // var userinfo = resp.userinfo;
    //             // var pin = userinfo.pin;
    //             // var mobnum = userinfo.mobnum;

    //             // create record in database

    //             var createUserArgs = {
    //                 username:username,
    //                 fullname:firstname+' '+lastname,
    //                 password: password,
    //                 email:email,
    //                 pin:pin,
    //                 type:usertype,
    //                 distributor_code: code,
    //                 valdoAccount:formattedValdoAccount,
    //                 no_rekening:no_rekening,
    //                 reference:reference,
    //                 onbehalf:onbehalf,
    //                 retailerId:retailerId,
    //                 distributorId:distributorId,
    //                 pickupId:pickupId
    //             };
                
    //             console.log('creating user record in db with args:', createUserArgs);

    //             return service_user.createUser(createUserArgs).then(entity => {
    //                 console.log('isi entity');
    //                 console.log(entity);
    //                 return MoneyAccess.register({mobnum:Number(formattedValdoAccount), fname:firstname, lname:lastname, email:email, pin:pin, dob:dob, pob:pob, usertype:usertype})
    //                 .then(data => {
    //                     var value = entity;
    //                     console.log('user db record has been created');
    //                     var out = service_formatter.format({obj:entity});
    //                     res.json(out);
    //                     })
    //                     .catch(MoneyAccessRegisterError, err => {
    //                         console.log('moneyaccess error:', err);
    //                         throw new UserRegistrationError(err.message);
    //                     });
    //             })
    //             .catch(DbRecordCreationError, err => {
    //                 console.log('db creation error:', err);
    //                 throw new UserRegistrationError(err.message);
    //             });
    //         }
    //     });
        
    //     // console.log('registering new user', mobnum);
    //     // generate a random pin of length greater than or equal to 6
    //     // var pin = Math.random().toString().slice(2,2+6);
    // })
    .catch(DOBInvalidFormat, err => {
        res.status(400).json({code: err.code, message:err.message});
    })
    .catch(UserRegistrationError, err => {
        res.status(400).json({code: err.code, message:err.message});
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({code: 0, message:'Gagal register user. ' + err});
    });

}

function google_auth(req, res) {
    console.log('google_auth');
    const token = req.swagger.params.token.value;
    const idToken = req.swagger.params.idToken.value;//android phone id
    const acl = acl_get_one_user;
    const requested_resource = {};
    var auth = new GoogleAuth;
    var CLIENT_ID = '873951639119-r16mjm4c927qr5tgmfqocafg2g2frcq3.apps.googleusercontent.com' /*google id client*/
    var client = new auth.OAuth2(CLIENT_ID, '', ''); /*get client from google id client*/

    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then((p_result) => {
        const [principal, is_allowed] = p_result;
        
        return new Promise((resolve, reject)=> {
            //Google verify id token
            client.verifyIdToken(
                idToken,
                CLIENT_ID,
                // Or, if multiple clients access the backend:
                //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3],
                (e, login) => {
                    if (e) { // jika id token tidak verify signin failed
                        reject(e);
                    } else{
                        var payload = login.getPayload();
                        return service_user.getOne({ 'email': payload['email'] }) //Check apakah user sudah terdaftar di database (filter by email)
                        .then(user => {
                            if (!user) {
                                resolve([null, payload]); //Jika user belum terdaftar di database
                            } else {
                                resolve([user, payload]); //Jika user sudah terdaftar di database
                            }
                        })
                    }                
                }
            )
        })
        .then(([user, gaccount]) => {
            // Jika user sudah terdaftar di database (Login sukes) Response (token & user_id)
            if (user) {
                var is_check_token_expiry = true;
                if (is_check_token_expiry) {
                    var delta_days_in_seconds = 259200; // 3 days
                    const expiry_ts = user.accessTokenExpiry; // expiry is seconds
                    const now_seconds = new Date().getTime() / 1000;
                    if (now_seconds > expiry_ts - delta_days_in_seconds) {
                        // renew access token and update db
                        return service_user.renewAccessToken({user:user}).then((new_access_token)=>{
                            var out = {token: new_access_token, user_id: user.id };
                            res.json(out);
                        });
                    }
                }
                var out = {token: user.accessToken, user_id: user.id};
                res.json(out);
            } else { 
                // tell client to register, with given email and name
                var fname = gaccount['name'];
                var names = fname.split(/\s/gi);                
                var lname = "";
                if (names.length > 1) {
                    fname = names[0];
                    names.shift();
                    lname = names.join(" ");
                }
                var out = {
                    mustRegister: true,
                    email: gaccount['email'],
                    fname: fname,
                    lname: lname
                };
                res.json(out);
            }
        })
    })
    .catch((error) => {
        console.log(error)
        // cant sign in, please re sign in
        res.status(400).json({code: 0, message:'signin failed ' + error});
    })
}
import util from 'util'
import _ from 'lodash'
import { sprintf } from "sprintf-js"
import queryString from 'query-string'
import Promise from 'bluebird'
import moment from 'moment'

// money-access errors
import { MoneyAccessRegisterError } from '@/moneyaccess/error/index'

import { DbRecordCreationError } from '@/server/error/Error'
import { UserRegistrationError } from '@/server/error/Error'
import { DOBInvalidFormat } from '@/server/error/Error'
import { UsernameAlreadyExistsError } from '@/server/error/Error'
import { UserNotFoundError } from '@/server/error/Error'
import { SigninError } from '@/server/error/Error'
import { ApiTokenInvalidError } from '@/server/error/Error'
import { AccessTokenExpiredError } from '@/server/error/Error'
import { UnauthorizedRequestError } from '@/server/error/Error'
import { ChangePinError } from '@/server/error/Error'
import { ResourceNotFoundError } from '@/server/error/Error'
import { ChangePasswordError } from '@/server/error/Error'

import ACL from '@/models/acl'
import { factory as acl_factory } from '@/models/acl'
import { AclDefault } from '@/models/acl'
import { Retailer } from '@/models/orm/index'
import { Distributor } from '@/models/orm/index'
import { Logs } from '@/models/orm/index'
import { Pickup } from '@/models/orm/index'
import { User } from '@/models/orm/index'
import { UserQuery } from '@/models/orm/user'

import api_ppob from '@/moneyaccess/api_ppob'
import base_repo from '@/server/repositories/base'
import service_logger from '@/server/services/logger'
import service_user from '@/server/services/user'
import service_bank_account from '@/server/services/bankaccount'
import service_token from '@/server/services/token'
import service_auth from '@/server/services/auth'
import service_formatter from '@/server/services/formatter'
import service_informasi from '@/server/services/informasi'
import service_notification from '@/server/services/notification'
import service_call_center from '@/server/services/call_center'
import { MoneyAccess } from '@/moneyaccess/index'

import common_handler from '@/server/api/controllers/common'
import common_util from '@/server/utils/util'

const acl_ppob = acl_factory.getDefault().addRoles(['all']);

const log = service_logger.getLogger({name: 'controller ppob', service:'ppob'});

function get_ppob_purchase_check_price(req, res)
{
    const log_action = "get_ppob_purchase_check_price";

    const token = req.swagger.params.token.value;
    const acl = acl_ppob;
    const requested_resource = {};
    const param_type = req.swagger.params.type;
    const param_no = req.swagger.params.no;
    const param_provider = req.swagger.params.provider;

    var type = _.isUndefined(param_type.value)?'':param_type.value;
    var provider = _.isUndefined(param_provider.value)?'':param_provider.value;
    var no = _.isUndefined(param_no.value)?'':param_no.value;

    Logs.create({action: log_action, description: "param req type: "+type+", no: "+no});
    
    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;

        if(type == 'pulsa') {
            type = type.trim().toLowerCase();
            no = no.trim().toLowerCase();

            no = no.replace(/^\+62/, '0');

            let operators = {
                TELKOMSEL: ["0812","0813","0821","0822","0852","0853","0823","0851"],
                INDOSAT: ["0814","0815","0816","0855","0856","0857","0858"],
                THREE: ["0895","0896","0897","0898","0899"],
                SMARTFREN: ["0881","0882","0883","0884","0885","0886","0887","0888","0889"],
                XL: ["0817","0818","0819","0859","0877","0878"],
                AXIS: ["0838","0831","0832","0833"]
            };

            let no_hp = no.slice(0, 4);

            _.find(operators, (operator, operator_name) => {
                if(_.includes(operator, no_hp)) {
                    provider = operator_name;
                    return true;
                }
            });
        }

        return api_ppob.ppob_check_price({product_type: type, provider: provider}).then(api_response => {
            return api_response;
        })
        .then(api_response => {
            let results = api_response.map( (result, index) => {
                
                let _result = {};
                
                _result.id = index;
                _result.provider = provider;
                _result.id_number = no;
                _result.type = type;
                _result.product_code = result.product_code;
                _result.product_name = result.product_name;

                _result.product_price = Number(result.product_price);
                _result.status = result.status == 'open';

                _result.cashback_customer = Number(result.cashback_customer);
                _result.fee_tpn = Number(result.fee_tpn);

                return _result;
            });

            res.json({results: results});
        });
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'Gagal request get_ppob_purchase_check_price.' + err});
    });
}

function ppob_product_type_and_provider(req, res)
{
    const param_type = req.swagger.params.type;
    var type = _.isUndefined(param_type.value)?'':param_type.value;
    
    var obj = {};

    if(type == 'pulsa')
        obj.providers = ['TELKOMSEL', 'INDOSAT', 'THREE', 'SMARTFREN', 'XL', 'AXIS'];
    else if(type == 'data')
        obj.providers = ['BOLT', 'AXIS', 'XL', 'TELKOMSEL', 'INDOSAT', 'THREE', 'SMARTFREN&apos;'];
    else if(type == 'game')
        obj.providers = ['ZYNGA', 'VIWAWA', 'CHERRY CREDIT', 'ULTIMATE ASCENT', 'TRAVIAN GAME', 'TERACORD', 'GAME STEAM', 'GEMSCOOL', 'MEGAXUS', 'LYTO', 'GARENA', 'DIGICASH', 'MOBILE LEGEND'];
    else if(type == 'voucher')
        obj.providers = ['ITUNES GIFCARD', 'UNIPIN', 'GOOGLE PLAY'];
    else if(type == 'pln_prabayar')
        obj.providers = ['20.000', '50.000', '100.000', '200.000', '500.000', '1.000.000', '5.000.000'];

    res.json(obj);
}

function post_ppob_inquiry(req, res)
{
    const log_action = "post_ppob_inquiry_service";

    const token = req.swagger.params.token.value;
    const acl = acl_ppob;
    const requested_resource = {};
    const param_type = req.swagger.params.type;
    const param_product_code = req.swagger.params.product_code;
    const param_no = req.swagger.params.no;
    const param_data = req.swagger.params.data;

    var type = _.isUndefined(param_type.value)?'':param_type.value;
    var product_code = _.isUndefined(param_product_code.value)?'':param_product_code.value;
    var no = _.isUndefined(param_no.value)?'':param_no.value;
    var data = _.isUndefined(param_data.value)?'':param_data.value;

    Logs.create({action: log_action, description: `param req type: ${type}, product_code: ${product_code}, no: ${no}, data: ${data}`});
    
    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        let cus1, cus2, cus3, misc = '';

        switch(type) {
            case 'telkom':
                cus1 = data;
                cus2 = no;
                break;
            case 'pln_prabayar':
                cus1 = no;
                misc = Number(data.replace(/\D/gi, ""));
                break;
            default:
                cus1 = no;
                break;
        }

        return api_ppob.ppob_inquiry({product_code: product_code, customer_number1:cus1, customer_number2:cus2, customer_number3: cus3, misc:misc}).then(api_response => {
            return api_response;
        })
        .then(api_response => {
            api_response.Nominal = Number(api_response.Nominal);
            api_response.AdminCharge = Number(api_response.AdminCharge);
            res.json({result: api_response});
        });
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'Gagal mendapatkan data inquiry, ' + err.message});
    });
}

function post_ppob_payment_service(req, res)
{
    const log_action = "post_ppob_payment_service";

    const token = req.swagger.params.token.value;
    const acl = acl_ppob;
    const requested_resource = {};
    const param_type = req.swagger.params.type;
    const param_product_code = req.swagger.params.product_code;
    const param_no = req.swagger.params.no;
    const param_data = req.swagger.params.data;
    const param_nominal = req.swagger.params.nominal;
    const param_ref_number = req.swagger.params.refnumber;
    const param_pin = req.swagger.params.pin;

    var type = _.isUndefined(param_type.value)?'':param_type.value;
    var product_code = _.isUndefined(param_product_code.value)?'':param_product_code.value;
    var no = _.isUndefined(param_no.value)?'':param_no.value;
    var data = _.isUndefined(param_data.value)?'':param_data.value;
    var nominal = _.isUndefined(param_nominal.value)?'':param_nominal.value;
    var refnumber = _.isUndefined(param_ref_number.value)?'':param_ref_number.value;
    var pin = _.isUndefined(param_pin.value)?'':param_pin.value;
    
    var p = common_handler.verify({token:token, acl:acl, resources:requested_resource})
    .then(p_result => {
        const [principal, is_allowed] = p_result;
        return p_result;
    })
    .then(p_result => {
        var [principal, is_allowed] = p_result;
        let cus1, cus2, cus3, misc = '';

        switch(type) {
            case 'telkom':
                cus1 = data;
                cus2 = no;
                break;
            case 'pln_prabayar':
                cus1 = no;
                misc = Number(data.replace(/\D/gi, ""));
                break;
            default:
                cus1 = no;
                break;
        }

        if(cus1 == undefined)
            cus1 == ' ';
        if(cus2 == undefined)
            cus2 == ' ';
        if(cus3 == undefined)
            cus3 == ' ';
        if(misc == undefined)
            misc == ' ';

        if(type == 'pulsa' || type == 'data' || type == 'game' || type == 'voucher') {

            Logs.create({action: log_action+" pulsa data game voucher", description: `param req model: ${type}, product_code: ${product_code}, customer_number1: ${cus1}, vaccount: ${principal.user.dataValues.valdoAccount}, pin: ${pin}`});

            return api_ppob.ppob_topup_service({model:type, product_code:product_code, customer_number1:cus1, vaccount:principal.user.dataValues.valdoAccount, pin:pin}).then(api_response => {
                return api_response;
            })
            .then(api_response => {
                Logs.create({action: "api response pulsa data game voucher "+log_action, description: JSON.stringify(api_response)});
                res.json({code: 200, message: 'Pembayaran berhasil'});
            });
        } else {

            Logs.create({action: log_action+" selain pulsa data game voucher", description: `param req model: ${type}, product_code: ${product_code}, customer_number1: ${cus1}, customer_number2: ${cus2}, customer_number3: ${cus3}, nominal: ${nominal}, refnumber: ${refnumber}, misc: ${misc}, vaccount: ${principal.user.dataValues.valdoAccount}, pin: ${pin}`});

            return api_ppob.ppob_payment_service({product_code:product_code, customer_number1:cus1, customer_number2:cus2, customer_number3:cus3, nominal:nominal, ref_number:refnumber, misc:misc, vaccount:principal.user.dataValues.valdoAccount, pin:pin}).then(api_response => {
                return api_response;
            })
            .then(api_response => {
                Logs.create({action: "api response selain pulsa data game voucher "+log_action, description: JSON.stringify(api_response)});
                res.json({code: 200, message: 'Pembayaran berhasil'});
            });
        }
    });
    return common_handler.exceptions(p, req, res)
    .catch(err => {
        console.log('generic error: ', err);
        res.status(400).json({code: 0, message:'Gagal mendapatkan data inquiry, ' + err.message});
    });
}

export { get_ppob_purchase_check_price }
export { ppob_product_type_and_provider }
export { post_ppob_payment_service }
export { post_ppob_inquiry }
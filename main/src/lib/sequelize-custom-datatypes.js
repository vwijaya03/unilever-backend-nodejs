import util from 'util'
import DataTypes from '@/node_modules/sequelize/lib/data-types'

/**
references:
http://stackoverflow.com/questions/29652538/sequelize-js-timestamp-not-datetime
*/

var timestampSqlFunc = function () {
    var defaultSql = 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP';
    if (this._options && this._options.notNull) {
        defaultSql += ' NOT NULL';
    }
    if (this._options && this._options.onUpdate) {
        // onUpdate logic here:
    }
    return defaultSql;
};
DataTypes.TIMESTAMP = function (options) {
    this._options = options;
    var date = new DataTypes.DATE();
    date.toSql = timestampSqlFunc.bind(this);
    if (!(this instanceof DataTypes.DATE)) return date;
    DataTypes.DATE.apply(this, arguments);
};
util.inherits(DataTypes.TIMESTAMP, DataTypes.DATE);
DataTypes.TIMESTAMP.prototype.toSql = timestampSqlFunc;

var obj = {};
obj.TIMESTAMP = DataTypes.TIMESTAMP;

export default obj

import bunyan from 'bunyan'
import service_logger from '@/server/services/logger'
import fs from 'fs'
import path from 'path'

const log = service_logger.getLogger({name: 'properties'});
const allowed_user_status = [ {'status': '0'}, {'status': '1'} ];

var ACCESS_TOKEN_WEBADMIN = '80a16ffa-a7e3-11e6-80f5-76304dec7eb7';
var ACCESS_TOKEN_INTERNAL_WEBADMIN = '0913ad38-b9e6-4e72-a859-f1f359cf7dfb';
var ACCESS_TOKEN_INTERNAL_DEV = '2b3e6865-bdae-4881-ba71-ea9c5f123e8d';

var env = process.env.NODE_ENV || 'development'

/*

Put these mapping in your /etc/hosts file:

For staging env:
valdo_wallet_retailer_host: 182.23.44.51
valdo_wallet_pickup_host: 182.23.44.51

For production env:
valdo_wallet_retailer_host: 192.168.12.227
valdo_wallet_pickup_host: 192.168.12.228

*/

var common = {
    version: '1.0.0',
    tmp_dir: '/tmp',
    public_dir: '/tmp',
    public_www_dir: '/tmp',

    server_secret:'w1thgre4tp0w3rc0m3sgr34tres0ns1b1l1ty',

    moneyaccess_apikey: 'webKEY123456789',
    moneyaccess_host: '192.168.12.227',

    internal_webadmin_access_token: ACCESS_TOKEN_INTERNAL_WEBADMIN,
    internal_dev_access_token : ACCESS_TOKEN_INTERNAL_DEV,
    webadmin_access_token: ACCESS_TOKEN_WEBADMIN
}

var database = JSON.parse(fs.readFileSync(path.join(__dirname, 'config', 'database.json')).toString());

var development = Object.assign(Object.assign({}, common), database);

// var staging = Object.assign(Object.assign({}, common), {
//     mariadb: {
//         host: 'itprovent-unilever-staging',
//         port: 3306,
//         user: 'nodebackend',
//         password: 'ku*SZa?E8$B&%FJv',
//         db: 'unilever'
//     },
//     valdo_wallet_retailer_host: 'valdo-wallet-retailer-host-staging',
//     valdo_wallet_pickup_host: 'valdo-wallet-pickup-host-staging'
// });

// var production = Object.assign(Object.assign({}, common), {
//     mariadb: {
//         host: 'itprovent-unilever-production',
//         port: 3306,
//         user: 'nodebackend',
//         password: 'exKc$bj*9Z?MbLUc',
//         db: 'unilever'
//     },
//     valdo_wallet_retailer_host: 'valdo-wallet-retailer-host-production',
//     valdo_wallet_pickup_host: 'valdo-wallter-pickup-host-production'
// });

var config = {};
if (env === 'production') {
    config = production;
} else if (env === 'staging') {
    config = staging;
} else {
    config = development;
}

log.info('env:', env);
log.info('config:', config);

export default config;

export { ACCESS_TOKEN_WEBADMIN }
export { ACCESS_TOKEN_INTERNAL_WEBADMIN }
export { ACCESS_TOKEN_INTERNAL_DEV }
export { allowed_user_status }
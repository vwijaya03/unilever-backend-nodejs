var obj = {
    1: {retailer_id:'1', total:'1000', currency:'IDR', dist_id:'1', is_paid:false, invoice_id:'invoice-1', invoice_date:'2016-09-01', due_date:'2016-10-01'},
    2: {retailer_id:'1', total:'2000', currency:'IDR', dist_id:'2', is_paid:false, invoice_id:'invoice-2', invoice_date:'2016-10-02', due_date:'2016-11-02'},
    3: {retailer_id:'2', total:'3000', currency:'IDR', dist_id:'3', is_paid:false, invoice_id:'invoice-3', invoice_date:'2016-11-03', due_date:'2016-12-03'},
    4: {retailer_id:'2', total:'4000', currency:'IDR', dist_id:'2', is_paid:false, invoice_id:'invoice-2', invoice_date:'2016-12-04', due_date:'2016-13-04'},
    5: {retailer_id:'3', total:'5000', currency:'IDR', dist_id:'3', is_paid:false, invoice_id:'invoice-3', invoice_date:'2016-13-05', due_date:'2016-14-05'}
}

export default obj
var obj = {
    1: {retailer_id:'1', date:'2016-09-01', amount:'11200', currency:'IDR', direction:0, balance:'25000', info:'bayar'},
    2: {retailer_id:'1', date:'2016-09-02', amount:'25000', currency:'IDR', direction:0, balance:'0', info:'bayar'},
    3: {retailer_id:'1', date:'2016-09-03', amount:'40000', currency:'IDR', direction:1, balance:'40000', info:'top-up'},
    4: {retailer_id:'2', date:'2016-09-04', amount:'15000', currency:'IDR', direction:0, balance:'0', info:'bayar'},
    5: {retailer_id:'2', date:'2016-09-05', amount:'80000', currency:'IDR', direction:1, balance:'80000', info:'top-up'},
    6: {retailer_id:'3', date:'2016-09-01', amount:'15000', currency:'IDR', direction:0, balance:'0', info:'bayar'},
    7: {retailer_id:'3', date:'2016-09-02', amount:'80000', currency:'IDR', direction:1, balance:'80000', info:'top-up'}
}

export default obj
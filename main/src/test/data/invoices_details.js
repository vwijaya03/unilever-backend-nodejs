var obj = {
    1: {invoice_id:'1', items:['sku1','sku2','sku3']},
    2: {invoice_id:'2', items:['sku3','sku4','sku5']},
    3: {invoice_id:'3', items:['sku6','sku7','sku8']},
    4: {invoice_id:'4', items:['sku9','sku10','sku11']},
    5: {invoice_id:'5', items:['sku12','sku13','sku14']}
}

export default obj
var obj = {
    1: {user_id:'user1', token:'user1token', name:'User1 Name', distributor_id:null, retailer_id:'1', pin:'1234', password:'pass', email:'user1@gmail.com'},
    2: {user_id:'user2', token:'user2token', name:'User2 Name', distributor_id:null, retailer_id:'2', pin:'2345', password:'pass', email:'user2@gmail.com'},
    3: {user_id:'user3', token:'user3token', name:'User3 Name', distributor_id:null, retailer_id:'3', pin:'3456', password:'pass', email:'user3@gmail.com'}
}

export default obj
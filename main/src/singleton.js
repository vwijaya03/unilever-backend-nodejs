require('rootpath')();

var exports = module.exports = {};

var client = {
    aerospike: null,
    redis: null,
    amqp: null,
    mariadb: null,
    mqtt: null
}

var connections = {
    amqp: null
}

exports.client = client;
exports.connections = connections;
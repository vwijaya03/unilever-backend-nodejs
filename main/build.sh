#!/bin/bash

CDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
NARGS=$#
ME=`basename $0`

#

ENV=$1
GRP=$2

source $ITP_UNILEVER_BACKEND/build/main/vars.sh

#

(cd $DIR && ./clean.sh $ENV $GRP)

echo "[ $TAG :: building unilever-backend for env:$ENV grp:$GRP]"
(cd $DIR/src && npm i && npm i --only=dev && npm run build)
echo ""

echo "creating node_modules symlink"
(cd $DIR/dist && sudo ln -s $DIR/src/node_modules ./node_modules)

#!/usr/bin/env bash

CDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
NARGS=$#
ME=`basename $0`

#

ENV=$1
GRP=$2
source $ITP_UNILEVER_BACKEND/build/main/vars.sh

echo "[ $TAG :: cleaning unilever-backend for env:$ENV grp:$GRP]"
(rm -fR $DIR/src/node_modules)
(cd $DIR/dist && rm -fR *)
echo ""


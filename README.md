[TOC]

# Requirements
*  NodeJs v5.11.1
*  NPM 3.8.6

# Install Libraries & Tools for Dev Environment.
* install pip
* install colorama
* install ansible
* make sure <path to scripts/tools> is included in your PATH

# Development Setup
## Initialize your dev env. put this in your .bashrc file.
```
source <path-to-codebase>/tools/init.sh
```

## Add itprovent-unilever-staging mapping in hosts file (linux: /etc/hosts)

### (for local machine)
```
182.23.44.51 valdo-wallet-retailer-host-development
182.23.44.51 valdo-waller-pickup-host-development
139.162.16.144 itprovent-unilever-development
```

### (for staging instance)
```
192.168.12.227 valdo-wallet-retailer-host-staging
192.168.12.228 valdo-wallet-pickup-host-staging
127.0.0.1 itprovent-unilever-staging
```

### (for production instance)
```
192.168.12.xxx valdo-wallet-retailer-host-production
192.168.12.xxx valdo-wallet-pickup-host-production
127.0.0.1 itprovent-unilever-production
```

# Development Flow

### running server in dev mode
```
(cd main/src && sudo NODE_ENV=development npm run dev)
```

### edit swagger.yml in real-time (in-browser) or edit swagger.yml manually
```
(cd main/src && swagger project edit)
```

### build and start swagger-ui
```
(cd tools/swagger-ui && npm i && npm i --only=dev && npm run build && npm run serve)
```

### open swagger-ui in browser to test API
```
http://localhost:8080
```

## Deploy using Fast-Dev Iteration Mode (against development env)
* Make sure nodejs app is already running on development server
* Deploy new codebase in fast-dev-iteration mode
```
(cd deploy/main && ./deploy-fast-dev.sh)
```

# Logging
Log file: /var/log/itp-unilever.json

# Swagger
## Swagger Specs
```
http://swagger.io/specification
```
# Credentials
* [Credentials](wikis/credentials.md)

# Users
* [Users](wikis/users.md)

# Secret Access Token
* Internal WebAdmin: 0913ad38-b9e6-4e72-a859-f1f359cf7dfb
* Internal Developers (please use during development only): 2b3e6865-bdae-4881-ba71-ea9c5f123e8d

# Error Codes
* [Error Codes](wikis/errors.md)

# Test
* [Main](wikis/test/main.md)

# Infrastructure
* [Ports](wikis/infra/ports.md)
* [HAProxy](wikis/infra/haproxy.md)
* [LetsEncrypt](wikis/infra/letsencrypt.md)
/*
https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql
*/

CREATE USER 'nodebackend'@'%' IDENTIFIED BY 'ku*SZa?E8$B&%FJv';
CREATE USER 'webadmin'@'%' IDENTIFIED BY 'S5z#$@93J5fCmpas';

GRANT ALL PRIVILEGES ON unilever.* TO 'nodebackend'@'%' IDENTIFIED BY 'ku*SZa?E8$B&%FJv' with grant option;
GRANT ALL PRIVILEGES ON *.* TO 'nodebackend'@'%' IDENTIFIED BY 'ku*SZa?E8$B&%FJv' with grant option;

GRANT ALL PRIVILEGES ON unilever . * TO 'webadmin'@'%' IDENTIFIED BY 'S5z#$@93J5fCmpas' with grant option;
GRANT ALL PRIVILEGES ON *.* TO 'webadmin'@'%' IDENTIFIED BY 'S5z#$@93J5fCmpas' with grant option;

GRANT ALL PRIVILEGES ON *.* TO 'nodebackend'@'%' IDENTIFIED BY 'ku*SZa?E8$B&%FJv' with grant option;

FLUSH PRIVILEGES;



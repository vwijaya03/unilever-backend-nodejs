use unilever;

SET foreign_key_checks = 0;

TRUNCATE TABLE retailers;
TRUNCATE TABLE distributors;
TRUNCATE TABLE users;
TRUNCATE TABLE transactions;

SET foreign_key_checks = 1;

INSERT INTO retailers (outlet_code, name) VALUES
('00179940', 'Retailer-00179940'),
('00180254', 'Retailer-00180254'),
('00179963', 'Retailer-00179963'),
('00180309', 'Retailer-00180309'),
('00179958', 'Retailer-00179958');

INSERT INTO distributors (dt_code, name) VALUES
('15195745', 'Distributor-15195745'),
('15195746', 'Distributor-15195746');

INSERT INTO users (username, type, password, pin, valdo_account, email, fullname, phone, salt, access_token, access_token_expiry, retailer_id, distributor_id) VALUES
('user1', 0, 'pass1', '1234', '08113001688', 'user1@gmail.com', 'user1 bill', '0811-700-1111', CONVERT(SUBSTRING(MD5(RAND()), -10), BINARY), 'user1token', TIMESTAMPADD(DAY,30,NOW()), 1, NULL),
('user2', 0, 'pass2', '1234', '08110000099', 'user2@gmail.com', 'user2 john', '0811-700-2222', CONVERT(SUBSTRING(MD5(RAND()), -10), BINARY), 'user2token', TIMESTAMPADD(DAY,30,NOW()), 2, NULL),
('user3', 1, 'pass3', '1234', '08113001689', 'user3@gmail.com', 'user3 maria', '0811-700-3333', CONVERT(SUBSTRING(MD5(RAND()), -10), BINARY), 'user3token', TIMESTAMPADD(DAY,-10,NOW()), 3, NULL),
('dist1', 1, 'pass3', '1234', '08110000013', 'dist1@gmail.com', 'dist1 unilever', '0811-800-0000', CONVERT(SUBSTRING(MD5(RAND()), -10), BINARY), 'dist1token', TIMESTAMPADD(DAY,-10,NOW()), NULL, 1),

('pickup1', 2, 'pass1', '1234', '', 'pickup13@gmail.com', 'pickup1 bob', '0811-700-3333', CONVERT(SUBSTRING(MD5(RAND()), -10), BINARY), 'pickup1token', TIMESTAMPADD(DAY,-10,NOW()), NULL, NULL),

('superadmin1', 10, 'pass1', 'pin', '', 'superadmin1@gmail.com', 'superadmin1', '0811-800-0000', CONVERT(SUBSTRING(MD5(RAND()), -10), BINARY), 'superadmin1token', TIMESTAMPADD(DAY,-10,NOW()), NULL, NULL),
('superadmin2', 10, 'pass2', 'pin', '', 'superadmin2@gmail.com', 'superadmin2', '0811-800-0000', CONVERT(SUBSTRING(MD5(RAND()), -10), BINARY), 'superadmin2token', TIMESTAMPADD(DAY,-10,NOW()), NULL, NULL),
('unileveradmin1', 9, 'pass1', 'pin', '', 'unileveradmin1@gmail.com', 'unileveradmin1', '0811-800-0000', CONVERT(SUBSTRING(MD5(RAND()), -10), BINARY), 'unileveradmin1token', TIMESTAMPADD(DAY,-10,NOW()), NULL, NULL),
('unileveradmin2', 9, 'pass2', 'pin', '', 'unileveradmin2@gmail.com', 'unileveradmin2', '0811-800-0000', CONVERT(SUBSTRING(MD5(RAND()), -10), BINARY), 'unileveradmin2token', TIMESTAMPADD(DAY,-10,NOW()), NULL, NULL);


INSERT INTO transactions (invoice_id, user_id, retailer_id, distributor_id, type, amount, transact_date) VALUES

# user 1
('150003000500',1,1,1,1,123001, TIMESTAMPADD(DAY,-60,NOW())),
('150003000120',1,1,1,0,123000, TIMESTAMPADD(DAY,-60,NOW())),
('150003000180',1,1,1,1,600001, TIMESTAMPADD(DAY,-60,NOW())),
('150003000180',1,1,1,0,600000, TIMESTAMPADD(DAY,-60,NOW())),
('150003000220',1,1,1,1,100001, TIMESTAMPADD(DAY,-60,NOW())),
('150003000220',1,1,1,0,100000, TIMESTAMPADD(DAY,-60,NOW())),
('150003000500',1,1,1,1,800001, TIMESTAMPADD(DAY,-60,NOW())),
('150003000500',1,1,1,0,800000, TIMESTAMPADD(DAY,-60,NOW())),
('150003000510',1,1,1,1,900001, TIMESTAMPADD(DAY,-60,NOW())),
('150003000510',1,1,1,0,900000, TIMESTAMPADD(DAY,-60,NOW())),
('150003000690',1,1,1,1,550001, TIMESTAMPADD(DAY,-60,NOW())),
('150003000690',1,1,1,0,550000, TIMESTAMPADD(DAY,-60,NOW())),

# user 2
('150001000600',2,2,1,1,600000, TIMESTAMPADD(DAY,-60,NOW())),
('150001000600',2,2,1,0,600000, TIMESTAMPADD(DAY,-60,NOW())),
('150001002660',2,2,1,1,120000, TIMESTAMPADD(DAY,-60,NOW())),
('150001002660',2,2,1,0,120000, TIMESTAMPADD(DAY,-60,NOW())),
('150001004490',2,2,1,1,660000, TIMESTAMPADD(DAY,-60,NOW())),
('150001004490',2,2,1,0,660000, TIMESTAMPADD(DAY,-60,NOW())),

# user 3
('150005000020',3,3,1,1,100000, TIMESTAMPADD(DAY,-60,NOW())),
('150005000020',3,3,1,0,100000, TIMESTAMPADD(DAY,-60,NOW())),
('150005000070',3,3,1,1,200000, TIMESTAMPADD(DAY,-60,NOW())),
('150005000070',3,3,1,0,200000, TIMESTAMPADD(DAY,-60,NOW())),
('150005000110',3,3,1,1,300000, TIMESTAMPADD(DAY,-60,NOW())),
('150005000110',3,3,1,0,300000, TIMESTAMPADD(DAY,-60,NOW()));



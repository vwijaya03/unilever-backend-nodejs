use unilever;


SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `transactions`;
DROP TABLE IF EXISTS `retailers`;
DROP TABLE IF EXISTS `distributors`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `users_banking`;

SET foreign_key_checks = 1;


CREATE TABLE IF NOT EXISTS `invoices` (
  `id` BIGINT(12) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dt_code` char(12) DEFAULT NULL,
  `le_code` char(12) DEFAULT NULL,
  `outlet_code` char(12) DEFAULT NULL,
  `cash_memo_total_amount` decimal(15,4) DEFAULT NULL,
  `cash_memo_balance_amount` decimal(15,4) DEFAULT NULL,
  `cashmemo_type` varchar(45) DEFAULT NULL,
  `invoice_id` varchar(45) DEFAULT NULL,
  `invoice_sales_date` timestamp NULL,
  `invoice_due_date` timestamp NULL,
  `invoice_payment_status` varchar(45) DEFAULT NULL,
  `product` varchar(45) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `quantity` varchar(45) DEFAULT NULL,
  `product_price_cs` decimal(15,4) DEFAULT NULL,
  `product_price_dz` decimal(15,4) DEFAULT NULL,
  `product_price_pc` decimal(15,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120453 DEFAULT CHARSET=latin1;

CREATE TABLE `pickup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pickup_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `informasi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `image` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- CREATE TABLE `invoice` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `session_id` varchar(45) DEFAULT NULL,
--   `file_name` varchar(100) DEFAULT NULL,
--   `dt_code` varchar(45) DEFAULT NULL,
--   `le_code` varchar(45) DEFAULT NULL,
--   `outlet_code` varchar(45) DEFAULT NULL,
--   `cash_memo_total_amount` decimal(15,4) DEFAULT NULL,
--   `cash_memo_balance_amount` decimal(15,4) DEFAULT NULL,
--   `cashmemo_type` varchar(45) DEFAULT NULL,
--   `invoice_id` varchar(45) DEFAULT NULL,
--   `invoice_sales_date` timestamp NULL DEFAULT NULL,
--   `invoice_due_date` timestamp NULL DEFAULT NULL,
--   `invoice_payment_status_paid` decimal(15,4) DEFAULT NULL,
--   `invoice_payment_status_unpaid` decimal(15,4) DEFAULT NULL,
--   `invoice_details` text,
--   `approval_date` timestamp NULL DEFAULT NULL,
--   `status` int(11) DEFAULT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=6985 DEFAULT CHARSET=latin1;

CREATE table IF NOT EXISTS `retailers` (
  `retailer_id` INT(12) UNSIGNED NOT NULL AUTO_INCREMENT,
  `outlet_code` char(12) UNIQUE NOT NULL,
  `name` varchar(255) NULL DEFAULT NULL,
  `address` varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`retailer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
ALTER TABLE `retailers` AUTO_INCREMENT = 1;
CREATE INDEX `retailer_outletcode_index` ON `retailers` (`outlet_code`);


CREATE table IF NOT EXISTS `distributors` (
  `distributor_id` INT(12) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dt_code` char(12) NOT NULL,
  `name` varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`distributor_id`, `dt_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
ALTER TABLE `distributors` AUTO_INCREMENT = 1;
CREATE INDEX `distributor_dtcode_index` ON `distributors` (`dt_code`);


-- CREATE TABLE IF NOT EXISTS `users` (
--   `user_id` INT(12) UNSIGNED NOT NULL AUTO_INCREMENT,
--   `username` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
--   `password` varbinary(128) NOT NULL,
--   `pin` varbinary(128) NOT NULL,
--   `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
--   `fullname` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
--   `onbehalf` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
--   `reference` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
--   `phone` varchar(64) NOT NULL,
--   `salt` varbinary(128) NULL,
--   `type` TINYINT NULL DEFAULT 0,
--   `roles` varchar(256) NULL DEFAULT NULL,
--   `retailer_id` INT(12) UNSIGNED NULL,
--   `distributor_id` INT(12) UNSIGNED NULL,
--   `access_token` varchar(128) NULL,
--   `access_token_expiry` timestamp NULL,
--   `valdo_account` varchar(32) NULL DEFAULT NULL,
--   `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--   PRIMARY KEY (`user_id`),
--   FOREIGN KEY(`retailer_id`) REFERENCES `retailers`(`retailer_id`)
--       ON DELETE NO ACTION ON UPDATE CASCADE,
--   FOREIGN KEY(`distributor_id`) REFERENCES `distributors`(`distributor_id`)
--       ON DELETE NO ACTION ON UPDATE CASCADE
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;
-- ALTER TABLE `users` AUTO_INCREMENT = 1;
-- ALTER TABLE `users` ADD CONSTRAINT unique_username UNIQUE (username);
-- ALTER TABLE `users` ADD CONSTRAINT unique_email UNIQUE (email);
-- ALTER TABLE `users` ADD CONSTRAINT unique_access_token UNIQUE (access_token);
-- CREATE INDEX `user_email_index` ON `users` (`email`);

CREATE TABLE `users` (
  `user_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 DEFAULT '',
  `password` varbinary(128) NOT NULL,
  `pin` varbinary(128) NOT NULL DEFAULT '',
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `fullname` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `nama_toko` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `onbehalf` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `reference` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_bin NOT NULL,
  `salt` varbinary(128) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '0',
  `roles` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `retailer_id` int(12) unsigned DEFAULT NULL,
  `distributor_id` int(12) unsigned DEFAULT NULL,
  `pickup_id` int(12) unsigned DEFAULT NULL,
  `sales_id` int(12) unsigned DEFAULT NULL,
  `fcm_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `access_token` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `access_token_expiry` timestamp NULL DEFAULT NULL,
  `valdo_account` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `unique_email` (`email`),
  UNIQUE KEY `unique_access_token` (`access_token`),
  KEY `retailer_id` (`retailer_id`),
  KEY `distributor_id` (`distributor_id`),
  KEY `user_email_index` (`email`),
  KEY `sales_id` (`sales_id`),
  CONSTRAINT `sales_id` FOREIGN KEY (`sales_id`) REFERENCES `sales` (`sales_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`retailer_id`) REFERENCES `retailers` (`retailer_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`distributor_id`) REFERENCES `distributors` (`distributor_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dt_code` varchar(50) DEFAULT NULL,
  `invoice_id` varchar(45) DEFAULT '',
  `product` varchar(45) DEFAULT '',
  `product_name` varchar(255) DEFAULT '',
  `quantity` decimal(18,10) DEFAULT '0.0000000000',
  `product_price_cs` decimal(20,8) DEFAULT '0.00000000',
  `product_price_dz` decimal(20,8) DEFAULT '0.00000000',
  `product_price_pc` decimal(20,8) DEFAULT '0.00000000',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58792 DEFAULT CHARSET=latin1;

CREATE TABLE `sales` (
  `sales_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `distributor_id` int(12) unsigned DEFAULT NULL,
  `sales_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sales_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
use unilever;

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `transactions`;

SET foreign_key_checks = 1;

-- CREATE table IF NOT EXISTS `transactions` (
--   `transaction_id` BIGINT(12) UNSIGNED NOT NULL AUTO_INCREMENT,
--   `from_user_id` INT(12) UNSIGNED NOT NULL,
--   `to_user_id` INT(12) UNSIGNED NULL DEFAULT NULL,
--   `invoice_id` varchar(32) NULL DEFAULT NULL,
--   `retailer_id` INT(12) UNSIGNED NULL DEFAULT NULL,
--   `distributor_id` INT(12) UNSIGNED NULL DEFAULT NULL,
--   `type` TINYINT(12) UNSIGNED NOT NULL DEFAULT 0,
--   `request` varchar(128) NULL DEFAULT NULL,
--   `amount` decimal(12, 4) NULL DEFAULT 0,
--   `currency_code` varchar(4) NULL DEFAULT 'IDR',
--   `transact_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
--   `valdo_tx_id` varchar(32) NULL DEFAULT NULL,
--   `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--   PRIMARY KEY (`transaction_id`),
--   FOREIGN KEY(`from_user_id`) REFERENCES `users`(`user_id`)
--     ON DELETE NO ACTION ON UPDATE CASCADE,
--   FOREIGN KEY(`to_user_id`) REFERENCES `users`(`user_id`)
--     ON DELETE NO ACTION ON UPDATE CASCADE,
--   FOREIGN KEY(`retailer_id`) REFERENCES `retailers`(`retailer_id`)
--     ON DELETE NO ACTION ON UPDATE CASCADE,
--   FOREIGN KEY(`distributor_id`) REFERENCES `distributors`(`distributor_id`)
--     ON DELETE NO ACTION ON UPDATE CASCADE
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
-- CREATE INDEX `transaction_invoice_index` ON `transactions` (`invoice_id`);
-- CREATE INDEX `transaction_from_user_index` ON `transactions` (`from_user_id`);
-- CREATE INDEX `transaction_to_user_index` ON `transactions` (`to_user_id`);
-- CREATE INDEX `transaction_retailer_index` ON `transactions` (`retailer_id`);
-- CREATE INDEX `transaction_distributor_index` ON `transactions` (`distributor_id`);

CREATE TABLE `transactions` (
  `transaction_id` bigint(12) unsigned NOT NULL AUTO_INCREMENT,
  `from_user_id` int(12) unsigned NOT NULL,
  `to_user_id` int(12) unsigned DEFAULT NULL,
  `dt_code` varchar(50) DEFAULT NULL,
  `invoice_id` varchar(32) DEFAULT NULL,
  `invoice_code` varchar(50) DEFAULT NULL,
  `retailer_id` int(12) unsigned DEFAULT NULL,
  `distributor_id` int(12) unsigned DEFAULT NULL,
  `type` tinyint(12) unsigned NOT NULL DEFAULT '0',
  `amount` decimal(12,4) DEFAULT '0.0000',
  `currency_code` varchar(4) DEFAULT 'IDR',
  `transact_date` timestamp NULL DEFAULT NULL,
  `valdo_tx_id` varchar(32) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`transaction_id`),
  KEY `transaction_invoice_index` (`invoice_id`),
  KEY `transaction_from_user_index` (`from_user_id`),
  KEY `transaction_to_user_index` (`to_user_id`),
  KEY `transaction_retailer_index` (`retailer_id`),
  KEY `transaction_distributor_index` (`distributor_id`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`from_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`to_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `transactions_ibfk_3` FOREIGN KEY (`retailer_id`) REFERENCES `retailers` (`retailer_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `transactions_ibfk_4` FOREIGN KEY (`distributor_id`) REFERENCES `distributors` (`distributor_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
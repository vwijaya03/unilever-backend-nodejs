/*

https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql

*/

CREATE USER 'nodebackend'@'%' IDENTIFIED BY 'P,}Bh#Y5jddU%!@?';
CREATE USER 'webadmin'@'%' IDENTIFIED BY '>2,,x%+Bv3FC>#.Y';

GRANT ALL PRIVILEGES ON unilever.* TO 'nodebackend'@'%' IDENTIFIED BY 'P,}Bh#Y5jddU%!@?' with grant option;
GRANT ALL PRIVILEGES ON *.* TO 'nodebackend'@'%' IDENTIFIED BY 'P,}Bh#Y5jddU%!@?' with grant option;

GRANT ALL PRIVILEGES ON unilever . * TO 'webadmin'@'%' IDENTIFIED BY '>2,,x%+Bv3FC>#.Y' with grant option;
GRANT ALL PRIVILEGES ON * . * TO 'webadmin'@'%' IDENTIFIED BY '>2,,x%+Bv3FC>#.Y' with grant option;

GRANT ALL PRIVILEGES ON *.* TO 'nodebackend'@'%' IDENTIFIED BY 'P,}Bh#Y5jddU%!@?' with grant option;

FLUSH PRIVILEGES;
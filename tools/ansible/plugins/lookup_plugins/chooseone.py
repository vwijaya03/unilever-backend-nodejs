#!/usr/bin/env python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import os

from ansible import utils
from ansible import constants
from ansible.errors import AnsibleError
from ansible.utils.color import stringc
from ansible.utils.unicode import to_bytes, to_unicode
from ansible.errors import AnsibleError
from ansible.utils.color import colorize, hostcolor
from ansible.compat.six import string_types
from ansible.compat.six.moves import configparser

from ansible.plugins.lookup import LookupBase
from asantoso_linode.odelin import Odelin

class LookupModule(LookupBase):

    configParser = configparser.ConfigParser()
    colorCode = "purple";
    errorColorCode = "red";
    color = constants.get_config(configParser, 'colors', 'ok', 'ANSIBLE_COLOR_CHANGED', colorCode);
    errorColor = constants.get_config(configParser, 'colors', 'ok', 'ANSIBLE_COLOR_CHANGED', errorColorCode);
    scriptFileName = os.path.realpath(__file__);

    def log(self, message, isError=False):
        activeColor = self.color
        if isError:
            activeColor = self.errorColor
        self._display.display(message, color=activeColor);

    # called by ansible
    # reference:
    #   https://github.com/ansible/ansible/blob/62979efa140ce9659beac6442b51bd8efe35d4ba/lib/ansible/plugins/lookup/__init__.py
    # terms = a list of parameters provided by the caller.
    def run(self, terms, variables, **kwargs):
        # IMPORTANT NOTE: we have to to convert the parameter as python list
        choices_list = list(terms[0]);
        # excludes = list(terms[1]);

        self.log(u"[ invoking lookup_plugin: %s ]" % (self.scriptFileName));
        # ansible expects return value as a list.
        numArgs = len(choices_list)
        print ("parameters :", choices_list)

        if numArgs > 0:
            # ansible expects that the output of a plugin must be a list.
            out = []
            out.append(choices_list[0])
            self.log(u"[ resp: %s ]" % (out));
            return out
        return None

# FOR TESTING

def run():
    module = LookupModule();
    module.run([["139.162.54.112", "139.162.49.93"], "139.162.49.93" ], None);
    module.run("139.162.54.112", None);

if __name__ == "__main__":
    run()

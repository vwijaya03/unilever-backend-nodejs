#### What is this?
The files in this directory define lookup methods for ansible
Example usage: abc.py will translate to ansible: {{ lookup('abc', args) }}

#### Configure ansible to read this plugin
Export this env pointing to the lookup_plugins dir

    export ANSIBLE_LOOKUP_PLUGINS=path/to/lookup_plugins

#### Dependencies
depends on "asantoso_linode" python library




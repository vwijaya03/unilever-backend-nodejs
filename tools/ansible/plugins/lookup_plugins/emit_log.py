#!/usr/bin/env python

from ansible import utils
from ansible import constants
from ansible.plugins.lookup import LookupBase
from ansible.utils.color import stringc
from ansible.utils.unicode import to_bytes, to_unicode
from ansible.errors import AnsibleError
from ansible.utils.color import colorize, hostcolor
from ansible.compat.six import string_types
from ansible.compat.six.moves import configparser

class EmitLog(object):

    colorCode = "purple";
    errorColorCode = "red";

    def __init__(self, *args, **kwargs):
        self.scriptFileName = os.path.realpath(__file__);
        p = configparser.ConfigParser()
        self.color = constants.get_config(p, 'colors', 'ok', 'ANSIBLE_COLOR_CHANGED', self.colorCode);
        self.errorColor = constants.get_config(p, 'colors', 'ok', 'ANSIBLE_COLOR_CHANGED', self.errorColorCode);
        self.plugin_name = kwargs.get("name", "")

    def log(self, message, isError=False):
        activeColor = self.color
        if isError:
            activeColor = self.errorColor
        self._display.display(message, color=activeColor);

    def __call__(self, func):
        def inner(*args, **kwargs):
            print "emit log"
            message = u"[ invoking plugin: %s ]" % \
                      (
                          self.scriptFileName
                      );
            self.log(message);

        return inner
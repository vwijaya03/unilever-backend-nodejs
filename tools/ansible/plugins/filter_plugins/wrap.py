import inspect

def wrap(value, *args, **kwargs):
    res = inspect.getargspec(wrap)
    print ("inspect: ", res)
    print ("args: ", args)
    saved_args = locals()
    print("saved_args is", saved_args)

    is_list = type(value) is list
    print ("wrapping : ", value)
    print ("is list? : ", is_list)
    print ("list len : ", len(value))
    return [ '"' + x + '"' for x in value]

class FilterModule(object):
    def filters(self):
        return {
            'wrap': wrap
        }
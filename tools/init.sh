#!/bin/bash

CDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
NARGS=$#
ME=`basename $0`

##########################################################################################

echo "itprovent unilever backend init.sh"

export ITP_UNILEVER_BACKEND="$DIR/.."

alias build_pylib="\
(cd $ITP_UNILEVER_BACKEND/tools/lib/superluminal && sudo python setup.py clean && sudo python setup.py install)
"

export PATH=$PATH:$DIR:$DIR/scripts:$DIR/scripts/encryption

export ANSIBLE_NOCOWS=1
export ANSIBLE_CONFIG="$ITP_UNILEVER_BACKEND/tools/ansible/ansible.cfg"
export SUPERLUMINAL_INVENTORY_DIR="$ITP_UNILEVER_BACKEND/deploy/inventory"

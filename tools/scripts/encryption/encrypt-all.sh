#!/bin/bash

CDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
NARGS=$#
ME=`basename $0`

source $DIR/secured.sh

print-ansible-vault-info.sh

read -rsp $'Press any key to continue. [CTRL-C to abort] \n' -n1 key

function clean_up {
    echo ""
    echo "!!! OPERATION ABORTED !!!"
    echo ""
    exit
}

trap clean_up SIGHUP SIGINT SIGTERM

for item in ${secured_list[*]}; do
    echo "encrypting $item"
    ansible-vault encrypt $item
done

#!/usr/bin/env bash

CDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
NARGS=$#
ME=`basename $0`

TARGET_DIR=$ITP_UNILEVER_BACKEND/config
yaml_files=$(find $TARGET_DIR -type f -iname "*.yml")
# elasticsearch_non_yml_files=$(find $TARGET_DIR/elasticsearch -type f -iname "*" ! -name "*.yml")

secured_list=("$yaml_files")

echo "# These files will be secured:"
for item in ${secured_list[*]}; do
    echo $item
done
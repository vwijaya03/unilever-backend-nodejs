#!/usr/bin/env bash

CDIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
NARGS=$#
ME=`basename $0`

(cd $DIR/../../main/src && swagger project edit)

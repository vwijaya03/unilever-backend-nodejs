from setuptools import setup

setup(name='superluminal',
      version='0.0.1',
      description='superluminal',
      url='http://github.com/asantoso/superluminal',
      author='Agus Santoso',
      author_email='agus.santoso@gmail.com',
      license='MIT',
      packages=['superluminal'],
      zip_safe=False,
      install_requires=[
      ]
)
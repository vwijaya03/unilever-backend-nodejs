#!/usr/bin/env python

import os
import sys
import subprocess
import threading
from threading import Thread
from colorama import Fore, Back, Style
from colorama import init

import multiprocessing
import Queue

def log(*args, **kwargs):
    color = kwargs.get("color", Fore.MAGENTA)
    style = kwargs.get("style", Style.NORMAL)

    for item in args:
        print (style + color + str(item) +  Style.RESET_ALL)

# executes command and returns exit code
def cmd(cmd, extras=None):
    cmd = cmd.split()

    fullcmd = "{0} {1}".format(" ".join(cmd), " ".join(extras))

    log ("{0} ::: [ executing command ]\n{1}".format(os.path.basename(__file__), fullcmd), color=Fore.BLUE)

    if extras is not None:
        cmd.extend(extras)

    code = os.spawnvpe(os.P_WAIT, cmd[0], cmd, os.environ)
    if code == 127:
        sys.stderr.write('{0}: command not found\n'.format(cmd[0]))
    return code

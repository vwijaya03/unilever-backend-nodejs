#!/usr/bin/env python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import os
import sys
import subprocess
import argparse
import operator
import re

from colorama import Fore, Back, Style

import multiprocessing
import Queue
import ConfigParser
from collections import Counter

from superluminal import common
from superluminal import subroutine as okt_infra_ansible

# this scripts supplies the following variables to ansible playbook:
#
# env: the environment
# grp: the group id

class Superluminal:

    def configure_parser(self, parser):
        parser.add_argument('--section', dest='section')
        parser.add_argument('--grp', dest='grp')
        parser.add_argument('--playbook-index', dest='playbook_index', help='index of playbook to execute')
        parser.add_argument('--ini', dest='ini', default='playbook.ini', help='playbook run config ini file')
        parser.add_argument('--default-user', dest='default_user')
        parser.add_argument('--module', dest='module', help='the module in ini file to execute. default is $env')
        parser.add_argument('--uninstall', dest='uninstall', help='sets the variable uninstall=true', action='store_const', const=True, default=False)
        parser.add_argument('--remote-access', dest='remote_access', action='store_const', const=True, default=False)
        parser.add_argument('--rm-remote-access', dest='remove_remote_access', action='store_const', const=True, default=False)
        parser.add_argument('--dryrun', dest='dryrun', action='store_const', const=True, default=False)

    def invoke_subroutine(self, args):
        log("{0} ::: [ pending command ]\nargs : ".format(os.path.basename(__file__)), args)

        if args.uninstall is True:
            args.vars = args.vars + " uninstall=true"
            okt_infra_ansible.run(args)
            log("returning..")
            return
        if args.remote_access is True:
            args.tags = ['ferm_remote_access']
            okt_infra_ansible.run(args)
            log("returning..")
            return
        if args.remove_remote_access is True:
            args.vars = args.vars + " remove_ferm_remote_access=true"
            okt_infra_ansible.run(args)
            log("returning..")
            return

        print ("[ invoking okt_infra_ansible ]")
        exit_code = okt_infra_ansible.run(args)
        return exit_code

    def getSectionsList(self, args):
        log ('getSectionsList. args:', args)
        log ('os.cwd:' + os.getcwd())
        config = ConfigParser.SafeConfigParser()
        playbook_ini_file = os.getcwd() + '/' + args.ini
        log('reading ' + playbook_ini_file)
        config.read(playbook_ini_file)
        labels = []
        for section in config.sections():
            labels.append(section)
            # log ('section :' + section)
            # for (key, val) in config.items(section):

        return labels

    def readSection(self, section_name, args, defaults):

        config = ConfigParser.SafeConfigParser(defaults)
        playbook_ini_filename = 'playbook.ini'

        if args.ini is not None:
            playbook_ini_filename = args.ini

        playbook_ini_file = os.getcwd() + '/' + playbook_ini_filename
        config.read(playbook_ini_file)

        section = Section()

        section.hosts = config.get(section_name, 'hosts')
        section.playbooks = config.get(section_name, 'playbook')
        section.vars = config.get(section_name, 'vars')
        section.port = config.get(section_name, 'port')
        section.user = config.get(section_name, 'ansible_user')
        section.dynamic_inventory = config.get(section_name, 'dynamic_inventory')
        section.become = config.get(section_name, 'become')
        section.ask_become_pass = config.get(section_name, 'ask_become_pass')
        section.ask_pass = config.get(section_name, 'ask_pass')
        section.ask_vault_pass = config.get(section_name, 'ask_vault_pass')
        section.require_tags = config.get(section_name, 'require_tags')
        section.group = config.get(section_name, 'group')
        section.local = config.get(section_name, 'local')

        if config.has_option(section_name, 'tags'):
            section.tags = config.get(section_name, 'tags')

        return section

    def getVarDefaults(self,
            Port=None,
            Hosts=None,
            Playbook=None,
            Vars=None,
            DynamicInventory=None,
            AnsibleUser=None,
            Become=False,
            AskVaultPass=False,
            AskBecomePass=False,
            AskPass=False,
            RequireTags=False,
            Tags=None,
            Group=None,
            Local=False
        ):
        # setup default values from environment variables and our custom values.

        app_defaults = {}

        app_defaults['hosts'] = Hosts
        app_defaults['playbook'] = Playbook
        app_defaults['vars'] = Vars
        app_defaults['port'] = Port
        app_defaults['ansible_user'] = AnsibleUser
        app_defaults['dynamic_inventory'] = DynamicInventory
        app_defaults['hosts'] = Hosts
        app_defaults['vars'] = ''
        app_defaults['local'] = Local
        app_defaults['become'] = Become
        app_defaults['ask_vault_pass'] = AskVaultPass
        app_defaults['ask_become_pass'] = AskBecomePass
        app_defaults['ask_pass'] = AskPass
        app_defaults['require_tags'] = RequireTags
        app_defaults['tags'] = Tags
        app_defaults['group'] = Group

        combined_defaults = combine_dicts(
            os.environ, app_defaults
        )

        return combined_defaults


    def run_from_cli(self):
        parser = argparse.ArgumentParser(description='execute-ansible-playbook')
        self.configure_parser(parser);
        okt_infra_ansible.configure_parser(parser);
        args = parser.parse_args();
        exit_code = self.run(args);
        return exit_code;

    def run(self, args=None):
        # parser = argparse.ArgumentParser(description='execute-ansible-playbook')
        # self.configure_parser(parser);
        # okt_infra_ansible.configure_parser(parser);
        # args = parser.parse_args()

        log ('running okt_infra_execute_playbook.py args:', args, color=Fore.YELLOW);

        section = args.section
        module = args.module
        grp = args.grp # value from cli takes highest precedence
        is_module_specified = module is not None;
        is_module_exists = False
        matching_sections_list = []
        cli_vars = ''
        cli_user = None
        env = None

        # read env which will determine the section of ini file we will read

        is_section_specified = section is not None
        is_grp_specified = grp is not None

        # grp value from cli takes highest precedence
        # grp value in section (.ini file) take middle precedence
        # grp value from section name takes lowest precedence

        if not is_section_specified:
            section = "default"
            grp = "default"
        else:
            section_parts = section.split(':')
            print ('section-parts:', section_parts)
            env = section_parts[0]
            if is_grp_specified:
                None
            else:
                if len(section_parts) >= 2:
                    grp = section_parts[1]
                else:
                    grp = "default"

        sectionsList = self.getSectionsList(args)

        is_section_exists = section in sectionsList;

        log ('sections-list:', sectionsList)
        log ('is_section_exists: ', is_section_exists)
        log ('is_module_specifed:', is_module_specified)

        if is_module_specified:
            pattern = re.compile(section + "")
            for x in sectionsList:
                res = pattern.search(x)
                if res is not None:
                    matching_sections_list.append(x)
                    print ('matched module: ', x)
            print ('matching sections for: {0} => {1}'.format(section, matching_sections_list))
            if (section + ":" + module) in matching_sections_list:
                is_module_exists = True
            if not is_module_exists:
                log ("section {0}:{1} not found for module {1} in .ini file.".format(section, module), color=Fore.RED)
                sys.exit(1)

        if not is_section_exists:
            log ("unrecognized environment: " + section.lower(), color=Fore.RED)
            log ("please provide valid section value. available values: {0}".format(sectionsList), color=Fore.RED)
            sys.exit(1)

        playbook_index = None
        if args.playbook_index is not None:
            playbook_index = int(args.playbook_index)

        default_port = '22'
        default_playbook = 'playbook-{0}.yml'.format(env)
        default_user = env

        print ('default_user', default_user);
        print ('default_port', default_port);
        print ('default_playbook', default_playbook);

        # override values in file with the CLI args
        if hasattr(args, 'vars'):
            cli_vars = args.vars
        if hasattr(args, 'user'):
            default_user = cli_user

        dynamic_inventory = os.environ.get('ANSIBLE_HOSTS')
        print ('dynamic_inventory', dynamic_inventory);

        module = args.module

        # Note: section overrides base-default values, module overrides section values

        parent_defaults = self.getVarDefaults(
            Hosts=None,
            Port=default_port,
            Playbook=default_playbook,
            AnsibleUser=default_user,
            DynamicInventory=dynamic_inventory
        )

        # print ('parent_defaults', parent_defaults);

        section_descriptor = {}

        if not is_section_exists and not is_module_specified:
            log ('[ERROR] section is not specified and module is not specified', color=Fore.RED);
            sys.exit(1)

        if is_section_exists:
            section_descriptor = self.readSection(section, args, parent_defaults)
        else:
            section_descriptor = parent_defaults

        if is_module_specified:
            print ('module is specified. module-name:', module)
            module_section_name = "{0}:{1}".format(section, module)
            print ('module target section name:', module_section_name);
            default_playbook = 'playbook-{0}-{1}.yml'.format(section, module)
            print ('module target playbook:', default_playbook);

            target_default_values = None

            if is_section_exists:
                target_default_values = self.getVarDefaults(
                    Hosts=section_descriptor.hosts,
                    Port=section_descriptor.port,
                    Playbook=default_playbook,
                    AnsibleUser= section_descriptor.user,
                    Vars=section_descriptor.vars,
                    DynamicInventory=section_descriptor.dynamic_inventory)
            else:
                target_default_values = parent_defaults

            try:
                section_descriptor = self.readSection(module_section_name, args, target_default_values)
            except ConfigParser.NoSectionError as err:
                log ('[ERROR] cannot read section. error:', err, color=Fore.RED)
                sys.exit(1)

        print ('resolved section descriptor: ', section_descriptor);

        # check if require_tags is True and tags are provided
        require_tags = str2bool(section_descriptor.require_tags)
        log ( 'has tags? ', hasattr(args, 'tags'));
        if require_tags and (not hasattr(args, 'tags') and section_descriptor.tags is None):
            log ("[ERROR] require_tags option set to True but tags are not provided", color=Fore.RED)
            sys.exit(1)

        playbooks_list = section_descriptor.playbooks.split(',')
        print ("playbook list", playbooks_list)

        if playbook_index is not None:
            playbook = playbooks_list

        log("Scheduled Playbooks ({0}): ".format(len(playbooks_list)), color=Fore.GREEN)
        for (i, value) in enumerate(playbooks_list):
            log("#{0} : {1} {2}".format(i+1, value, (('[selected]' if playbook_index is not None and playbook_index == i else '[selected]')) ), color=Fore.GREEN)

        if args.dryrun:
            sys.exit(0)

        for (i, value) in enumerate(playbooks_list):

            log("Running playbook #{0} : {1}".format(i+1, value), color=Fore.GREEN)

            if playbook_index is not None:
                if i != playbook_index:
                    continue

            playbook = playbooks_list[i]

            if not hasattr(args, 'ssh_port'):
                args.ssh_port = section_descriptor.port
            if not hasattr(args, 'hosts'):
                args.hosts = section_descriptor.hosts
            if not hasattr(args, 'become'):
                args.become = str2bool(section_descriptor.become)
            if not hasattr(args, 'ask_become_pass'):
                args.ask_become_pass = str2bool(section_descriptor.ask_become_pass)
            if not hasattr(args, 'ask_pass'):
                args.ask_pass = str2bool(section_descriptor.ask_pass)
            if not hasattr(args, 'ask_vault_pass'):
                args.ask_vault_pass = str2bool(section_descriptor.ask_vault_pass)
            if not hasattr(args, 'local'):
                args.local = section_descriptor.local
            if not hasattr(args, 'playbook'):
                args.playbook = playbook
            if cli_vars is not None:
                args.vars = section_descriptor.vars + " " + cli_vars
            else:
                args.vars = section_descriptor.vars

            args.vars = args.vars + " grp="+grp


            args.dynamic_inventory = section_descriptor.dynamic_inventory

            if not hasattr(args, 'user'):
                args.user = section_descriptor.user

            if not hasattr(args, 'tags'):
                args.tags = section_descriptor.tags

            exit_code = self.invoke_subroutine(args)
            if exit_code != 0:
                sys.exit(exit_code)


class Section(object):

    def __init__(self, *args, **kwargs):

        self.hosts = None
        self.playbooks = None
        self.vars = None
        self.port = None
        self.user = None
        self.become = False
        self.ask_vault_pass = False
        self.ask_become_pass = False
        self.ask_pass = False
        self.local = False
        self.dynamic_inventory = None
        self.tags = None

def log(*args, **kwargs):
    color = kwargs.get("color", Fore.MAGENTA)
    style = kwargs.get("style", Style.NORMAL)
    for item in args:
        print (style + color + str(item) +  Style.RESET_ALL)

def combine_dicts(a, b, op=operator.add):
    return dict(a.items() + b.items() +
                [(k, op(a[k], b[k])) for k in set(b) & set(a)])

def str2bool(obj):
    if isinstance(obj, (int)):
        return obj
    else:
        return obj.lower() in ("yes", "true", "t", "1")

def run():
    main = Superluminal();
    main.run_from_cli();

if __name__ == "__main__":
    run()

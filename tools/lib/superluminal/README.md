Superluminal Python Module

### What is it?
superluminal allows more flexible execution of ansible playbook. 

### Licensing
Please see license file.

### Authors
agus.santoso@gmail.com

### Install as system-wide module
Run this command after you make changes to the code.

    sudo python setup.py install

#### Reference Docs
https://python-packaging.readthedocs.org/en/latest/minimal.html

#!/usr/bin/env python

import os
import sys
import subprocess
import argparse

import threading
from threading import Thread

from colorama import Fore, Back, Style
from colorama import init

import multiprocessing
import Queue

import sys, traceback

from superluminal import common

################################################

def configure_parser(parser):

    parser.add_argument('--user', dest='user', help='the username used for ssh', default=argparse.SUPPRESS)
    parser.add_argument('--playbook', dest='playbook', default=argparse.SUPPRESS)
    parser.add_argument('--inventory', dest='inventory', default=argparse.SUPPRESS, help='inventory_filename relative to the SUPERLUMINAL_INVENTORY_DIR')
    parser.add_argument('--dynamic_inventory', default=argparse.SUPPRESS, dest='dynamic_inventory', help='dynamic inventory script')
    parser.add_argument('--vars', dest='vars', default=argparse.SUPPRESS, help='extra_vars which is a list of key=value pairs separated by space')
    parser.add_argument('--hosts', dest='hosts', default=argparse.SUPPRESS, help="all or the name of the group in the inventory. can also be a regex pattern.")
    parser.add_argument('--tags', dest='tags', default=argparse.SUPPRESS, help='a list of ansible tags separated by comma')
    parser.add_argument('--nopause', dest='nopause', action='store_const', const=True, default=argparse.SUPPRESS)
    parser.add_argument('--local', dest='local', action='store_const', const=True, default=argparse.SUPPRESS)
    parser.add_argument('--ping-only', dest='ping_only', action='store_const', const=True, default=argparse.SUPPRESS)
    parser.add_argument('--list-hosts-only', dest='list_hosts_only', action='store_const', const=True, default=argparse.SUPPRESS)
    parser.add_argument('--become', dest='become', action='store_const', const=True, default=argparse.SUPPRESS)
    parser.add_argument('--ask-become-pass', dest='ask_become_pass', action='store_const', const=True, default=argparse.SUPPRESS)
    parser.add_argument('--ask-pass', dest='ask_pass', action='store_const', const=True, default=argparse.SUPPRESS)
    parser.add_argument('--ask-vault-pass', dest='ask_vault_pass', action='store_const', const=True, default=argparse.SUPPRESS)
    parser.add_argument('--verbose', dest='verbose', action='store_const', const=True, default=argparse.SUPPRESS)
    parser.add_argument('--ssh-port', dest='ssh_port', default=argparse.SUPPRESS)

def handleError(Message):
    print "ERROR: "+Message
    exit(1)

def asList(value):
    if value is None:
        return [];
    if type(value) is list:
        return value;
    return [value];

def log(*args, **kwargs):
    color = kwargs.get("color", Fore.MAGENTA)
    style = kwargs.get("style", Style.NORMAL)

    for item in args:
        print (style + color + str(item) +  Style.RESET_ALL)



def str2bool(obj):
    if isinstance(obj, (int)):
        return obj
    else:
        return v.lower() in ("yes", "true", "t", "1")


def run(args):

    inventory_dir = os.environ.get('SUPERLUMINAL_INVENTORY_DIR');

    #
    # if oketrade_build is None:
    #     log ('OKETRADE_BUILD env var is not defined', color=Fore.RED)
    #     os.exit(1)

    if not args.local and args.hosts is None:
        handleError("hosts are required for non-local")

    log ('superluminal-subroutine. args:', args, color=Fore.YELLOW)

    ansible_ssh_port = args.ssh_port;
    playbook_file = args.playbook
    user = args.user

    # dynamic inventory has higher priority
    use_dynamic_inventory = args.dynamic_inventory is not None
    if use_dynamic_inventory:
        inventory = args.dynamic_inventory
    else:
        inventory = "{0}/{1}".format(inventory_dir, str(args.inventory))

    become = ''
    ask_become_pass=''
    ask_pass=''
    ask_vault_pass=''
    variables = ''
    tags = ''
    verbose = ''
    ping_only = getattr(args, 'ping_only', False)
    list_hosts_only = getattr(args, 'list_hosts_only', False)
    hosts = 'all'
    vars = None

    is_local = getattr(args, 'local', False)
    no_pause = getattr(args, 'nopause', False)

    if getattr(args, 'verbose', False):
        verbose = '-vvvv'
    if getattr(args, 'become', False):
        become = '--become'
    if getattr(args, 'ask_become_pass', False):
        ask_become_pass = '--ask-become-pass'
    if getattr(args, 'ask_pass', False):
        ask_pass = '--ask-pass'
    if getattr(args, 'ask_vault_pass', False):
        ask_vault_pass = '--ask-vault-pass'
    if getattr(args, 'vars', None) is not None:
        vars = args.vars
        variables = vars
    if getattr(args, 'tags', None) is not None:
        tagsValue = ",".join(asList(args.tags))
        tags = " --tags {0}".format(tagsValue)
    if getattr(args, 'hosts', None) is not None:
        hosts = args.hosts


    print 'args.become :', args.become
    print 'args.ask_pass :', args.ask_pass
    print 'args.ask_become_pass :', args.ask_become_pass
    print 'args.ask_vault_pass :', args.ask_vault_pass

    print 'become :', become
    print 'ask_pass :', ask_pass
    print 'ask_become_pass :', ask_become_pass
    print 'ask_vault_pass :', ask_vault_pass

    print 'variables :', variables

    # variables = 'env=prod';

    ###

    ping_cmd_template = "ansible -i {inventory} {hosts} -m ping {verbose} -u {user} "
    local_cmd_template = "ansible-playbook -i \",localhost\" -c local {playbook_file} {verbose} -u {user} {become} {ask_pass} {ask_become_pass} {ask_vault_pass} {tags}"
    remote_cmd_template = "ansible-playbook -i {inventory} {playbook_file} {verbose} -u {user} {become} {ask_pass} {ask_become_pass} {ask_vault_pass} {tags}"

    variables = "ansible_ssh_port={0} {1}".format(ansible_ssh_port, variables)

    if use_dynamic_inventory:
        # refresh ansible inventory cache
        # print "[ refreshing dynamic inventory cache ]"
        # refresh_cmd = "{inventory} --refresh-cache".format(inventory=inventory);
        # infra_common.cmd(refresh_cmd, None);
        print ""

    if ping_only:
        print ("ping only..")
        ping_cmd = "";
        ping_cmd = ping_cmd_template.format(
            inventory=inventory,
            hosts=hosts,
            verbose=verbose,
            user=user,
            become=become,
            ask_become_pass=ask_become_pass,
            ask_pass=ask_pass,
            variables=variables
        );
        extras = ["--extra-vars", variables]
        print ("[ ping command ] : ", ping_cmd, "[ extras ] : ", extras)
        common.cmd(ping_cmd, extras);
        sys.exit(0)

    #######################################

    template = local_cmd_template

    if not is_local :
        template = remote_cmd_template
        variables = " {0} {1}".format(variables, "target_hosts="+hosts);
    else:
        variables = variables + " target_hosts=all"
        if vars is not None:
            variables = " {0} {1}".format(variables, vars);

    print ('using playbook: ', playbook_file)

    bashCommand = template.format(
            inventory=inventory,
            playbook_file=playbook_file,
            verbose=verbose,
            user=user,
            become=become,
            ask_become_pass=ask_become_pass,
            ask_pass=ask_pass,
            ask_vault_pass=ask_vault_pass,
            variables=variables,
            tags=tags
        )

    #######################################

    execute_cmd = bashCommand;
    listhosts_cmd = bashCommand + " --list-hosts";

    # note: we pass variables separately from the command otherwise the split command would split the vars string and
    # variables wont get passed.

    extras = ["--extra-vars", variables]

    #######################################

    common.cmd(listhosts_cmd, extras);

    if list_hosts_only:
        sys.exit(0)

    #######################################

    if not no_pause:
        raw_input("Press Enter to continue...");

    if not is_local:
        # cmd(listhosts_cmd, extras);
        print '[ remote connection ]'
    else:
        print '[ local connection ]'

    log("[ pending command ] : ", execute_cmd)
    log("extras:", extras)

    exit_code = common.cmd(execute_cmd, extras);
    return exit_code
#

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='superluminal-subroutine ansible vps execute');
    configure_parser(parser);
    args = parser.parse_args()
    print args;
    exit_code = run(args);
    sys.exit(exit_code)


